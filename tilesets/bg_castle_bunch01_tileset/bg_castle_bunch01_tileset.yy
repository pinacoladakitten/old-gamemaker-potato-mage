{
  "spriteId": {
    "name": "bg_castle_bunch01",
    "path": "sprites/bg_castle_bunch01/bg_castle_bunch01.yy",
  },
  "tileWidth": 128,
  "tileHeight": 256,
  "tilexoff": 0,
  "tileyoff": 0,
  "tilehsep": 0,
  "tilevsep": 0,
  "spriteNoExport": false,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "out_tilehborder": 2,
  "out_tilevborder": 2,
  "out_columns": 1,
  "tile_count": 1,
  "autoTileSets": [],
  "tileAnimationFrames": [],
  "tileAnimationSpeed": 15.0,
  "tileAnimation": {
    "FrameData": [
      0,
    ],
    "SerialiseFrameCount": 1,
  },
  "macroPageTiles": {
    "SerialiseWidth": 0,
    "SerialiseHeight": 0,
    "TileSerialiseData": [],
  },
  "parent": {
    "name": "DREAM",
    "path": "folders/Tile Sets/DREAM.yy",
  },
  "resourceVersion": "1.0",
  "name": "bg_castle_bunch01_tileset",
  "tags": [],
  "resourceType": "GMTileSet",
}