{
  "spriteId": {
    "name": "land_piece01",
    "path": "sprites/land_piece01/land_piece01.yy",
  },
  "tileWidth": 524,
  "tileHeight": 512,
  "tilexoff": 0,
  "tileyoff": 0,
  "tilehsep": 0,
  "tilevsep": 0,
  "spriteNoExport": false,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "out_tilehborder": 2,
  "out_tilevborder": 2,
  "out_columns": 1,
  "tile_count": 1,
  "autoTileSets": [],
  "tileAnimationFrames": [],
  "tileAnimationSpeed": 15.0,
  "tileAnimation": {
    "FrameData": [
      0,
    ],
    "SerialiseFrameCount": 1,
  },
  "macroPageTiles": {
    "SerialiseWidth": 0,
    "SerialiseHeight": 0,
    "TileSerialiseData": [],
  },
  "parent": {
    "name": "DREAM",
    "path": "folders/Tile Sets/DREAM.yy",
  },
  "resourceVersion": "1.0",
  "name": "land_piece01_tileset",
  "tags": [],
  "resourceType": "GMTileSet",
}