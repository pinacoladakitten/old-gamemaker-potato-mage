{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_enm_rat01_d",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 20,
  "bbox_right": 105,
  "bbox_top": 65,
  "bbox_bottom": 233,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 128,
  "height": 256,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"8fbd3bc2-1805-4372-a8f1-f47ae555e718",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2cb39f7e-6446-45fd-9694-f09c71ac3da0",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c8e2c868-3853-437e-a9df-5b14ca94f089",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"29a3e734-5566-4e8b-9866-50eb07b9ec9b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"3a91ac92-e961-420b-bf40-b6d1c554c06b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ecd59508-69b8-47b6-b263-c158c2766c5b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"529ff13d-0514-4841-ac63-227f370f18dd",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"8b4583c2-09e6-4c90-b61a-5e15df292c56",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"436f3861-5ae7-47dd-9844-296b28d6a06f",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 9.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a1e60fe7-8d2a-445b-b45c-33380584e72c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8fbd3bc2-1805-4372-a8f1-f47ae555e718","path":"sprites/spr_enm_rat01_d/spr_enm_rat01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cbf6c95b-d434-4e53-ad21-e49017385dbb","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2cb39f7e-6446-45fd-9694-f09c71ac3da0","path":"sprites/spr_enm_rat01_d/spr_enm_rat01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f6b91537-7859-48d4-9712-a4a317681c1c","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c8e2c868-3853-437e-a9df-5b14ca94f089","path":"sprites/spr_enm_rat01_d/spr_enm_rat01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f585e621-281b-4369-a34a-e4ea1525164f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"29a3e734-5566-4e8b-9866-50eb07b9ec9b","path":"sprites/spr_enm_rat01_d/spr_enm_rat01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b5f83562-51bd-4b63-931a-e99772f96d40","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3a91ac92-e961-420b-bf40-b6d1c554c06b","path":"sprites/spr_enm_rat01_d/spr_enm_rat01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"518276dd-2b7f-484a-9c11-66ffcf1f9fd1","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ecd59508-69b8-47b6-b263-c158c2766c5b","path":"sprites/spr_enm_rat01_d/spr_enm_rat01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4f12e010-254c-40be-90e6-b3452df9379c","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"529ff13d-0514-4841-ac63-227f370f18dd","path":"sprites/spr_enm_rat01_d/spr_enm_rat01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bbb11e7d-dc71-4991-bc2a-aad8260395cd","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8b4583c2-09e6-4c90-b61a-5e15df292c56","path":"sprites/spr_enm_rat01_d/spr_enm_rat01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"abd6c14b-c233-48a8-a363-cb9c76f8b763","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"436f3861-5ae7-47dd-9844-296b28d6a06f","path":"sprites/spr_enm_rat01_d/spr_enm_rat01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 128,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"f8517771-b330-42d5-a23e-f5ef5005b4e7","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "rat",
    "path": "folders/Sprites/enm/smallEnms/sewer01/rat.yy",
  },
}