{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_nephal_combat_idle01",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 43,
  "bbox_right": 204,
  "bbox_top": 85,
  "bbox_bottom": 449,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 256,
  "height": 512,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d33d2c12-ba01-4c28-a38a-92a33809c07e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c4e8d40f-2d50-4168-b2da-5bbaa7a3eded",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ff948094-9ba1-460e-aa2a-f211ee7daa71",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"aff3416d-3449-4ed0-800d-0d9c7e6974b6",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"12ecddd5-8271-4f77-83d3-f34649aecbbc",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"943c35d2-6f80-4e2d-beba-a5f4171ca429",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"375499b8-5dca-4cdd-81ef-ec5a2674ce57",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 7.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"800b5b28-ec08-4d32-8b9d-8c531af23208","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d33d2c12-ba01-4c28-a38a-92a33809c07e","path":"sprites/spr_nephal_combat_idle01/spr_nephal_combat_idle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"20ec86b1-e0b1-4510-957b-a1ff47082d1e","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c4e8d40f-2d50-4168-b2da-5bbaa7a3eded","path":"sprites/spr_nephal_combat_idle01/spr_nephal_combat_idle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0853e7c5-948f-402b-9512-e0297ac97b1e","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ff948094-9ba1-460e-aa2a-f211ee7daa71","path":"sprites/spr_nephal_combat_idle01/spr_nephal_combat_idle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"509f7cc4-6d42-443b-8631-cb08cab2077c","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aff3416d-3449-4ed0-800d-0d9c7e6974b6","path":"sprites/spr_nephal_combat_idle01/spr_nephal_combat_idle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d9891fde-8602-4345-b68a-05b9cb26c090","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12ecddd5-8271-4f77-83d3-f34649aecbbc","path":"sprites/spr_nephal_combat_idle01/spr_nephal_combat_idle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1c4b48e4-e2b8-4d2f-afdb-ce03afd9dde5","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"943c35d2-6f80-4e2d-beba-a5f4171ca429","path":"sprites/spr_nephal_combat_idle01/spr_nephal_combat_idle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3e326bea-a422-4ae0-85d0-a310cb91540b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"375499b8-5dca-4cdd-81ef-ec5a2674ce57","path":"sprites/spr_nephal_combat_idle01/spr_nephal_combat_idle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"66099cad-6d72-457e-abb3-aeb8ce64a8b8","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "big_nephal",
    "path": "folders/Sprites/enm/boss/big_nephal.yy",
  },
}