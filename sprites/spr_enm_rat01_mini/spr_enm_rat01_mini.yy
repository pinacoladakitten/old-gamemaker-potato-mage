{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_enm_rat01_mini",
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 27,
  "bbox_right": 42,
  "bbox_top": 58,
  "bbox_bottom": 73,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 64,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"1abdb237-c18c-4fb9-8612-ef28add55b10",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a2b522e1-228c-4ffd-bdff-d37f38d2e488",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"13304d1d-7d9a-4400-8025-9fc51701d6b8",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d0ef48b2-0826-428a-b5fc-bbbe2a044667",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ec899cf4-c8f4-4252-969a-51b1200034f3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"1cf62c12-a039-4fbf-a3e0-67bb0159b32e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6c05b45a-eb89-4bad-b803-4acdf2bc04f5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f1f75a1e-69c1-45bf-9af4-5fbfc6bbf0bb",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"54beb790-8be1-4712-a8f5-3c218291e8ee",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 9.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"f4741e87-3280-47f6-a806-7a9251cc1f4b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1abdb237-c18c-4fb9-8612-ef28add55b10","path":"sprites/spr_enm_rat01_mini/spr_enm_rat01_mini.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"983733ec-9ea1-4154-ba80-73b82e5a7410","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a2b522e1-228c-4ffd-bdff-d37f38d2e488","path":"sprites/spr_enm_rat01_mini/spr_enm_rat01_mini.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"81b85501-e20e-45d3-8e63-347415929d6f","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"13304d1d-7d9a-4400-8025-9fc51701d6b8","path":"sprites/spr_enm_rat01_mini/spr_enm_rat01_mini.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cf929c36-f016-4086-abe8-a37312181c86","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d0ef48b2-0826-428a-b5fc-bbbe2a044667","path":"sprites/spr_enm_rat01_mini/spr_enm_rat01_mini.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9b45a60b-c6ce-4837-889e-20c85a8af5a3","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ec899cf4-c8f4-4252-969a-51b1200034f3","path":"sprites/spr_enm_rat01_mini/spr_enm_rat01_mini.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0809c1cf-3c95-4233-bc7f-cd07548aba58","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1cf62c12-a039-4fbf-a3e0-67bb0159b32e","path":"sprites/spr_enm_rat01_mini/spr_enm_rat01_mini.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6a17739d-6c80-47c5-bc47-4074a0a4f3ef","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6c05b45a-eb89-4bad-b803-4acdf2bc04f5","path":"sprites/spr_enm_rat01_mini/spr_enm_rat01_mini.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e475a400-2295-448c-8911-25ef92e0e523","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f1f75a1e-69c1-45bf-9af4-5fbfc6bbf0bb","path":"sprites/spr_enm_rat01_mini/spr_enm_rat01_mini.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cbee013d-0d7e-4086-aebb-bdc28a940788","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"54beb790-8be1-4712-a8f5-3c218291e8ee","path":"sprites/spr_enm_rat01_mini/spr_enm_rat01_mini.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"7ac09255-ae60-4ff3-9e83-d1eef0974027","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "rat",
    "path": "folders/Sprites/enm/smallEnms/sewer01/rat.yy",
  },
}