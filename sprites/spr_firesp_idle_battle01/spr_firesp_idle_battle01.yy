{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_firesp_idle_battle01",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 8,
  "bbox_right": 255,
  "bbox_top": 0,
  "bbox_bottom": 255,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 256,
  "height": 256,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"caf3a1b6-d685-48dc-990e-884baf4c4d85",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9fa9227b-2208-43d9-989d-ffc692a3541c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"330df0a3-b02f-4851-a127-5b58f681249d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d4e58d57-caae-46cd-9c40-108b1c993594",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"377608d1-0cc6-424e-b051-7fd99fee03a6",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7ab04d07-fb6c-434f-b92b-5ded0ee2fe8b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"79befe90-2e5e-402f-8485-1666506d32cf",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 7.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"3d1f00d8-a727-4b45-b4e6-091423e4004f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"caf3a1b6-d685-48dc-990e-884baf4c4d85","path":"sprites/spr_firesp_idle_battle01/spr_firesp_idle_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"387b0c20-f77f-46be-815b-98dd57cd03fb","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9fa9227b-2208-43d9-989d-ffc692a3541c","path":"sprites/spr_firesp_idle_battle01/spr_firesp_idle_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"22cdb611-d935-4b84-ae36-c7dcc216362b","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"330df0a3-b02f-4851-a127-5b58f681249d","path":"sprites/spr_firesp_idle_battle01/spr_firesp_idle_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"689ef249-0c84-4c41-92d4-f7d5fd73bb73","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d4e58d57-caae-46cd-9c40-108b1c993594","path":"sprites/spr_firesp_idle_battle01/spr_firesp_idle_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f66c91b0-87f1-4d9c-91c3-bc4decd93050","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"377608d1-0cc6-424e-b051-7fd99fee03a6","path":"sprites/spr_firesp_idle_battle01/spr_firesp_idle_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"59006419-2f26-4797-b9b7-020c9fd2c4b1","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7ab04d07-fb6c-434f-b92b-5ded0ee2fe8b","path":"sprites/spr_firesp_idle_battle01/spr_firesp_idle_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4a2d55ec-f577-4ae2-8a5f-5b106d58e6a6","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"79befe90-2e5e-402f-8485-1666506d32cf","path":"sprites/spr_firesp_idle_battle01/spr_firesp_idle_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 128,
    "yorigin": 128,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"65ddd2da-f14e-483c-8f48-2628d41e5688","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "fire_spirit",
    "path": "folders/Sprites/enm/boss/fire_spirit.yy",
  },
}