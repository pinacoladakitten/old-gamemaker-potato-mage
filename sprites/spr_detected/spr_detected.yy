{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_detected",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 12,
  "bbox_right": 44,
  "bbox_top": 0,
  "bbox_bottom": 55,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"4050bfa8-d566-457b-baa6-7608df32068c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"326ca9fd-bd96-453f-a856-bb48052f8c1a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"bae9842d-7ab2-4657-a5a4-cd044d6a7fb9",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6711efcf-efa1-49dc-a125-d838b737ab85",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"19d8c662-36b5-4169-af3a-144078cd0ce8",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9cf48ac6-2714-43af-ab72-1f80cefc8053",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"bac3040a-a8de-44fa-9d73-123a95ab49ec","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4050bfa8-d566-457b-baa6-7608df32068c","path":"sprites/spr_detected/spr_detected.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e3223abc-1298-4722-9d94-3f03eae019f1","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"326ca9fd-bd96-453f-a856-bb48052f8c1a","path":"sprites/spr_detected/spr_detected.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b593dd30-f507-4e33-a0d7-17b33a667419","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bae9842d-7ab2-4657-a5a4-cd044d6a7fb9","path":"sprites/spr_detected/spr_detected.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"22120ed2-640a-40b0-9026-3753ad1652e4","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6711efcf-efa1-49dc-a125-d838b737ab85","path":"sprites/spr_detected/spr_detected.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2ea309a6-55e8-49e4-b45c-6f30ded2355f","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"19d8c662-36b5-4169-af3a-144078cd0ce8","path":"sprites/spr_detected/spr_detected.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9b8d07e2-e290-4823-a494-34383b4b3591","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9cf48ac6-2714-43af-ab72-1f80cefc8053","path":"sprites/spr_detected/spr_detected.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"ed26410c-98ea-48dd-abb4-ee13b495b1d4","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "enemy",
    "path": "folders/Sprites/overworld/enemy.yy",
  },
}