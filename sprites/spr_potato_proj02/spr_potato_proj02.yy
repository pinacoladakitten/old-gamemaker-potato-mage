{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_potato_proj02",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 61,
  "bbox_top": 2,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5bb6ac15-003a-4581-9fac-3665bec6438a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d899e9d8-604d-42fc-a128-2ea0c43976ab",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6748e4d3-5456-4c3c-af53-7993c3d57c5f",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e767d5e4-7741-42d7-9793-e4a9807b7ad9",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"203af2ba-1bb4-481f-a708-664636fcd803",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ff4cd33f-5fc1-4200-97d6-3c356fd31ba8",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"788691f6-5bac-42a6-b02f-3bd8d19fcf19","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5bb6ac15-003a-4581-9fac-3665bec6438a","path":"sprites/spr_potato_proj02/spr_potato_proj02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fbd400ad-250b-4144-855f-943d1dc120f6","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d899e9d8-604d-42fc-a128-2ea0c43976ab","path":"sprites/spr_potato_proj02/spr_potato_proj02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"184f630a-926d-4e3b-8605-0a09836256db","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6748e4d3-5456-4c3c-af53-7993c3d57c5f","path":"sprites/spr_potato_proj02/spr_potato_proj02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d2019ed-eaa7-44f7-960c-5e39ea30cfb9","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e767d5e4-7741-42d7-9793-e4a9807b7ad9","path":"sprites/spr_potato_proj02/spr_potato_proj02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2c923ef3-d693-4719-8b89-b3949889321d","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"203af2ba-1bb4-481f-a708-664636fcd803","path":"sprites/spr_potato_proj02/spr_potato_proj02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d1b76acb-0170-41dd-b69d-6308ad2648f4","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ff4cd33f-5fc1-4200-97d6-3c356fd31ba8","path":"sprites/spr_potato_proj02/spr_potato_proj02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"b243f102-bd2e-47c3-a155-70e1f946a7c2","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "attacks_effects",
    "path": "folders/Sprites/char/potato_mage/attacks_effects.yy",
  },
}