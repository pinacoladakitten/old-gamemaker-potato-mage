{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_potato_proj",
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 9,
  "bbox_right": 22,
  "bbox_top": 13,
  "bbox_bottom": 24,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ef8284d1-2a09-4ca0-bea5-6298e8294e4c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e6c9cec0-ddd6-4eb6-8393-4dc75af60599",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2292bc86-e2f3-46c5-8f09-8b9928b678c3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f7b1d997-761c-406a-a084-ea7e93f468f1",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c2e60f6c-19e7-4d88-8d00-969a6459cd21",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"54e48534-b30d-4a5e-b5a6-4cce92c7a904",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ba13c389-1c35-4afd-87a0-e4b285de17bb",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"55a55391-d946-4f77-a66f-4c80e78a0839",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d398a066-0bac-419d-8c10-d7accfebdca7",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f86cfb04-81fd-4f4e-b7a8-799e5dfc9eed",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 10.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"34f2c098-5c17-4ede-80f8-3a048b81a89a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ef8284d1-2a09-4ca0-bea5-6298e8294e4c","path":"sprites/spr_potato_proj/spr_potato_proj.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8f69b1c6-36dd-41b8-b53c-a32679ba3494","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e6c9cec0-ddd6-4eb6-8393-4dc75af60599","path":"sprites/spr_potato_proj/spr_potato_proj.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"676274e6-2832-48d0-8c89-d4edbb9e0196","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2292bc86-e2f3-46c5-8f09-8b9928b678c3","path":"sprites/spr_potato_proj/spr_potato_proj.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e3815ab7-a57f-4b4e-b59f-1ce9bf0e4c42","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f7b1d997-761c-406a-a084-ea7e93f468f1","path":"sprites/spr_potato_proj/spr_potato_proj.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"712e66dd-55ef-4fcf-9932-2716d853fc25","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c2e60f6c-19e7-4d88-8d00-969a6459cd21","path":"sprites/spr_potato_proj/spr_potato_proj.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9997f59-d4aa-44b2-bdf1-ca2caf99d72e","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"54e48534-b30d-4a5e-b5a6-4cce92c7a904","path":"sprites/spr_potato_proj/spr_potato_proj.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6796bbb4-f4ea-4338-ad9e-d8955a1e4757","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ba13c389-1c35-4afd-87a0-e4b285de17bb","path":"sprites/spr_potato_proj/spr_potato_proj.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d1bf5e40-6d47-403e-9bcd-9654fa710d26","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"55a55391-d946-4f77-a66f-4c80e78a0839","path":"sprites/spr_potato_proj/spr_potato_proj.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ccc20fb8-4eaa-4e3b-947d-8e584189ac81","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d398a066-0bac-419d-8c10-d7accfebdca7","path":"sprites/spr_potato_proj/spr_potato_proj.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c1aacd7d-5aa7-4b03-a57e-4ce73336244f","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f86cfb04-81fd-4f4e-b7a8-799e5dfc9eed","path":"sprites/spr_potato_proj/spr_potato_proj.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"a6e93b8e-b997-4a5a-aca4-630e87d6d620","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "attacks_effects",
    "path": "folders/Sprites/char/potato_mage/attacks_effects.yy",
  },
}