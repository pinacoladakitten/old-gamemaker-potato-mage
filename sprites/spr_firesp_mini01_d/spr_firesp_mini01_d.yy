{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_firesp_mini01_d",
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 65,
  "bbox_right": 65,
  "bbox_top": 65,
  "bbox_bottom": 65,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"b04e1f08-0e1a-4e7c-8054-53445da10095",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"200ed244-4045-448b-8775-f980bd77a39e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"17923580-63c4-45fb-a23c-fd0044b00257",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"b0f3c4ea-25a6-4d61-a525-4b1020f28908",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9fad7714-1e9c-4f8e-a41e-cb7523bbb544",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"84846760-12e0-4e39-aacc-9de25edd2de3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"4b568b32-15c4-40ce-acd4-8624d281e658",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"99af14ee-7511-4b92-9675-6139c811875f",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"13c6d59c-ada0-4caa-93a6-6b617204025d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"39e5ee8e-31b7-4308-be92-b286e24dc98f",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 10.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"bff367af-22c4-4482-a983-62db000c49d0","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b04e1f08-0e1a-4e7c-8054-53445da10095","path":"sprites/spr_firesp_mini01_d/spr_firesp_mini01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"18112f83-f36f-4d03-a298-d4d412d0d686","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"200ed244-4045-448b-8775-f980bd77a39e","path":"sprites/spr_firesp_mini01_d/spr_firesp_mini01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9db6acf1-3951-4bcc-bd3f-b6e2ee1dabb6","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"17923580-63c4-45fb-a23c-fd0044b00257","path":"sprites/spr_firesp_mini01_d/spr_firesp_mini01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2d52b534-17aa-4a00-b63b-1a2aef933cec","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b0f3c4ea-25a6-4d61-a525-4b1020f28908","path":"sprites/spr_firesp_mini01_d/spr_firesp_mini01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b7355595-4650-437c-b116-1b38f61f497d","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9fad7714-1e9c-4f8e-a41e-cb7523bbb544","path":"sprites/spr_firesp_mini01_d/spr_firesp_mini01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"772e0d0e-55cd-46d9-9c70-e4d99d362b61","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"84846760-12e0-4e39-aacc-9de25edd2de3","path":"sprites/spr_firesp_mini01_d/spr_firesp_mini01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"662b98ca-ba97-441f-8c9c-a93f39ad2272","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4b568b32-15c4-40ce-acd4-8624d281e658","path":"sprites/spr_firesp_mini01_d/spr_firesp_mini01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f7f9199a-15a1-4bc1-8a1f-4f4006b62431","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"99af14ee-7511-4b92-9675-6139c811875f","path":"sprites/spr_firesp_mini01_d/spr_firesp_mini01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eaef2f64-63b9-4da1-bea3-364117c640dd","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"13c6d59c-ada0-4caa-93a6-6b617204025d","path":"sprites/spr_firesp_mini01_d/spr_firesp_mini01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7793e364-fe0e-4cfe-aa17-eeeffa1f8589","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"39e5ee8e-31b7-4308-be92-b286e24dc98f","path":"sprites/spr_firesp_mini01_d/spr_firesp_mini01_d.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"f5bc7fba-c391-423d-a5cb-e8c277bef755","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "2nd_phase",
    "path": "folders/Sprites/enm/boss/fire_spirit/2nd_phase.yy",
  },
}