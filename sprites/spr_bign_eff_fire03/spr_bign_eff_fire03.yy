{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_bign_eff_fire03",
  "bboxMode": 2,
  "collisionKind": 2,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 62,
  "bbox_right": 66,
  "bbox_top": 62,
  "bbox_bottom": 66,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"cb22bfe3-5a31-4b10-b7e0-4afca97b5d1d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d1319d73-bfd1-4616-a586-67c943bafa3c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6f818618-e459-4ef6-9c0f-3c6e93450a96",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"3c46cb6e-090a-4c7c-9d3b-dcb132aa06c6",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6c61254c-c4f8-4cca-bec1-37b034de1ccd",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"31ac17b6-1ddc-4563-8f7e-f40bd13e7e29",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"0452ce05-b7e9-4e88-9fc2-d9e4c7dc3cb0",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7a9b856a-0c02-46de-9c6b-ec6b32948a41",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7f1a5077-48fc-4d58-84e7-8ce173e67044",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"1af5710b-39d8-4b0f-9585-4a216ff4dbaf",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 10.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d74ed7bf-5b54-4a2e-87c6-46bc4c7035f4","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cb22bfe3-5a31-4b10-b7e0-4afca97b5d1d","path":"sprites/spr_bign_eff_fire03/spr_bign_eff_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fbd6d70b-a0df-4e23-82ec-2f891ef570aa","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d1319d73-bfd1-4616-a586-67c943bafa3c","path":"sprites/spr_bign_eff_fire03/spr_bign_eff_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9a3ca32d-d1ff-4503-8199-6c0227adca16","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6f818618-e459-4ef6-9c0f-3c6e93450a96","path":"sprites/spr_bign_eff_fire03/spr_bign_eff_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e68ac9d6-b66f-40b7-85e1-6126fdb556aa","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c46cb6e-090a-4c7c-9d3b-dcb132aa06c6","path":"sprites/spr_bign_eff_fire03/spr_bign_eff_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bed3bfe0-f68b-4cdd-8293-d38bea15c952","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6c61254c-c4f8-4cca-bec1-37b034de1ccd","path":"sprites/spr_bign_eff_fire03/spr_bign_eff_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"db0102cb-9e5a-4693-8b27-2fcc4cb23066","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"31ac17b6-1ddc-4563-8f7e-f40bd13e7e29","path":"sprites/spr_bign_eff_fire03/spr_bign_eff_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1cea23d7-228c-409b-bcd4-6b0fc667f011","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0452ce05-b7e9-4e88-9fc2-d9e4c7dc3cb0","path":"sprites/spr_bign_eff_fire03/spr_bign_eff_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"06efd3d7-f163-4531-9727-0450a7050577","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a9b856a-0c02-46de-9c6b-ec6b32948a41","path":"sprites/spr_bign_eff_fire03/spr_bign_eff_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"afbdeb4a-8e56-46b1-af59-f3a7515ba7fb","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7f1a5077-48fc-4d58-84e7-8ce173e67044","path":"sprites/spr_bign_eff_fire03/spr_bign_eff_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"29b750aa-e6ef-4fae-b92f-8336f126f963","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1af5710b-39d8-4b0f-9585-4a216ff4dbaf","path":"sprites/spr_bign_eff_fire03/spr_bign_eff_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"5a7dbd32-f4a2-4d50-81f4-b78ad03172fb","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "effects",
    "path": "folders/Sprites/enm/boss/big_nephal/effects.yy",
  },
}