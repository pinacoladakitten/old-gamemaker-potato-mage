{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_point_chg_tree",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 20,
  "bbox_top": 3,
  "bbox_bottom": 29,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c46d7340-4302-49f4-ba89-3298a1a76018",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"712bf269-2e18-4c49-a987-2b9c5a597808",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e74d23f4-2b9c-4642-b5c5-516d0126ac18",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"b9a5dafb-5712-43d2-8d8e-71fc5fd34b36",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2014a081-aa32-477d-b989-9359fa974895",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"0ede9898-72ac-494c-b7cc-c9f6d13842f5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"4684316a-6362-4a7a-941e-a245c5bc4bdc",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"1de9b93c-2214-4649-8ddb-9f28bce6793f",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"3ad09efe-ca95-45e0-a8d1-69eb070e09c5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ddb4cca4-4d62-4bdd-9d60-5dce030a7768",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f75d5ad3-9824-4216-a37b-489a66311771",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5fc1984b-5191-4412-821a-f1d51efb4ea7",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 12.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"cb82c58a-20e9-4f80-95d2-1b35eb65efe0","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c46d7340-4302-49f4-ba89-3298a1a76018","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"91eebeed-23fc-4dc3-aad0-bb71c0e64c3f","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"712bf269-2e18-4c49-a987-2b9c5a597808","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c6b9fcef-d983-4bd6-aa33-a0dd15c349f5","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e74d23f4-2b9c-4642-b5c5-516d0126ac18","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7da46a74-7aaf-47ea-b3b5-62331e3eabf3","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b9a5dafb-5712-43d2-8d8e-71fc5fd34b36","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"22912277-5d2b-4903-9078-b44225c4e3f2","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2014a081-aa32-477d-b989-9359fa974895","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"907a2753-3ac8-4f5d-8f04-28073c215171","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0ede9898-72ac-494c-b7cc-c9f6d13842f5","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"01f80c83-6891-48b7-9fee-d0d8be50aa5c","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4684316a-6362-4a7a-941e-a245c5bc4bdc","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1b295d72-8c33-41f9-89ac-94e5a1fe2c8a","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1de9b93c-2214-4649-8ddb-9f28bce6793f","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"da157a23-af5d-4db5-8b86-4fa670401079","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3ad09efe-ca95-45e0-a8d1-69eb070e09c5","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"992738ed-50c9-45fa-8c2a-cb665edcc4b9","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ddb4cca4-4d62-4bdd-9d60-5dce030a7768","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5f000add-821c-4915-a732-e77dacfc3cce","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f75d5ad3-9824-4216-a37b-489a66311771","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cdd2e7bf-6981-4e8b-8c8b-7b7ab9215abc","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5fc1984b-5191-4412-821a-f1d51efb4ea7","path":"sprites/spr_point_chg_tree/spr_point_chg_tree.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"c8bcc06a-9ca9-4eea-aba9-0687b381b7ea","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "skillTree",
    "path": "folders/Sprites/HUD/inventory/skillTree.yy",
  },
}