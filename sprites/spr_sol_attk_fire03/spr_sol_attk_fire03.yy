{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_sol_attk_fire03",
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 61,
  "bbox_right": 67,
  "bbox_top": 61,
  "bbox_bottom": 67,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a5c0c70a-12ff-4a80-aa0b-86aa791fbb25",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9e354190-7303-417e-a40b-cd63f6dbf8f5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e1a55e48-542d-40f9-90d0-3fd479275625",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f204403e-5bcf-4109-b243-f8bb3b65d8c8",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"012a9d21-876c-44d4-aaae-104b4651bba1",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"24e2aa27-5bc0-4a63-ac7b-e01b9939445f",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"32353f4c-1f4e-4fb5-8017-c62aa5428f56",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"428497eb-129c-4dc8-bdf1-26e27db2d844",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6fd39a65-edff-4599-a269-eb15f5bb939d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9611e3aa-bd5c-426c-a8b0-06d5b2cb9e8e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d3453cb5-f157-4117-8ccd-5764d148f770",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7e146452-632d-4820-9826-6891a0e074f9",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ff864808-84db-49b3-9a40-e7663ec5cf7e",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 13.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1b28503d-fe43-4ae4-b84e-8190b933110f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a5c0c70a-12ff-4a80-aa0b-86aa791fbb25","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"880112b0-550b-4a77-af8e-8ad524273f3a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9e354190-7303-417e-a40b-cd63f6dbf8f5","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0b1dd36d-5bc3-49e5-8b2b-24e462aeaf02","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e1a55e48-542d-40f9-90d0-3fd479275625","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8c4b79f5-b0ff-4422-b163-a4b98377c57d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f204403e-5bcf-4109-b243-f8bb3b65d8c8","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"196bb5a0-9785-4bff-8f76-70e50749b59e","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"012a9d21-876c-44d4-aaae-104b4651bba1","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5bebf357-4413-452f-be5f-c0643d715ae8","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"24e2aa27-5bc0-4a63-ac7b-e01b9939445f","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d41b3c45-c149-48cc-9df1-7f681ffc141c","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"32353f4c-1f4e-4fb5-8017-c62aa5428f56","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"40a0b2d4-c011-48cd-a8c8-76494e5c91a4","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"428497eb-129c-4dc8-bdf1-26e27db2d844","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aa77bf3d-474d-4f71-93d7-e51ed8e4a372","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6fd39a65-edff-4599-a269-eb15f5bb939d","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f313d675-bc5e-4588-96e4-bab25ede8f7f","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9611e3aa-bd5c-426c-a8b0-06d5b2cb9e8e","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5e153b14-626a-4c0d-a64a-ee287e909d3d","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d3453cb5-f157-4117-8ccd-5764d148f770","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cf12d221-7ec0-4508-b345-0e316b982ae3","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7e146452-632d-4820-9826-6891a0e074f9","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e6f8bb73-af96-4ff8-9d2e-990c2e87e375","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ff864808-84db-49b3-9a40-e7663ec5cf7e","path":"sprites/spr_sol_attk_fire03/spr_sol_attk_fire03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"d9793742-e4f4-4bbb-99e4-732173edf2ca","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "effects",
    "path": "folders/Sprites/char/sol/effects.yy",
  },
}