{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_target",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 40,
  "bbox_right": 87,
  "bbox_top": 23,
  "bbox_bottom": 96,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"319c0068-d28a-48aa-9c9f-2a6bcfd08d6b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"36c7354d-185b-4d66-acdc-f1ef4c1ec57d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ed7136eb-2222-4bfa-a0c4-b79ed235cd06",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2a1b0f5c-1c87-47a4-890a-cd266d763899",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"fd901258-e3fd-452a-a195-dd79054a29ae",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e49c723d-99fd-46d7-9ad8-efa1df53c00b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e32b4710-aa94-4c24-be6c-2a088b205c66",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"82257118-8500-434a-bc80-9a003e2fe091",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"871a4358-8405-4fc5-9929-e305001be8b3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9f414b45-61bb-4b5c-b161-e2d0709788c7",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 10.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ecb82711-e8df-46fb-8409-e18ed929f47f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"319c0068-d28a-48aa-9c9f-2a6bcfd08d6b","path":"sprites/spr_target/spr_target.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a3cd51c8-e10d-4230-9987-4b8d474bdd05","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"36c7354d-185b-4d66-acdc-f1ef4c1ec57d","path":"sprites/spr_target/spr_target.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1bf9209c-308f-4262-a95f-01e8e4f0c7fe","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ed7136eb-2222-4bfa-a0c4-b79ed235cd06","path":"sprites/spr_target/spr_target.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"692df3bd-32cc-47d0-9e08-b58c27484bb3","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2a1b0f5c-1c87-47a4-890a-cd266d763899","path":"sprites/spr_target/spr_target.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f4984897-81c7-4af8-a5ee-302a12ebb09a","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fd901258-e3fd-452a-a195-dd79054a29ae","path":"sprites/spr_target/spr_target.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00c1526a-2c6c-4ce4-80ce-e43412197eb3","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e49c723d-99fd-46d7-9ad8-efa1df53c00b","path":"sprites/spr_target/spr_target.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9d1a1a5a-22a3-4913-97e8-e35b21ec6049","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e32b4710-aa94-4c24-be6c-2a088b205c66","path":"sprites/spr_target/spr_target.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"04dae94f-a109-43f2-ad7a-b06c50e884d2","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"82257118-8500-434a-bc80-9a003e2fe091","path":"sprites/spr_target/spr_target.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"07792bce-2a39-4ee5-a0b9-9074b76ab670","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"871a4358-8405-4fc5-9929-e305001be8b3","path":"sprites/spr_target/spr_target.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"44b001d6-e9af-4aea-98f8-19b6d1d66146","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9f414b45-61bb-4b5c-b161-e2d0709788c7","path":"sprites/spr_target/spr_target.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"831c3829-27c2-49ff-8742-c02bfcd7efac","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "battle",
    "path": "folders/Sprites/HUD/battle.yy",
  },
}