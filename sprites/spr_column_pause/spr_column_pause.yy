{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_column_pause",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 408,
  "bbox_top": 0,
  "bbox_bottom": 1079,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 409,
  "height": 1080,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"62f0dc4c-5877-4fe4-a5a0-f7ffff8fab59",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"684681a7-6aaa-40ba-95a0-e9cd16b94766",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d6f15f94-aaf7-4af0-abb7-fc173d58f235",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a076f62f-68a1-4b90-a6f0-f884ceba05a0",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"81060a34-e760-4186-8d34-1a194dd241bf",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6abff943-2aca-4685-87c7-2321d3bc5d03",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"8ad1d335-99e9-4b9b-9ac1-ab61e8216da1","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"62f0dc4c-5877-4fe4-a5a0-f7ffff8fab59","path":"sprites/spr_column_pause/spr_column_pause.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cbdad788-7607-471f-9fed-6daa34569103","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"684681a7-6aaa-40ba-95a0-e9cd16b94766","path":"sprites/spr_column_pause/spr_column_pause.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ef9689b5-353c-4e5f-b073-a514ec4c7c5c","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d6f15f94-aaf7-4af0-abb7-fc173d58f235","path":"sprites/spr_column_pause/spr_column_pause.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0e54e7b8-319a-459c-a1e0-94f8c70ff585","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a076f62f-68a1-4b90-a6f0-f884ceba05a0","path":"sprites/spr_column_pause/spr_column_pause.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"92ca8ebb-5d37-49e6-af86-96beb6c914aa","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"81060a34-e760-4186-8d34-1a194dd241bf","path":"sprites/spr_column_pause/spr_column_pause.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5a10fce0-964f-49ce-8cf8-94e0a7c6b1c8","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6abff943-2aca-4685-87c7-2321d3bc5d03","path":"sprites/spr_column_pause/spr_column_pause.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 199,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"538576f7-1910-45f3-92cd-f90752b77e87","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "general",
    "path": "folders/Sprites/HUD/inventory/general.yy",
  },
}