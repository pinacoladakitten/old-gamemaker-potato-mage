{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_bign_eff04",
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 55,
  "bbox_right": 76,
  "bbox_top": 56,
  "bbox_bottom": 77,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9f2c2aab-0526-4744-be4f-53a4e4ce94df",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"551f9ff5-b3c3-45d6-bb82-6081aa0c5f1d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"83b3d39e-cfdb-4cbd-ae1a-d6db1fe566d8",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"dad7f2d9-3a9b-45a3-bbf8-4fd9132621fa",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7b6f64c2-fc01-4b3c-bfe6-6f2d1489dba9",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e9494990-1b1a-4c2d-b0fe-f3f90eade7ae",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e483deb7-815b-4223-b8a0-42a678e5ba01",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d309a88f-3da9-42c6-ad23-ef8b082cb9bd",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"8dffe57e-f81f-48b4-8c33-96b9689ddfeb",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 9.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"cce8ca51-f359-46ee-b338-57aefad09bf3","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9f2c2aab-0526-4744-be4f-53a4e4ce94df","path":"sprites/spr_bign_eff04/spr_bign_eff04.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6c65262c-2584-4ea3-b48e-9f41e9a14e6b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"551f9ff5-b3c3-45d6-bb82-6081aa0c5f1d","path":"sprites/spr_bign_eff04/spr_bign_eff04.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4b418c65-8aca-4154-887d-4e867c50f9a3","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"83b3d39e-cfdb-4cbd-ae1a-d6db1fe566d8","path":"sprites/spr_bign_eff04/spr_bign_eff04.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8f2d6edc-078b-4e68-a073-ea0a73f81262","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dad7f2d9-3a9b-45a3-bbf8-4fd9132621fa","path":"sprites/spr_bign_eff04/spr_bign_eff04.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1cd918a8-f40d-416b-ada6-39bebba12b84","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7b6f64c2-fc01-4b3c-bfe6-6f2d1489dba9","path":"sprites/spr_bign_eff04/spr_bign_eff04.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"743d36e3-9b99-4aa8-805c-27736694b091","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e9494990-1b1a-4c2d-b0fe-f3f90eade7ae","path":"sprites/spr_bign_eff04/spr_bign_eff04.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6b3b5f78-95ae-4f67-8ba9-b50da3c474c3","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e483deb7-815b-4223-b8a0-42a678e5ba01","path":"sprites/spr_bign_eff04/spr_bign_eff04.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"01a46849-0b65-4f41-a9b8-91e8600e0b3e","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d309a88f-3da9-42c6-ad23-ef8b082cb9bd","path":"sprites/spr_bign_eff04/spr_bign_eff04.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ed090242-a97e-485c-ad8f-f9dddb55fa4c","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8dffe57e-f81f-48b4-8c33-96b9689ddfeb","path":"sprites/spr_bign_eff04/spr_bign_eff04.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"dee889fe-5aff-4f3c-81af-eb4c7c36799e","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "effects",
    "path": "folders/Sprites/enm/boss/big_nephal/effects.yy",
  },
}