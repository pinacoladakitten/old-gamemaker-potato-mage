{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_mag_parasite_anm01",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 9,
  "bbox_right": 123,
  "bbox_top": 7,
  "bbox_bottom": 110,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "DynamicTexturePage": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6856c981-095b-4a67-9bc9-36013b2f823c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ffb265fb-8a5c-4af1-8622-2222df687015",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"097c3d8c-d95a-49d9-89e9-79591e626276",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f76173e5-5439-4840-8901-8cf564515ff3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"cc8c107b-e8a1-4001-90e8-f955939297da",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6dd9aee2-eaa8-4c78-8956-17e0dd4f8e24",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d1f1e25d-d263-49cc-9f8e-a94f02734350",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"3266ff03-6ec2-4fc1-a9ca-2694b05de648",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"babb63ec-b7cd-4ebb-8ee1-934fe028d2d5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"44ee40a1-7603-4f86-ba12-0af2a58d82ad",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f4d8f51c-6156-4b5a-8dbd-683421d36f41",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5154f1ad-58e8-4da8-a5b1-922f8d38242c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"14bb0aef-5626-40b2-90aa-6fd5ae86ee4e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"b17003a2-6797-4605-9561-09e89cf4cda3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2a74e61c-2437-4e19-95e9-121f50b4f8bc",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"0136ed60-e54f-4ebb-b725-68841f68d51b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"abf44b28-a746-4ec7-85b9-dd66cb2e8d31",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"546a3901-40eb-451b-a42c-5bec5b71ba85",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"0538641d-7762-432d-a1ec-33eafc9e633d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"20bd594e-a2a7-4e8e-b86a-2e5c8a347eb6",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"8c888659-e66b-42f3-a4ad-a858bf6f649d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a8dc698a-63fa-4842-b8a0-97b238e7538f",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"658fd969-8b17-4802-b841-302fd8190bea",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c1dd04e9-b0ea-43dc-9d12-4ace20ea6fc9",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c22c0c15-453b-43fc-9091-a0ee92c0e242",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"8d4bc71a-2443-41ca-84c8-42bcabd8095f",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ec1b79fa-db06-4f0c-b248-381be0202ac9",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"3e504559-d9ef-4d6c-9a54-556a863f7a1f",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a16a0bb9-9048-4177-90f5-8cd261617e6f",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"bb4f091f-a90f-4d4b-ba66-3fa77762b469",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"81879ad4-0d6a-41cc-b3d7-b2684014d593",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"4737e0b9-da4d-464e-a76a-3b2a38127cd1",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a2aadbd7-018b-4885-b97b-4e83862b64f2",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"0b104ade-ffa8-4397-a3fd-1a4a85d7f299",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 34.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a3d0790e-e40c-487d-a8fc-81be0a2e287a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6856c981-095b-4a67-9bc9-36013b2f823c","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"65b22f33-34bb-4edd-9962-44903646e3d7","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ffb265fb-8a5c-4af1-8622-2222df687015","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"38b6e0ac-1dd4-484e-b857-02fe64802029","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"097c3d8c-d95a-49d9-89e9-79591e626276","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c4dd6896-b5cf-41f5-954e-40c07a10ae0a","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f76173e5-5439-4840-8901-8cf564515ff3","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aeae7fb6-0d3b-495d-a44a-a6209e7de11d","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc8c107b-e8a1-4001-90e8-f955939297da","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"420f3a70-39f0-431a-a98f-66d5df43ca0b","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6dd9aee2-eaa8-4c78-8956-17e0dd4f8e24","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"66ce5c33-5c70-40ab-b80a-01bfd45c7ee6","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d1f1e25d-d263-49cc-9f8e-a94f02734350","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fc052f90-b1a5-445b-b2bd-f2ba4a8926d4","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3266ff03-6ec2-4fc1-a9ca-2694b05de648","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5046715a-0fe7-4028-a259-9e3b6b02ff1a","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"babb63ec-b7cd-4ebb-8ee1-934fe028d2d5","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"26c7f5d6-9325-4b20-8ff2-13e4e52d65ea","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"44ee40a1-7603-4f86-ba12-0af2a58d82ad","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"93553b4a-c9e6-4af5-91af-ccb2768f75f0","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f4d8f51c-6156-4b5a-8dbd-683421d36f41","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7c33150d-34b0-4d26-84a3-b4ce901e0649","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5154f1ad-58e8-4da8-a5b1-922f8d38242c","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3b31240e-9b1e-447c-81c0-8dc5b4910440","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"14bb0aef-5626-40b2-90aa-6fd5ae86ee4e","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"71326b16-95bb-44a5-ac2b-b139ccf0f999","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b17003a2-6797-4605-9561-09e89cf4cda3","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"59470865-f5e5-4c73-b83d-788fed92392e","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2a74e61c-2437-4e19-95e9-121f50b4f8bc","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0b7a4569-f652-47a1-9a18-1cd6a9cea05f","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0136ed60-e54f-4ebb-b725-68841f68d51b","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"de41fdda-4bc3-464c-8275-7ed0863235d3","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"abf44b28-a746-4ec7-85b9-dd66cb2e8d31","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e454ce3b-d15a-4a42-875b-2fb7e17335e9","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"546a3901-40eb-451b-a42c-5bec5b71ba85","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9488512-58fa-4e05-8db6-67e433804a7f","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0538641d-7762-432d-a1ec-33eafc9e633d","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0d977874-4556-44dc-837f-e7512114fdf9","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"20bd594e-a2a7-4e8e-b86a-2e5c8a347eb6","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"672eba97-898a-4113-b196-b5dedf4ef873","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8c888659-e66b-42f3-a4ad-a858bf6f649d","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c1e007b7-2816-420c-b459-acc6422efe2c","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a8dc698a-63fa-4842-b8a0-97b238e7538f","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e39dc26d-8309-4a88-8b15-e3656268ef93","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"658fd969-8b17-4802-b841-302fd8190bea","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9ca2466-1cd0-4030-9f38-44d5e04ea793","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c1dd04e9-b0ea-43dc-9d12-4ace20ea6fc9","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dbfdb516-f0b2-4e9c-9fb2-9dd34d61dea8","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c22c0c15-453b-43fc-9091-a0ee92c0e242","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d53a573-a50b-4d1a-a9d4-0300d2d1d8e4","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8d4bc71a-2443-41ca-84c8-42bcabd8095f","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0ad73abd-16fb-4759-b655-c40440aba32d","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ec1b79fa-db06-4f0c-b248-381be0202ac9","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e592372b-dec6-4d0d-a917-247183480a46","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3e504559-d9ef-4d6c-9a54-556a863f7a1f","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a649db57-c72b-4a80-a70c-0cd742636e8d","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a16a0bb9-9048-4177-90f5-8cd261617e6f","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"331bf96e-d418-4f4f-9c0d-850469d86522","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bb4f091f-a90f-4d4b-ba66-3fa77762b469","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2c3fa91c-6340-4e0b-a3e6-cb641b8a4a70","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"81879ad4-0d6a-41cc-b3d7-b2684014d593","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4142d5d4-699f-4e81-8d9f-a0799e2894aa","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4737e0b9-da4d-464e-a76a-3b2a38127cd1","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1103ee51-fc39-4d26-a289-6269a77c1879","Key":32.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a2aadbd7-018b-4885-b97b-4e83862b64f2","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c3643589-cb41-4d3a-a470-c28162b91d4f","Key":33.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0b104ade-ffa8-4397-a3fd-1a4a85d7f299","path":"sprites/spr_mag_parasite_anm01/spr_mag_parasite_anm01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"fbb943dd-04a4-4ca1-bdf3-92fbfe6a98ef","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "mag_parasite",
    "path": "folders/Sprites/char/potato_mage/attacks_effects/mag_parasite.yy",
  },
}