{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_slash01",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 121,
  "bbox_top": 36,
  "bbox_bottom": 127,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"cad7481a-daea-46a5-8cbb-5110c66645c5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c8a5ceff-af47-4266-a2c4-015d103351bc",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"abf7269b-a9d7-4d91-945e-f9f5e4ce6e2a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"dde473cd-a85f-44ee-8fab-f29eec000d55",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"bde60191-5969-4f92-acf4-44ce27c57839",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7a7e40a9-98f0-4912-8970-db2bf58c5233",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"314725e9-0321-4d5e-89b4-bcb90162568d",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 7.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5053bf3b-98f8-493a-bf2f-a10c1a77ecd5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cad7481a-daea-46a5-8cbb-5110c66645c5","path":"sprites/spr_slash01/spr_slash01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7577aa39-a2f8-4926-ba75-faa92d60ee93","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c8a5ceff-af47-4266-a2c4-015d103351bc","path":"sprites/spr_slash01/spr_slash01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c1fc2f64-c995-435f-9064-2a7c859506ff","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"abf7269b-a9d7-4d91-945e-f9f5e4ce6e2a","path":"sprites/spr_slash01/spr_slash01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1c72a70e-5719-4115-bfd0-5ebc7bc917c5","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dde473cd-a85f-44ee-8fab-f29eec000d55","path":"sprites/spr_slash01/spr_slash01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bbe30409-c088-4745-8492-c82ca3928d9f","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bde60191-5969-4f92-acf4-44ce27c57839","path":"sprites/spr_slash01/spr_slash01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f8a715ea-d9b3-431a-9351-dad4f156dc48","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a7e40a9-98f0-4912-8970-db2bf58c5233","path":"sprites/spr_slash01/spr_slash01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"95433947-bbe1-4d32-89d0-e0f7d63582de","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"314725e9-0321-4d5e-89b4-bcb90162568d","path":"sprites/spr_slash01/spr_slash01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"8ace8b93-590d-47c1-976d-154e842a50bf","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "attacks_effects",
    "path": "folders/Sprites/char/potato_mage/attacks_effects.yy",
  },
}