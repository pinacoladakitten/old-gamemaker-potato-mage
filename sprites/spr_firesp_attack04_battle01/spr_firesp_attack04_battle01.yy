{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_firesp_attack04_battle01",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 8,
  "bbox_right": 255,
  "bbox_top": 0,
  "bbox_bottom": 255,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 256,
  "height": 256,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ca94bc57-763e-409b-91a0-4d8e6f7a0e39",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7efb5686-647e-4682-bc28-d0bd6b3bf71a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5ba1eb11-3312-4449-834f-e504bd56426b",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"faadc25a-4710-444b-a436-67c692d7b4d3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ffc8c5b6-3505-4160-b726-d34056578d02",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c89e2e6f-9e24-433f-9f51-273e1a71d064",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2f3dadce-3d38-4fbe-8473-49bf35bab7e3",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 7.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d99558d0-7bba-4d0a-9f44-206ec12934c2","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ca94bc57-763e-409b-91a0-4d8e6f7a0e39","path":"sprites/spr_firesp_attack04_battle01/spr_firesp_attack04_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ba42e21d-8840-499e-a108-1ef2cd634293","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7efb5686-647e-4682-bc28-d0bd6b3bf71a","path":"sprites/spr_firesp_attack04_battle01/spr_firesp_attack04_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6cfa750-5c4b-4a25-a0ff-df8291f29adb","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5ba1eb11-3312-4449-834f-e504bd56426b","path":"sprites/spr_firesp_attack04_battle01/spr_firesp_attack04_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"60623da1-cb51-464b-ad44-f0da4c1d6fdb","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"faadc25a-4710-444b-a436-67c692d7b4d3","path":"sprites/spr_firesp_attack04_battle01/spr_firesp_attack04_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"674d9b1c-a62e-47e7-913d-825dc2070cf5","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ffc8c5b6-3505-4160-b726-d34056578d02","path":"sprites/spr_firesp_attack04_battle01/spr_firesp_attack04_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"778380fc-54a7-4325-8021-38f954f4f165","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c89e2e6f-9e24-433f-9f51-273e1a71d064","path":"sprites/spr_firesp_attack04_battle01/spr_firesp_attack04_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"acf09b9a-f8ac-4610-a08e-337217c6b995","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2f3dadce-3d38-4fbe-8473-49bf35bab7e3","path":"sprites/spr_firesp_attack04_battle01/spr_firesp_attack04_battle01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 128,
    "yorigin": 128,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"bfc0ecbf-3c44-4019-bbc7-06a64d09dbf9","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "fire_spirit",
    "path": "folders/Sprites/enm/boss/fire_spirit.yy",
  },
}