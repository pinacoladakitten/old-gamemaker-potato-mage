{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_enm_exp",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 37,
  "bbox_right": 209,
  "bbox_top": 42,
  "bbox_bottom": 225,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 256,
  "height": 256,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7fc41c86-ca17-4f8d-8c35-fc070ed9dd4e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"640a40f0-3f0d-4cea-81c4-fce8127ee6b5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f0784a7d-0de2-4936-9b01-164510c117b5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"bfb894d0-0551-481e-b36c-91a0b484aad3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5e1be486-9a1b-4ef0-bdaa-5ad200704cb1",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"3aa88ead-77cb-493b-a801-102ce02fbe7c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"6d02db7f-6ff6-4f75-a8a8-25fb38665ae1",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"0509ff58-2cf4-48c9-a89e-d16948399387",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"742b381e-9b4d-4d17-960a-f6bbe189b931",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2687e771-f2f1-48c0-85b2-4dcc8c320a1e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"06bf0ad2-36f8-435a-ace1-9dcd08636725",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"fcbe743d-3f20-4c75-ae19-ef3d1a6154bb",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"30b2d144-3d42-4183-8cf7-b78f12c7772c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"21fc8072-f930-4c02-95a1-74220a850a6c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"0faaa87f-ce4f-42a9-8a43-e79eb7256ee6",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ddb13d79-1505-4529-90d4-3f472fa341e7",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"b2f17277-09e5-40ee-bab6-4c35a3445e77",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 17.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2b45783a-57fa-4c08-b14a-03f5e21026ff","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7fc41c86-ca17-4f8d-8c35-fc070ed9dd4e","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c6a2dc55-27a8-439d-965f-d021b892126c","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"640a40f0-3f0d-4cea-81c4-fce8127ee6b5","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5a1a187b-466b-4be5-9f42-b80bd2f55a5d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f0784a7d-0de2-4936-9b01-164510c117b5","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2572d40c-cd1c-436b-9288-daf546afa3bf","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bfb894d0-0551-481e-b36c-91a0b484aad3","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2ec44f3d-21a4-42ce-8a56-f9d347ce62fd","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5e1be486-9a1b-4ef0-bdaa-5ad200704cb1","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f25658fb-c002-4ae7-9fb3-4eebc325d4fe","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3aa88ead-77cb-493b-a801-102ce02fbe7c","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f612bc05-0feb-4b24-b56c-153ca60909dc","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6d02db7f-6ff6-4f75-a8a8-25fb38665ae1","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"722d963e-053b-4ff0-bec9-587f0a4b7b8f","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0509ff58-2cf4-48c9-a89e-d16948399387","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d0337c3f-04a8-48ef-a02f-3d05851c986b","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"742b381e-9b4d-4d17-960a-f6bbe189b931","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ecf8be64-5321-4124-8836-e0c8fc8842ae","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2687e771-f2f1-48c0-85b2-4dcc8c320a1e","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1eb7d9e6-01ff-432e-89cc-80aaa16a1ee4","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"06bf0ad2-36f8-435a-ace1-9dcd08636725","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"92f92acb-88cb-4777-abb5-49221e456eee","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fcbe743d-3f20-4c75-ae19-ef3d1a6154bb","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b6a73d6d-a23e-44dc-a4d3-026a8c263a8d","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"30b2d144-3d42-4183-8cf7-b78f12c7772c","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1e921820-1c5c-4daf-b564-1f9dd5328e3d","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"21fc8072-f930-4c02-95a1-74220a850a6c","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a92f7fe1-2d54-402c-95d7-5703084d1675","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0faaa87f-ce4f-42a9-8a43-e79eb7256ee6","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"17425737-9040-41f5-9487-f9f16a8354ad","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ddb13d79-1505-4529-90d4-3f472fa341e7","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"589024f6-0751-41fe-ad7e-2c95607a96a9","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b2f17277-09e5-40ee-bab6-4c35a3445e77","path":"sprites/spr_enm_exp/spr_enm_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 128,
    "yorigin": 128,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"15d87ac3-6304-46bb-9799-b915d2cb6057","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "enemy",
    "path": "folders/Sprites/Effects/battle/enemy.yy",
  },
}