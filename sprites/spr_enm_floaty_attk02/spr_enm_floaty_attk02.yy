{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_enm_floaty_attk02",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 11,
  "bbox_right": 199,
  "bbox_top": 82,
  "bbox_bottom": 251,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 256,
  "height": 256,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c94b8ff3-d82e-4eb0-8760-fb8f5c8d583c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ed3133be-d95d-4a55-8075-ba006f8715ab",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c69ce341-84f1-40b6-8251-32c700562c9c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"685fb133-9b28-49e9-9eea-08d5de7757ed",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"45a4ebbb-99e5-4831-8ebc-68c1e7d1a22a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e99b1c6b-37e4-41ed-9c49-77d623db8ac0",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d7f24c4d-74ed-40be-9e5c-5b5de5ea5ebb",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"64b4a7ac-c919-4bcb-ba2a-518ac5e043f8",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5f53623a-d45e-462e-bfb1-52f279898747",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 9.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"282e7d2d-4874-402d-8a16-6987a742865e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c94b8ff3-d82e-4eb0-8760-fb8f5c8d583c","path":"sprites/spr_enm_floaty_attk02/spr_enm_floaty_attk02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9adf37d6-4590-4266-b09f-c18184ee3496","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ed3133be-d95d-4a55-8075-ba006f8715ab","path":"sprites/spr_enm_floaty_attk02/spr_enm_floaty_attk02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bf6d3b2a-88b3-4f4d-9de1-2b40c4ab89b2","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c69ce341-84f1-40b6-8251-32c700562c9c","path":"sprites/spr_enm_floaty_attk02/spr_enm_floaty_attk02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a3d5648-57c3-4b7f-a825-bf2a22ecb832","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"685fb133-9b28-49e9-9eea-08d5de7757ed","path":"sprites/spr_enm_floaty_attk02/spr_enm_floaty_attk02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9972b9f6-e8ef-4ce8-b1e5-b83a362ee1f5","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"45a4ebbb-99e5-4831-8ebc-68c1e7d1a22a","path":"sprites/spr_enm_floaty_attk02/spr_enm_floaty_attk02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6f88164-f718-49ac-8ad4-7bc0c9bc9035","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e99b1c6b-37e4-41ed-9c49-77d623db8ac0","path":"sprites/spr_enm_floaty_attk02/spr_enm_floaty_attk02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b19bce7f-5e38-42ee-8e21-1b67d5cb9ca0","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d7f24c4d-74ed-40be-9e5c-5b5de5ea5ebb","path":"sprites/spr_enm_floaty_attk02/spr_enm_floaty_attk02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2d17a56c-ba4d-40a8-a4c6-0f930a18974e","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"64b4a7ac-c919-4bcb-ba2a-518ac5e043f8","path":"sprites/spr_enm_floaty_attk02/spr_enm_floaty_attk02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fde53b19-7af8-4cc3-9df3-27f0306afe0b","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5f53623a-d45e-462e-bfb1-52f279898747","path":"sprites/spr_enm_floaty_attk02/spr_enm_floaty_attk02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 128,
    "yorigin": 128,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"df799ee2-aa2f-4c92-a621-7572472a5879","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "blade_floaty",
    "path": "folders/Sprites/enm/smallEnms/dream01/blade_floaty.yy",
  },
}