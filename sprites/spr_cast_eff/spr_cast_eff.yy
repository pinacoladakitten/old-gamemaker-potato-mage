{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_cast_eff",
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 40,
  "bbox_right": 90,
  "bbox_top": 22,
  "bbox_bottom": 105,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "DynamicTexturePage": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"10d55cd1-3da8-4ce3-bfc2-8c09e445908a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f367ccd6-7eef-45b0-b8ac-48a67b0e051c",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"2d899511-faff-437f-9e9c-687ac285c8a3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a19a69ed-591e-42c5-b034-7f8b02272a77",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9209b1ce-c235-4422-89de-eaeb0e20ce03",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a31b6a18-8f6a-4e22-800f-477fb6d80de9",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"49b7800f-0364-4151-900f-888d778c9041",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"31db153f-ec1e-44d9-a186-0eb32dfde2ff",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"17591ccc-8649-4c3c-a44b-22b6dd943eaf",},
  ],
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "",
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.5,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 9.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5dcd9271-cc30-458c-9bc3-067b3ea6bd86","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"10d55cd1-3da8-4ce3-bfc2-8c09e445908a","path":"sprites/spr_cast_eff/spr_cast_eff.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1611ec97-760e-4a9a-8ff0-77d2f98900fd","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f367ccd6-7eef-45b0-b8ac-48a67b0e051c","path":"sprites/spr_cast_eff/spr_cast_eff.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"990c898b-4d1a-4ae9-ba30-0b1afa331872","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2d899511-faff-437f-9e9c-687ac285c8a3","path":"sprites/spr_cast_eff/spr_cast_eff.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"454e1c94-7d56-47f7-9412-139239d33f46","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a19a69ed-591e-42c5-b034-7f8b02272a77","path":"sprites/spr_cast_eff/spr_cast_eff.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"acdd16c2-06ad-4ea9-8464-21f38727fe62","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9209b1ce-c235-4422-89de-eaeb0e20ce03","path":"sprites/spr_cast_eff/spr_cast_eff.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a3cb1808-13f9-435d-8bbb-4c5dfae1dac0","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a31b6a18-8f6a-4e22-800f-477fb6d80de9","path":"sprites/spr_cast_eff/spr_cast_eff.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f84a0a2b-f2d6-42f5-8240-85334d69a097","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49b7800f-0364-4151-900f-888d778c9041","path":"sprites/spr_cast_eff/spr_cast_eff.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"67d422d2-c8b3-4432-af00-83b6d3ee3bca","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"31db153f-ec1e-44d9-a186-0eb32dfde2ff","path":"sprites/spr_cast_eff/spr_cast_eff.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ba1ea1b5-94ff-402f-89e2-d03636d4b038","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"17591ccc-8649-4c3c-a44b-22b6dd943eaf","path":"sprites/spr_cast_eff/spr_cast_eff.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
  },
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"811bd3ef-55a4-4fad-896e-f327732b840c","visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "enemy",
    "path": "folders/Sprites/Effects/battle/enemy.yy",
  },
}