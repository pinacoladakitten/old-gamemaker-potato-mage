{
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_mag_deathspike02",
  "duration": 0.0,
  "parent": {
    "name": "magic",
    "path": "folders/Sounds/Potato/magic.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_mag_deathspike02",
  "tags": [],
  "resourceType": "GMSound",
}