{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.62,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_attack_firesp_spawnmin",
  "duration": 0.0,
  "parent": {
    "name": "FireSp",
    "path": "folders/Sounds/enm/FireSp.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_attack_firesp_spawnmin",
  "tags": [],
  "resourceType": "GMSound",
}