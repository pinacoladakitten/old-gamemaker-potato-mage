{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.5,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_water_sprout01",
  "duration": 0.0,
  "parent": {
    "name": "overworld_Misc",
    "path": "folders/Sounds/overworld_Misc.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_water_sprout01",
  "tags": [],
  "resourceType": "GMSound",
}