{
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "conversionMode": 0,
  "compression": 3,
  "volume": 1.0,
  "preload": true,
  "bitRate": 320,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "soundFile": "snd_overw_afternoon01",
  "duration": 0.0,
  "parent": {
    "name": "overworld",
    "path": "folders/Sounds/BG_MUSIC/overworld.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_overw_afternoon01",
  "tags": [],
  "resourceType": "GMSound",
}