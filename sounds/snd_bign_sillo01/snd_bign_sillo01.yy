{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.44,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_bign_sillo01",
  "duration": 0.0,
  "parent": {
    "name": "BigN",
    "path": "folders/Sounds/enm/BigN.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_bign_sillo01",
  "tags": [],
  "resourceType": "GMSound",
}