{
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.06,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_dodge_smoke",
  "duration": 0.0,
  "parent": {
    "name": "Potato",
    "path": "folders/Sounds/Potato.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_dodge_smoke",
  "tags": [],
  "resourceType": "GMSound",
}