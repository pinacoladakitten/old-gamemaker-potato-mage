{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "snd_bign_battle02",
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 320,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_bign_battle02",
  "duration": 136.98611,
  "parent": {
    "name": "boss",
    "path": "folders/Sounds/BG_MUSIC/battle/boss.yy",
  },
}