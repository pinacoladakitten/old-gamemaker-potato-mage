{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.6,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_step",
  "duration": 0.0,
  "parent": {
    "name": "overworld_Misc",
    "path": "folders/Sounds/overworld_Misc.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_step",
  "tags": [],
  "resourceType": "GMSound",
}