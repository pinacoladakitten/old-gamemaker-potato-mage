{
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "soundFile": "snd_boss_sol",
  "duration": 0.0,
  "parent": {
    "name": "boss",
    "path": "folders/Sounds/BG_MUSIC/battle/boss.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_boss_sol",
  "tags": [],
  "resourceType": "GMSound",
}