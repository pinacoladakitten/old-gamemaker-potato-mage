{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.23,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_attk_hit_p03",
  "duration": 0.0,
  "parent": {
    "name": "Potato",
    "path": "folders/Sounds/Potato.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_attk_hit_p03",
  "tags": [],
  "resourceType": "GMSound",
}