{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_attack_sword_p01",
  "duration": 0.0,
  "parent": {
    "name": "Potato",
    "path": "folders/Sounds/Potato.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_attack_sword_p01",
  "tags": [],
  "resourceType": "GMSound",
}