{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_darkness02",
  "duration": 0.0,
  "parent": {
    "name": "magic",
    "path": "folders/Sounds/Potato/magic.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_darkness02",
  "tags": [],
  "resourceType": "GMSound",
}