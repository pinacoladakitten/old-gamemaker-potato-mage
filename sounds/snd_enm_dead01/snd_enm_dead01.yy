{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.22,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_enm_dead01",
  "duration": 0.0,
  "parent": {
    "name": "general",
    "path": "folders/Sounds/enm/general.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_enm_dead01",
  "tags": [],
  "resourceType": "GMSound",
}