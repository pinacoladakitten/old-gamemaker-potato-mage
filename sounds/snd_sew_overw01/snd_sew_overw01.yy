{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "snd_sew_overw01",
  "conversionMode": 0,
  "compression": 3,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_sew_overw01",
  "duration": 320.1306,
  "parent": {
    "name": "overworld",
    "path": "folders/Sounds/BG_MUSIC/overworld.yy",
  },
}