{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "soundFile": "snd_grand_clock01",
  "duration": 0.0,
  "parent": {
    "name": "event",
    "path": "folders/Sounds/overworld_Misc/event.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_grand_clock01",
  "tags": [],
  "resourceType": "GMSound",
}