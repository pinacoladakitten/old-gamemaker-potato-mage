{
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_overw_ambience",
  "duration": 0.0,
  "parent": {
    "name": "overworld_Misc",
    "path": "folders/Sounds/overworld_Misc.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_overw_ambience",
  "tags": [],
  "resourceType": "GMSound",
}