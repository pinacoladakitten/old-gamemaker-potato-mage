{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_static_sound",
  "duration": 0.0,
  "parent": {
    "name": "BigN",
    "path": "folders/Sounds/enm/BigN.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_static_sound",
  "tags": [],
  "resourceType": "GMSound",
}