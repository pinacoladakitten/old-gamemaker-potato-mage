{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.55,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_attack_firesp01",
  "duration": 0.0,
  "parent": {
    "name": "FireSp",
    "path": "folders/Sounds/enm/FireSp.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_attack_firesp01",
  "tags": [],
  "resourceType": "GMSound",
}