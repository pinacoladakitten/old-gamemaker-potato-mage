{
  "audioGroupId": {
    "name": "sfx",
    "path": "audiogroups/sfx",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.44,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "soundFile": "snd_attack_sol03",
  "duration": 0.0,
  "parent": {
    "name": "Sol",
    "path": "folders/Sounds/enm/Sol.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_attack_sol03",
  "tags": [],
  "resourceType": "GMSound",
}