{
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "soundFile": "snd_BELFRY_title",
  "duration": 0.0,
  "parent": {
    "name": "misc_title_menu",
    "path": "folders/Sounds/BG_MUSIC/misc_title_menu.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_BELFRY_title",
  "tags": [],
  "resourceType": "GMSound",
}