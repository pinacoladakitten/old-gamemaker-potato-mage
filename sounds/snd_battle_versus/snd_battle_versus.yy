{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "snd_battle_versus",
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 192,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_battle_versus",
  "duration": 8.097959,
  "parent": {
    "name": "battle",
    "path": "folders/Sounds/BG_MUSIC/battle.yy",
  },
}