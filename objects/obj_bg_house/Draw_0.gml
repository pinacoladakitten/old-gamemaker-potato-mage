d3d_set_lighting(true); 
texture_set_repeat(true);
d3d_transform_add_scaling(3.5,3.5,3.5)
d3d_transform_add_rotation_z(90)
d3d_transform_add_translation(x,y,0)
d3d_model_draw(model0,0,0,0,background_get_texture(bg_house_brick));
d3d_set_lighting(false); 
d3d_model_draw(model1,0,0,0,background_get_texture(bg_house_light));
d3d_set_lighting(true); 
d3d_model_draw(model2,0,0,0,background_get_texture(bg_house_roof));
d3d_model_draw(model3,0,0,0,background_get_texture(bg_house_stone));
d3d_model_draw(model4,0,0,0,background_get_texture(bg_house_wood));
d3d_transform_set_identity();
d3d_set_lighting(false);

