/// @description Drawing
var ss,cc,tex;
tex = sprite_get_texture(spr_shadow,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);

d3d_set_lighting(false);
draw_set_alpha(alpha);
var tex03;
tex03 = sprite_get_texture(sprite_index,-1);
//Sphere 01
d3d_transform_add_rotation_y(r);
d3d_transform_add_rotation_z(0);
d3d_transform_add_scaling(xsc,ysc*0.25,zsc);
d3d_transform_add_translation(x,y,z);
d3d_draw_ellipsoid(0+12,0+12,12,0-12,0-12,-12,tex03,1,1,10);
d3d_transform_set_identity();
//Sphere 02
d3d_transform_add_scaling(xsc*0.9,ysc*0.25,zsc*0.9);
d3d_transform_add_rotation_y(r+90);
d3d_transform_add_rotation_x(90);
d3d_transform_add_translation(x,y,z);
d3d_draw_ellipsoid(0+12,0+12,12,0-12,0-12,-12,tex03,1,1,10);
d3d_transform_set_identity();
//Sphere 03
d3d_transform_add_scaling(xsc*0.8,ysc*0.25,zsc*0.8);
d3d_transform_add_rotation_y(r+45);
d3d_transform_add_rotation_x(45);
d3d_transform_add_translation(x,y,z);
d3d_draw_ellipsoid(0+12,0+12,12,0-12,0-12,-12,tex03,1,1,10);
d3d_transform_set_identity();

//Shadow
draw_set_alpha(alpha*0.25);
d3d_transform_add_scaling(xsc,ysc,zsc);
d3d_transform_add_translation(x,y,1);
d3d_draw_floor(0+10,0+10,0,0-10,0-10,0,tex,1,1);
d3d_transform_set_identity();
draw_set_alpha(1);
draw_set_colour(-1);

