/// @description Sword Slash Proj 02
spr = spr_bign_eff02_glow
image_index = 0
xsc = 1
ysc = 1
zsc = 1
yscmax = 1
z = obj_big_n.z
r = 0
hit = 0
hit2 = 0
alarm[0] = 4*room_speed
alarm[1] = 0.1*room_speed
alpha = 0
image_speed = 0.25
pspeed = irandom_range(0,0);
pspeedup = choose(0.05,0.1);
audio_play_sound(snd_bign_swordproj01,10,false);

