/// @description Main Step
if enm_pause = 0
if phase == 2
{
    time = min(time+0.3,timemax);
    if time >= timemax
    {
        alarm[0] = 1;
    }
}
if instance_exists(obj_target)
{
    obj_target.z = 5;
}

///Attack Commence
if attack_commence = 1
{
    //////Attack 1////////AOE Puddles
    if attk_numb = 1 or attk_numb = 7 or attk_numb = 8
    {
        if image_index = 0
        if attack_times = -1
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack_ini01
            {
                image_speed = 0.3
                sprite_index = spr_nephal_combat_attack_ini01
            }
            //set time, pause, attack times
            time = 0
            enm_pause = 1
            attack_times = irandom_range(1,2)
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack_ini01
        {
            enm_pause = 1
            if image_index >= 8
            {
                image_speed = 0
                alpha = max(alpha-0.05,0);
                //Create Sillouette
                if !instance_exists(obj_bign_sillo01)
                {instance_create(x-1,y,obj_bign_sillo01);}
            }
        }
        //after commencing
        if sprite_index = spr_nephal_combat_attack_ini01
        if alpha = 0
        {
            sprite_index = spr_nephal_combat_attack03
            image_index = 0
        }
        if sprite_index = spr_nephal_combat_attack03
        {
            enm_pause = 1
            if fade = 0
            {
                //movement in air
                alpha = min(alpha+0.1,1);
                y += sign(y-yorg)*0.1;
                z += 0.1
                //animation play
                if image_index < 4
                {image_speed = 0.1}
                //teleport to random local and pose
                if image_index = 0
                {
                    z = irandom_range(z+15,z+20);
                    y = irandom_range(y-50,y+50);
                    instance_create(x,y,obj_eff_enm_bign01_hard);
                    instance_create(x,y,obj_eff_enm);
                    obj_eff_enm.z = z
                    obj_eff_enm.spr = spr_bign_eff02
                    //Frame Advance
                    image_index = 1
                }
                //end of animation
                if image_index >= 4
                {
                    image_speed = 0
                    //Create Sillouette
                    instance_create(x-1,y,obj_bign_sillo01);
                    obj_bign_sillo01.z = z
                    fade = 1
                }
            }
            //end of attack
            if fade = 1
            {
                alpha = max(alpha-0.1,0);
                if alpha <= 0
                {
                    //Extra Attack
                    if attack_times = 2
                    {instance_create(obj_potato_combat.x,obj_potato_combat.y,obj_aoemark_bign02_hard);}
                    //Set Idle
                    sprite_index = spr_nephal_combat_idle01
                    image_index = 0
                }
            }
        }
    }
    ////////Attack 2//////////Sword Slash Spheres
    if attk_numb = 2 or attk_numb = 3
    {
        if image_index = 0
        if attack_times = -1
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack_ini01
            {
                image_speed = 0.4
                sprite_index = spr_nephal_combat_attack_ini01
            }
            //set time, pause, attack times
            time = 0
            enm_pause = 1
            attack_times = irandom_range(6,10)
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack_ini01
        {
            enm_pause = 1
            if image_index >= 8
            {
                image_speed = 0
                alpha = max(alpha-0.1,0);
                //Create Sillouette
                if !instance_exists(obj_bign_sillo01)
                {instance_create(x-1,y,obj_bign_sillo01);}
            }
        }
        //after commencing
        if sprite_index = spr_nephal_combat_attack_ini01
        if alpha = 0
        {
            sprite_index = spr_nephal_combat_attack03;
            image_index = 0;
        }
        if sprite_index = spr_nephal_combat_attack03
        {
            enm_pause = 1
            if fade = 0
            {
                //movement in air
                alpha = min(alpha+0.1,1);
                y += sign(y-yorg)*0.1;
                z += 0.1
                //animation play
                if image_index < 4
                {image_speed = 0.5}
                //teleport to random local and pose
                if image_index = 0
                {
                    z = irandom_range(z+20,z+30);
                    y = irandom_range(y-30,y+30);
                    instance_create(x,y,obj_eff_enm_bign02_hard);
                    instance_create(x,y,obj_eff_enm);
                    obj_eff_enm.z = z;
                    obj_eff_enm.spr = spr_bign_eff02;
                    //Frame Advance
                    image_index = 1;
                }
                //end of animation
                if image_index >= 4
                {
                    image_speed = 0;
                    //Create Sillouette
                    instance_create(x-1,y,obj_bign_sillo01);
                    obj_bign_sillo01.z = z;
                    attack_times -= 1;
                    fade = 1;
                }
            }
            //end of attack
            if fade = 1
            {
                alpha = max(alpha-0.05,0);
                if alpha <= 0
                {
                    if attack_times = 0
                    {
                        sprite_index = spr_nephal_combat_idle01;
                        image_index = 0;
                    }
                    else
                    {
                        sprite_index = spr_nephal_combat_attack03;
                        y = yorg;
                        z = zorg;
                        image_index = 0;
                        fade = 0;
                    }
                }
            }
        }
    }
    //////Attack 3////////Standard Instant Hit
    if attk_numb = 4 or attk_numb = 5
    {
        if image_index = 0
        if attack_times = -1
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack_ini01
            {
                image_speed = 0.3;
                sprite_index = spr_nephal_combat_attack_ini01;
                instance_create(x-1,y,obj_bign_sillo01);
            }
            //set time, pause, attack times
            time = 0
            enm_pause = 1
            attack_times = irandom_range(2,2)
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack_ini01
        {
            enm_pause = 1
            if image_index >= 8
            {
                image_speed = 0
                alpha = max(alpha-0.2,0);
                if alpha = 0
                {
                    if attack_times = 2{time = timemax-1}
                    //Create Sillouette
                    if !instance_exists(obj_bign_sillo01)
                    {instance_create(x-1,y,obj_bign_sillo01);}
                    instance_create(obj_potato_combat.x+1,obj_potato_combat.y,obj_eff_enm);
                    instance_create(obj_potato_combat.x,obj_potato_combat.y,obj_aoemark_bign02_hard);
                    obj_eff_enm.spr = spr_bign_eff04
                    sprite_index = spr_nephal_combat_idle01
                    image_index = 0
                }
            }
        }
    }
    //////Attack 4////////Fire Tempest
    if attk_numb = 6
    {
        if image_index = 0
        if attack_times = -1
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack01
            {
                image_speed = 0.3
                sprite_index = spr_nephal_combat_attack01
                instance_create(x-1,y,obj_bign_sillo01);
                instance_create(x,y,obj_eff_enm_bign03_hard);
            }
            //set time, pause, attack times
            time = 0
            enm_pause = 1
            attack_times = irandom_range(1,1)
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack01
        {
            enm_pause = 1
            if image_index >= 8
            {
                image_speed = 0
                obj_camera.cam_shake = 1
                obj_camera.cam_shakeleng = 0.5
                if !instance_exists(obj_eff_enm_bign03_hard)
                {
                    alpha = max(alpha-0.1,0);
                    if alpha = 0
                    {
                        instance_create(x,y,obj_bign_sillo01);
                        sprite_index = spr_nephal_combat_idle01
                        image_index = 0
                    }
                }
            }
        }
    }
    //////Attack 5////////IDK...
    if attk_numb = 7
    {
        if image_index = 0
        if attack_times = -1
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack02
            {
                image_speed = 0.3
                sprite_index = spr_nephal_combat_attack02
                instance_create(x-1,y,obj_bign_sillo01);
            }
            //set time, pause, attack times
            time = 0
            enm_pause = 1
            attack_times = irandom_range(1,1)
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack02
        {
            enm_pause = 1
            if image_index >= 3
            {
                if !instance_exists(obj_eff_enm_bign04)
                {instance_create(x+0,y+20,obj_eff_enm_bign04);}
                if !instance_exists(obj_bign_sillo01)
                {instance_create(x-1,y,obj_bign_sillo01);}
            }
            if image_index >= 6
            {
                image_speed = 0
                alpha = max(alpha-0.1,0);
                if alpha = 0
                {
                    //Create Sillouette
                    if !instance_exists(obj_bign_sillo01)
                    {instance_create(x-1,y,obj_bign_sillo01);}
                    sprite_index = spr_nephal_combat_idle01
                    image_index = 0
                }
            }
        }
    }
    ///End all attacks
    if sprite_index = spr_nephal_combat_idle01
    {
        alpha = min(alpha+0.1,1);
        y = yorg
        z = zorg
        fade = 0
        if alpha = 1
        {
            attk_numb = -1
            image_speed = 0.05
            enm_pause = 0
            attack_times = -1
            obj_camera.cam_shake = 0
            attack_commence = 0
        }
    }
}

///Dead
if hp < 1
{
    with obj_bign_sillo01_hard
    {fade = 1;}
    attack_commence = 0;
    obj_camera.cam_shake = 0;
    image_speed = 0.1;
    with obj_eff_enm
    {
        instance_destroy();
    }
    enm_pause = 1;
    with obj_target
    {
        s = 0
    }
    if !instance_exists(obj_bign_battle_end)
    {
        instance_create(0,0,obj_bign_battle_end);
        potato_exp += 5800;
        image_speed = 0.1;
        obj_potato_combat.pause_combat = 1;
        enm_pause = 1;
        ds_list_delete(enemies,self);
        enm_numb -= 1;
    }
    //Set Player to Max HP
    hp_potato = hp_potato_max;
}

///Reaction to Player Attacks
scr_Player_Attacks_ALL();

///Misc
//Cam Shake Check
if sprite_index = spr_nephal_combat_idle01
{
    if instance_exists(obj_camera)
    {
        obj_camera.cam_shake = 0;
    }
}
//Speed Up
//90%
if hp < maxhp*0.9
{timemax = 30;}
//80%
if hp < maxhp*0.8
{timemax = 20;}
//50%
if hp < maxhp*0.5
{timemax = 16;}

