/// @description Attack Initial
if enm_pause = 0
if phase = 2
{
    time = 0;
    enm_pause = 1;
    attk_numb = irandom_range(1,7);
    if hp > maxhp*0.9
    if attk_numb = 4
    {
        time = timemax
        enm_pause = 0
        alarm[0] = 1
        exit;
    }
    //Cast Effect
    instance_create(x+1,y,obj_eff_enm);
    var casteff = instance_find(obj_eff_enm,instance_number(obj_eff_enm)-1);
    //Effect Type
    if attk_numb = 1 or attk_numb = 6 
    or attk_numb = 7 or attk_numb = 8
    {casteff.spr = spr_cast_eff03}
    if attk_numb = 9
    {casteff.spr = spr_cast_eff01}
    if attk_numb = 4 or attk_numb = 5
    {casteff.spr = spr_cast_eff}
    image_index = 0;
    alarm[1] = 0.5*room_speed;
}

