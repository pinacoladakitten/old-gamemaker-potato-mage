/// @description Setup
image_speed = 0.05
draw_set_alpha_test(true)
draw_set_alpha_test_ref_value(40)
maxhp = 4850
hp = 4850
attack = 0
time = 0
timemax = 30
explode = 0
enm_pause = 0
attk_numb = 0
sprite_index = spr_nephal_combat_idle01
attack_times = -1
attack_commence = 0
enm_numb += 1;
ds_list_add(enemies, self);
z = 0
phase = 2
alpha = 1
fade = 0
if phase = 1
{
    alarm[2] = 2*room_speed
}
//Origin
xorg = x
yorg = y
zorg = z

///Misc
instance_create(x-3,y+10,obj_bign_sillo01_hard);

///Defensive Vars
//(0.1 = 10% dmg reduction, so on...)
physical_defense = 0.2;
fire_defense = 0.3;
nature_defense = -0.25;
magic_defense = 0.2;
lightning_defense = 0.2;
dark_defense = 0.5;
earth_defense = 0.2;

//Attack Scaling
instance_create(0, 0, obj_bign_attkStats);
