/// @description Movement & Alpha
//Movement
z += zspeed
z1 -= zspeed
z2 += zspeed
    if zcycle1 = 0
    {
        zspeed = min(zspeed+0.001,0.05)
    }
    if zcycle1 = 1
    {
        zspeed = max(zspeed-0.001,-0.05)
    }
    if z >= 5
    {
        zcycle1 = 1
    }
    if z <= -5
    {
        zcycle1 = 0
    }

