/// @description Main
//Countdown Event Timer
if (phoneRingTime <= phoneSpeak){
    phoneRingTime += 1;
}
//Event Trigger 00
if (phoneRingTime == phoneRing){
    if (!audio_is_playing(snd_telephoneRing)){
        audio_play_sound(snd_telephoneRing, 10, true);
    }
}
//Event Trigger 01
if (phoneRingTime == phonePickUp){
    if (audio_is_playing(snd_telephoneRing)){
        audio_stop_sound(snd_telephoneRing);
    }
    if (!audio_is_playing(snd_telephonePickup)){
        audio_play_sound(snd_telephonePickup, 10, false);
    }
}
if (phoneRingTime == phoneSpeak){
    //Create Text
    if (!instance_exists(obj_BELFRY_textbox)){
        instance_create(0, 0, obj_BELFRY_textbox);
    }dia_belfry_intro00();
}
dia_belfry_intro00();
//If Text End
if (instance_exists(obj_BELFRY_textbox)){
        if (obj_BELFRY_textbox.active == 0){
            if (!instance_exists(obj_trans_fade_in)){
                instance_create(0, 0, obj_trans_fade_in);
                obj_trans_fade_in.roomgoto = rm_BELFRY_intro02;
            }
        }
}

