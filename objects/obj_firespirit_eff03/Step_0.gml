/// @description Movement
r += 2.5
if xsc < 10
{
    xsc += 0.5
    ysc += 0.5
}
if move = 1
{
    zed = min(zed+(30-zed)*0.05,30)
    if zed >= 25
    {
        move = 0
        alarm[2] = 1*room_speed
    }
}
if move = 2
{
    y = max(y-(y-64)*0.02,64)
    if y <= 69
    {
        move = 0
        alarm[3] = 1*room_speed
    }
}
if move = 3
{
    zed = max(zed-(zed-0)*0.05,0)
    if obj_potato_overw.sprite_index = spr_potato_ow_carried
    {
        obj_potato_overw.image_speed = 0
        obj_potato_overw.image_index = 0
    }
    if zed <= 2
    {
        zed = 0
        move = 0
        alarm[0] = 2*room_speed
    }
}
if fade = 1
{
    alpha = max(alpha-0.01,0)
    if alpha = 0
    {
        obj_potato_overw.sprite_index = spr_potato_ow_idle_f
        instance_destroy();
    }
}

///Misc
obj_potato_overw.x = x
obj_potato_overw.y = y
obj_potato_overw.z = zed+5

