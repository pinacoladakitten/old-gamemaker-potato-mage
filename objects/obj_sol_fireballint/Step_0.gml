if draw = 1{r += 2.5}
if draw = 2{r += 0.5}
if (z > 0)
{
    z -= 5
    enm_pause = 1
    obj_potato_combat.pause_combat = 1
}
if (z = 10)
{
    audio_play_sound(snd_fire_intro,10,false)
}
if (z = 0)
{
    if instance_exists(obj_boss_sol)
    {
        alpha = max(alpha-0.005,0.2)
        xs += 0.25
        ys += 0.25
        xsc -= 0.25
        ysc -= 0.25
        xss += 0.5
        yss += 0.5
        zs -= 0.25
        obj_boss_sol.draw = 1
    }

}
if (xsc = 0)
{
    draw = 2
}
if draw = 2
{
    ze = min(ze+0.5,0)
}
//Destroy
if !instance_exists(obj_boss_sol)
{
    zz -= 1
}

