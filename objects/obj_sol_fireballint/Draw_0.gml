d3d_set_lighting(false);
var tex,tex01,tex02;
{
tex = sprite_get_texture(spr,-1);
tex01 = sprite_get_texture(spr_sol_fireint02,-1);
tex02 = sprite_get_texture(spr_sol_fireint04,-1);
}
draw_set_alpha(alpha);
d3d_transform_add_rotation_z(r)
d3d_transform_add_translation(x+15,y,zz)
if draw = 1
{
d3d_draw_cylinder(0+(xsc),0+(ysc),z+100,0-(xsc),0-(ysc),z,tex01,1,1,0,10)
d3d_draw_ellipsoid(0+(xs),0+(ys),z-7,0-(xs),0-(ys),z+(zs),tex,1,1,10)
}
else
{
d3d_draw_cylinder(0+(xss),0+(yss),z+(zs),0-(xss),0-(yss),z-7,tex,1,1,0,10)
}
if draw = 2
{
d3d_transform_add_translation(50,0,0)
d3d_draw_cylinder(150,150,ze+40,-150,-150,ze,tex,4,1,0,10)
d3d_transform_add_translation(25,0,0)
d3d_draw_cylinder(130,130,ze+65,-130,-130,ze+25,tex02,10,1,0,10)
d3d_draw_cylinder(125,125,ze+20,-125,-125,ze,tex,4,1,0,10)
d3d_draw_cylinder(100,100,ze+10,-100,-100,ze,tex,8,1,1,10)
}

d3d_transform_set_identity();
draw_set_alpha(1);

