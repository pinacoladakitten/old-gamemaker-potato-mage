/// @description Movement
xsc -= 0.25;
ysc -= 0.25;
r += 10;
z += 5;
if (image_index = 12 or xsc = 0)
{
    instance_destroy();
}

//Ember Effect
++embers;
if(embers >= 5){
    instance_create(x+2, y, obj_PotatoFireEmbers);
    embers = 0;
}

