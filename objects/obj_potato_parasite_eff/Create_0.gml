/// @description Setup
spr = sprite_index;
image_speed = 0.25;
xsc = random_range(0.5,1.3);
ysc = xsc;
zsc = xsc;
z = 0;
r = 0;
xdir = random_range(-0.5,0.5);
ydir = random_range(-0.5,0.5);
zdir = random_range(-0.5,0.5);
y += irandom_range(-20,20);
z += irandom_range(-20,20);
alpha = 1;

