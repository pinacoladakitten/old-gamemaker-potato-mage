if instance_exists(obj_camera){cam = obj_camera}
if instance_exists(obj_camera_ow){cam = obj_camera_ow}
d3d_set_lighting(true); 
ss = sin(cam.direction*pi/180);
cc = cos(cam.direction*pi/180);
d3d_transform_add_rotation_z(rot);
d3d_transform_add_scaling(scl,scl,scl);
d3d_transform_add_translation(x,y,0);
d3d_draw_wall(0+12*ss,0+12*cc,22,0-12*ss,0-12*cc,-2,sprite_get_texture(sprite_index,-1),1,1)
d3d_draw_wall(1,0+32*cc,0.01,1,0-32*cc,-3,sprite_get_texture(spr_shadow,0),1,1)
d3d_transform_set_identity();
d3d_set_lighting(false); 

