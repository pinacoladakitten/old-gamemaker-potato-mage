/// @description Setup
ymax = y+10;
ymin = y-10;
z = 0;
alpha = 0;
time = 0;
timemax = 35;
attack_commence = 0;
attk_numb = -1;
active = 0;
fade = 0;
sprite_index = spr_nephal_combat_idle01;
image_speed = 0.05;
if !audio_is_playing(snd_bign_sillo01)
{
    audio_play_sound(snd_bign_sillo01,10,false);
}
targetx = obj_potato_combat.x;
targety = obj_potato_combat.y;
alarm[2] = 10*room_speed;
alarm[0] = 48*room_speed;

