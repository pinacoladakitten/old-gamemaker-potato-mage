/// @description Main
if fade = 0
{alpha = min(alpha+0.05,0.55);}
if fade = 1
{
    alpha = max(alpha-0.05,0);
    if alpha <= 0
    {instance_destroy();}
}
//Attacks
if attack_commence = 1
{
    //Attack 1 Orbs
    if attk_numb = 1
    {
        image_speed = 0.25;
        if image_index < 8
        {
            sprite_index = spr_nephal_combat_attack_ini01;
            time = 0;
            with obj_eff_enm_bign04_hard
            {fade = 1;}
        }
        if image_index = 8
        {
            instance_create(670+irandom_range(-25,15),340+irandom_range(-60,30),obj_aoemark_bign03_hard);
            instance_create(670+irandom_range(-25,15),340+irandom_range(-60,30),obj_aoemark_bign03_hard);
            instance_create(670+irandom_range(-25,15),340+irandom_range(-60,30),obj_aoemark_bign03_hard);
            instance_create(670+irandom_range(-25,15),340+irandom_range(-60,30),obj_aoemark_bign03_hard);
            if obj_big_n_hard.hp < obj_big_n_hard.maxhp*0.5
            {
                var orbtarget = obj_potato_combat;
                instance_create(orbtarget.x,orbtarget.y,obj_aoemark_bign03_hard);
            }
            audio_play_sound(snd_bign_sword01,10,false);
            audio_sound_gain(snd_bign_sword01,0.25,0);
        }
        if image_index > 8
        {
            instance_create(x+1,y,obj_bign_sillo01);
            sprite_index = spr_nephal_combat_idle01;
            attack_commence = 0;
            attk_numb = 0;
            image_speed = 0.05;
        }
    }
    //Attack 2 Eldrich Blast
    if attk_numb = 2
    {
        if image_index = 0
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack02
            {
                image_speed = 0.3
                sprite_index = spr_nephal_combat_attack02
                instance_create(x-1,y,obj_bign_sillo01);
            }
            //set time, pause, attack times
            time = 0;
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack02
        {
            if image_index >= 3
            {
                if !instance_exists(obj_eff_enm_bign05_hard)
                {instance_create(x+0,y+20,obj_eff_enm_bign05_hard);}
                if !instance_exists(obj_bign_sillo01)
                {instance_create(x-1,y,obj_bign_sillo01);}
            }
            if image_index >= 6
            {
                image_speed = 0
                alpha = max(alpha-0.1,0);
                if alpha = 0
                {
                    //Create Sillouette
                    if !instance_exists(obj_bign_sillo01)
                    {instance_create(x-1,y,obj_bign_sillo01);}
                    instance_create(x+1,y,obj_bign_sillo01);
                    sprite_index = spr_nephal_combat_idle01;
                    attack_commence = 0;
                    attk_numb = 0;
                    image_speed = 0.05;
                }
            }
        }
    }
}

///Time
if active = 1
{
    time = min(time+0.03,timemax);
    if time = timemax
    if instance_number(obj_eff_enm) = 0
    {
        alarm[0] = 1;
    }
}
//Misc
if obj_big_n_hard.hp < obj_big_n_hard.maxhp*0.85
{
    timemax = 25;
}
if obj_big_n_hard.hp < obj_big_n_hard.maxhp*0.5
{
    timemax = 18;
}
if obj_big_n_hard.hp < obj_big_n_hard.maxhp*0.25
{
    timemax = 12;
}

