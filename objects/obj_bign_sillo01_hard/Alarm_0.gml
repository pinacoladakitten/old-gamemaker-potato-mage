/// @description Attack Initial
if fade = 0
{
    time = 0;
    //Attack Type
    if active = 1
    {attk_numb = irandom_range(1,2);}
    else{attk_numb = irandom_range(1,1);}
    //Cast Effect
    instance_create(x+1,y,obj_eff_enm);
    var casteff = instance_find(obj_eff_enm,instance_number(obj_eff_enm)-1);
    //Effect Type
    if attk_numb = 1
    {casteff.spr = spr_cast_eff03}
    if attk_numb = 2
    {casteff.spr = spr_cast_eff01}
    if !audio_is_playing(snd_bign_sillo01)
    {audio_play_sound(snd_bign_sillo01,10,false);}
    alarm[1] = 0.5*room_speed;
}
else{alarm[0] = 1;}

