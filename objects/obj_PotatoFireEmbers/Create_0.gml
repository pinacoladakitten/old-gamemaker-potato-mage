/// @description Setup
spr = sprite_index;
image_speed = 0.25;
image_index = irandom_range(0,9);
xsc = random_range(0.5,1);
ysc = xsc;
zsc = xsc;
z = 0;
r = 0;
y = irandom_range(y-1,y+1);
z = irandom_range(1,3);
xdir = random_range(-0.5,0.5);
ydir = random_range(-0.5,0.5);
zdir = random_range(0.5,0.75);
alpha = 1;

