/// @description Effects
if effect01 = 1
{
    xssc = min(xssc+0.000002,0.00015)
    yssc = max(yssc-0.000002,-0.00015)
    if xs < 10
    {
        xs += xssc
    }
    if ys > 0.44
    {
        ys += yssc
    }
}

