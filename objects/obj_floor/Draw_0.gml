d3d_set_lighting(true);
var tex,tex01;
tex = background_get_texture(bg)
tex01 = sprite_get_texture(spr,0)

if floor_bg = 0
{
    d3d_transform_add_rotation_z(xs*2)
    d3d_transform_add_translation(0,0,0)
    d3d_draw_ellipsoid(0,0,-10,room_width,room_height,0,tex,10,10,24)
    d3d_transform_set_identity();
}
if floor_bg = 1
{
    d3d_transform_add_rotation_z(xs*2)
    d3d_transform_add_translation(x-100,y-100,0)
    d3d_draw_cylinder(0,0,-5,200,200,-100,tex01,5,5,true,24)
    d3d_draw_ellipsoid(-10,-10,-10,210,210,0,tex01,5,5,24)
    d3d_transform_set_identity();
}

if ceiling = 1
{
d3d_draw_block(0,0,100,room_width,room_height,99,tex,10,10)
}
d3d_set_lighting(false);

