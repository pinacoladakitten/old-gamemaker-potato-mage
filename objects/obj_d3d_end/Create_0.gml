//End 3D mode
d3d_end(); 
d3d_set_hidden(false);
draw_set_alpha(1);
draw_set_color(c_white);
//Enable lighting
d3d_set_lighting(false); 
//Enable Fog
d3d_set_fog(false, c_black, 50, 500)
//Enable backface culling. This stops the renderer from drawing polygons that
//face away from the camera, speeding the render up.
d3d_set_culling(false); 

//Define and enable a global directional light.
d3d_light_define_direction(1,1,0.5,0,c_white);
d3d_light_enable(1,false);

