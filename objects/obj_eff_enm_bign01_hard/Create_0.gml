/// @description Sword Slash Proj 01 HARD
spr = spr_bign_eff03;
image_index = 0;
xsc = 1;
ysc = 1;
zsc = 1;
yscmax = 1;
z = obj_big_n_hard.z;
r = 0;
hit = 0;
hit2 = 0;
alarm[0] = 4*room_speed;
alarm[1] = 0.1*room_speed;
alarm[2] = 0.2*room_speed;
alpha = 0;
pspeed = choose(1.8,2);
audio_play_sound(snd_bign_swordproj02,10,false);
move_towards_point(obj_potato_combat.x,obj_potato_combat.y,pspeed);
