/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(ord("Z"))
    if string_length(strings[text]) < pos
    if !instance_exists(obj_trans_fade_in_out)
    if text != 11
        {
        pos = 0
        text = text+1
            if text = 5
            {
                instance_create(0,0,obj_trans_fade_in_out)
            }
            if text = 8
            {
                instance_create(0,0,obj_trans_fade_in_out)
            }
        }
        if (text = 11)
        {
            if !instance_exists(obj_intro_focus){instance_create(0,0,obj_intro_focus)}
        }
        if (text > maxstrings)
        {
            instance_destroy();
        }
    maxstrings = 13
}

///Events
if instance_exists(obj_trans_fade_in_out)
if obj_trans_fade_in_out.alpha = 1
{
    instance_create(0,0,obj_scene_picture);
    if text = 5
    {
    obj_scene_picture.bg = bg_intro_cutscene01
    }
    if text = 8
    {
    obj_scene_picture.bg = bg_intro_cutscene02
    }
    draw_scene = 1
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
audio_play_sound(snd_text_speech,10,false)
}

