/// @description Start up
pos = 1
text = 0
active = 1
draw_scene = 0
if instance_exists(obj_intro_soul)
{
    with obj_intro_soul
    {
    pause = 1
    }
}

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
...ah...yes...YES...much better!
"
strings[1] = @"???:
It isn't my forte to converse with someone so far beyond my reach. 
"
strings[2] = @"???:
But now...we are close...yes...there is nothing to fear anymore...I am not one to 
spoil my own words and seek avarice off of you any time I am able to...
"
strings[3] = @"???:
...no no no NO...I have lost all already...yes...just a husk of shadow wandering about 
this empty void of sadness and sorrow...
"
strings[4] = @"???:
...I am truly greatful to have stumbled upon another soul who's down here with me...
very much so...thank you...even if you have had no intention to see me...I am still
delighted...
"
strings[5] = @"???:
...now...you must be wondering...what this place is? Based on observation this place 
is indeed dark and full of loneliness...one should feel nauseous upon the thought of 
bewilderment in a place like this...
"
strings[6] = @"???:
...yes truly horrifying to any soul...myself included when I first became of what I am
at this very moment...
"
strings[7] = @"???:
...how have I conquered this monstrous situation you may ask? Well...that's a story 
for another time...what matters most is that now you won't have to suffer any longer
...I am here for you...
"
strings[8] = @"???:
... ... ...speaking of stories...I have a tale to comfort you...yes yes...to embark 
your mind on a journey...away from this dreadful place...
"
strings[9] = @"???:
... yes...a story, seriously...with all of the sorrow this present offers I want 
you to escape...yes...I know you just got here, but this will help...trust me...
"
strings[10] = @"???:
now...allow your mind to drift across the ocean that is your imagination...I really 
want you to become well immersed...Ok...
"
strings[11] = @"???:
concentrate...focus...
"
strings[12] = @"???:
...keep going...
"
strings[13] = @"???:
almost...there...
"

