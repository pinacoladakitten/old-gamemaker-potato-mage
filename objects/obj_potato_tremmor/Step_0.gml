/// @description Main Event
vsp -= grav;
z += (vsp/2);
rotx += rot_spd;
roty += rot_spd;
rotz += rot_spd;
//When hit ground
if z <= 15 and hit = 0{
    vsp = 5;
    rot_spd = 1;
    with obj_camera{cam_shake = 1;}
    xx = x;
    yy = y;
    hit = 1;
    //Audio Play
    audio_play_sound(snd_tremmor_summon02, 10, false);
}
if hit == 1{ //After hitting the ground
    image_xscale += 0.25; //Shockwave Scale
    image_yscale += 0.25; //^^^
    xsc = image_xscale; //Defining scale vars
    ysc = image_yscale; //Defining scale vars
    shockwave = true;
    alpha -= 0.01;
    y += 1; //Big Rock Y movement
    x -= 1; //Big Rock X movement
    y2 -= 1 //Small Rock Y movement
    x2 += 1 //Small Rock X movement
    y3 -= 0.6 //Small Rock Y movement 2
    x3 += 0.5 //Small Rock X movement 2
}
if z <= -30{
    with obj_camera{cam_shake = 0;}
    if alpha <= 0{
        instance_destroy();
    }
}

