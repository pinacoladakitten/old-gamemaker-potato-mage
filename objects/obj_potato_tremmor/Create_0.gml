/// @description Vars and Stuff
hit = 0;
z = 150;
grav = 0.25;
vsp = 0;
effect = 0;
on_hit = 0;
image_speed = 0.5;
rot_spd = irandom_range(1,5);
rotx = 0;
roty = 0;
rotz = 0;
alpha = 1;
shockwave = false;
xx = 0;
yy = 0;
xsc = 0;
ysc = 0;
zsc = 1;
image_xscale = 0;
image_yscale = 0;
//Create/Load Model
rock_spell01 = d3d_model_create();
d3d_model_load(rock_spell01, "spells_and_effects/tremmor/rock_spell01.d3d");
//List of Enemies hit
global.tremmor_targets_hit = ds_list_create();
//Audio play
audio_play_sound(snd_tremmor_summon01, 10, false);
//Misc Rock Vars
x2 = x+5;
y2 = y-5;
x3 = x-5;
y3 = y+5;

///Stats
potency = 1*potato_mag*0.3 + 1.4*potato_haste;

