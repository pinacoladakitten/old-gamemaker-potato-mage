/// @description Draw Everything
var ss, cc, tex, tex01;
d3d_set_lighting(true);
ss = sin(obj_camera.direction*pi/180); //Face the Camera if 2D
cc = cos(obj_camera.direction*pi/180); //Face the Camera if 2D
tex = background_get_texture(rock02); //Texture
tex01 = background_get_texture(sew_waterfall02); //Texture
//Transform Model
d3d_transform_add_scaling(1.25, 1.25, 1.25);
d3d_transform_add_rotation_x(rotx);
d3d_transform_add_rotation_y(roty);
d3d_transform_add_rotation_z(rotz);
d3d_transform_add_translation(x, y, z);
d3d_model_draw(rock_spell01, 0, 0, 0, tex);
d3d_transform_set_identity();
if shockwave == true{
    //Draw Shockwave
    d3d_set_lighting(false);
    draw_set_alpha(alpha);
    d3d_transform_add_scaling(xsc, ysc, zsc*0.5);
    d3d_transform_add_rotation_z(rotz*2);
    d3d_transform_add_translation(xx, yy, 0);
    d3d_draw_ellipsoid(0-32, 0-32, 0+8, 0+32, 0+32, 0, tex01, 1, 1, 12);
    d3d_draw_cylinder(0-32, 0-32, 0+12, 0+32, 0+32, 0, tex01, 5, 2, false, 12);
    d3d_transform_set_identity();
    draw_set_alpha(1);
    d3d_set_lighting(true);
    //Draw More Rocks
    d3d_transform_add_scaling(0.4, 0.4, 0.4);
    d3d_transform_add_rotation_x(rotx*-1);
    d3d_transform_add_rotation_y(roty*-1);
    d3d_transform_add_rotation_z(rotz*-1);
    d3d_transform_add_translation(x2, y2, z/2);
    d3d_model_draw(rock_spell01, 0, 0, 0, tex);
    d3d_transform_set_identity();
    //Draw More Rocks 2
    d3d_transform_add_scaling(0.2, 0.2, 0.2);
    d3d_transform_add_rotation_x(rotx*-1);
    d3d_transform_add_rotation_y(roty*-1);
    d3d_transform_add_rotation_z(rotz*-1);
    d3d_transform_add_translation(x3, y3, z/2);
    d3d_model_draw(rock_spell01, 0, 0, 0, tex);
    d3d_transform_set_identity();
}

