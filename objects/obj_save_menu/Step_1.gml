/// @description Saving/Loading
if keyboard_check_pressed(global.keyb_confirm)
or gamepad_button_check_pressed(0, global.gpi_confirm)
{
    //Selecting Files
    audio_play_sound(snd_select,10,false)
    //Loading
    if saving = 0
    if file_exists("savedata.sav")
    {
        ini_open("savedata.sav");
        if ini_section_exists("Save"+string(file))
        {
            scr_loadgame();
            instance_create(0,0,obj_trans_fade_in)
            obj_trans_fade_in.roomgoto = potato_room
        }
        ini_close();
    }
    //Saving
    if saving = 1
    {
        scr_savegame();
    }
}

