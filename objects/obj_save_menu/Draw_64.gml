/// @description Draw_sprites
draw_background_stretched(bg_pause,0,0,global.display_gui_width,global.display_gui_height);
draw_sprite(spr_file_select,0,640+xx,50+yy)
draw_sprite(spr_file_select,0,640+xx,266+yy)
draw_sprite(spr_file_select,0,640+xx,480+yy)
if i = 1
{
    draw_sprite(spr_inventory_select,0,896+xx,200+yy)
}
if i = 2
{
    draw_sprite(spr_inventory_select,0,896+xx,400+yy)
}
if i = 3
{
    draw_sprite(spr_inventory_select,0,896+xx,600+yy)
}

///Draw_progress
draw_set_font(font0)
draw_set_halign(fa_right)
if file_exists("savedata.sav")
{
ini_open("savedata.sav");
//File1---------------------
//Room Name
draw_text(840+xx,240+yy,string_hash_to_newline(ini_read_string("Save1","RoomName","NewGame")));
//Party Size
var party_size01 = ini_read_real("Save1","Party",0);
if party_size01 = 1 {draw_sprite(spr_potato_portrait_save,0,460+xx,150+yy);}
//File2---------------------
//Room Name
draw_text(840,455,string_hash_to_newline(ini_read_string("Save2","RoomName","NewGame")));
//Party Size
var party_size02 = ini_read_real("Save2","Party",0);
if party_size02 = 1 {draw_sprite(spr_potato_portrait_save,0,460+xx,370+yy);}
//File3---------------------
//Room Name
draw_text(840,665,string_hash_to_newline(ini_read_string("Save3","RoomName","NewGame")));
//Party Size
var party_size03 = ini_read_real("Save3","Party",0);
if party_size03 = 1 {draw_sprite(spr_potato_portrait_save,0,460+xx,580+yy);}
ini_close();
}
else
{
draw_text(840+xx,240+yy,string_hash_to_newline("NewGame"));
draw_text(840+xx,455+yy,string_hash_to_newline("NewGame"));
draw_text(840+xx,665+yy,string_hash_to_newline("NewGame"));
}

///Debug
if instance_exists(obj_global_overw)
{
    if obj_global_overw.draw_coords = 1
    {
        draw_set_font(font0)
        draw_set_halign(fa_left)
        draw_text(0,0,string_hash_to_newline("xx: " + string(xx) + "yy: " + string(yy)));
        yy -= keyboard_check(vk_up)*0.5;
        yy += keyboard_check(vk_down)*0.5;
        xx += keyboard_check(vk_right)*0.5;
        xx -= keyboard_check(vk_left)*0.5;
    }
}

