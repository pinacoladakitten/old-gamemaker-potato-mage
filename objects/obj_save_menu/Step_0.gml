/// @description Main Controls
//Exit
if keyboard_check_pressed(global.keyb_cancel)
or gamepad_button_check_pressed(0, global.gpi_cancel)
{
    if saving = 0{instance_create(992,768,obj_title)}
    if saving = 1{obj_save_station.activated = 1}
    instance_destroy();
    file = 1
}
//Moving
if keyboard_check_pressed(global.keyb_up)
or gamepad_button_check_pressed(0, global.gpi_up)
{
    i = max(i-1,1)
    file = max(file-1,1)
    audio_play_sound(snd_move_menu,10,false)
}
if keyboard_check_pressed(global.keyb_down)
or gamepad_button_check_pressed(0, global.gpi_down)
{
    i = min(i+1,3)
    file = min(file+1,3)
    audio_play_sound(snd_move_menu,10,false)
}

