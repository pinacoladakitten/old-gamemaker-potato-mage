/// @description Loading Model
obj_loading.image_index += 0.25
if modelload = 0
{
    d3d_model_load(model0_01, "maps/belfry/overworld1/black.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 1
{
    d3d_model_load(model1_01, "maps/belfry/overworld1/black_tile.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 2
{
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 3
{
    d3d_model_load(model3_01, "maps/belfry/overworld1/grass01.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 4
{
    d3d_model_load(model4_01, "maps/belfry/overworld1/grass02.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 5
{
    d3d_model_load(model5_01, "maps/belfry/overworld1/leaves01.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 6
{
    d3d_model_load(model6_01, "maps/belfry/overworld1/ribbons01.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 7
{
    d3d_model_load(model7_01, "maps/belfry/overworld1/ribbons011.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 8
{
    d3d_model_load(model8_01, "maps/belfry/overworld1/ribbons0111.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 9
{
    d3d_model_load(model9_01, "maps/belfry/overworld1/ribbons01111.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 10
{
    d3d_model_load(model10_01, "maps/belfry/overworld1/rock01b.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 11
{
    d3d_model_load(model11_01, "maps/belfry/overworld1/white.d3d");
    obj_loading.draw = 0
    //Event Setup
    if (room == rm_BELFRY_intro02){
        instance_create(0, 0, obj_trans_fade_in);
        obj_trans_fade_in.roomgoto = rm_belfry_intro01;
    }
}

