/// @description Draw Model
if room = rm_BELFRY_ow01
{
    d3d_set_culling(true);
    draw_set_alpha(1);
    d3d_set_lighting(true); 
    texture_set_repeat(true);
    d3d_transform_add_scaling(1,1,1);
    d3d_transform_add_rotation_z(-90);
    d3d_transform_add_translation(x,y,0);
	d3d_set_fog(true, c_black, 500, 4000);
    d3d_set_lighting(false);
    d3d_model_draw(model0_01,0,0,0,background_get_texture(belfy_black));
    d3d_set_lighting(false); 
    d3d_model_draw(model1_01,0,0,0,background_get_texture(belfy_black_tile));
    d3d_model_draw(model3_01,0,0,0,background_get_texture(belfy_grass01));
    d3d_model_draw(model4_01,0,0,0,background_get_texture(belfy_grass02));
    d3d_model_draw(model5_01,0,0,0,background_get_texture(belfy_leaves01));
    d3d_model_draw(model6_01,0,0,0,background_get_texture(belfy_ribbons01));
    d3d_model_draw(model7_01,0,0,0,background_get_texture(belfy_ribbons01));
    d3d_model_draw(model8_01,0,0,0,background_get_texture(belfy_ribbons01));
    d3d_model_draw(model9_01,0,0,0,background_get_texture(belfy_ribbons001));
    d3d_model_draw(model10_01,0,0,0,background_get_texture(belfy_rock01b));
    d3d_model_draw(model11_01,0,0,0,background_get_texture(belfy_white));
    d3d_set_lighting(false); 
    d3d_transform_set_identity();
}

