/// @description Main
var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);

///On Enemy
d3d_set_lighting(false);
if spr = spr_cast_eff
{
    d3d_draw_wall(x+12*ss,y+12*cc,z,x-12*ss,y-12*cc,z-24,tex,1,1)
}
if spr = spr_cast_eff01
{
    d3d_draw_wall(x+12*ss,y+12*cc,z,x-12*ss,y-12*cc,z-24,tex,1,1)
}
if spr = spr_cast_eff02
{
    d3d_draw_wall(x+12*ss,y+12*cc,z,x-12*ss,y-12*cc,z-24,tex,1,1)
}
if spr = spr_cast_eff03
{
    d3d_draw_wall(x+12*ss,y+12*cc,z,x-12*ss,y-12*cc,z-24,tex,1,1)
}
if spr = spr_enm_exp
{
    d3d_draw_wall(x+30*ss,y+30*cc,50,x-30*ss,y-30*cc,-10,tex,1,1)
}
d3d_set_lighting(true);

///Floaty Effects
if spr = spr_slash_fire
{
d3d_draw_wall(x+20*ss,y+20*cc,30,x-20*ss,y-20*cc,-10,tex,1,1)
}
if (spr = spr_dslash01 or spr = spr_dslash02)
{
d3d_draw_wall(x+16*ss,y+16*cc,22,x-16*ss,y-16*cc,-10,tex,1,1)
}
//Meta
if spr = spr_slash_fire02
{
d3d_draw_wall(x+20*ss,y+20*cc,30,x-20*ss,y-20*cc,-10,tex,1,1)
}
if (spr = spr_dslash03 or spr = spr_dslash04)
{
d3d_draw_wall(x+16*ss,y+16*cc,22,x-16*ss,y-16*cc,-10,tex,1,1)
}

///Blade Floaty Effects
if spr = spr_slash_orange
{
d3d_draw_wall(x+20*ss,y+20*cc,30,x-20*ss,y-20*cc,-10,tex,1,1)
}

///Boss: Sol Effects
if spr = spr_sol_attk_fire01
{
d3d_draw_wall(x+15*ss,y+15*cc,30,x-15*ss,y-15*cc,0,tex,1,1)
}
if spr = spr_sol_attk_fire02
{
d3d_draw_wall(x+15*ss,y+15*cc,28,x-15*ss,y-15*cc,-2,tex,1,1)
}

///Boss: Big N Effects
if spr = spr_bign_eff01
{
    d3d_draw_wall(x+25*ss,y+25*cc,40,x-25*ss,y-25*cc,-10,tex,1,1)
}
if spr = spr_bign_eff02
{
    d3d_draw_wall(x+25*ss,y+25*cc,z+40,x-25*ss,y-25*cc,z-10,tex,1,1)
}
if spr = spr_bign_eff04
{
    d3d_draw_wall(x+15*ss,y+15*cc,27,x-15*ss,y-15*cc,-3,tex,1,1)
}

