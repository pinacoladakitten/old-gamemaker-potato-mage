/// @description Misc
if instance_number(obj_eff_enm) > 1
{
instance_destroy();
}
//Enemy Pause
enm_pause = 1;
//Music
if enm_numb = 0
{
    if audio_is_playing(combat_music)
    {
        audio_sound_gain(combat_music,0,10*room_speed)
        audio_sound_gain(combat_music_pt2,0,10*room_speed)
    }
}

///Base Effects

//Attack Cast Effect
if spr = spr_cast_eff or spr = spr_cast_eff01 or spr = spr_cast_eff02 
or spr = spr_cast_eff03
{
    z += 1
    if image_index = 0
    {
        audio_play_sound(snd_enm_ready01,10,false);
    }
    if image_index = 9
    {
        instance_destroy();
    }
}
//Enemy Explode Effect
if spr = spr_enm_exp
{
    if image_index = 1
    {
        audio_play_sound(snd_enm_dead02,10,false)
    }
    if image_index = 16
        {
        if enm_numb = 0
            {
                if !instance_exists(obj_victory)
                {
                    instance_create(x,y,obj_victory);
                }
            }
        instance_destroy();
        }
}


///Floaty Attacks
//Attack 1
if spr = spr_slash_fire
if image_index > 1
if image_index < 4
if hit = 0
{
    if potato_dodge = 0
        {
            hp_potato -= 15
            obj_potato_combat.hit = 1
            hit = 1
        }
    if potato_dodge = 1
        {
            hp_potato -= 0
            if !instance_exists(obj_encourage_words)
            if image_index = 3
                {
                instance_create(x,y,obj_encourage_words)
                hit = 1
                }
        }
}

//Attack 2
if (spr = spr_dslash01 or spr = spr_dslash02)
if image_index > 1
if image_index < 3
if hit = 0
{
    if potato_dodge = 0
        {
            hp_potato -= 8
            obj_potato_combat.hit = 1
            hit = 1
        }
    if potato_dodge = 1
        {
            hp_potato -= 0
            if !instance_exists(obj_encourage_words)
            if image_index = 2
                    {
                    instance_create(x,y,obj_encourage_words)
                    hit = 1
                    }
        }
}
//Destroy
if spr = spr_slash_fire or spr = spr_dslash01 or spr = spr_dslash02
{
if image_index = 6 {instance_destroy();}
}

///Meta Floaty Attacks
//Attack 1
if spr = spr_slash_fire02
if image_index > 1
if image_index < 4
if hit = 0
{
    if potato_dodge = 0
        {
            hp_potato -= 45
            obj_potato_combat.hit = 1
            hit = 1
        }
    if potato_dodge = 1
        {
            hp_potato -= 0
            if !instance_exists(obj_encourage_words)
            if image_index = 3
                {
                    instance_create(x,y,obj_encourage_words)
                    hit = 1
                }
        }
}

//Attack 2
if (spr = spr_dslash03 or spr = spr_dslash04)
if image_index > 1
if image_index < 3
if hit = 0
{
    if potato_dodge = 0
        {
            hp_potato -= 20
            obj_potato_combat.hit = 1
            hit = 1
        }
    if potato_dodge = 1
        {
            hp_potato -= 0
            if !instance_exists(obj_encourage_words)
            if image_index = 2
                    {
                        instance_create(x,y,obj_encourage_words)
                        hit = 1
                    }
        }
}
//Destroy
if spr = spr_slash_fire02 or spr = spr_dslash03 or spr = spr_dslash04
{
    if image_index = 6 {instance_destroy();}
}

///Blade_Floaty Attacks

//Attack 1
if spr = spr_slash_orange
if image_index > 1
if image_index < 3
if hit = 0
{
    if potato_dodge = 0
        {
            hp_potato -= 18
            obj_potato_combat.hit = 1
            hit = 1
        }
    if potato_dodge = 1
        {
            hp_potato -= 0
            if !instance_exists(obj_encourage_words)
            if image_index = 2
                    {
                    instance_create(x,y,obj_encourage_words)
                    hit = 1
                    }
        }
}
//Destroy
if spr = spr_slash_orange
{
    if image_index = 4 {instance_destroy();}
}

///Boss: Sol_Attacks
image_speed = 0.25
//Attack 1
if spr = spr_sol_attk_fire01
if image_index > 2
if image_index < 4
if hit = 0
{
    if potato_dodge = 0
        {
            hp_potato -= 15
            obj_potato_combat.hit = 1
            hit = 1
        }
    if potato_dodge = 1
        {
            hp_potato -= 0
            if !instance_exists(obj_encourage_words)
            if image_index = 3
            {
                instance_create(x,y,obj_encourage_words)
                if instance_exists(obj_evt_tutorial_sol01)
                {
                    obj_evt_tutorial_sol01.alarm[1] = 1*room_speed
                }
                hit = 1
            }
        }
}
//Destroy
if spr = spr_sol_attk_fire01 
{
    if image_index = 7 
    {
    instance_destroy();
    }
}
//Attack 2
if spr = spr_sol_attk_fire02
{
    x = obj_potato_combat.x
    y = obj_potato_combat.y
    if image_index > 12
    if image_index < 14
    if hit = 0
    {
        if potato_dodge = 0
            {
                hp_potato -= 10
                obj_potato_combat.hit = 1
                hit = 1
            }
        if potato_dodge = 1
            {
            hp_potato -= 0
                if !instance_exists(obj_encourage_words)
                if image_index = 13
                {
                    instance_create(x,y,obj_encourage_words)
                    if instance_exists(obj_evt_tutorial_sol01)
                    {
                        obj_evt_tutorial_sol01.alarm[1] = 1*room_speed
                    }
                    hit = 1
                }
            }
    }
}
//Destroy
if spr = spr_sol_attk_fire02
{
    if image_index = 23 
    {
    instance_destroy();
    }
}
//Attack 3
if spr = spr_sol_attk_fire03
{
    move_towards_point(obj_potato_combat.x,obj_potato_combat.y,1)
    if image_index = 1 or image_index = 5 or image_index = 10
    {
        instance_create(x,y,obj_eff_enm_misc01)
    }
    if place_meeting(x,y,obj_potato_combat)
    if hit = 0
    {
        if potato_dodge = 0
            {
            hp_potato -= 10
            obj_potato_combat.hit = 1
            hit = 1
            }
        if potato_dodge = 1
            {
            hp_potato -= 0
            if !instance_exists(obj_encourage_words){instance_create(x,y,obj_encourage_words)}
            }
    }
}
//Destroy
if spr = spr_sol_attk_fire03
if x >= obj_potato_combat.x
{
instance_create(x,y,obj_eff_enm_misc01)
instance_destroy();
}

///Boss: Big N Attacks
//Attack 1
if spr = spr_bign_eff01
{
    image_speed = 0.6
    if image_index >= 13
    {
        instance_destroy();
    }
}
if spr = spr_bign_eff02
{
    image_speed = 0.45
    if image_index >= 8
    {
        instance_destroy();
    }
}
//Attack Melee
if spr = spr_bign_eff04
{
    if image_index > 1
    if image_index < 4
    if hit = 0
    {
        if potato_dodge = 0
        {
            //Norm Dmg
            hp_potato -= 15;
            //Hard Dmg
            if instance_exists(obj_big_n_hard){hp_potato -= 18;}
            //On Hit
            obj_potato_combat.hit = 1;
            hit = 1;
        }
        if potato_dodge = 1
        {
            hp_potato -= 0
            if !instance_exists(obj_encourage_words)
            if image_index >= 3
            {
                instance_create(x,y,obj_encourage_words)
                hit = 1
            }
        }
    }
    if spr = spr_bign_eff04
    {
        image_speed = 0.4
        if image_index >= 8
        {
            instance_destroy();
        }
    }
}

