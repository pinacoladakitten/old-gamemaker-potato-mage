/// @description Erase from list
with obj_combat_hud_all
{
    ds_list_delete(pbufftype, ds_list_find_index(pbufftype, 3));
    pbuffs -= 1;
}
with(obj_potato_bactions){
    timer_reduce -= 0.15;
}

