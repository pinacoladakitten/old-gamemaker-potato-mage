if abs(z-obj_potato_overw.z) <= 5
{
    audio_play_sound(snd_item_pickup01,10,false)
    with obj_inventory_ow
    {
    maxwep += 1
    inventorywep[maxwep-1] = 1 //Dagger
    }
    
    scr_DestroyUsedObjs();
    activee = 1
    //Create Prompt
    instance_create(0,0,obj_item_obtained)
    obj_item_obtained.item = spr_inv_dagger
    obj_item_obtained.fade = 0
    obj_item_obtained.alarm[0] = 3*room_speed
    //Destroy
    instance_destroy();
}

