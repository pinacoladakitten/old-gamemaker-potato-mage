/// @description Attack Start
if instance_exists(obj_evt_tutorial_sol01)
{
    //About to attack, then cancel
    if obj_evt_tutorial_sol01.text = 5
    {obj_evt_tutorial_sol01.active = 1}
    if obj_evt_tutorial_sol01.text = 15
    {obj_evt_tutorial_sol01.active = 1} 
    if obj_evt_tutorial_sol01.text = 23
    {obj_evt_tutorial_sol01.active = 1} 
    //Set Attack Tutorial
    if obj_evt_tutorial_sol01.active = 0
    {
        image_index = 0
        if attack_type = 1
        {spr = spr_sol_attack01}
        if attack_type = 2
        {spr = spr_sol_attack02}
        if attack_type = 3
        {spr = spr_sol_attack03}
        if attack_type = 4
        {spr = spr_sol_attack04}
    }
}
//Set Attack Normal
if !instance_exists(obj_evt_tutorial_sol01)
{
    image_index = 0
    if attack_type = 1
    {spr = spr_sol_attack01}
    if attack_type = 2
    {spr = spr_sol_attack02}
    if attack_type = 3
    {spr = spr_sol_attack03}
    if attack_type = 4
    {spr = spr_sol_attack04}
}

