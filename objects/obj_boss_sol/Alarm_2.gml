/// @description Dummy Destroy
with obj_enm_dummy01
{
    hp -= 9000
    image_index = 0
    spr = spr_enm_dummy01_hit
    instance_create(x+1,y,obj_dmg_numbers)
    instance_create(x+1,y,obj_eff)
    obj_dmg_numbers.dmg = 100
    with obj_eff
    {
        spr = spr_fire
    }
    with obj_dmg_numbers
    {
        potato = 1
    }
    if hp < 1
    {
        image_index = 0
    }
}

with obj_potato_combat {
	pause_combat = 0;
}