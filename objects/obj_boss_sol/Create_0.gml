/// @description Stats
maxhp = 750
hp = 750
fire_resist = 0.5
time = 0
timemax = 30

///Setup
image_speed = 0.125
draw_set_alpha_test(true)
draw_set_alpha_test_ref_value(40)
instance_create(x,y,obj_sol_fireballint)
draw = 0
attack = 0
explode = 0
enm_pause = 0
attk_numb = 0
spr = spr_sol_combat01
enm_numb += 1
attack_type = 1
attack_again = 0
//Scaling Values
xs = 1
ys = 1
z = 0
zs = 1
r = 0
ds_list_add(enemies, self)
alarm[2] = 2*room_speed

///Defensive Vars
//(0.1 = 10% dmg reduction, so on...)
physical_defense = 0;
fire_defense = 0.8;
nature_defense = 0;
magic_defense = 0.8;
lightning_defense = -0.2;
dark_defense = -0.3;
earth_defense = 0.2;

