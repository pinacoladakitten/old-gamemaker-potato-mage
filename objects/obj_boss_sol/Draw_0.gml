var ss,cc,tex;
{
tex = sprite_get_texture(spr,-1);
}
tex01 = sprite_get_texture(spr_shadow,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
if draw = 1
{
    d3d_transform_set_scaling(xs,ys,zs);
    d3d_transform_add_rotation_y(r);
    d3d_transform_add_translation(x,y,19);
    d3d_draw_wall(0+20*ss,0+20*cc,20,0-20*ss,0-20*cc,-20,tex,1,1);
    d3d_transform_set_identity();
    d3d_draw_wall(x+10*ss,y+10*cc,1.5,x-10*ss,y-10*cc,0,tex01,1,1);
}

