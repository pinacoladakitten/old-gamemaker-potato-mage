/// @description Attack Initial
if !instance_exists(obj_eff_enm)
if enm_pause = 0
{
    time = 0
    //Attack Type
    if !instance_exists(obj_evt_tutorial_sol01){
		attack_type = irandom_range(1,4)}
    //Create Cast Effect
    instance_create(x+1,y,obj_eff_enm)
    if attack_type = 1
    {
        with obj_eff_enm
        {spr = spr_cast_eff}
    }
    if attack_type = 2 or attack_type = 3
    {
        with obj_eff_enm
        {spr = spr_cast_eff01}
    }
    if attack_type = 4
    {
        with obj_eff_enm
        {spr = spr_cast_eff03}
    }
    //Attack Commence
    attk_numb = 1
    alarm[1] = 0.75*room_speed
    //Attack Choice
    if attack_type = 1
    {spr = spr_sol_combat01}
    if attack_type = 2
    {spr = spr_sol_combat01}
    if attack_type = 3
    {spr = spr_sol_combat01}
    if attack_type = 4
    {spr = spr_sol_combat01}
    enm_pause = 1
}

