/// @description Time
if enm_pause = 0
{
    time = min(time+0.45,timemax)
}

if time = timemax
if instance_number(obj_eff_enm) = 0
{
    alarm[0] = 1
}

///Attack Commence
//Attack 01
if spr = spr_sol_attack01
{
    image_speed = 0.25
    if image_index = 5
    {
        audio_play_sound(snd_attack_sol01,10,false)
        instance_create(obj_potato_combat.x+1,obj_potato_combat.y,obj_eff_enm)
        with obj_eff_enm
            {
            spr = spr_sol_attk_fire01
            }
    }
    if image_index = 14
    {
        spr = spr_sol_combat01
        image_speed = 0.125
        image_index = 0
        enm_pause = 0
        if !instance_exists(obj_evt_tutorial_sol01){attack_again = irandom_range(1,3)}
            if attack_again = 1
            {
            alarm[0] = 2*room_speed
            }
    }
}
//Attack 02
if spr = spr_sol_attack02
{
image_speed = 0.25
    if image_index = 0
    {
        audio_play_sound(snd_attack_sol02,10,false)
        instance_create(obj_potato_combat.x+1,obj_potato_combat.y,obj_eff_enm)
        with obj_eff_enm
            {
            spr = spr_sol_attk_fire02
            }
    }
    if image_index = 13
    {
        spr = spr_sol_combat01
        image_speed = 0.125
        image_index = 0
        enm_pause = 0
    }
}
//Attack 03
if spr = spr_sol_attack03
{
image_speed = 0.25
    if image_index = 9
    {
        audio_play_sound(snd_attack_sol01,10,false)
        instance_create(x,y,obj_eff_enm)
        with obj_eff_enm
            {
            spr = spr_sol_attk_fire03
            sprite_index = spr
            }
    }
    if image_index = 13
    {
        spr = spr_sol_combat01
        image_speed = 0.125
        image_index = 0
        enm_pause = 0
    }
}
//Attack 04
if spr = spr_sol_attack04
{
    image_speed = 0.25
    if image_index = 9
    {
        audio_play_sound(snd_attack_sol01,10,false)
        if instance_exists(obj_aoemark_sol01)
        {with(obj_aoemark_sol01){instance_destroy();}}
        if !instance_exists(obj_aoemark_sol01)
        {instance_create(obj_potato_combat.x,obj_potato_combat.y,obj_aoemark_sol01);}
        alarm[3] = 5*room_speed
    }
    if image_index = 13
    {
        spr = spr_sol_combat01
        image_speed = 0.125
        image_index = 0
        enm_pause = 1
    }
}

///Dead
if hp < 1
{
    with obj_eff_enm
    {instance_destroy();}
    spr = spr_sol_d01
    enm_pause = 1
    with obj_target
    {s = 0}
    if image_index = 1
    {audio_play_sound(snd_enm_dead01,10,false)}
    if image_index = 6
    {
        instance_create(0,0,obj_sol_battle_dia01);
        ds_list_delete(enemies,self)
        enm_numb -= 1
        potato_exp += 300
        image_index = 7
    }
    if image_index >= 7
    {
        image_speed = 0
    }
}

///Reaction to Player Attacks
scr_Player_Attacks_ALL();

///Misc
if instance_exists(obj_evt_tutorial_sol01)
{hp = maxhp}
//Speed Up
if hp < maxhp*0.5
{
    timemax = 25
}

