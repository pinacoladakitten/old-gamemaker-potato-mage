/// @description Dead
if (hp < 1){
    with (obj_eff_enm){
        instance_destroy();
    }
	
    if (spr != spr_enm_dummy01_d){
        spr = spr_enm_dummy01_d;
        image_index = 0;
    }
	
    enm_pause = 1;
    with (obj_target){s = 0;}
	
    if (image_index == 0){
        if !audio_is_playing(snd_enm_dead01){
            audio_play_sound(snd_enm_dead01,10,false);
        }
    }
    else if (image_index == 8){image_speed = 0;}
	
    xs = max(xs-0.010,0.15);
    ys = max(ys-0.010,0.15);
    zs = max(zs-0.010,0.15);
    r = min(r + 3, 90);
	
    if (xs <= 0.15){
		enm_pause = 0;
        ds_list_delete(enemies,self);
        enm_numb -= 1;
        exp_obtained += 50;
		if (!instance_exists(obj_victory) and enm_numb = 0){instance_create(x,y,obj_victory);}
        instance_destroy();
    }
}

///Reaction to Player Attacks
scr_Player_Attacks_ALL();
if (!place_empty(x, y) and spr != spr_enm_dummy01_hit){
    image_index = 0;
    spr = spr_enm_dummy01_hit;
}

///If hit by attack
if (image_index == 3 and spr == spr_enm_dummy01_hit){spr = spr_enm_dummy01;}

