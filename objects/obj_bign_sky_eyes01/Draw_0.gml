/// @description Draw
d3d_set_lighting(false);
var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
var tex03;
tex03 = sprite_get_texture(spr,-1);
//Model
draw_set_alpha(alpha);
d3d_transform_add_rotation_z(r);
d3d_transform_add_translation(x,y,z);
d3d_draw_wall(0+(xsc)*ss,0+(ysc)*cc,0+zsc,0-(xsc)*ss,0-(ysc)*cc,0-zsc,tex03,1,1);
d3d_transform_set_identity();
draw_set_alpha(1);

