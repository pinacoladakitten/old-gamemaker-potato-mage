/// @description Setup
image_speed = 0.05
draw_set_alpha_test(true)
draw_set_alpha_test_ref_value(40)
maxhp = 850
hp = 850
if room = rm_boss_firesp_prev_dream3D
{
    hp = 350
}
attack = 0
time = 0
timemax = 40
explode = 0
enm_pause = 0
attk_numb = 0
spr = spr_firesp_idle_battle01
enm_numb += 1
ds_list_add(enemies, id);
attack_times = -1
sneeze = 0
firewfire = 0

///Defensive Vars
//(0.1 = 10% dmg reduction, so on...)
physical_defense = 0.1;
fire_defense = 0.9;
nature_defense = -0.25;
magic_defense = 0;
lightning_defense = -0.1;
dark_defense = 0;
earth_defense = 0;

