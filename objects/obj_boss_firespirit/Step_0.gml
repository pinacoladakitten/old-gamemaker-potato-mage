/// @description Main Step
if enm_pause = 0
{
    time = min(time+0.17,timemax)


    if time = timemax
    if instance_number(obj_eff_enm) = 0
    {
        alarm[0] = 1;
    }
}
if instance_exists(obj_target)
{
    obj_target.z = 5
}

///Attack Commence
if spr = spr_firesp_attack01_battle01
if sneeze = 0
{
    //Attack 1
    if attk_numb = 1
    {
        image_speed = 0.1
        if image_index = 0
        if attack_times = -1
        {
            time = 0
            enm_pause = 1
            attack_times = irandom_range(2,4)
        }
        if image_index = 2
        {
            instance_create(obj_potato_combat.x,obj_potato_combat.y-50,obj_eff_enm_firesp01)
            attack_times -= 1
        }
        if image_index >= 4
        if attack_times = 0
        {
            spr = spr_firesp_idle_battle01
            image_speed = 0.05
            attack_times = -1
            enm_pause = 0
            attk_numb = 0
        }
    }
    //Attack 2
    if attk_numb = 2
    {
        image_speed = 0.1
        if image_index = 0
        if attack_times = -1
        {
            time = 0
            enm_pause = 1
            attack_times = irandom_range(2,3)
        }
        if image_index = 2
        {
            instance_create(x,y,obj_eff_enm_firesp02)
            attack_times -= 1
        }
        if image_index >= 4
        if attack_times = 0
        {
            spr = spr_firesp_idle_battle01
            image_speed = 0.05
            enm_pause = 0
            attk_numb = 0
            attack_times = -1
        }
    }
    //Attack 3
    if attk_numb = 3
    {
        image_speed = 0.1
        if image_index = 0
        if attack_times = -1
        {
            time = 0
            enm_pause = 1
            attack_times = irandom_range(1,1)
        }
        if image_index = 2
        {
            instance_create(obj_potato_combat.x,obj_potato_combat.y,obj_eff_enm_firesp03)
            attack_times -= 1
        }
        if image_index >= 4
        if attack_times = 0
        {
            spr = spr_firesp_idle_battle01
            image_speed = 0.05
            attack_times = -1
            enm_pause = 0
            attk_numb = 0
        }
    }
}

///Reaction to Player Attacks
scr_Player_Attacks_ALL();

///Dead
if hp < 1
if room = rm_boss_firesp_dream3D
{
    with obj_eff_enm
    {
        instance_destroy();
    }
    if spr != spr_firesp_d_battle01
    {
        spr = spr_firesp_d_battle01
        image_index = 0
    }
    enm_pause = 1
    with obj_target
    {
        s = 0
    }
    if image_index = 0
    {
        audio_play_sound(snd_enm_dead01,10,false)
        image_speed = 0.1
    }
    if image_index = 5
    {
        ds_list_delete(enemies,self)
        enm_numb -= 1
        instance_create(0,0,obj_boss_firesp_dia01);
    }
    if image_index = 6
    {
        image_speed = 0
        obj_potato_combat.pause_combat = 1
        enm_pause = 1
    }
}
if hp <= maxhp/1.5
if room = rm_boss_firesp_dream3D
{
    obj_floor.effect01 = 1
}

///Misc
if room = rm_boss_firesp_prev_dream3D
if sneeze = 0
{
    if hp <= 50
    {
        spr = spr_firesp_idle_battle01
        alarm[2] = 3*room_speed
        enm_pause = 1
        obj_potato_combat.pause_combat = 1
        sneeze = 1
    }
}
if room = rm_boss_firesp_prev_dream3D
if sneeze = 1
if spr = spr_firesp_attack01_battle01
{
    if image_index = 2
    {
        image_speed = 0
    }
}
if sneeze = 1
if instance_exists(obj_battle_textbox)
if !instance_exists(obj_trans_fade_in_dung)
{
    with obj_battle_textbox
    {
        battle_fade = 1
    }
}

