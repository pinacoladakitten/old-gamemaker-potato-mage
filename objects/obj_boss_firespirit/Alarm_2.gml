/// @description Sneeze
obj_potato_combat.no_death = 1
spr = spr_firesp_attack01_battle01
attk_numb = 0
pause_combat = 1
enm_pause = 1
instance_create(x,y,obj_eff_enm_firesp_sneezel);
with obj_eff_enm
{
    instance_destroy();
}

instance_create(0,0,obj_battle_textbox)
time = 0
//Battle Text
with obj_battle_textbox
{
maxstrings = 11
strings[0] = @"???:
AAACHHOOOOO!!!!
"
strings[1] = @"???:
Sniff...sniff...th-that smell!! I can't stand it anymore!
"
strings[2] = @"???:
It can't be! No NO NO!!!
"
strings[3] = @"???:
You wouldn't by chance have any...P-P-POTATOS!
"
strings[4] = @"Mythor:
ugh!...everything h-hurts...also...I'm actually a...potato-ugh!
"
strings[5] = @"???:
WHAT!? YOU!!? REALLY!? A POTATO!? OF ALL THINGS I COME ACROSS!
"
strings[6] = @"Mythor:
Is there a problem or something-
"
strings[7] = @"???:
I'M ALLERGIC TO POTATOS! SON OF A-...UGGGHHHHH!!!!
"
strings[8] = @"???:
YOU HAVE NO IDEA, I GET TERRIBLY ILL FROM THEM! THIS IS NOT A JOKE!
"
strings[9] = @"???:
HOW DID I NOT CATCH THIS! I'VE FELT OFF EVER SINCE I CAME CLOSE TO
YOU! IT WASN'T AS NOTICEABLE BEFORE, SO I SHRUGGED IT OFF!
"
strings[10] = @"???:
BUT IT'S GOTTEN ON MY NERVES SINCE MY SINUSES HAVE PROGRESSIVELY
WORSENED! I CAN BARELY BREATHE, WHY ME!
"
strings[11] = @"???:
THIS BATTLE IS OVER!
"
}

