/// @description Go to Room
room_persistent = false;
if !instance_exists(obj_trans_fade_in)
{
    instance_create(0,0,obj_trans_fade_in);
    obj_trans_fade_in.roomgoto = rm_overw_sewdung01_02;
    pause = 1;
    obj_potato_overw.hsp = 1.5;
    obj_potato_overw.sprite_index = spr_potato_ow_r;
    potato_x = 778;
    potato_y = 471;
    potato_z = 10;
}

