/// @description Potato Variables

//Stats
globalvar potato_vitality;
potato_vitality = 6;

globalvar potato_intellect;
potato_intellect = 8;

globalvar potato_dexterity;
potato_dexterity = 5;

globalvar potato_haste;
potato_haste = 2;

//Attack & Scales
globalvar potato_attk_bns;
potato_attk_bns = 3*potato_dexterity;

globalvar potato_mag_bns;
potato_mag_bns = 4*potato_intellect;

globalvar potato_attk_max;
potato_attk_max = 10;

globalvar potato_mag_max;
potato_mag_max = 10;

globalvar potato_attk;
potato_attk = potato_attk_max;

globalvar potato_mag;
potato_mag = potato_mag_max;

//Base Stuff & Scales
globalvar mp_potato_max;
mp_potato_max = 12*potato_intellect;

globalvar hp_potato_max;
hp_potato_max = 9*potato_vitality;

globalvar mp_potato;
mp_potato = mp_potato_max ;

globalvar hp_potato;
hp_potato = hp_potato_max;

globalvar potato_dodge;
potato_dodge = 0;

//Skill Trees
//Volatile
globalvar potato_fire_skill01;
potato_fire_skill01 = 0;
globalvar potato_fire_skill02;
potato_fire_skill02 = 0;
globalvar potato_fire_skill03;
potato_fire_skill03 = 0;
//Pure Potato
globalvar potato_pure_skill01;
potato_pure_skill01 = 0;
globalvar potato_pure_skill02;
potato_pure_skill02 = 0;
globalvar potato_pure_skill03;
potato_pure_skill03 = 0;
//Gloom
globalvar potato_gloom_skill01;
potato_gloom_skill01 = 0;
globalvar potato_gloom_skill02;
potato_gloom_skill02 = 0;
globalvar potato_gloom_skill03;
potato_gloom_skill03 = 0;

//Experience
globalvar potato_max_exp;
potato_max_exp = 100;

globalvar potato_exp;
potato_exp = 0

globalvar potato_skillp;
potato_skillp = 18;

globalvar potato_equip;
potato_equip = 0;

globalvar potato_combo;
potato_combo = 1

//Bonus Actions
//Actions Obtained
global.actions_potato = ds_list_create();
ds_list_add(global.actions_potato,"attack");
ds_list_add(global.actions_potato,"solanum");
ds_list_add(global.actions_potato,"ember");
ds_list_add(global.actions_potato,"tremmor");
ds_list_add(global.actions_potato,"parasite");
ds_list_add(global.actions_potato,"deathspike");
global.actions_pot_equip = ds_list_create();
//Actions Equipped to hotbar
global.actions_pot_equip = ds_list_create();
var b = 0;
for(b = 0 ; b <= 6 ; ++b){
    ds_list_add(global.actions_pot_equip, 0);
}
ds_list_replace(global.actions_pot_equip, 0, "attack");
ds_list_replace(global.actions_pot_equip, 1, "solanum");
ds_list_replace(global.actions_pot_equip, 2, "ember");

//Misc
global.wep_durability_max = 100;
global.wep_durability = global.wep_durability_max;

///~~Potato Ability Costs~~
//Spell Costs
//Ember
globalvar pot_fire_cost;
pot_fire_cost = 45;
//Solanum
globalvar pot_solanum_cost;
pot_solanum_cost = 80;
//Asterid
globalvar pot_asterid_cost;
pot_asterid_cost = 40;
//Tremmor
global.pot_tremmor_cost = 55;
//Parasite
global.pot_parasite_cost = 22;
//Deathspike
global.pot_deathspike_cost = 32;
//Attack
global.pot_attack_cost = 25;
//Rolling
global.pot_roll_cost = 6;

///Enemy Pause
globalvar enm_pause;
enm_pause = 0;

