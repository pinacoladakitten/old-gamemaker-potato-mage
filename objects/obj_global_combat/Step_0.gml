/// @description Potato Step Vars

//Attack Scaling
potato_attk_bns = 3*potato_dexterity * potato_combo;
potato_mag_bns = 4*potato_intellect * potato_combo;
potato_attk = potato_attk_max + potato_attk_bns;
potato_mag = potato_mag_max + potato_mag_bns;

//Base Stats Scaling
mp_potato_max = 2*potato_intellect + 80;
hp_potato_max = 7*potato_vitality + 3*potato_dexterity;

//Base Stats
potato_attk = round(potato_attk);
potato_mag = round(potato_mag);
hp_potato = round(hp_potato);

//Max Health
if hp_potato > hp_potato_max
{hp_potato = hp_potato_max;}
//Max Mana
if mp_potato > mp_potato_max
{mp_potato = mp_potato_max;}

//Combo
if potato_combo >= 2
{potato_combo = 2;}
//Exp Level Up
if !instance_exists(obj_victory)
if potato_exp >= potato_max_exp
{
    potato_exp -= potato_max_exp;
    potato_max_exp *= 1.5;
    potato_skillp += 1;
}

//Skills
//Volatile----------------------------
//Vol Skill 1
if potato_intellect >= 13
{potato_fire_skill01 = 1}
else
{potato_fire_skill01 = 0;}
//Vol Skill 2
if potato_intellect >= 18
{potato_fire_skill02 = 1}
else
{potato_fire_skill02 = 0;}
//Vol Skill 3
if potato_intellect >= 23
{potato_fire_skill03 = 1;}
else
{potato_fire_skill03 = 0;}
//Pure----------------------------
//Pure Skill 1
if potato_intellect >= 9
if potato_haste >= 6
{potato_pure_skill01 = 1;}
else
{potato_pure_skill01 = 0;}
//Pure Skill 2
if potato_intellect >= 10
if potato_haste >= 10
{potato_pure_skill02 = 1;}
else
{potato_pure_skill02 = 0;}
//Pure Skill 3 (Add Asterid)
if potato_intellect >= 12
if potato_haste >= 13{
    potato_pure_skill03 = 1;
    //Add to List of Actions
    if ds_list_find_index(global.actions_potato, "asterid") == -1{
        ds_list_add(global.actions_potato, "asterid");
        //Equip to List of Actions
        for(b = 0 ; b <= 6 ; ++b){
            if (ds_list_find_value(global.actions_pot_equip, b) = 0)
            if !ds_list_find_index(global.actions_pot_equip, "asterid"){
                ds_list_replace(global.actions_pot_equip, b, "asterid");
            }
        }
    }
    //Add to List of Known Spells
    if ds_list_find_index(inventorymagic, 3) == -1{
        obj_inventory_ow.maxmagic += 1
        ds_list_add(inventorymagic, 3);
    }
}
else{
    potato_pure_skill03 = 0;
    if ds_list_find_index(global.actions_potato, "asterid"){
        ds_list_delete(global.actions_potato, (ds_list_find_index(global.actions_potato, "asterid")));
        ds_list_replace(global.actions_pot_equip, (ds_list_find_index(global.actions_pot_equip, "asterid")), 0);
    }
    if ds_list_find_index(inventorymagic, 3){
        obj_inventory_ow.maxmagic -= 1;
        ds_list_delete(inventorymagic, (ds_list_find_index(inventorymagic, 3)));
    }
}

//Gloom----------------------------
//Glm Skill 1
if potato_intellect >= 9
if potato_dexterity >= 9
{potato_gloom_skill01 = 1;}
else
{potato_gloom_skill01 = 0;}
//Glm Skill 2
if potato_intellect >= 10
if potato_dexterity >= 13
{potato_gloom_skill02 = 1;}
else
{potato_gloom_skill02 = 0;}
//Glm Skill 3
if potato_intellect >= 12
if potato_dexterity >= 16
{potato_gloom_skill03 = 1;}
else
{potato_gloom_skill03 = 0;}
//Spell Effects----------------------------~
//Fire Skill 1
if potato_fire_skill01 = 1
{pot_fire_cost = 36;} //-20% Cost
else
{pot_fire_cost = 45;}
//Pure Skill 1
if potato_pure_skill01 = 1
{pot_solanum_cost = 72;} //-10% Cost
else
{pot_solanum_cost = 80;}

//Wep Durability Clamp
global.wep_durability = clamp(global.wep_durability, 0, global.wep_durability_max);

