/// @description Fireball 1
spr = spr_sol_attk_fire03
image_speed = 0.25
image_index = 0
audio_play_sound(snd_attack_sol03,10,false)
xsc = 5
ysc = 0
yscmax = 5
z = 0
zmax = 10
r = 0
hit = 0
hit2 = 0
alarm[0] = 4*room_speed
alarm[1] = 0.5*room_speed
pspeed = irandom_range(1,2)

