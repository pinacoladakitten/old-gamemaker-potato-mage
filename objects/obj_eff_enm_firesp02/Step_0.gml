/// @description Hitbox
if place_meeting(x,y,obj_potato_combat)
if hit = 0
{
    if potato_dodge = 0
        {
            hp_potato -= 18
            obj_potato_combat.hit = 1
            hit = 1
        }
    if potato_dodge = 1
    if hit2 = 0
        {
            hp_potato -= 0
            if place_meeting(x,y,obj_potato_combat){instance_create(x,y,obj_encourage_words)}
            hit = 1
            hit2 = 1
        }
}

///Movement
r += 10
if z < zmax
{
    z += 0.5
}
if ysc < yscmax
{
    ysc += 0.5
}
if x < obj_potato_combat.x
{
    move_towards_point(obj_potato_combat.x,obj_potato_combat.y,pspeed)
}

///Misc
d3d_set_lighting(true);
d3d_light_define_point(3,x,y,0,200,c_yellow)
d3d_light_enable(3,true);
d3d_set_lighting(false);

