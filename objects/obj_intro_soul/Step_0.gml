/// @description Movement
vsp = 0
hsp = 0
if pause = 0
{
    if keyboard_check(vk_right)
    {
    vsp = -3
    }
    if keyboard_check(vk_left)
    {
    vsp = 3
    }
    if keyboard_check(vk_up)
    {
    hsp = -3
    }
    if keyboard_check(vk_down)
    {
    hsp = 3
    }
}

///Collision
if (place_meeting(x+hsp,y,obj_col))
{
    while(!place_meeting(x+sign(hsp),y,obj_col))
    {
    x += sign(round(hsp));
    }
    hsp = 0;
}
x += hsp
//Vertical Collision
if (place_meeting(x,y+vsp,obj_col))
{
    while(!place_meeting(x,y+sign(vsp),obj_col))
    {
    y += sign(vsp);
    }
    vsp = 0;
}
y += vsp

