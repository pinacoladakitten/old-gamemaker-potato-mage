/// @description Start up
pos = 1;
text = 0;
active = 0;
port = spr_port;
vol = 0;
potato_port = spr_potato_port_f;
kass_port = spr_kaas_port_default;
potato_xsc = 1;
kass_xsc = -1;
port_alpha = 0;

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Mythor:
Huh, dead end. Was there supposed to be some kind of ladder?
"
strings[1] = @"Mythor:
Dunno how they all got up there, maybe they climbed the walls? Great, looks like I'm gonna
have to somehow climb my way up. Where do I even begin...
"
strings[2] = @"Mythor:
!!?
"
strings[3] = @"Mythor:
Woah! I guess what's going on is somehow affecting the water pressure down here. Hm, hey!
I could ride this current upward to get to where I need to go!
"
strings[4] = @"Mythor:
Hope this doesn't hurt too badly...
"

