/// @description Activate Water Sprout
with obj_water_sprout01
{
    active = 1;
    instance_create(-880,103,obj_water_vapor);
    instance_create(-880,103,obj_water_vapor);
    instance_create(-880,103,obj_water_vapor);
    instance_create(-880,103,obj_water_vapor);
    instance_create(-880,103,obj_water_vapor);
    instance_create(-880,103,obj_water_vapor);
}
instance_create(0,0,obj_trans_fade_in_out);
audio_play_sound(snd_water_sprout01,10,false);
audio_play_sound(snd_water_sprout02,10,false);
obj_trans_fade_in_out.color = c_silver;
obj_trans_fade_in_out.fadespd = 0.007;
alarm[0] = 7*room_speed;

