image_speed = 0.05
angle = 0
angle01 = 0
z = 0

///Start up
pos = 1
text = 0
active = 0
draw_scene = 0

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Kassadin:
Hey Myth! Make sure to use all of your skill points, and equip your weapon, yeah 
there are more enemies through that door for some reason.
"
strings[1] = @"Kassadin:
Just pick which one you wanna fight and you'll be on your way. Hope you have fun!
"
strings[2] = @"Kassadin:
Sadly you can't respec just yet, the developer of this game is quite lazy, but soon
they'll add the ability so you can test out each skill tree and stuff.
"
strings[3] = @"Kassadin:
Also I don't know if you've noticed, but there's an enemy stuck between the counter
and the wall...uh...um, feel free to test out stuff with it as well I guess. Dunno how 
it got there in the first place.
"

