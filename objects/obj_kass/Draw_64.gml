/// @description Main event
if active = 1
{
    if draw_scene = 0
    {
        draw_set_alpha(0.5);
        draw_rectangle_colour(0,0,global.display_gui_width,global.display_gui_height,c_black,c_black,c_black,c_black,0);
    }
    //Setup
    draw_set_font(font0);
    draw_set_halign(fa_left);
    draw_set_color(c_white);
    draw_set_alpha(1);
    //Textbox
    var width = global.display_gui_width;
    var height = global.display_gui_height;
    draw_sprite(spr_textbox,0,(width*0.5)-640,height*0.8);
    draw_sprite(spr_kaas_portrait,0,(width*0.5)-600,(height*0.8)+10);
    //The Text
    cstr = string_copy(strings[text],1,pos);
    draw_text_ext_transformed((width*0.5)-430,(height*0.8)+10,string_hash_to_newline(cstr),-1,-1,1,1,0);
    pos = min(pos+2,255);
    if keyboard_check(global.keyb_cancel)
    or gamepad_button_check(0, global.gpi_cancel)
    {
        pos = min(pos+5,255);
    }
}

