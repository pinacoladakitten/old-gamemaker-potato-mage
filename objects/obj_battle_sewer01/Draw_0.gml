/// @description Draw
if room = rm_battle_sewer3D
or room = rm_boss_gsquid_prev
{
    d3d_set_lighting(true); 
    texture_set_repeat(true);
    d3d_transform_add_scaling(1,1,1);
    d3d_transform_add_rotation_z(0);
    d3d_transform_add_translation(x+1649,y+23,-2);
    d3d_set_lighting(false); 
    d3d_model_draw(modelb00,0,0,0,background_get_texture(bg_castle01_light02));
    d3d_set_lighting(true); 
    d3d_model_draw(modelb01,0,0,0,background_get_texture(bg_castle01_nolight));
    d3d_model_draw(modelb02,0,0,0,background_get_texture(bg_water));
    d3d_model_draw(modelb03,0,0,0,background_get_texture(sew_black01));
    d3d_model_draw(modelb04,0,0,0,background_get_texture(sew_brick03));
    d3d_model_draw(modelb05,0,0,0,background_get_texture(sew_grate03));
    d3d_model_draw(modelb06,0,0,0,background_get_texture(sew_pipe01));
    d3d_model_draw(modelb07,0,0,0,background_get_texture(sew_pipe02));
    d3d_model_draw(modelb08,0,0,0,background_get_texture(sew_tile01));
    d3d_model_draw(modelb09,0,0,0,background_get_texture(sew_tile02));
    d3d_model_draw(modelb10,0,0,0,sprite_get_texture(sew_waterfall01,-1));
    d3d_transform_set_identity();
    //Fog and Other Settings
    d3d_set_fog(true, __background_get_colour( ), 1000, 4200);
}

