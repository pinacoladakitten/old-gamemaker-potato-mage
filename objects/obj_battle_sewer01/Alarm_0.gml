/// @description Loading
if modelload = 0
{
    d3d_model_load(modelb00, "maps/mp_sewdung01/battle/bg_castle01_light02.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 1
{
    d3d_model_load(modelb01, "maps/mp_sewdung01/battle/bg_castle01_nolight.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 2
{
    d3d_model_load(modelb02, "maps/mp_sewdung01/battle/bg_water.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 3
{
    d3d_model_load(modelb03, "maps/mp_sewdung01/battle/sew_black01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 4
{
    d3d_model_load(modelb04, "maps/mp_sewdung01/battle/sew_brick03.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 5
{
    d3d_model_load(modelb05, "maps/mp_sewdung01/battle/sew_grate03.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload =6
{
    d3d_model_load(modelb06, "maps/mp_sewdung01/battle/sew_pipe01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 7
{
    d3d_model_load(modelb07, "maps/mp_sewdung01/battle/sew_pipe02.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 8
{
    d3d_model_load(modelb08, "maps/mp_sewdung01/battle/sew_tile01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 9
{
    d3d_model_load(modelb09, "maps/mp_sewdung01/battle/sew_tile02.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 10
{
    d3d_model_load(modelb10, "maps/mp_sewdung01/battle/sew_waterfall01.d3d")
}

