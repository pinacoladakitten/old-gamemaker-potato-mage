/// @description Draw
d3d_set_lighting(true);
draw_set_alpha(alpha);
var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
d3d_transform_add_scaling(xs,ys,zs);
d3d_transform_add_translation(x,y,z);
d3d_draw_floor(0+5,0+5,0,0-5,0-5,0,tex,1,1);
d3d_transform_set_identity();
draw_set_alpha(1);
d3d_set_lighting(false);

