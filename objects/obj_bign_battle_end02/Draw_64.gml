/// @description Main event
//Setup
draw_set_font(font22);
draw_set_halign(fa_left);
draw_set_color(c_white);
draw_set_alpha(1);
//The Text
cstr = string_copy(strings[text],1,pos);
draw_text_ext_transformed(50,10,string_hash_to_newline(cstr),-1,-1,1,1,0);
pos = min(pos+1,string_length(strings[text]));

