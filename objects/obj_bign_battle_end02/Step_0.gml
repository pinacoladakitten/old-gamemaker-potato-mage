/// @description Text Control
if keyboard_check_pressed(global.keyb_confirm)
or gamepad_button_check_pressed(0, global.gpi_confirm)
if string_length(strings[text]) <= pos
if !instance_exists(obj_trans_fade_in_out)
{
    pos = 0
    text = text+1
    if (text > maxstrings)
    {
        pos = 0
        text = 0
        instance_destroy();
        instance_create(0,0,obj_trans_fade_in);
        obj_trans_fade_in.roomgoto = rm_thanks03
    }
}

///Controller Skip
if gamepad_button_check_pressed(0, global.gpi_menu)
{
    ///Skip
    if !instance_exists(obj_trans_fade_in)
    {
        instance_create(0,0,obj_trans_fade_in);
        obj_trans_fade_in.roomgoto = rm_thanks03
    }
}

