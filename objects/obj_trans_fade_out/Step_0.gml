/// @description Main Event: Fade Out
if active == true{
    alpha -= spd;
}
if alpha = 0
{instance_destroy();}
if music = 1 //Play Music and Resume
{
    pause = 0
    audio_stop_sound(combat_music);
    audio_resume_sound(overw_music);
    if audio_sound_get_gain(overw_music) = 0
    {
        audio_sound_gain(overw_music,0.65,5*room_speed);
    }
}
if music = 2 //Play Music
{
    if !audio_is_playing(overw_music)
    {
        audio_play_sound(overw_music,10,true);
        audio_sound_gain(overw_music,0.65,5*room_speed);
    }
}
if dead = 1 //Fade In After Death
{room_restart();
room_persistent = false;
dead = 0;}

