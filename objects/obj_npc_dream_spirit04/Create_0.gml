/// @description Vars
image_speed = 0.1
angle = 0
angle01 = 0
alpha = 0.75
port = spr_port
image_index = irandom_range(1,3)

///Start up
pos = 1
text = 0
active = 0
draw_scene = 0

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
Ugh...another one.
"
strings[1] = @"???:
Sorry, I'm just sick and tired of this. Anyways...hi, you probably
have no idea what this place is or how you got here...right?
"
strings[2] = @"Mythor:
Yea-...
"
strings[3] = @"???:
Well, as you can see-...wait...what am I seeing?
"
strings[4] = @"???:
What is this!? Oh my goodness!!
"
strings[5] = @"Mythor:
W-what's wrong?
"
strings[6] = @"???:
JUST WHO ARE YOU!? WHAT DO YOU WANT FROM ME!? NONONONO NO!
NOT AGAIN, NOT IN A MILLION YEARS!!!
"
strings[7] = @"???:
STAY BACK! JUST...GET AWAY FROM ME!!! I'M LOSING MY MIND OVER
THIS INSANITY!!
"
strings[8] = @"???:
EVERY DAY IT'S THE SAME ROUTINE OVER AND OVER AND OVER AGAIN
AGAIN AGAIN AGAIN!!! I CAN'T BEAR THIS ANY LONGER!!
"
strings[9] = @"Mythor:
........
"
strings[10] = @"Mythor:
...ok
"

