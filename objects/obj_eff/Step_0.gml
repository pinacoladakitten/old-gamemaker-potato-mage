/// @description Effect Types
//Hit
if spr = spr_eff_hit01
{
    y += dir
    z += dir2
    if image_index = 8
    {
        instance_destroy();
    }
}
//Doom Clouds
if spr = spr_doomcloud02
{
    y += dir*0.25
    z += dir2*0.25
    r += 2
    x += xdir
    xs -= 0.0075
}
//Fire
if spr = spr_fire
{
    image_speed = 0.125;
    //Ember Effect
    ++embers;
    if (embers >= 5){
        instance_create(x+2, y, obj_PotatoFireEmbers);
        embers = 0;
    }
}
//Sword Slash1
if spr = spr_slash01
{
    image_speed = 0.25
    if image_index = 6
        {
            instance_destroy();
        }
}
//Scaling
ys = xs
zs = xs
if xs <= 0
{
    instance_destroy();
}


