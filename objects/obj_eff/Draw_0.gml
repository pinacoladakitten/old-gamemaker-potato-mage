/// @description Drawing Effects
d3d_set_lighting(false);
var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);

//On Enemy
//Hit Effect
if spr = spr_eff_hit01
{
    d3d_draw_wall(x+5*ss,y+5*cc,z,x-5*ss,y-5*cc,z-10,tex,1,1);
}
if spr = spr_doomcloud02
{
    d3d_transform_add_rotation_x(r);
    d3d_transform_add_scaling(xs,ys,zs);
    d3d_transform_add_translation(x,y,z-8);
    d3d_draw_wall(0+5*ss,0+5*cc,0+5,0-5*ss,0-5*cc,0-5,tex,1,1);
    d3d_transform_set_identity();
}
//Fire Effect
if spr = spr_fire
{
    //Draw Effect
    d3d_transform_add_scaling(xs,ys,zs);
    d3d_transform_add_translation(x,y,0);
    d3d_draw_wall(0+10*ss,0+10*cc,0+20,0-10*ss,0-10*cc,0,tex,1,1);
    d3d_transform_set_identity();
    //Effect Stuff
    if image_index >= 17
    {
        fire_fade = 1
    }
    if fire_fade = 0
    {
        xs = min(xs+0.05,1)
        ys = min(ys+0.05,1)
        zs = min(zs+0.05,1)
    }
    else
    {
        xs = max(xs-0.05,0)
        ys = max(ys-0.05,0)
        zs = max(zs-0.05,0)
    }
    if fire_fade = 1
    if zs = 0
    {
        instance_destroy();
    }
}
//Slash Effect
if(spr = spr_slash01)
{
    d3d_draw_wall(x+20*ss,y+20*cc,30,x-20*ss,y-20*cc,-10,tex,1,1);
}
d3d_set_lighting(true);

