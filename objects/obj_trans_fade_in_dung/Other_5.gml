instance_destroy();
if audio_is_playing(snd_battle_victory02)
{
    audio_stop_sound(snd_battle_victory02);
}
if dead = 1
{
    if audio_is_playing(combat_music)
    {
        audio_stop_sound(combat_music);
    }
}

