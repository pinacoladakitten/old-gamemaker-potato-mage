if ruum = 0 {alpha += 0.01}
if ruum = 1 {alpha += 0.02}
if alpha > 1
{
with obj_trans_battle
    {
    instance_activate_all();
    instance_destroy();
    }
with obj_trans_battle_notpersist
    {
    instance_activate_all();
    instance_destroy();
    }
}
if alpha > 1
{
    with(obj_camera_ow){
        battle_zoom = 0;
        fov = 40;
    }
    if ruum = 0
    {
        room_goto(potato_room);
        instance_create(0,0,obj_trans_fade_out);
        with(obj_trans_fade_out){music = 1}; 
    }
    if ruum = 1
    {
        room_goto(potato_battle_room);
    }
}

