/// @description Create Leafs
leafSpawn += 1;
if(leafSpawn >= leafSpawnMax){
    xspwn = irandom_range(x-40, x+40);
    yspwn = irandom_range(y-40, y+40);
    zspwn = irandom_range(z, z+140);
    instance_create(xspwn, yspwn, obj_BELFRY_LeafEff);
    var targetpart = instance_find(obj_BELFRY_LeafEff, instance_number(obj_BELFRY_LeafEff)-1);
    targetpart.z = zspwn;
}

