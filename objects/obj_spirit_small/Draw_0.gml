/// @description Draw NPC
d3d_set_lighting(false);
var ss,cc,tex;
tex = sprite_get_texture(sprite_index,-1);
tex01 = sprite_get_texture(spr_ow_shadow,-1);
ss = sin(obj_camera_ow.direction*pi/180);
cc = cos(obj_camera_ow.direction*pi/180);

draw_set_alpha(alpha);
d3d_transform_add_scaling(scale,scale*0.9,scale);
d3d_transform_add_rotation_z(angle01);
d3d_transform_add_translation(x,y,z);
d3d_draw_wall(0+3*ss,0+3*cc,6,0-3*ss,0-3*cc,0,tex,1,1)
d3d_draw_wall(0+2*ss,0+2*cc,2,0-2*ss,0-2*cc,-1,tex01,1,1)
d3d_transform_set_identity();
draw_set_alpha(1);

