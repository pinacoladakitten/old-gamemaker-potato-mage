/// @description Setup
//Initialize
width = window_get_width();
height = window_get_height();
with obj_trans_fade_out{instance_destroy();}

//Variables
persistence = 1
battle_music = 1
pause = 1;
image_speed = 0.175
xx = 0
yy = 0
alpha = 1
rot = 0
size = 1
size0 = 1
gain = 0.55
room_persistent = true;
alarm[0] = 2;

