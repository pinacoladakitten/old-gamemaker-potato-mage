/// @description Draw Everything
var ss, cc, tex, tex01, tex02, tex03;
d3d_set_lighting(true);
tex = sprite_get_texture(spr_deathSpike, 0); //Texture
tex01 = sprite_get_texture(spr_deathspikeOrb, 0); //Texture
tex02 = sprite_get_texture(spr_deathspikeOrb, 0); //Texture
tex03 = sprite_get_texture(spr_deathspikeOrbGround, 0); //Texture
//Transform Model
draw_set_alpha(alpha);
d3d_transform_add_rotation_x(rotx);
d3d_transform_add_rotation_y(roty);
d3d_transform_add_rotation_z(rotz);
d3d_transform_add_scaling(xsc, ysc, zsc);
d3d_transform_add_translation(x, y, z);
d3d_model_draw(deathspike, 0, 0, 0, tex);
d3d_model_draw(deathspike01, 0, 0, 0, tex01);
d3d_transform_set_identity();
draw_set_alpha(1);

//Orb
if (deathOrbCreate == true){
    //Orb1
    draw_set_alpha(alpha);
    d3d_transform_add_rotation_x(deathOrbRotX);
    d3d_transform_add_rotation_y(deathOrbRotY);
    d3d_transform_add_rotation_z(deathOrbRotZ);
    d3d_transform_add_scaling(OrbXSC, OrbYSC, OrbZSC);
    d3d_transform_add_translation(x+2, y, z);
    d3d_draw_ellipsoid(0+15, 0+15, 0+15, 0-15, 0-15, 0-15, tex02, 2, 2, 18);
    d3d_transform_set_identity();
    draw_set_alpha(1);
    //Orb2 Ground
    draw_set_alpha(alpha);
    d3d_transform_add_rotation_z(deathOrbRotZ);
    d3d_transform_add_scaling(OrbXSC, OrbYSC, OrbZSC);
    d3d_transform_add_translation(x, y, z);
    d3d_draw_floor(0+35, 0+35, 0, 0-35, 0-35, 0, tex03, 1, 1);
    d3d_transform_set_identity();
}

