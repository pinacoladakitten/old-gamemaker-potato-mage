/// @description Main Event
if (z < 0){
    z = min(z + 7, 1);
}
else{
    timerfade += 1;
    if (timerfade >= timerfadeMax){
        ysc = max(ysc-0.1, 0);
    }
    else{
        ysc = min(ysc + 0.1, 2);
        zsc = max(zsc - 0.4, 2);
    }
    if (ysc <= 0) and (OrbYSC <= 0){
        instance_destroy();
    }
    //Create Orb
    deathOrbCreate = true;
    //Set Collision Mask
    if (mask_index != spr_deathSpike) and (hit == 0){
        mask_index = spr_deathSpike;
        if !(audio_is_playing(snd_mag_deathspike02)){
            audio_play_sound(snd_mag_deathspike02, 10, false);
        }
    }
}
//Orb
if (deathOrbCreate == true){
    //Check time to see when to fade (disappear)
    if (deathOrbFadeTime >= deathOrbFadeTime_max){
        deathOrbFade = true;
    }
    //Else, do its actions and stuff
    else{
        OrbYSC = min(OrbYSC+0.1, 1);
        deathOrbRotZ += 2;
        deathOrbRotY += 2;
        deathOrbRotX += 2;
        deathOrbFadeTime += 1;
    }
    //When fading away
    if (deathOrbFade == true){
        OrbYSC = max(OrbYSC - 0.1, 0);
        OrbXSC = max(OrbXSC - 0.1, 0);
        OrbZSC = max(OrbZSC - 0.1, 0);
    }
}
//Spawn Petal Eff
if (petalSpawn < petalSpawnMax){
    petalSpawn += 2;
}
else{
    instance_create(x, y, obj_potato_deathspikePetal);
}

