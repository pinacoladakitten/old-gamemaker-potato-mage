/// @description Vars and Stuff
//if hit
hit = 0;
//Z & other coords
z = -300;
xx = 0;
yy = 0;
//image speed
image_speed = 0.5;
//rotation
rotx = 0;
roty = 0;
rotz = 180;
//alpha
alpha = 1;
//scale
xsc = 2;
ysc = 1.5;
zsc = 4;
//timer
timerfade = 0;
timerfadeMax = 2*room_speed;
//Create/Load Model
deathspike = d3d_model_create();
deathspike01 = d3d_model_create();
d3d_model_load(deathspike, "spells_and_effects/deathSpike/deathSpike.d3d");
d3d_model_load(deathspike01, "spells_and_effects/deathSpike/deathSpike01.d3d");
//Audio play
audio_play_sound(snd_tremmor_summon01, 10, false);
//Orb
deathOrbCreate = false;
deathOrbRotZ = 0;
deathOrbRotY = 0;
deathOrbRotX = 0;
OrbXSC = 0.5;
OrbYSC = 0;
OrbZSC = 0.5;
deathOrbFade = false;
deathOrbFadeTime = 0;
deathOrbFadeTime_max = 2*room_speed;
//Spawn Petals
petalSpawn = 0;
petalSpawnMax = 0.01*room_speed;


///Stats
potency = 1*potato_mag*0.2 + 1.9*potato_dexterity;

