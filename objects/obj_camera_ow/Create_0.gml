/// @description Setup
//Coords
camx = 0; //Added x
camy= 0; // Added y
camz = -5; //Z
//Offset/Rot
camzoff = 2;
camf = 25;
xu = 0;
yu = 0;
zu = 1;
//Debug
cam_control = 0;
eff_alpha = 0;
hsp = 0;
vsp = 0;
//Misc
zoom = 150;
battle_zoom = 0;
//Looking to and from
camxto = -100;
camyto = y;
camzto = 20;
camzfrom = 34;
//Misc
cam_shake = 0
cam_stop = 0
cam_shakemax = 1;
//Start 3D mode
d3d_start(); 
d3d_set_hidden(true);
draw_set_alpha(1);
draw_set_color(c_white);
d3d_set_shading(true)
//Enable lighting
d3d_set_lighting(true);
//Enable Fog
d3d_set_fog(true, c_black, 50, 800)
//Enable backface culling. This stops the renderer from drawing polygons that
//face away from the camera, speeding the render up.
d3d_set_culling(false); 
//Define and enable a global directional light.
d3d_light_define_direction(1,1,0.5,-0.25,c_white);
d3d_light_enable(1,true);
d3d_light_enable(2,true);

///Cam Settings
//FOV
fov = 45;
//Resolution
r = 1920/1080;
//Misc
if instance_exists(obj_playerTarget)
{camz = obj_playerTarget.z-5;}

