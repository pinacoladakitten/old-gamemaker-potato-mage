/// @description Room_Settings
d3d_light_define_direction(1,1,0.5,-0.25,c_white);
d3d_light_enable(1,true);
if room = rm_overw_home01
{
    d3d_light_define_direction(1,1,0,1,c_orange);
    d3d_light_enable(1,true);
    zoom = 130
}
if room = rm_overw_town01
{
    d3d_light_define_direction(1,-0.5,0.25,1.25,c_orange);
    d3d_light_enable(1,true);
    d3d_light_define_direction(7,1,0.5,-0.25,c_white);
    d3d_light_enable(7,true);
    zoom = 130
}
if room = rm_overw_dream3D or room = room33
{
    d3d_light_define_direction(1,1,0,-0.25,c_white);
    d3d_light_enable(1,true);
    d3d_light_define_direction(7,1,0,0.25,c_navy);
    d3d_light_enable(7,true);
    zoom = 130
}
if room = rm_test_stage
{
    d3d_light_define_direction(1,0.25,0,-0.25,c_white);
    d3d_light_enable(1,true);
    zoom = 130
}
if room = rm_overw_sewdung01
{
    d3d_light_define_direction(1,0.1,0,0.1,c_orange);
    d3d_light_enable(1,true);
    d3d_light_define_direction(7,-0.1,0,-0.1,c_orange);
    d3d_light_enable(7,true);
    zoom = 130
}

