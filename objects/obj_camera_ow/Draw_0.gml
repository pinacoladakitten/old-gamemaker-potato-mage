/// @description *Video Settings*
r = global.display_width/global.display_height;
//Camera
d3d_set_projection_ext(x+camx+battle_zoom, y+camy, camzfrom+camz, camxto+camx+battle_zoom, camyto+camy, camzto+camz, xu, yu, zu, fov, r, 1, 32000);

/* */
///Fog
if room = rm_training_01{d3d_set_fog(true, c_black, 150, 1000)}
if room = rm_boss_sol{d3d_set_fog(true, c_black, 150, 1000)}
if room = rm_overw_home01{d3d_set_fog(false, c_black, -150, 800)}
if room = rm_overw_town01{d3d_set_fog(false, c_black, -150, 800)}
if room = rm_overw_dream_initial{d3d_set_fog(true, c_white, 1000, 3000)}
if room = rm_overw_dream3D or room = room33{d3d_set_fog(true, __background_get_colour( ), 0, 9200)}
if room = rm_test_stage{d3d_set_fog(false, __background_get_colour( ), 0, 9200)}
if room = rm_battle_dream3D{d3d_set_fog(true, __background_get_colour( ), 50, 800)}

/* */
/*  */
