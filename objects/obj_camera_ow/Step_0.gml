/// @description Movement
if (instance_exists(obj_playerTarget))
{
    if pause = 0
    {
        if (cam_stop = 0)
        {
            //vsp = obj_playerTarget.vsp
            x = obj_playerTarget.x+zoom
            y = obj_playerTarget.y
            //hsp = obj_playerTarget.hsp
            camxto = obj_playerTarget.x
            camyto = obj_playerTarget.y
            //Cam Z Axis
            if camz < obj_playerTarget.z-5
            {camz = min(camz+(obj_playerTarget.z+camzoff-camz)*0.05,obj_playerTarget.z-5)}
            if camz > obj_playerTarget.z-5
            {camz = max(camz-(camz-obj_playerTarget.z+camzoff)*0.05,obj_playerTarget.z-5)}
        }
    }
    if (pause = 1)
    {
        hsp = 0;
        vsp = 0;
    }
}

///Misc
//Screen Shake
if cam_shake = 1{
    camz = clamp(irandom_range(cam_shakemax-1,cam_shakemax+1),cam_shakemax-1,cam_shakemax+1);
}
//FOV scale
fov = fov - battle_zoom;
//Y Up
yu = max(yu-0.005,0);

if(instance_exists(obj_playerTarget)) return;
 //**Inputs Vertical**\\
key_up = -(keyboard_check(global.keyb_up) or (gamepad_axis_value(0, gp_axislv) < 0)); //Key Up
key_down = keyboard_check(global.keyb_down) or (gamepad_axis_value(0, gp_axislv) > 0); //Key Down
//**Inputs Horizontal**\\
key_left = keyboard_check(global.keyb_left) or (gamepad_axis_value(0, gp_axislh) < 0); //Keys Left & Right
key_right = -(keyboard_check(global.keyb_right) or (gamepad_axis_value(0, gp_axislh) > 0));

x += key_up + key_down;
y += key_left + key_right;