/// @description Destroy
alpha -= 0.02
x += xdir
y += ydir
z += zdir
xsc -= 0.01
ysc -= 0.01
if alpha <= 0 
{
    instance_destroy();
}

