/// @description Draw Stuff
var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);

d3d_set_lighting(false);
var tex03;
tex03 = sprite_get_texture(spr_mag_heal01,-1);
var tex04;
tex04 = sprite_get_texture(spr_mag_heal02,-1);
//Part 1
d3d_transform_add_rotation_z(r)
d3d_transform_add_translation(x,y,0)
draw_set_alpha(alpha)
d3d_draw_cylinder(10,10,z,-10,-10,0,tex04,2,1,0,10)
d3d_transform_set_identity();
draw_set_alpha(1)
//Part 2
d3d_transform_add_scaling(xsc,ysc,1);
d3d_transform_add_translation(x,y,0)
d3d_draw_cylinder(7,7,zz,-7,-7,0,tex03,1,1,0,18)
d3d_draw_cylinder(8,8,zz*0.2,-8,-8,0,tex03,1,1,0,18)
d3d_transform_set_identity();

