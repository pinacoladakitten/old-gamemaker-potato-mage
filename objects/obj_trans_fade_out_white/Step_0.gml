/// @description Main Event: Fade Out in White
alpha -= 0.01
if alpha = 0
{
    instance_destroy();
}
 
if music = 1
{
    if !audio_is_playing(overw_music)
    {
        audio_play_sound(overw_music,10,true);
    }
}

