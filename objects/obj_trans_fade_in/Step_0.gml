/// @description Main Event: Room Type and Death Fade In
if ruum != 2
{
    alpha += 0.01
}
else
{
    alpha += 0.005
}
if alpha > 1
{
    if dead = 1
    {
        hp_potato = hp_potato_max
        mp_potato = mp_potato_max
    }
    room_goto(roomgoto);
}

