/// @description Draw
d3d_set_lighting(true);
var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
tex01 = sprite_get_texture(spr_shadow,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
draw_set_alpha(alpha);
d3d_transform_add_rotation_z(rotZ);
d3d_transform_add_scaling(xsc,ysc,zsc);
d3d_transform_add_translation(x,y,z);
d3d_draw_wall(0+10*ss,0+10*cc,20,0-10*ss,0-10*cc,0,tex,1,1)
d3d_draw_wall(0+1,0+10*cc,1,0+1,0-10*cc,0,tex01,1,1)
d3d_transform_set_identity();
draw_set_alpha(1);

