/// @description Status Effects, Misc, & Passives
if (keyboard_check_pressed(global.keyb_die)){
	hp_potato = 0;
}
sprite_index = spr;
//Victory
if spr = spr_potato_victory01
{
    image_speed = 0.2;
    if image_index >= 13
    {
        image_speed = 0;
    }
}
//Statuses
//Willpower Regen
if wp_regen_pause == false{
        mp_potato = min(mp_potato + (1 + potato_haste * 0.02) * 0.2, mp_potato_max); //If not moving
        //mp_potato = min((mp_potato + (1 + potato_haste * 0.02) * 0.1), mp_potato_max); //If moving (removed)
}
//Willpower Difference Bar Value
if willpower_diff > mp_potato{--willpower_diff;}
else{willpower_diff = mp_potato;}
//Health Difference Bar Value
if health_diff > hp_potato{--health_diff;}
else{health_diff = hp_potato;}


//If Target is left (MAYBEEEEE...)
// (moving == 0) and (obj_target.y > y) and (spr != spr_potato_roll02){
            //ysc = -1;
    //}
    //else{
       // ysc = 1;
    //}
//}

///MAIN Attack Actions 
//Basic Attack
if action_attk = 0
{
    //Stop Moving & Cancel any actions
    moving = 0;
    magictype = 0;
    attack_type01 = 0;
    //Set Move speed to 0
    hsp = 0;
    vsp = 0;
    //Frame Data
    if image_index = 5
    {
        audio_play_sound(snd_attack_p01,10,false);
        instance_create(x,y,obj_potato_poj);
    }
    if image_index > 5
    {
        //Animation Cancel
        if (keyboard_check_pressed(global.keyb_action01) 
        or gamepad_button_check_pressed(0, global.gpi_action01))
        {spr = spr_potato_combat01;}
    }
    if image_index >= 14
    {
        image_speed = 0.125;
        image_index = 0;
        spr = spr_potato_combat01;
        action_attk = -1;
    }
}
//Magic: Solanum
if action_attk = 1
{
    //Stop Moving & Cancel any actions
    moving = 0;
    magictype = 0;
    attack_type01 = 0;
    //Set Move speed to 0
    hsp = 0;
    vsp = 0;
    //Potato Throw
    if spr = spr_potato_magic01
    {
        if image_index = 8
        {
            audio_play_sound(snd_attack_p01,10,false);
            instance_create(x,y,obj_potato_heal);
        }
        
        if image_index = 9
        {
            spr = spr_potato_magic01pt2;
            image_index = 0;
        }
    }
    
    if spr = spr_potato_magic01pt2
    {
        if image_index = 9
        {
            action_attk = -1;
            spr = spr_potato_combat01;
            image_speed = 0.125;
        }
    }
}
//Magic: Ember
if action_attk = 2
{
    //Stop Moving & Cancel any actions
    moving = 0;
    magictype = 0;
    attack_type01 = 0;
    //Set Move speed to 0
    hsp = 0;
    vsp = 0;
    //Potato Throw
    if spr = spr_potato_magic01
    {
        if image_index = 8{
            audio_play_sound(snd_mag_attk_p01,10,false);
            instance_create(x,y,obj_potatofire_proj);
        }
        if image_index = 9{
            spr = spr_potato_magic01pt2;
            image_index = 0;
        }
    }
    
    if spr = spr_potato_magic01pt2
    {
        //Animation Cancel
        if (keyboard_check_pressed(global.keyb_action01) 
        or gamepad_button_check_pressed(0, global.gpi_action01))
        {spr = spr_potato_combat01;}
        if image_index = 9
        {
            action_attk = -1;
            spr = spr_potato_combat01;
            image_speed = 0.125;
        }
    }
}
//Magic: Asterid
if action_attk = 3
{
    //Stop Moving & Cancel any actions
    moving = 0;
    magictype = 0;
    attack_type01 = 0;
    //Set Move speed to 0
    hsp = 0;
    vsp = 0;
    //Frame Data
    if image_index = 5
    {
        audio_play_sound(snd_attack_p01,10,false);
        instance_create(x,y,obj_potato_asterid);
    }
    if image_index > 5
    {
        //Animation Cancel
        if (keyboard_check_pressed(global.keyb_action01) 
        or gamepad_button_check_pressed(0, global.gpi_action01))
        {spr = spr_potato_combat01;}
    }
    if image_index >= 14
    {
        image_speed = 0.125;
        image_index = 0;
        spr = spr_potato_combat01;
        action_attk = -1;
    }
}
//Magic: Tremmor
if action_attk = 4
{
    //Stop Moving & Cancel any actions
    moving = 0;
    magictype = 0;
    attack_type01 = 0;
    //Set Move speed to 0
    hsp = 0;
    vsp = 0;
    //Potato Throw
    if spr = spr_potato_magic01
    {
        if image_index = 8{
            audio_play_sound(snd_mag_attk_p01,10,false);
            instance_create(x-50, y-20, obj_potato_tremmor);
        }
        if image_index = 9{
            spr = spr_potato_magic01pt2;
            image_index = 0;
        }
    }
    if spr = spr_potato_magic01pt2
    {
        //Animation Cancel
        if (keyboard_check_pressed(global.keyb_action01) 
        or gamepad_button_check_pressed(0, global.gpi_action01))
        {spr = spr_potato_combat01;}
        if image_index = 9
        {
            action_attk = -1;
            spr = spr_potato_combat01;
            image_speed = 0.125;
        }
    }
}
//Magic: Parasite
if action_attk = 5
{
    //Stop Moving & Cancel any actions
    moving = 0;
    magictype = 0;
    attack_type01 = 0;
    //Set Move speed to 0
    hsp = 0;
    vsp = 0;
    //Frame Data
    if image_index = 5
    {
        audio_play_sound(snd_attack_p01, 10, false);
        if instance_exists(obj_target){
            instance_create(obj_target.x+8, obj_target.y, obj_potato_parasite);
        }
    }
    if image_index > 5
    {
        //Animation Cancel
        if (keyboard_check_pressed(global.keyb_action01) 
        or gamepad_button_check_pressed(0, global.gpi_action01))
        {spr = spr_potato_combat01;}
    }
    if image_index >= 14
    {
        image_speed = 0.125;
        image_index = 0;
        spr = spr_potato_combat01;
        action_attk = -1;
    }
}
//Magic: Death Spike
if action_attk = 6
{
    //Stop Moving & Cancel any actions
    moving = 0;
    magictype = 0;
    attack_type01 = 0;
    //Set Move speed to 0
    hsp = 0;
    vsp = 0;
    //Anime Pt1
    if (spr = spr_potato_magic01)
    {
        //Frame Data
        if image_index = 5
        {
            audio_play_sound(snd_attack_p01, 10, false);
            if instance_exists(obj_target){
                //Create Object
                var deathtarget = instance_create(obj_target.x+1, obj_target.y, obj_potato_deathspike);
                //Add Attack Bonus
                if (deathSpikeAttkBns == true){
                    deathtarget.potency *= 2.5;
                    deathSpikeAttkBns = false;
                }
            }
        }
        if (image_index = 9){
                spr = spr_potato_magic01pt2;
                image_index = 0;
            }
    }
    //Anime Pt2
    if (spr == spr_potato_magic01pt2){
        if image_index = 9
            {
                action_attk = -1;
                spr = spr_potato_combat01;
                image_speed = 0.125;
            }
    }
    //Rolling Cancel
    if ((image_index > 6) or (spr == spr_potato_magic01pt2))
    {
        //Animation Cancel
        if (keyboard_check_pressed(global.keyb_action01) 
        or gamepad_button_check_pressed(0, global.gpi_action01))
        {spr = spr_potato_combat01;}
        
        //Rolling Cartwheel
        if (keyboard_check(global.keyb_combat_moveL) or (gamepad_axis_value(0, gp_axislh) < 0) or
        keyboard_check(global.keyb_combat_moveR) or (gamepad_axis_value(0, gp_axislh) > 0)){
            spr = spr_potato_roll02;
            action_attk = -1;
            image_index = 0;
        }
    }
}
//Checks for attack end
if spr = spr_potato_combat01
{action_attk = -1;
image_speed = 0.125;}

///Dodging
if (keyboard_check_pressed(global.keyb_action01) 
or gamepad_button_check_pressed(0, global.gpi_action01))
{
    if (spr = spr_potato_combat01 or spr = spr_potato_run_toward_l 
    or spr = spr_potato_run_toward_r)
    {
        if mp_potato >= global.pot_roll_cost
        if !(spr = spr_potato_roll01)
        if pause_combat = 0
        {
            //Rolling Cost
            mp_potato -= global.pot_roll_cost;
            //Stop Movement
            moving = 0;
            //Set Image Speed and frame
            image_speed = 0.2;
            image_index = 0;
            //Play Sound
            audio_play_sound(snd_dodge_p01,10,false);
            //Set Sprite
            spr = spr_potato_roll01;
            //Set WP Regen Timer
            wp_regen_pause = true;
            alarm[0] = 0.5*room_speed;
            //Set Dodge Status
            dodging = true;
        }
    }
}
//Idle Dodge
if (spr = spr_potato_roll01)
{
    hsp = 0;
    vsp = 0;
    if image_index > 1
    if image_index < 7
    {potato_dodge = 1;}
    else
    {potato_dodge = 0;}
    if image_index >= 9
    {
        image_index = 0;
        spr = spr_potato_combat01;
        image_speed = 0.125;
        moving = 0;
        image_index = 0;
        time_pause = 0;
        dodging = false;
    }
}
//Moving Cartwheel Dodge
if (spr = spr_potato_roll02)
{
    //at image index 0
    if (image_index == 0){
        //if dodge sound is not playing
        if !audio_is_playing(snd_dodge_p01){
            audio_play_sound(snd_dodge_p01, 10, false); //Play
            audio_play_sound(snd_dodge_smoke, 10, false); //Set Gain
        }
        //display smoke effect
        for (var i = 0; i < 4; ++i){
            instance_create(x, y, obj_potato_deathspike_smoke);
        }
    }
    //set xspeed to 0
    vsp = 0;
    //set roll speed & make it so y is affected by hspeed
    var spd = max(1.6 - image_index*0.1, 0);
    y += hsp;
    //set frame spd and frame
    image_speed = 0.25;
    //stop running
    moving = 0;
    //set alpha & set dodge status
    alpha = 0.5;
    dodging = true;
    //Movement Input
    if (dodge_dir == 0){
        if (keyboard_check(global.keyb_combat_moveL) or (gamepad_axis_value(0, gp_axislh) < 0)){ //Left
            dodge_dir = 1;
        }
        else{ //Right
            dodge_dir = -1;
        }
    }
    if (image_index > 1)
    if (image_index < 8)
    {potato_dodge = 1;}
    else
    {potato_dodge = 0;}
    if (image_index == 4){
        //display smoke effect
        for (var i = 0; i < 4; ++i){
            instance_create(x, y, obj_potato_deathspike_smoke);
        }
    }
    if (image_index > 11)
    {
        image_index = 0; //set image index to default
        spr = spr_potato_combat01; //set back to idle anim
        image_speed = 0.125; //set image speed
        time_pause = 0; //set time pause
        dodge_dir = 0;//reset direction
        ysc = 1; //set scale back
        vsp = 0; //set speeds back
        hsp = 0;
        alpha = 1; //set alpha back
        dodging = false; //set status back
    }
    //Movement Direction
    if (dodge_dir == -1){ //Right
        hsp = -1 * spd;
        ysc = 1;
    }
    if (dodge_dir == 1){ //Left
        hsp = 1 * spd;
        ysc = -1;
    }
    //Clamp Movement
    y = clamp(y, ymin, ymax);
    x = clamp(x, xmin, xmax);
}

///Moving
if keyboard_check(global.keyb_combat_moveR) or keyboard_check(global.keyb_combat_moveL) 
or keyboard_check(global.keyb_combat_moveU) or keyboard_check(global.keyb_combat_moveD)
or (gamepad_axis_value(0, gp_axislv) != 0) or (gamepad_axis_value(0, gp_axislh) != 0)
if(moving = 0 and spr = spr_potato_combat01 and pause_combat = 0 and dodging = false)
{
    image_speed = 0.2;
    image_index = 0;
    moving = 1;
}
//If Running
if (moving = 1 and pause_combat = 0)
{
    //Stm drain
    if stm_drain = 1
    {time_pause = 1
    time = max(time-0,10);}
    image_speed = 0.25;
    //Command Check
    if instance_exists(obj_comm_wheel_potato)
    {obj_comm_wheel_potato.pause_com = 1}
    //Move Horizontal
    key_right = -(keyboard_check(global.keyb_combat_moveR) or (gamepad_axis_value(0, gp_axislh) > 0));
    key_left = keyboard_check(global.keyb_combat_moveL) or (gamepad_axis_value(0, gp_axislh) < 0);
    hsp = (key_right + key_left) * 0.35;
    //Move Vertical
    key_up = -(keyboard_check(global.keyb_combat_moveU)or (gamepad_axis_value(0, gp_axislv) < 0));
    key_down = keyboard_check(global.keyb_combat_moveD)or (gamepad_axis_value(0, gp_axislv) > 0);
    vsp = (key_up + key_down) * 0.35;
    //Sprite
    if vsp > 0 or vsp < 0{spr = spr_potato_run_toward_r}
    if hsp > 0{spr = spr_potato_run_toward_l}
    if hsp < 0{spr = spr_potato_run_toward_r}
    //Moving
    y += hsp
    x += vsp
    //Max Movement & Stop Moving
    y = clamp(y,ymin,ymax);
    x = clamp(x,xmin,xmax);
    if (hsp = 0 and vsp = 0)
    {
        spr = spr_potato_combat01;
        image_speed = 0.125;
        pause_combat = 0;
        moving = 0;
        image_index = 0;
        time_pause = 0;
        hsp = 0;
        vsp = 0;
    }
}

///Hurt & KO

//Hurt
if hit = 1
{
    potato_attk = potato_attk_max
    potato_mag = potato_mag_max
    potato_combo = 1
    audio_play_sound(snd_player_hurt,10,false);
    instance_create(x+1,y,obj_eff);
    obj_eff.spr = spr_eff_hit01
    if potato_fire_skill03 = 1
    {
        mp_potato = min(mp_potato+mp_potato_max*0.1,mp_potato_max);
    }
    hit = 0
}

//KO
if hp_potato <= 0
if no_death = 0
{
    hsp = 0;
    vsp = 0;
    if spr != spr_potato_ko01
    {
        moving = 0
        pause_combat = 1
        audio_sound_gain(combat_music,0,25*room_speed);
        if !instance_exists(obj_gameover){instance_create(0,0,obj_gameover)}
        image_index = 0
        image_speed = 0.2
        spr = spr_potato_ko01
        if time > 90
        {
            time -= 10
        }
    }
    if image_index >= 13
    if spr = spr_potato_ko01
    {
        image_speed = 0
    }
}
if hp_potato <= 0
if no_death = 1
{
    hp_potato = 1
}

