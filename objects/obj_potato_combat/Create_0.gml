/// @description Setup
image_speed = 0.125;
draw_set_alpha_test(true);
draw_set_alpha_test_ref_value(40);
potato_attk = potato_attk_max;
potato_mag = potato_mag_max;
potato_combo = 1;
spr = spr_potato_combat01;
z = 0;
//Attack
attack = 0;
attack_type01 = 0;
//Magic Type
magic01 = 0;
magic02 = 0;
magictype = 0;
willpower_diff = mp_potato;
//Time
time = 0;
charge = 0;
charge_attk = 1;
nocharge = 0;
//Pause
pause_combat = 0;
time_pause = 0;
//Hit
hit = 0;
no_death = 0;
health_diff = hp_potato;
//Weapon
instance_create(x,y,obj_wep_potato_combat);
//Ally list add
ds_list_add(allies, self);
ally_numb += 1;
//Move Values
ymaxmove = 40;
yminmove = -70;
xmaxmove = 20;
xminmove = -25;
//Movement
hsp = 0;
vsp = 0;
ymax = y+ymaxmove;
ymin = y+yminmove;
xmax = x+xmaxmove;
xmin = x+xminmove;
key_right = 0;
key_left = 0;
moving = 0;
//Cartwheel
dodge_dir = 0; //direction
vspDccl = 0; //Decceleration
//Dodging
dodging = false;
//Misc
commence = 0;
alarm[0] = 1*room_speed;
alpha = 1;
stm_drain = 1;
wp_regen_pause = false;
//Image
xsc = 1;
ysc = 1;
zsc = 1;
rotZ = 0;
//Attack Actions
action_attk = 0;
action_heal = 0;
action_glob_ready = false;
instance_create(0,0,obj_potato_bactions);

///Misc
instance_create(x,y,obj_bound_box);

//Skill Effects
deathSpikeAttkBns = false;

