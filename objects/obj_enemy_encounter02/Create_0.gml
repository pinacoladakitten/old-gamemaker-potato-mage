/// @description Start up
pos = 1
text = 0
active = 1
port = spr_port
vol = 0

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Mythor:
I did it Kass!! They gone!-
"
strings[1] = @"Mythor:
Oh, there's more of them up ahead.
"
strings[2] = @"Mythor:
Huh, well we can stand up to them! Right Kass?
"
strings[3] = @"...:
......
"
strings[4] = @"Mythor:
(Where did he-.... I guess he really did have to go.)
"
strings[5] = @"Mythor:
*sigh*...(looks like I'm on my own still, in this creepy place.)
"
strings[6] = @"Mythor:
(No, Kass said he was always with me no matter the case!)
"
strings[7] = @"Mythor:
(So as long as I believe in him, it'll feel as if I'm not even alone
anymore...right?)
"
strings[8] = @"Mythor:
(I sound so desperate for a friend right now-mainly for Kass to be here.)
"
strings[9] = @"Mythor:
(I wish he was actually here though...everything would be a lot easier-
he would know how to handle this.)
"
strings[10] = @"Mythor:
(Kass believes in me though, and I can't let him down-no not today!)
"
strings[11] = @"Mythor:
(I'll be back Kass, I'll be back to tell you everything.)
"

