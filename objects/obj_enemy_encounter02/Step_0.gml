/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(ord("Z"))
    if string_length(strings[text]) <= pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        if (text > maxstrings)
        {
            pos = 0
            text = 0
            active = 0
            pause = 0
            obj_potato_overw.sprite_index = spr_potato_ow_idle_f
            audio_play_sound(overw_music,10,true);
            audio_sound_gain(overw_music,1,0);
            instance_destroy();
        }
    }
    maxstrings = 11
}

///Events
if active = 1
{
    pause = 1
    if text = 8
    {
        audio_sound_gain(overw_music,vol,0);
        vol = min(vol+0.01,1)
    }
}
//Portraits
if string_char_at(strings[text], 1) = "M"
{
    port = spr_potato_portrait_text
}
else
{
    port = spr_port
}
//Misc
if audio_sound_get_gain(snd_battle_evt01) = 0
{
    audio_stop_sound(snd_battle_evt01);
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
audio_play_sound(snd_text_speech,10,false)
}

