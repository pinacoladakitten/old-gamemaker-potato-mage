/// @description Main Event
if hit = 1
{
    //Weapon Effects
    if spr = spr_potato_proj
    {
        if effect = 0{audio_play_sound(choose(snd_attk_hit_p01,snd_attk_hit_p02,snd_attk_hit_p03),10,false)}
        vsp -= grav
        move_towards_point(obj_target.x,obj_target.y,0)
        y += 1
        x += 1
        z += (vsp/2)
        effect = 1
        mask_index = spr_none;
    }
    if spr = spr_slash01
    {
        instance_destroy();
    }
    //Skill Effect
    if potato_gloom_skill02 = 1
    if on_hit = 0
    {
        hp_potato += round(proj_attk*0.05)
        instance_create(x+1,y,obj_heal_numbers)
        obj_heal_numbers.heal = proj_attk*0.05
        on_hit = 1
    }
}
if z <= -10
{
    instance_destroy();
}

