/// @description Vars and Stuff
hit = 0;
z = 0;
grav = 0.25;
vsp = 0;
effect = 0;
on_hit = 0;
move_towards_point(obj_target.x,obj_target.y,5);
image_speed = 0.5;

///Stats
if global.wep_durability > 0{ //**Durability**//
    proj_attk = potato_attk;
}
else{
    proj_attk = potato_attk*0.1;
}

//Gloom Skill 1
if potato_gloom_skill01 = 1
{
    proj_attk += potato_attk*0.25
    if hp_potato = hp_potato_max
    {
        proj_attk += potato_attk*0.1
    }
}

//Gloom Skill 3
if potato_gloom_skill03 = 1
{
    if hp_potato = hp_potato_max
    {
        proj_attk += potato_attk*0.1
    }
    //Critical
    var critical = irandom_range(1,10)
    if critical = 1
    {
        proj_attk *= 2
        instance_create(x,y,obj_encourage_words);
        obj_encourage_words.spr = spr_critical
    }
}

/* */
///Sprites
spr = spr_slash01;

if potato_equip = 0 //Default
{spr = spr_potato_proj;}

if potato_equip = 1 //Dagger
{spr = spr_slash01;}

if potato_equip = 2 //Dark Stick
{spr = spr_slash01;}

/* */
/*  */
