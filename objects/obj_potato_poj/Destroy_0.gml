/// @description Weapon Effects
if spr = spr_slash01
{
    instance_create(x+1,y,obj_eff01);
    obj_eff01.spr = spr_slash01;
}
//Sound
if spr = spr_slash01
{
    audio_play_sound(snd_attack_sword_p01,10,false);
}
//Combo
potato_combo += 0.01;
potato_attk *= potato_combo;
potato_mag *= potato_combo;

