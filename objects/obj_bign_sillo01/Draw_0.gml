/// @description Draw Setup
d3d_set_lighting(false)
var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
tex01 = sprite_get_texture(spr_shadow,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
//Draw
draw_set_alpha(alpha);
draw_set_color(c_black);
d3d_transform_add_translation(x,y,z+1);
d3d_draw_wall(0+16*ss,0+16*cc,50,0-16*ss,0-16*cc,-10,tex,1,1)
d3d_transform_set_identity();
draw_set_color(-1);
draw_set_alpha(1);

