/// @description Setup
spr = spr_nephal_combat_idle01
ymax = y+10
ymin = y-10
z = 0
alpha = 1
if !audio_is_playing(snd_bign_sillo01)
{
    audio_play_sound(snd_bign_sillo01,10,false);
}
if instance_exists(obj_big_n)
{
    spr = obj_big_n.sprite_index;
}
if instance_exists(obj_big_n_hard)
{
    spr = obj_big_n_hard.sprite_index;
}

