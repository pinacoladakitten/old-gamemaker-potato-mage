/// @description Main Event: Fade In Then Out
if fade = 1
{
    if fade_out = 0 //Fade In
    {
        alpha += fadespd
    }
    if fade_out = 1 //Fade Out
    {
        alpha -= fadespd
    }
    //Alpha Flags
    if alpha >= 1 //If fully faded in
    {
        fade_out = 1
    }
    if alpha <= 0 //If fully faded out
    if fade_out = 1
    {
        instance_destroy();
    }
}

