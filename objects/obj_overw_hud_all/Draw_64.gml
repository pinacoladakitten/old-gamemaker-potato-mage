/// @description Draw Everything
d3d_set_lighting(false);
d3d_set_fog(false, -1, 0, 10000);
draw_set_alpha(0.5*alpha);
draw_sprite(spr_overw_panel01,0,20,30);
draw_set_alpha(1*alpha);
draw_set_halign(fa_left);
draw_set_font(font0);
draw_sprite(spr_potato_portrait_save,0,100,80);
draw_text_colour(164,44,string_hash_to_newline("Mythor: " + string(hp_potato)),c_black,c_black,c_black,c_black,alpha);
draw_text(160,40,string_hash_to_newline("Mythor: " + string(hp_potato)));
//Bar Colors
var hp_barcol;
hp_barcol = make_color_rgb(255, 185, 185);
//Health Bar
draw_healthbar(170,80,350,92,(hp_potato/hp_potato_max)*100,c_black,hp_barcol,hp_barcol,0,false,false)
draw_sprite(spr_hp_bar01,0,170,80);
draw_set_alpha(1);

