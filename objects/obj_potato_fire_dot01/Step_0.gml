/// @description Main Event
if fade = 0
{
    alpha = min(alpha+0.05,1)
}
if fade = 1
{
    alpha = max(alpha-0.05,0)
    mask_index = spr_none;
}
if alpha <= 0 
{
    instance_destroy();
}
//Target Check
if target = 0
{fade = 1;}
if instance_exists(target){
    x = target.x;
    y = target.y;
}
//If Overlapping
if place_meeting(x ,y, obj_potato_fire_dot01){
    if id != instance_find(obj_potato_fire_dot01, instance_number(obj_potato_fire_dot01-1)){
        //Do Nothing
    }
    else{
        fade = 1;
    }
}
//Ember Effect
++embers;
if (embers >= 10){
    instance_create(x+2, y, obj_PotatoFireEmbers);
    embers = 0;
}

