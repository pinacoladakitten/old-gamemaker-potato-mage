/// @description Damage Tick
if instance_exists(target)
if fade != 1
{
    var hp_loss = (1.3*potato_mag*0.6 + 15) * 0.1;
    target.hp -= hp_loss;
    instance_create(x+1,y,obj_dmg_numbers02);
    var targetdmg = instance_find(obj_dmg_numbers02, instance_number(obj_dmg_numbers02)-1);
    targetdmg.dmg = hp_loss;
}
if !instance_exists(target)
{
    fade = 1;
}
alarm[0] = 1*room_speed;

