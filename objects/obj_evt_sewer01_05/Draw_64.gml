/// @description Main event
if active = 1
{
    draw_set_alpha(0.5);
    draw_rectangle_colour(0,0,window_get_width(),window_get_height(),c_black,c_black,c_black,c_black,0);
    //Setup
    draw_set_font(font0);
    draw_set_halign(fa_left);
    draw_set_color(c_white);
    draw_set_alpha(1);
    //Textbox
    draw_sprite(spr_textbox,0,0,550);
    draw_sprite(port,0,40,560);
    //The Text
    cstr = string_copy(strings[text],1,pos);
    draw_text_ext_transformed(250,560,string_hash_to_newline(cstr),-1,-1,1,1,0);
    pos = min(pos+2,string_length(strings[text])+1);
    if keyboard_check(global.keyb_cancel)
    or gamepad_button_check(0, global.gpi_cancel)
    {pos = min(pos+5,string_length(strings[text])+1);}
    //Text Choices
    if text = 6
    if string_length(strings[text]) < pos
    {
        draw_sprite(spr_text_option,0,640,360);
        if pick = 0 {draw_sprite_ext(spr_inventory_select,-1,415,335,-1,1,0,-1,1)}
        if pick = 1 {draw_sprite_ext(spr_inventory_select,-1,415,383,-1,1,0,-1,1)}
        {
            //Shadow
            draw_set_colour(c_black);
            draw_text(424,329,string_hash_to_newline(choice[0]));
            draw_text(424,377,string_hash_to_newline(choice[1]));
            draw_set_colour(-1);
            //Normal
            draw_text(420,325,string_hash_to_newline(choice[0]));
            draw_text(420,373,string_hash_to_newline(choice[1]));
        }
    }
}

