/// @description Boss Encounter
with(obj_spawn)
{
    spawn1 = irandom_range(2,2);
    spawn2 = irandom_range(2,2);
    spawn3 = irandom_range(2,2);
    spawn4 = irandom_range(2,2);
    spawn5 = irandom_range(2,2);
    
    enemy[0] = obj_enm_rat01
    if spawn2 = 1 {enemy[1] = obj_enm_rat01}
    if spawn3 = 1 {enemy[2] = obj_enm_rat01}
    if spawn4 = 1 {enemy[3] = obj_enm_rat01}
    if spawn5 = 1 {enemy[4] = obj_enm_rat01}
}
///Battle Trans
potato_battle_room = rm_boss_gsquid_prev;
instance_create(512,384,obj_trans_battle);
obj_trans_battle.battle_music = 0;
//Music
combat_music = snd_heightened_battle01;
combat_music_pt2 = snd_heightened_battle01;
//Next Event
alarm[0] = 2*room_speed;

