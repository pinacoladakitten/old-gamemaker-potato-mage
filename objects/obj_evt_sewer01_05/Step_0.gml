/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if string_length(strings[text]) <= pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        //If NOT using text choice, then progress text
        if text != 6
        {
            pos = 0
            text = text+1
        }
        if text = 2
        {active = 0;}
        if text = 10
        {
            //Cam Shake
            obj_camera_ow.cam_shake = 1;
            obj_camera_ow.cam_shakemax = obj_camera_ow.camz;
            audio_play_sound(snd_tremmor01,10,true);
            audio_sound_gain(snd_tremmor01,1,0);
            //Change Music
            audio_stop_sound(overw_music);
            audio_play_sound(snd_heightened_battle01,10,true);
            audio_sound_gain(snd_heightened_battle01,0.65*global.music_volume,0);
            //Create Effect
            with obj_darkness_swirl01
            {
                z = 650;
                xsc = 0;
                ysc = 0;
                zsc = 0;
            }
            //Darkness Sound
            audio_play_sound(snd_darkness01,10,false);
        }
        if text = 18
        {audio_sound_gain(snd_tremmor01,0,30*room_speed);}
        if (text = 21)
        {
            active = 1.5;
            obj_camera_ow.cam_shake = 0;
            alarm[2] = 1;
        }
    }
    if text > maxstrings
    {
        active = 0;
        pause = 0;
        alarm[3] = 1*room_speed;
    }
    maxstrings = 25;
}

///Events
if active = 1
{pause = 1}
//Portraits
if active = 1
{
    if string_char_at(strings[text], 1) = "M"
    {port = spr_potato_portrait_text;}
    if string_char_at(strings[text], 1) = "?"
    {port = spr_port;}
}
if evt = 1
if text = 0
{
    with obj_camera_ow
    {
        camz = min(camz+(767-camz)*0.01,767);
    }
    if obj_camera_ow.camz > 760
    {
        if !instance_exists(obj_darkness_swirl01)
        {
            instance_create(-2448,64,obj_darkness_swirl01);
            var targetpart = instance_find(obj_darkness_swirl01,instance_number(obj_darkness_swirl01)-1);
            targetpart.size = 3;
            instance_create(-2458,34,obj_darkness_swirl01);
            var targetpart = instance_find(obj_darkness_swirl01,instance_number(obj_darkness_swirl01)-1);
            targetpart.size = 4;
            instance_create(-2478,94,obj_darkness_swirl01);
            var targetpart = instance_find(obj_darkness_swirl01,instance_number(obj_darkness_swirl01)-1);
            targetpart.size = 5;
            obj_darkness_swirl01.z = 767;
            //Audio Darkness
            audio_play_sound(snd_darkness01,10,false);
            audio_play_sound(snd_darkness02,10,false);
        }
    }
    if obj_camera_ow.camz > 766
    {
        if !instance_exists(obj_sewer01_gsquid01)
        {
            instance_create(-2447,64,obj_sewer01_gsquid01);
            obj_sewer01_gsquid01.z = 755;
        }
    }
}
if text = 2
{
    if obj_camera_ow.camz > 610
    {obj_sewer01_gsquid01.fade = 1;}
    with obj_camera_ow
    {
        camz = max(camz-(camz-617)*0.01,620);
    }
    if obj_camera_ow.camz < 622
    {obj_sewer01_gsquid01.fade = 0;
    obj_sewer01_gsquid01.z = 619;}
    if obj_camera_ow.camz <= 620
    {active = 1;}
}
if text = 6
{
    //Pick Controls
    if keyboard_check_pressed(global.keyb_down)
    or gamepad_button_check_pressed(0, global.gpi_down)
    {pick = min(pick+1,1);
    audio_play_sound(snd_move_menu,10,false);}
    if keyboard_check_pressed(global.keyb_up)
    or gamepad_button_check_pressed(0, global.gpi_up)
    {pick = max(pick-1,0);
    audio_play_sound(snd_move_menu,10,false);}
    //Select Pick
    if string_length(strings[text]) < pos
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    {
        audio_play_sound(snd_select,10,false);
        if pick = 0
        {text = 9;
        pos = 0;}
        if pick = 1
        {text = 7;
        pos = 0;}
    }
}
if text = 21
{
    audio_stop_sound(overw_music);
    pause = 1;
    obj_trans_fade_out.music = 3;
}
if text = 23
{
    with obj_darkness_swirl01
    {fade = 1;}
    with obj_sewer01_gsquid01
    {
        fade = 1;
        if alpha <= 0
        {instance_destroy();}
        if !audio_is_playing(snd_darkness02)
        {audio_play_sound(snd_darkness02,10,false);}
    }
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false);
}

