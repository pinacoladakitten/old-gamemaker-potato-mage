/// @description Start up
pos = 1;
text = 0;
active = 0;
port = spr_port;
vol = 0;
potato_port = spr_potato_port_f;
kass_port = spr_kaas_port_default;
potato_xsc = 1;
kass_xsc = -1;
port_alpha = 0;
evt = 0;
pick = 0;
image_speed = 0.25;

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
Hah! I'm back! Hmmmmm, let's see how my plan is all coming together now, the sewers will
be all mine, then next is the entire city! Haha! Do they really think they have a chance?
"
strings[1] = @"???:
!?...
"
strings[2] = @"???:
It's YOU!? No NO! What are you doing here!? This isn't what it looks like really! I'm just
uh...tending to personal matters is all.
"
strings[3] = @"Mythor:
Uhm, um, um-
"
strings[4] = @"???:
Oh come on! Wouldn't He understand? You are a servant of Him after all, here go and tell
Him that everything's fine, it's just a small business matter. Can't you do that for me...
please?
"
strings[5] = @"???:
So go along now, nobody would want to get in trouble or anything for going rogue am I
right? So see ya, bye, make sure to tell Him that I'm still doing my job ok? I'll reward
you nicely!
"
strings[6] = @"???:
Good good! Once I complete my little 'personal endeavor' you shall be rewared with 
something of great value, this I swear-...hmmm...I mean you are a servant of Him right? 
You know WHO I'm talking about and aren't some freeloader.
"
strings[7] = @"Mythor:
...Y-Yeah...
"
strings[8] = @"???:
Don't give me just a 'Y-Yeah' with unnatural pauses,  I'd expect a servant of Him to be
more confident you know!...I still haven't gotten my answer! Just tell me at least His
name, every servant should know that!
"
strings[9] = @"Mythor:
Uh-um...you know...it's......like the God of you know....something, am I right? Ok really, 
I legitimately have no idea who you're talking about. (Zero regrets.)
"
strings[10] = @"???:
YOU CAN'T BE SERIOUS!?...Ahem! Well uh, hehe-hahaha-...*cough* *cough*...HAHAHA!!
Foolish mortal!! You dare speak in the presence of Hades himself!!?
"
strings[11] = @"???:
You had me fooled right there for a hot minute!! Even Lords themselves can have uh...
off-days in which you mortals would call them!! However, my collectiveness HAS returned
and no more tricks shall be pulled against me!!
"
strings[12] = @"???:
Ah YES! MY plan! These miserable beings are all mine! My army will slowly come to
being complete, and once it does, nothing can stand in MY way!!
"
strings[13] = @"Mythor:
W-What!!?-
"
strings[14] = @"???:
Foolish mortal!! I, Hades, shall rid thine's soul from this world, tis too weak!!
Become of servant of ME, HADES!! Hahaha!!
"
strings[15] = @"Mythor:
So you're the one behind all of this!? Where are you taking their souls!? Why are
you doing this in the first place!?
"
strings[16] = @"???:
Your simple mind is too frail to understand what it's like to TRULY be weak, my hunger
for power only matters from this point onward! 
"
strings[17] = @"???:
Nobody will stand in my way!! YOU HEAR ME!? NOBODY! I, Hades, shall take over all that
is left in this miserable realm!
"
strings[18] = @"Mythor:
I--I can't let you do that! If anything where are their souls going after they've been
freed...anything please!
"
strings[19] = @"???:
Oh-ho! That!...That I cannot tell the likes of YOU! Otherwise my plans would be foiled!
NOW mortal!! Feel my wrath, feel my pain, become apart of it all and serve me!!
"
strings[20] = @"Mythor:
Here he comes!!
"
//After Battle
strings[21] = @"???:
Hehe! You're a bit tougher than I thought! Still, you're too late, everything's already
set in stone!
"
strings[22] = @"???:
I have a couple errands to run now, hope you enjoy your stay down here, hahaha!!
"
strings[23] = @"Mythor:
Wait!!
"
strings[24] = @"Mythor:
I need to hurry and catch up with him! But how am I gonna be able to do that if he could
just teleport like that...
"
strings[25] = @"Mythor:
No matter! I should still try to at least prevent his plans from finishing up. Hopefully
something-anything at this point will be able to buy me some time.
"

///Dia Choices
choice[0] = "Uh...um..."
choice[1] = "Y-Yeah..."

