/// @description Draw Model
if room = rm_test_stage
{
    draw_set_alpha(1);
    d3d_set_lighting(true); 
    texture_set_repeat(true);
    d3d_transform_add_scaling(1,1,1)
    d3d_transform_add_rotation_z(0)
    d3d_transform_add_translation(x,y,0)
    d3d_model_draw(model0,0,0,0,background_get_texture(test_tex));
    d3d_set_lighting(true); 
    draw_set_alpha(1);
    d3d_transform_set_identity();
}

