/// @description Drawing
var ss,cc,tex;
tex = sprite_get_texture(spr_shadow,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);

d3d_set_lighting(false);
draw_set_alpha(alpha);
var tex03;
tex03 = sprite_get_texture(spr,-1);
//Sphere 01
d3d_transform_add_scaling(xsc,ysc,zsc);
d3d_transform_add_rotation_z(r);
d3d_transform_add_rotation_y(-90);
d3d_transform_add_rotation_z(dir);
d3d_transform_add_translation(x,y,z);
d3d_draw_ellipsoid(0+6,0+6,6,0-6,0-6,-6,tex03,1,1,18);
d3d_transform_set_identity();
//Sphere 02
d3d_transform_add_scaling(xsc*0.75,ysc*0.75,zsc*1.5);
d3d_transform_add_translation(0,0,-6);
d3d_transform_add_rotation_z(-r);
d3d_transform_add_rotation_y(-90);
d3d_transform_add_rotation_z(dir);
d3d_transform_add_translation(x,y,z);
d3d_draw_ellipsoid(0+6,0+6,6,0-6,0-6,-6,tex03,1,1,18);
d3d_transform_set_identity();
//Shadow
d3d_draw_wall(x+1,y+10*cc,1,x+1,y-10*cc,0,tex,1,1);

