/// @description Eldrich Blast Proj
spr = spr_bign_eff05;
image_index = 0;
xsc = 1;
ysc = 1;
zsc = 0.5;
yscmax = 1;
z = 20;
r = 0;
hit = 0;
hit2 = 0;
alarm[0] = 10*room_speed;
alarm[1] = 0.05*room_speed;
alpha = 0;
image_speed = 0.5;
pspeed = choose(0.8,1);
podirection = 0;
fade = 0;
dir = 0;
audio_play_sound(snd_bign_magic01,10,false);

