/// @description Movement
r += 10
if fade = 0
{alpha = min(alpha+0.1,1);}
if fade = 1
{alpha = max(alpha-0.1,0);}
xsc = max(xsc-0.02,0.3);
ysc = max(ysc-0.02,0.3);
zsc = min(zsc+0.02,1.5);
if z > 10
{
    z -= 0.1
}
if instance_exists(obj_potato_combat)
{
    if x < obj_potato_combat.x
    {
        move_towards_point(obj_potato_combat.x,obj_potato_combat.y,pspeed)
    }
}
dir = point_direction(x,y,obj_potato_combat.x,obj_potato_combat.y);

///Hitbox
if place_meeting(x,y,obj_potato_combat)
if hit = 0
{
    if potato_dodge = 0
        {
            hp_potato -= 16 * obj_bign_attkStats.attkScalingHard;
            obj_potato_combat.hit = 1
            hit = 1
            fade = 1;
        }
    if potato_dodge = 1
    if hit2 = 0
        {
            hp_potato -= 0
            if place_meeting(x,y,obj_potato_combat){instance_create(x,y,obj_encourage_words)}
            hit = 1
            hit2 = 1
            fade = 1;
        }
}

///Misc
d3d_set_lighting(true);
d3d_light_define_point(3,x,y,0,200,c_purple)
d3d_light_enable(3,true);
d3d_set_lighting(false);

