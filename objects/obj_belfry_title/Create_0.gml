/// @description Model Create
model0_01 = d3d_model_create();
model1_01 = d3d_model_create();
model2_01 = d3d_model_create();
model3_01 = d3d_model_create();
model4_01 = d3d_model_create();
model5_01 = d3d_model_create();
model6_01 = d3d_model_create();
model7_01 = d3d_model_create();
//Model Load
d3d_model_load(model0_01, "maps/belfry/title/black.d3d");
d3d_model_load(model1_01, "maps/belfry/title/grass02.d3d");
d3d_model_load(model2_01, "maps/belfry/title/leaves01.d3d");
d3d_model_load(model3_01, "maps/belfry/title/ribbons001.d3d");
d3d_model_load(model4_01, "maps/belfry/title/ribbons01.d3d");
d3d_model_load(model5_01, "maps/belfry/title/rock01b.d3d");
d3d_model_load(model6_01, "maps/belfry/title/white.d3d");
d3d_model_load(model7_01, "maps/belfry/overworld1/grass01.d3d");
modelload = 0;
image_speed = 1;
//Set opacity textures
draw_set_alpha_test(true)
draw_set_alpha_test_ref_value(40)

