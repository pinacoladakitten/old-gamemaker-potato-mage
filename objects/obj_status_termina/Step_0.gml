/// @description Events
if countdown = 0
{
    hp_potato = 0
}
if instance_exists(obj_victory)
if instance_exists(obj_gameover)
{
    instance_destroy();
}
//Cloud Rotation
r = min(r+2,360)
ysc = xsc
zsc = xsc
alpha = min(alpha+0.05,0.85);
if r >= 360
{
    r = 0
}
//Cloud Growth
if grow = 1
{
    xsc = min(xsc+0.03,1.1);
}
else
{
    xsc = max(xsc-0.02,0);
}
if xsc >= 1.1
{
    grow = 0
}
if xsc = 0
{
    if depth != 0
    {
        alpha = 0
        enm_pause = 0
    }
    depth = 0
}
//Move
if target != 0
{
    x = target.x
    y = target.y
}

