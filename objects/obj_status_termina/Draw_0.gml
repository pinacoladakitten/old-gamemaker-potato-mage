/// @description Draw Numbers & Clouds
d3d_set_lighting(false);
draw_set_alpha(alpha);
var ss,cc,tex;
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
tex = sprite_get_texture(spr_doomcloud01,-1);

//Draw text
d3d_transform_add_rotation_z(90);
d3d_transform_add_scaling(4,0.75,1);
d3d_transform_add_translation(x-80,y,0);
draw_set_font(terminafont);
draw_set_color(c_red);
draw_set_halign(fa_center);
draw_text(0,0,string_hash_to_newline(countdown));
d3d_transform_set_identity();

//Default Stuff
draw_set_color(c_white);
draw_set_halign(fa_left);
draw_set_font(font0);
draw_set_alpha(1);
d3d_set_lighting(false);

//Draw Cloud
draw_set_alpha(alpha);
d3d_transform_add_rotation_x(r);
d3d_transform_add_scaling(xsc,ysc,zsc);
d3d_transform_add_translation(x+1,y,10);
d3d_draw_wall(0+10*ss,0+10*cc,10,0-10*ss,0-10*cc,-10,tex,1,1)
d3d_set_depth(depth)
d3d_transform_set_identity();
draw_set_alpha(1);

