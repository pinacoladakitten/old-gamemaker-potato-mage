/// @description Draw Model
if room = rm_overw_sewdung01
{
    d3d_set_lighting(true);
    draw_set_alpha(0.75)
    tex = background_get_texture(bg);
    d3d_model_draw(model2,x,y,0,tex);
    draw_set_alpha(1)
    d3d_set_lighting(true);
}
if room = rm_overw_sewdung01_02
{
    d3d_set_lighting(true);
    draw_set_alpha(0.75)
    tex = background_get_texture(bg);
    d3d_model_draw(model2,x+711,y+387,2,tex);
    draw_set_alpha(1)
    d3d_set_lighting(true);
}

