/// @description Main Step
if enm_pause = 0
if phase = 2
{
    time = min(time+0.25,timemax)
    if time = timemax
    if instance_number(obj_eff_enm) = 0
    {
        alarm[0] = 1
    }
}
if instance_exists(obj_target)
{
    obj_target.z = 5
}

///Attack Commence
if attack_commence = 1
{
    //////Attack 1////////
    if attk_numb = 1 or attk_numb = 6
    {
        if image_index = 0
        if attack_times = -1
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack_ini01
            {
                image_speed = 0.3
                sprite_index = spr_nephal_combat_attack_ini01
            }
            //set time, pause, attack times
            time = 0
            enm_pause = 1
            attack_times = irandom_range(1,2)
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack_ini01
        {
            enm_pause = 1
            if image_index >= 8
            {
                image_speed = 0
                alpha = max(alpha-0.05,0);
                //Create Sillouette
                if !instance_exists(obj_bign_sillo01)
                {instance_create(x-1,y,obj_bign_sillo01);}
            }
        }
        //after commencing
        if sprite_index = spr_nephal_combat_attack_ini01
        if alpha = 0
        {
            sprite_index = spr_nephal_combat_attack03
            image_index = 0
        }
        if sprite_index = spr_nephal_combat_attack03
        {
            enm_pause = 1
            if fade = 0
            {
                //movement in air
                alpha = min(alpha+0.1,1);
                y += sign(y-yorg)*0.1;
                z += 0.1
                //animation play
                if image_index < 4
                {image_speed = 0.1}
                //teleport to random local and pose
                if image_index = 0
                {
                    z = irandom_range(z+15,z+20);
                    y = irandom_range(y-50,y+50);
                    instance_create(x,y,obj_eff_enm_bign01);
                    instance_create(x,y,obj_eff_enm);
                    obj_eff_enm.z = z
                    obj_eff_enm.spr = spr_bign_eff02
                    //Frame Advance
                    image_index = 1
                }
                //end of animation
                if image_index >= 4
                {
                    image_speed = 0
                    //Create Sillouette
                    instance_create(x-1,y,obj_bign_sillo01);
                    obj_bign_sillo01.z = z
                    fade = 1
                }
            }
            //end of attack
            if fade = 1
            {
                alpha = max(alpha-0.1,0);
                if alpha <= 0
                {
                    sprite_index = spr_nephal_combat_idle01
                    image_index = 0
                }
            }
        }
    }
    ////////Attack 2//////////
    if attk_numb = 2
    {
        if image_index = 0
        if attack_times = -1
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack_ini01
            {
                image_speed = 0.3
                sprite_index = spr_nephal_combat_attack_ini01
            }
            //set time, pause, attack times
            time = 0
            enm_pause = 1
            attack_times = irandom_range(3,4)
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack_ini01
        {
            enm_pause = 1
            if image_index >= 8
            {
                image_speed = 0
                alpha = max(alpha-0.05,0);
                //Create Sillouette
                if !instance_exists(obj_bign_sillo01)
                {instance_create(x-1,y,obj_bign_sillo01);}
            }
        }
        //after commencing
        if sprite_index = spr_nephal_combat_attack_ini01
        if alpha = 0
        {
            sprite_index = spr_nephal_combat_attack03
            image_index = 0
        }
        if sprite_index = spr_nephal_combat_attack03
        {
            enm_pause = 1
            if fade = 0
            {
                //movement in air
                alpha = min(alpha+0.1,1);
                y += sign(y-yorg)*0.1;
                z += 0.1
                //animation play
                if image_index < 4
                {image_speed = 0.1}
                //teleport to random local and pose
                if image_index = 0
                {
                    z = irandom_range(z+20,z+25);
                    y = irandom_range(y-60,y+60);
                    instance_create(x,y,obj_eff_enm_bign02);
                    instance_create(x,y,obj_eff_enm);
                    obj_eff_enm.z = z
                    obj_eff_enm.spr = spr_bign_eff02
                    //Frame Advance
                    image_index = 1
                }
                //end of animation
                if image_index >= 4
                {
                    image_speed = 0
                    //Create Sillouette
                    instance_create(x-1,y,obj_bign_sillo01);
                    obj_bign_sillo01.z = z
                    attack_times -= 1
                    fade = 1
                }
            }
            //end of attack
            if fade = 1
            {
                alpha = max(alpha-0.025,0);
                if alpha <= 0
                {
                    if attack_times = 0
                    {
                        sprite_index = spr_nephal_combat_idle01
                        image_index = 0
                    }
                    else
                    {
                        sprite_index = spr_nephal_combat_attack03
                        y = yorg
                        z = zorg
                        image_index = 0
                        fade = 0
                    }
                }
            }
        }
    }
    //////Attack 3////////
    if attk_numb = 3
    {
        if image_index = 0
        if attack_times = -1
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack_ini01
            {
                image_speed = 0.3
                sprite_index = spr_nephal_combat_attack_ini01
                instance_create(x-1,y,obj_bign_sillo01);
            }
            //set time, pause, attack times
            time = 0
            enm_pause = 1
            attack_times = irandom_range(2,2)
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack_ini01
        {
            enm_pause = 1
            if image_index >= 8
            {
                image_speed = 0
                alpha = max(alpha-0.1,0);
                if alpha = 0
                {
                    if attack_times = 2{time = timemax-5}
                    //Create Sillouette
                    if !instance_exists(obj_bign_sillo01)
                    {instance_create(x-1,y,obj_bign_sillo01);}
                    instance_create(obj_potato_combat.x+1,obj_potato_combat.y,obj_eff_enm);
                    obj_eff_enm.spr = spr_bign_eff04
                    sprite_index = spr_nephal_combat_idle01
                    image_index = 0
                }
            }
        }
    }
    //////Attack 4////////
    if attk_numb = 4
    {
        if image_index = 0
        if attack_times = -1
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack01
            {
                image_speed = 0.3
                sprite_index = spr_nephal_combat_attack01
                instance_create(x-1,y,obj_bign_sillo01);
                instance_create(x,y,obj_eff_enm_bign03);
            }
            //set time, pause, attack times
            time = 0
            enm_pause = 1
            attack_times = irandom_range(1,1)
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack01
        {
            enm_pause = 1
            if image_index >= 8
            {
                image_speed = 0
                obj_camera.cam_shake = 1
                obj_camera.cam_shakeleng = 0.5
                if !instance_exists(obj_eff_enm_bign03)
                {
                    alpha = max(alpha-0.1,0);
                    if alpha = 0
                    {
                        instance_create(obj_big_n.x,obj_big_n.y,obj_bign_sillo01);
                        sprite_index = spr_nephal_combat_idle01
                        image_index = 0
                    }
                }
            }
        }
    }
    //////Attack 5////////
    if attk_numb = 5
    {
        if image_index = 0
        if attack_times = -1
        {
            //change sprite
            if sprite_index != spr_nephal_combat_attack02
            {
                image_speed = 0.3
                sprite_index = spr_nephal_combat_attack02
                instance_create(x-1,y,obj_bign_sillo01);
            }
            //set time, pause, attack times
            time = 0
            enm_pause = 1
            attack_times = irandom_range(1,1)
        }
        //commencing attack
        if sprite_index = spr_nephal_combat_attack02
        {
            enm_pause = 1
            if image_index >= 3
            {
                if !instance_exists(obj_eff_enm_bign04)
                {instance_create(x+0,y+20,obj_eff_enm_bign04);}
                if !instance_exists(obj_bign_sillo01)
                {instance_create(x-1,y,obj_bign_sillo01);}
            }
            if image_index >= 6
            {
                image_speed = 0
                alpha = max(alpha-0.1,0);
                if alpha = 0
                {
                    //Create Sillouette
                    if !instance_exists(obj_bign_sillo01)
                    {instance_create(x-1,y,obj_bign_sillo01);}
                    sprite_index = spr_nephal_combat_idle01
                    image_index = 0
                }
            }
        }
    }
    ///End all attacks
    if sprite_index = spr_nephal_combat_idle01
    {
        alpha = min(alpha+0.1,1);
        y = yorg
        z = zorg
        fade = 0
        if alpha = 1
        {
            attk_numb = -1
            image_speed = 0.05
            enm_pause = 0
            attack_times = -1
            obj_camera.cam_shake = 0
            attack_commence = 0
        }
    }
}

///Reaction to Player Attacks
scr_Player_Attacks_ALL();
//Phase Effects
if phase = 1
{
    physical_defense = 1;
    fire_defense = 1;
    nature_defense = 1;
    magic_defense = 1;
    lightning_defense = 1;
    dark_defense = 1;
    earth_defense = 1;
}
if phase = 2
{
    physical_defense = 0.2;
    fire_defense = 0.3;
    nature_defense = -0.25;
    magic_defense = 0.2;
    lightning_defense = 0.2;
    dark_defense = 0.5;
    earth_defense = 0.2;
}
if place_meeting(x,y,obj_potato_poj)
or place_meeting(x,y,obj_potatofire_proj)
or place_meeting(x,y,obj_potato_fire_dot01)
or place_meeting(x,y,obj_potato_asterid)
{
    if phase = 1
    {
        instance_create(x+1,y,obj_eff_enm);
        obj_eff_enm.spr = spr_bign_eff01;
        //Create Sillouette
        instance_create(x-1,y,obj_bign_sillo01);
        obj_bign_sillo01.spr = spr_nephal_combat_idle01;
        //Create Parry
        instance_create(x,y,obj_encourage_words);;
        obj_encourage_words.spr = spr_parried;
        //Sound
        audio_play_sound(snd_bign_parry01,10,false);
        //With Dots
        with obj_potato_fire_dot01
        {fade = 1;}
    }
}

///Dead
if hp < 1
if room = rm_boss_big_n01
{
    attack_commence = 0;
    obj_camera.cam_shake = 0;
    image_speed = 0.1;
    with obj_eff_enm
    {
        instance_destroy();
    }
    enm_pause = 1;
    with obj_target
    {
        s = 0;
    }
    if !instance_exists(obj_bign_battle_end)
    {
        instance_create(0,0,obj_bign_battle_end);
        potato_exp += 1800;
        ds_list_delete(enemies,self);
        enm_numb -= 1;
        image_speed = 0.1;
        image_speed = 0;
        obj_potato_combat.pause_combat = 1;
        enm_pause = 1;
    }
    //Set Player to Max HP
    hp_potato = hp_potato_max;
}

///Misc
if instance_exists(obj_bign_termina01)
{
    if obj_bign_termina01.countdown = 10
    if !instance_exists(obj_battle_textbox_bign)
    {
        instance_create(0,0,obj_battle_textbox_bign);
        with obj_battle_textbox_bign
        {
            strings[0] = @"         ???:
            Hm..."
        }
    }
}
if instance_exists(obj_bign_termina01)
{
    if obj_bign_termina01.countdown = 1
    if obj_bign_termina01.fade = 0
    if !instance_exists(obj_battle_textbox_bign)
    {
        audio_sound_gain(combat_music,0,30*room_speed);
        instance_create(0,0,obj_battle_textbox_bign);
        with obj_battle_textbox_bign
        {
            maxstrings = 9
            strings[0] = @"         ???:
            Alright, alright...I've considered your life."
            strings[1] = @"         ???:
            I've got some time to kill, so tell ya what..."
            strings[2] = @"         Mythor:
            Uhm..."
            strings[3] = @"         ???:
            Hm, if you can surprise my expectations of how you would fare in combat I'll let
            you live...for now anyway."
            strings[4] = @"         Mythor:
            I-I guess I'll t-try..."
            strings[5] = @"         ???:
            Huh...you guess? What kind of answer is that? Sheesh who did I decide to pick on
            today, such a sorry welp. Small too, like half my size haha!"
            strings[6] = @"         ???:
            Since you're so doubtful I'll go easy, just for you, out of the kindness of my heart,
            hahahaha! Even if you ARE associated with Hades he'd have a good laugh!"
            strings[7] = @"         Mythor:
            Errgghhh! I'll show you!...Yeah I'll surprise you! I need to!
            I need to see Kass again and tell him all about this!"
            strings[8] = @"         ???:
            Such gusto! Yes! This is what I was looking for in a match! Now I feel more comfortable.
            Geeze kid back there you almost made me want to make it quick for ya."
            strings[9] = @"         ???:
            Now show me what you've got! Hahaha!"
        }
    }
}
//Cam Shake Check
if sprite_index = spr_nephal_combat_idle01
{
    if instance_exists(obj_camera)
    {
        obj_camera.cam_shake = 0
    }
}
//Speed Up
if hp < maxhp*0.5
{
    timemax = 25
}

