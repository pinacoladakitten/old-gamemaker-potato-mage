/// @description Attack Initial
if enm_pause = 0
if phase = 2
{
    time = 0
    enm_pause = 1
    attk_numb = irandom_range(1,6)
    if hp > maxhp*0.75
    if attk_numb = 4 or attk_numb = 5
    {
        time = timemax
        enm_pause = 0
        alarm[0] = 1
        exit;
    }
    //Create Cast Effect
    instance_create(x+1,y,obj_eff_enm)
    //Cast Effect Number
    var casteff = instance_find(obj_eff_enm,instance_number(obj_eff_enm)-1);
    //Cast Effect Type
    if attk_numb = 1 or attk_numb = 4 or attk_numb = 6
    {casteff.spr = spr_cast_eff03}
    if attk_numb = 2 or attk_numb = 5
    {casteff.spr = spr_cast_eff01}
    if attk_numb = 3
    {casteff.spr = spr_cast_eff}
    image_index = 0;
    alarm[1] = 0.5*room_speed;
}

