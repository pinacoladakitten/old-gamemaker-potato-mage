/// @description Dia01
obj_potato_combat.pause_combat = 1
enm_pause = 1
instance_create(0,0,obj_battle_textbox_bign)
    with obj_battle_textbox_bign
    {
        maxstrings = 3
        strings[0] = @"         ???:
        I don't care what you say. His scent reeks all over you and it's painstakingly obvious you could either be em' 
        or be allied with him."
        strings[1] = @"         ???:
        Astaroth will be pleased to see either die by my hand."
        strings[2] = @"         Mythor:
        Please! I'm telling you the truth! I have no idea who this Hades guy is! If you would just listen-"
        strings[3] = @"         ???:
        Sorry to cut you off, but I don't have time for this. Just die."
    }

