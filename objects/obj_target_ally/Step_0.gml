/// @description Movement
if active = 1
if keyboard_check_pressed(global.keyb_left) or keyboard_check_pressed(global.keyb_up)
or gamepad_button_check_pressed(0, global.gpi_left) or gamepad_button_check_pressed(0, global.gpi_up)
{
    if draw = 1{audio_play_sound(snd_wheel_click01,10,false)}
    if !is_undefined(ds_list_find_value(allies,s+1))
    {
        do{s+=1}
        until(!is_undefined(ds_list_find_value(allies,s)))
    }
    alpha = 0
}

if active = 1
if keyboard_check_pressed(global.keyb_right) or keyboard_check_pressed(global.keyb_down)
or gamepad_button_check_pressed(0, global.gpi_right) or gamepad_button_check_pressed(0, global.gpi_down)
{
    if draw = 1{audio_play_sound(snd_wheel_click01,10,false)}
    if !is_undefined(ds_list_find_value(allies,s-1))
    {
        do{s-=1}
        until(!is_undefined(ds_list_find_value(allies,s)))
    }
    alpha = 0
}

///Finding Objects
if ally_numb > 0
{
    if !instance_exists(ds_list_find_value(allies,s))
    {
        if is_undefined(ds_list_find_value(allies,s+1))
        {
            do{s-=1}
            until(!is_undefined(ds_list_find_value(allies,s)))
        }
        if is_undefined(ds_list_find_value(allies,s-1))
        {
            do{s+=1}
            until(!is_undefined(ds_list_find_value(allies,s)))
        }
    }
    if !is_undefined(ds_list_find_value(allies,s))
    if instance_exists(ds_list_find_value(allies,s))
    {
        x = ds_list_find_value(allies,s).x;
        y = ds_list_find_value(allies,s).y;
    }
}

///Controls
if ally_numb > 0
{
    if !is_undefined(ds_list_find_value(allies,s))
    if !instance_exists(ds_list_find_value(allies,s))
    {
        if active = 1
        {
            if keyboard_check(global.keyb_left) or keyboard_check(global.keyb_up)
            or gamepad_button_check(0, global.gpi_left) or gamepad_button_check(0, global.gpi_up)
            {
                s+=1
            }
            if keyboard_check(global.keyb_right) or keyboard_check(global.keyb_down)
            or gamepad_button_check(0, global.gpi_right) or gamepad_button_check(0, global.gpi_down)
            {
                s-=1
            }
        }
        if keyboard_check(vk_nokey)
        {
            s+=1
        }
    }
}

///Misc
alpha = min(alpha+0.03,1)

