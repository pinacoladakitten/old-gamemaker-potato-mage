/// @description Main event
if active = 1
{
    draw_set_alpha(0.5)
    draw_rectangle_colour(0,0,1024,768,c_black,c_black,c_black,c_black,0)
    //Setup
    draw_set_font(font0);
    draw_set_halign(fa_left);
    draw_set_color(c_white);
    draw_set_alpha(1)
    //Portraits
    draw_sprite_ext(potato_port,0,200,200,1,1,0,potato_color,potato_alpha)
    draw_sprite_ext(kaas_port,0,800,50,1,1,0,kaas_color,kaas_alpha)
    //Textbox
    draw_sprite(spr_textbox,0,0,550)
    
    //The Text
    cstr = string_copy(strings[text],1,pos)
    draw_text_ext_transformed(40,560,string_hash_to_newline(cstr),-1,-1,1,1,0)
    pos = min(pos+2,255)
    if keyboard_check(ord("X"))
    {
        pos = min(pos+5,255)
    }
}

