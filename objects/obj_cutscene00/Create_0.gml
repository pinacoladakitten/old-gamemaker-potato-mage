//Start up
pos = 1
text = 0
potato_port = spr_potato_port_q
potato_alpha = 0
potato_color = c_black
kaas_port = spr_kaas_port_h
kaas_alpha = 0
kaas_color = c_black
active = 0
alarm[0] = 2*room_speed
if instance_exists(obj_potato_overw)
{
    with obj_potato_overw
    {
    pause = 1
    }
}

///Strings
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Kaas:
Alright! Mythor, if we wanna win this, you've gotta start your training today!
"
strings[1] = @"Kaas:
Common! We need to get you into that fighting spirit!
"
strings[2] =@"Mythor:
Did you even sign me up for the tournament? Wait, wait, did I even agree to doing 
this in the first place?!
"
strings[3] = @"Kaas:
Well since I've already signed you up that should give you the answer to both of 
those questions.
"
strings[4] =@"Mythor:
You did not...
"
strings[5] = @"Kaas:
Yes I did! Common, this is a once in a life time opportunity that will greatly 
improve your magic skills, making it essential for me to have you undergo. So, 
up to it, we've got a lot of training to cover!
"
strings[6] =@"Mythor:
Alright...
"

