/// @description Text Control
if active = 1
if keyboard_check_pressed(ord("Z"))
if string_length(strings[text]) < pos
    {
    pos = 0
    text = text+1
    }
    
    if (text > maxstrings)
    {
    instance_create(0,0,obj_trans_fade_in_next)
    instance_destroy();
    }
    
//Events
maxstrings = 6

///Events
if text = 0
{
potato_alpha = 0
potato_color = c_black
kaas_alpha = min(kaas_alpha+0.1,1)
kaas_color = max(kaas_color-1,-1)
}
if text = 2
{
potato_alpha = min(potato_alpha+0.1,1)
potato_color = max(potato_color-1,-1)
}
if text = 3
{
kaas_port = spr_kaas_port_s
}
if text = 4
{
potato_port = spr_potato_port_f
}
if text = 5
{
kaas_port = spr_kaas_port_h
}
if text = 6
{
potato_port = spr_potato_port_q
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
audio_play_sound(snd_text_speech,10,false)
}

