/// @description Setup
spawnTimer = 0;
spawnTimerMax = random_range(0.025*room_speed, 0.05*room_speed);
spawnSide = irandom_range(0, 1);
xx = x;
yy = 0;
if (instance_exists(obj_camera_ow)){
    if (spawnSide = 0){
        yy = irandom_range(obj_camera_ow.y+50, obj_camera_ow.y+100);
    }
    if (spawnSide = 1){
        yy = irandom_range(obj_camera_ow.y-50, obj_camera_ow.y-100);
    }
}

