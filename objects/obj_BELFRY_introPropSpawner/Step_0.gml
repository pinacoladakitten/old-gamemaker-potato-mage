/// @description Main
if (spawnTimer < spawnTimerMax)
{
    spawnTimer += 1;
}
else
{
    //Create Prop
    if (instance_number(obj_BELFRY_introProp) < 20){
        instance_create(xx, yy, obj_BELFRY_introProp);
    }
        //Reset
        spawnTimerMax = random_range(0.05*room_speed, 0.1*room_speed);
        spawnSide = irandom_range(0, 1);
        if (instance_exists(obj_camera_ow)){
            if (spawnSide = 0){
                yy = irandom_range(obj_camera_ow.y+50, obj_camera_ow.y+100);
            }
            if (spawnSide = 1){
                yy = irandom_range(obj_camera_ow.y-50, obj_camera_ow.y-100);
            }
        }
        spawnTimer = 0;
}

