/// @description Movement
//Rotation
if r >= 360 {r = 0}
r += 7
if r02 >= 360 {r02 = 0}
r02 += 4
//Z Axis
if z < 125
{
    z += 0.5
}
//Scale
if xsc <= 45
if shrink = 0
{
    xsc += 0.25
    ysc += 0.25
}
if shrink = 1
{
    xsc -= 8
    ysc -= 8
    z += 20
    zsph += 20
    alpha = max(alpha-0.025,0)
}
//Alpha
if alpha = 0
{
    instance_destroy();
}

d3d_set_lighting(true);
d3d_light_define_point(3,x,y,0,200,c_yellow)
d3d_light_enable(3,true);
d3d_set_lighting(false);

