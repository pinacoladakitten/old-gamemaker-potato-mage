/// @description Draw All
d3d_set_lighting(true);
var ss,cc,tex,tex01;
tex = sprite_get_texture(spr_darkness_swirl01,0);
tex01 = sprite_get_texture(spr_darkness_swirl02,0);
ss = sin(obj_camera_ow.direction*pi/180);
cc = cos(obj_camera_ow.direction*pi/180);

//Part 1
draw_set_alpha(1);
d3d_transform_add_rotation_x(r);
d3d_transform_add_scaling(xsc,ysc,zsc);
d3d_transform_add_translation(x,y,z);
d3d_draw_wall(0-1,0+20,20,0-1,0-20,-20,tex,1,1);
d3d_transform_set_identity();
//Part 2
d3d_transform_add_rotation_x(r*-1);
d3d_transform_add_scaling(xsc*0.5,ysc*0.5,zsc*0.5);
d3d_transform_add_translation(x,y,z);
d3d_draw_wall(0-1,0+20,20,0-1,0-20,-20,tex01,1,1);
d3d_transform_set_identity();

