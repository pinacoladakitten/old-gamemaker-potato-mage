/// @description Main Event
if fade = 0
{
    xsc = min(xsc+0.025,size);
    ysc = min(ysc+0.025,size);
    zsc = min(zsc+0.025,size);
}
else
{
    xsc = max(xsc-0.025,0);
    ysc = max(ysc-0.025,0);
    zsc = max(zsc-0.025,0);
    if xsc <= 0
    {instance_destroy();}
}
r += 1;
if r >= 360
{r = 0;}

