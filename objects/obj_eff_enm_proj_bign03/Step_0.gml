/// @description Movement
r += 10
z = min(z+2,zmax);
xsc = min(xsc+0.5,3);
ysc = min(ysc+0.5,3);
motion_set(dir,5);

///Lighting Effect
d3d_set_lighting(true);
d3d_light_define_point(3,x,y,0,2000,c_red)
d3d_light_enable(3,true);
d3d_set_lighting(false);

///Hitbox
if place_meeting(x,y,obj_potato_combat)
if hit = 0
{
    //Norm Dmg
    hp_potato -= 18
    //Hard Dmg
    if instance_exists(obj_eff_enm_bign03_hard)
    {hp_potato -= 18;}
    //On Hit
    obj_potato_combat.hit = 1
    hit = 1
}

///Misc

