/// @description Start up
pos = 1
text = 0
active = 0
port = spr_port
vol = 0
potato_port = spr_potato_port_f;
kass_port = spr_kaas_port_default;
potato_xsc = 1;
kass_xsc = -1;
port_alpha = 0;
pause = 1;

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
What's wrong with you Jace!! I-I don't understand what's going on, why won't you talk to
me!?
"
strings[1] = @"???:
I'm struggling to keep going! Without you I don't know what I would do! Please please
PLEASE! If only you could just say something, I-I just want to hear your voice!
"
strings[2] = @"???:
......
"
strings[3] = @"???:
Anyone!! If you can hear me!! WE need help!!
"

