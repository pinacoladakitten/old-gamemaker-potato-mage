/// @description Destroy
alpha -= 0.01
x += xdir
y += ydir
z += zdir
xsc -= 0.005
ysc -= 0.005
if alpha <= 0 
{
    instance_destroy();
}

