/// @description Draw All
if active = 1
{
    d3d_set_lighting(true);
    draw_set_alpha(alpha);
    var ss,cc,tex;
    tex = sprite_get_texture(spr,-1);
    tex01 = sprite_get_texture(spr_ow_shadow,-1);
    ss = sin(obj_camera_ow.direction*pi/180);
    cc = cos(obj_camera_ow.direction*pi/180);
    //OG
    d3d_transform_add_rotation_z(0);
    d3d_transform_add_translation(x,y,z);
    d3d_draw_wall(0+12*ss,0-12*cc,0+58,0-12*ss,0+12*cc,0+8,tex,1,1);
    d3d_draw_wall(0+20*ss,0+20*cc,0+3,0-20*ss,0-20*cc,0+0,tex01,1,1);
    d3d_transform_set_identity();
    //Clone 1
    draw_set_alpha(cl_alpha);
    d3d_transform_add_rotation_z(r);
    d3d_transform_add_translation(x,y+15,z);
    d3d_draw_wall(0+12*ss,0-12*cc,0+58,0-12*ss,0+12*cc,0+8,tex,1,1);
    d3d_draw_wall(0+20*ss,0+20*cc,0+3,0-20*ss,0-20*cc,0+0,tex01,1,1);
    d3d_transform_set_identity();
    //Clone 2
    d3d_transform_add_rotation_z(r);
    d3d_transform_add_translation(x,y-15,z);
    d3d_draw_wall(0+12*ss,0-12*cc,0+58,0-12*ss,0+12*cc,0+8,tex,1,1);
    d3d_draw_wall(0+20*ss,0+20*cc,0+3,0-20*ss,0-20*cc,0+0,tex01,1,1);
    d3d_transform_set_identity();
    draw_set_alpha(1);
    d3d_set_lighting(false);
}

