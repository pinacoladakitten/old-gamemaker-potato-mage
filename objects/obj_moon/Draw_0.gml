/// @description Draw
var ss,cc,tex;
tex = background_get_texture(background20);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
draw_set_alpha(1);
d3d_transform_add_scaling(xsc,ysc,zsc);
d3d_transform_add_translation(x,y,z);
d3d_draw_wall(0+10*ss,0+10*cc,20,0-10*ss,0-10*cc,0,tex,1,1)
d3d_transform_set_identity();
draw_set_alpha(1);

