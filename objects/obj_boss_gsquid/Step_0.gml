/// @description Time in Battle
if enm_pause = 0
if attack_commence = 0
{time = min(time+0.2,timemax)}

if time = timemax
if instance_number(obj_eff_enm) = 0
{alarm[0] = 1}

///Dead
if hp < 1
{
    //Stop Attacks
    with obj_eff_enm
    {instance_destroy();}
    //Set Sprite
    if spr != spr_enm_gsquid01_d
    {
        spr = spr_enm_gsquid01_d
        image_index = 0
    }
    //Pause
    enm_pause = 1
    //Reset Target
    with obj_target
    {s = 0}
    //Play sound on first frame
    if image_index = 0
    {audio_play_sound(snd_enm_dead01,10,false)}
    //Stop dead animation
    if image_index = 8
    {image_speed = 0}
    //Shrink and Rotate Enemy when dead
    xs = max(xs-0.015,0.15)
    ys = max(ys-0.015,0.15)
    zs = max(zs-0.015,0.15)
    r += 7
    //Enemy Explode/Actually Dead
    if xs = 0.15
    {
        instance_create(x,y,obj_eff_enm)
        obj_eff_enm.spr = spr_enm_exp
        ds_list_delete(enemies,self)
        enm_numb -= 1
        exp_obtained += 250
        instance_destroy();
    }
}

///Attack Commence
//Attack Animation Type
if attack_commence = 1
{
    //Pause
    enm_pause = 1;
    //Attack01
    if attk_numb = 1
    {
        //Attack Sprite
        spr = spr_enm_gsquid01_attack01
        //Do Attack
        if image_index = 2
        {
            //Create Attack
            instance_create(obj_potato_combat.x+1,obj_potato_combat.y,obj_eff_enm);
            with obj_eff_enm
            {spr = spr_slash_fire}
            //Attack Sound
            audio_play_sound(snd_enm_slash01,10,false);
        }
        //Attack End
        if image_index >= 6
        {
            spr = spr_enm_gsquid01;
            image_index = 0;
            attack_commence = 0;
            enm_pause = 0;
            attk_numb = 0;
        }
    }
    //Attack02
    if attk_numb = 2
    {
        //Attack Times and change sprite
        if spr = spr_enm_gsquid01
        {attack_times = irandom_range(2,4);
        spr = spr_enm_gsquid01_attack02;}
        //Change Sprite
        //Subtract Attack Times
        if image_index = 0
        {attack_times -= 1}
        //Create AOE
        if image_index = 3
        {instance_create(x,y,obj_enm_rat_aoemark01);}
        //End Attack
        if image_index >= 6
        if attack_times = 0
        {alarm[2] = 4*room_speed;
        image_index = 0;
        spr = spr_enm_gsquid01;
        attk_numb = 0;}
    }
    //Attack03
    if attk_numb = 3
    {
        //Attack Sprite
        if spr = spr_enm_gsquid01
        {attack_times = irandom_range(1,2);
        spr = spr_enm_gsquid01_attack02;}
        //Subtract Attack Times
        if image_index = 0
        {attack_times -= 1}
        //Do Attack
        if image_index = 2
        {
            //Create Attack
            instance_create(x+1,y,obj_eff_enm_gsquid01);
            //Attack Sound
            audio_play_sound(snd_enm_slash01,10,false);
        }
        //Attack End
        if image_index >= 6
        if attack_times = 0
        {
            spr = spr_enm_gsquid01
            image_index = 0
            attack_commence = 0
            enm_pause = 0
            attk_numb = 0
        }
    }
}
//Cancel Regardless
if attack_commence = 0
if attk_numb = 0
{
    if spr = spr_enm_gsquid01_attack02
    if image_index >= 6
    {
        spr = spr_enm_gsquid01
        image_index = 0
        enm_pause = 0
        attack_commence = 0
        attk_numb = 0
    }
}

///Reaction to Player Attacks
scr_Player_Attacks_ALL();

