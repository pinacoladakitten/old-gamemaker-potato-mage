/// @description Draw
d3d_set_lighting(true);
var ss,cc,tex;
tex = sprite_get_texture(sprite_index, -1);
ss = sin(obj_camera_ow.direction*pi/180);
cc = cos(obj_camera_ow.direction*pi/180);
draw_set_alpha(alpha);
d3d_transform_add_scaling(xsc, ysc, zsc);
d3d_transform_add_translation(x, y, z);
d3d_draw_wall(0+5*ss, 0+5*cc, 0+10, 0-5*ss, 0-5*cc, 0, tex, 1, 1);
d3d_transform_set_identity();
d3d_set_lighting(false);
draw_set_alpha(1);

