/// @description Setup
z = 0;
alpha = 1;
xsc = 0;
ysc = 0;
zsc = 0;
x += irandom_range(-4, 4);
y += irandom_range(-4, 4);
z += irandom_range(0, 2);
scaleRate = random_range(0.01, 0.05);
scaleMax = random_range(0.5, 1);
scaleFade = false;
if instance_exists(obj_potato_overw)
{z = obj_potato_overw.z-4;}

