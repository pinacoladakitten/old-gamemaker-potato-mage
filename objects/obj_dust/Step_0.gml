/// @description Main
z += 0.25;
if(scaleFade == false){
    xsc += scaleRate;
    if(xsc >= scaleMax){
        scaleFade = true;
    }
}
else{
    xsc -= scaleRate;
}
xsc = clamp(xsc, 0, scaleMax);
ysc = xsc;
zsc = xsc;

//Alpha check
if (xsc <= 0){
    instance_destroy();
}

