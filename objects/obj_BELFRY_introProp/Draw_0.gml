/// @description Draw Model
d3d_set_culling(true);
d3d_set_fog(true, __background_get_colour( ), -500, 500);
texture_set_repeat(true);
d3d_transform_add_scaling(1,1,1);
d3d_transform_add_rotation_z(r);
d3d_transform_add_translation(x,y,0);
d3d_set_lighting(false);
switch (propType){
    case 0:
        d3d_model_draw(prop_00 , 0, 0, 0, background_get_texture(belfy_grass01));
        break;
    case 1:
        d3d_model_draw(prop_00 , 0, 0, 0, background_get_texture(belfy_grass02));
        break;
    case 2:
        d3d_model_draw(prop_00 , 0, 0, 0, background_get_texture(belfy_rock01b));
        break;
}
d3d_set_lighting(false); 
d3d_transform_set_identity();
//Fog and Other Settings
d3d_set_fog(true, __background_get_colour( ), -500, 500);
d3d_set_culling(false);
draw_set_colour(-1);

