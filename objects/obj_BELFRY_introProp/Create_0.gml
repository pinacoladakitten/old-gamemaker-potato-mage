/// @description Model Create
prop_00 = d3d_model_create();
propType = irandom_range(0, 2);
r = -90;
//Model Load
switch (propType){
    case 0:
        d3d_model_load(prop_00, "maps/belfry/intro/grass01.d3d");
        break;
    case 1:
        d3d_model_load(prop_00, "maps/belfry/intro/grass02.d3d");
        break;
    case 2:
        d3d_model_load(prop_00, "maps/belfry/intro/rock.d3d");
        break;
}
//Rotate if rock
if (propType == 2){
    r = irandom_range(0, 360);
}

