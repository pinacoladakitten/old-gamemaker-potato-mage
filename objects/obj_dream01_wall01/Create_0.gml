/// @description Start up
pos = 1
text = 0
active = 0
port = spr_port
vol = 0

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"????:
A great wall stands before you, leaving a dead end.
"
strings[1] = @"????:
Hehehe, are you ready to give up now? You have nowhere to go.
"
strings[2] = @"????:
Unless! You find some sort of key that just might open the way
for you! Ever thought of that?
"
strings[3] = @"????:
A great wall stands before you-...wait, you already have the key!?
"
strings[4] = @"????:
Uh...this is awkward...um...shoot! See what I had planned initially
was to give you a hint, you know, to figure out a way around this
barrier.
"
strings[5] = @"????:
However, it seems that you've known what exactly to do already. This
perplexes me, but whatever, it's still going to-OOPS, almost spoiled
everything haha...eh um.
"

