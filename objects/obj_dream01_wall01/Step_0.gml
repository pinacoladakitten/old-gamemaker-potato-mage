/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(ord("Z"))
    if string_length(strings[text]) <= pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        if (text = 3)
        {
            pos = 0
            text = 0
            active = 0
            pause = 0
            instance_destroy();
        }
        if (text > maxstrings)
        {
            pos = 0
            text = 0
            active = 0
            pause = 0
            instance_destroy();
        }
    }
    maxstrings = 5
}

///Events
if active = 1
{
    pause = 1
}
//Portraits
if string_char_at(strings[text], 1) = "M"
{
    port = spr_potato_portrait_text
}
else
{
    port = spr_port
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

