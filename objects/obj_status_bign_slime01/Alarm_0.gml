/// @description Dot Hit
if hitslime = 1
{
    hp_potato -= 2 * obj_bign_attkStats.attkScaling;
    hitslime = 0
    //Erase from list
    with obj_combat_hud_all
    {
        ds_list_delete(pbufftype, ds_list_find_index(pbufftype, "bignslime"));
        pbuffs -= 1
    }
    instance_destroy();
}

