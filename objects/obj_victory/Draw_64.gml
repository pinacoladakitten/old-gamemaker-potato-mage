/// @description Draw Victory Screen
if drawgui = 1
{
    speedx = 0;
    speedy = 0;
    draw_set_alpha(0.5);
    draw_rectangle_colour(0,0,global.display_gui_width,global.display_gui_height,c_black,c_black,c_black,c_black,0);
    draw_set_alpha(alpha01);
    //Victory Portrait
    draw_sprite(spr_potato_vict_port,0,portx,porty);
    //Potato Exp Bar
    var potato_exp_barcol;
    potato_exp_barcol = make_color_rgb(255, 231, 157)
    draw_healthbar(portx-195,porty+220,portx+177,porty+250,(potato_exp/potato_max_exp)*100,c_black,potato_exp_barcol,potato_exp_barcol,0,false,false);
    draw_sprite(spr_potato_exp_bar,0,portx,porty+240);
    //Results
    draw_sprite(spr_results_vict,0,resultsx,resultsy);
    draw_set_alpha(alpha02);
    draw_set_halign(fa_center);
    draw_set_font(font1);
    draw_text(portx,porty+285,string_hash_to_newline("+"+string(exp_obtained)+" XP"));
    draw_set_font(font0);
    draw_set_halign(fa_left);
    draw_set_alpha(1);
}

