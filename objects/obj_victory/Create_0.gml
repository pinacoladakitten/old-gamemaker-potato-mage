/// @description Setup
if instance_exists(obj_potato_combat)
{
    x = obj_potato_combat.x+40
    y = obj_potato_combat.y+5
    obj_potato_combat.pause_combat = 1
    obj_potato_combat.time = 0
    obj_potato_combat.moving = 0
}
if instance_exists(obj_status_termina)
{
    obj_status_termina.count_pause = 1
}
speedx = random_range(0.5,0.75)
speedy = random_range(-0.25,0.25)
drawgui = 0
width = display_get_gui_width();
height = display_get_gui_height();
portx = width
porty = height/2
alpha01 = 0
alpha02 = 0
resultsx = 0
resultsy = 10
potato_exp_get = potato_exp
exp_add = 0
music_stop = 0
potato_exp_add = 0
alarm[0] = 3*room_speed
alarm[1] = 1*room_speed

///Audio
if audio_is_playing(combat_music) or audio_is_playing(combat_music_pt2)
if music_stop = 0
{
    audio_stop_sound(combat_music);
    audio_stop_sound(combat_music_pt2);
    audio_play_sound(snd_battle_victory,10,false);
    audio_sound_gain(snd_battle_victory,0.55,0);
}

