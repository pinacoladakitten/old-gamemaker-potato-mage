/// @description Misc
//Level Up
if potato_exp >= potato_max_exp
{
    instance_create(portx+225,porty+200,obj_encourage_words);
    obj_encourage_words.spr = spr_level_up;
    if !audio_is_playing(snd_exp_levelup)
    {
        audio_play_sound(snd_exp_levelup,10,false);
    }
    potato_exp -= potato_max_exp;
    potato_max_exp *= 1.5;
    potato_skillp += 1;
}

///Movement
x += speedx
y += speedy
//Camera Movement
with obj_camera
{
    x = obj_victory.x
    y = obj_victory.y
    camx = 0
    camy = 0
    camxto = obj_potato_combat.x
    camyto = obj_potato_combat.y
}
//Portrait Fade
if drawgui = 1
{
    alpha01 = min(alpha01+0.006,1)
    portx = max(portx-6,width/2)
    resultsx = min(resultsx+3,width/8)
    if audio_is_playing(snd_battle_victory)
    if !audio_is_playing(snd_battle_victory02)
    {
        audio_sound_gain(snd_battle_victory,0,10*room_speed);
        audio_play_sound(snd_battle_victory02,10,true);
        audio_sound_gain(snd_battle_victory02,0.55,0);
    }
}
if portx = width/2
{
    alpha02 = min(alpha02+0.01,1)
}
//Confirm Fade In
if drawgui = 1
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    {
        if alpha02 = 1
        if exp_add = 0
        {
            exp_add = 1
        }
        if portx >= max(portx-6,width/2)
        {
            alpha01 = 1
            portx = width/2
            resultsx = width/8
            alpha02 = 1
        }
        //Fade Out
        if exp_add = 2
        {
            instance_create(0,0,obj_trans_fade_in_dung);
            audio_sound_gain(snd_battle_victory02,0,15*room_speed);
            exp_add = 3
        }
    }
    //Experience Add
    if exp_add = 1
    {
        potato_exp_add = min(potato_exp_add+(potato_max_exp/50), exp_obtained);
        if potato_exp_add <= exp_obtained
        {
            potato_exp += (potato_max_exp/50)
        }
        //Exp Add Sound
        if potato_exp_add < exp_obtained
        if !audio_is_playing(snd_exp_add)
        {
            audio_play_sound(snd_exp_add,10,false);
        }
        //Exp Add Done
        if potato_exp_add = exp_obtained
        {
            exp_add = 2
        }
    }
}
//Skip Victory
if drawgui = 0
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    {
        drawgui = 1
    }
}

///Pause
enm_pause = 1
if instance_exists(obj_potato_combat)
{
    obj_potato_combat.pause_combat = 1
}
if instance_exists(obj_comm_wheel_potato)
{
    obj_comm_wheel_potato.pause_com = 1
}

