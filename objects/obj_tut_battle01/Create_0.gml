active = 0
pos = 1
text = 0
potato_port = spr_potato_port_q
potato_alpha = 0
potato_color = c_black
kaas_port = spr_kaas_port_default
kaas_alpha = 0
kaas_color = c_black
alarm[0] = 1*room_speed

///Strings
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Mythor:
What the heck are these?
"
strings[1] = @"Kaas:
Well, they're dummies silly! I mean you obviously agreed to train, right? 
So I went through the effort of creating these guys just for this occasion
because I knew you were totally committed to winning this tourney. 
"
strings[2] = @"Mythor:
You seriously went through this much effort? Well, I appreciate it really, but
honestly I never wanted to enter this tourney in the first pla-
"
strings[3] = @"Kaas:
Ok, anyway, here's what you need to focus on when fighting opponents Mythor.
You see that icon titled 'Attack' on your command wheel?
"
strings[4] = @"Mythor:
My command wheel? Um...ok? I'll just picture one in my head.
"
strings[5] = @"Kaas:
Great, now select the command by pressing Z, once selected pick whatever enemy you 
want to attack, then simply hit Z again to execute the attack.
"
strings[6] = @"Kaas:
You're not supposed to see this lol.
"
strings[7] = @"Kaas:
Awesome! See Mythor? You were born for this!
"
strings[8] = @"Mythor:
All I did was throw a potato at one of the dummies. Is this really all
it takes?
"
strings[9] = @"Kaas:
Yup...well, there's still more to cover, but you've gotten the most important
out of the way. Now I want you to look at your stamina bar, it's to the right of
your command wheel.
"
strings[10] = @"Kaas:
This is basically your energy to do things. So if you want to attack again you
would need to wait for the bar to fill up. 
"
strings[11] = @"Mythor:
So I'm completely defensless until this supposed 'bar' fills up again? Why can't
I just attack immediately again? Seriously, I don't get these mechanics.
"
strings[12] = @"Kaas:
Well, you're not completely defenseless actually. While your stamina is recharging
you could perform a dodge by pressing C.
"
strings[13] = @"Kaas:
But since none of these dummies know how to attack I'll find a more...
suited opponent for ya.
"

