/// @description Text Control
if active = 1
if keyboard_check_pressed(ord("Z"))
if string_length(strings[text]) < pos
    {
    pos = 0
    text = text+1
    }
    
    if (text > maxstrings)
    {
    instance_destroy();
    }
    
//Events
maxstrings = 5

///Events
if text = 6
{
active = 0
}
if text = 7
{
active = 1
}

if active = 1
{
with obj_comm_wheel_potato{pause_com = 1}
}
if active = 0
{
with obj_comm_wheel_potato{pause_com = 0}
}

///Check triggers
if instance_exists(obj_potato_poj)
{
alarm[1] = 1*room_speed
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

