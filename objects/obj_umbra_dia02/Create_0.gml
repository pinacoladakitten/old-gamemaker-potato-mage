/// @description Start up
pos = 1
text = 0
active = 0

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
...I am a stranger...yes...but...I do not want to be one
...to you I mean...
"
strings[1] = @"???:
...all it takes... is for us to bond with one another...
"
strings[2] = @"???:
...yes yes...to share our feelings...talk about life to make us feel a little less 
frightened of this world.
"
strings[3] = @"???:
...frightened...yes...but to what are you so afraid of? Loneliness? Depression? Pain!? 
Feeling stuck in a world of hatred all because of something so unforgivably unfair!?
"
strings[4] = @"???:
Please!...talk to me...you seem to want to let it all out
...I can help you...if you would just talk to me!...
"

