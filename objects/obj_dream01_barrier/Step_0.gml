/// @description Movement & Alpha
//Movement
z += zspeed
z1 -= zspeed
z2 += zspeed
    if zcycle1 = 0
    {
        zspeed = min(zspeed+0.001,0.05)
    }
    if zcycle1 = 1
    {
        zspeed = max(zspeed-0.001,-0.05)
    }
    if z >= 5
    {
        zcycle1 = 1
    }
    if z <= -5
    {
        zcycle1 = 0
    }
//Alpha
if instance_exists(obj_potato_overw)
if fade = 1
{
    if instance_exists(col)
    {
        with(col)
        {
            instance_destroy();
        }
    }
    if distance_to_object(obj_potato_overw) < 65
    or instance_exists(obj_skybox_small01)
    {
        alpha = max(alpha-0.01,0)
    }
    else
    {
        alpha = min(alpha+0.01,0.70)
    }
}

