/// @description Draw NPC
d3d_set_lighting(true);
var ss,cc,tex;
tex = sprite_get_texture(sprite_index,-1);
tex03 = sprite_get_texture(spr_talk,-1);
tex01 = sprite_get_texture(spr_ow_shadow,-1);
ss = sin(obj_camera_ow.direction*pi/180);
cc = cos(obj_camera_ow.direction*pi/180);

d3d_transform_add_rotation_z(angle01);
d3d_transform_add_translation(x,y,z);
d3d_draw_wall(0+7*ss,0+7*cc,25,0-7*ss,0-7*cc,0,tex,1,1);
d3d_draw_wall(0+15*ss,0+15*cc,5,0-15*ss,0-15*cc,0,tex01,1,1);
d3d_transform_set_identity();

if place_meeting(x,y,obj_potato_overw)
if pause = 0
{
    d3d_set_lighting(false);
    d3d_transform_add_rotation_z(angle);
    d3d_transform_add_translation(x,y,z);
    d3d_draw_wall(0,0+3,34,0,0-3,28,tex03,1,1)
    d3d_transform_set_identity();
}


