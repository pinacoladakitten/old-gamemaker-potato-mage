/// @description Angles
angle+=2
if angle = 360
{angle = 0}
//Turn Right
if (obj_potato_overw.y < y)
if distance_to_object(obj_potato_overw) < 20
{angle01 = min(angle01+10,180);}
//Turn Left
if (obj_potato_overw.y > y)
if distance_to_object(obj_potato_overw) < 20
{angle01 = max(angle01-10,0);}
//Turn Default
if distance_to_object(obj_potato_overw) > 20
{angle01 = max(angle01-10,0);}

///Text Control
if active = 1
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if string_length(strings[text]) < pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        if (text > maxstrings)
        {
            pos = 0;
            text = 0;
            active = 0;
            pause = 0;
        }
    }
    maxstrings = 0;
}

///Events
if active = 1
{pause = 1;}
//Portraits
if active = 1
if text < maxstrings
{
    if string_char_at(strings[text], 1) = "M"
    {port = spr_potato_portrait_text;}
    if string_char_at(strings[text], 1) = "S"
    {port = spr_port;}
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{audio_play_sound(snd_text_speech,10,false);}

