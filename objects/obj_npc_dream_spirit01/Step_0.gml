/// @description Angles
angle+=2
if angle = 360
{
    angle = 0
}
if (obj_potato_overw.y < y)
if distance_to_object(obj_potato_overw) < 20
{
    angle01 = min(angle01+10,180)
}
if (obj_potato_overw.y > y)
if distance_to_object(obj_potato_overw) < 20
{
    angle01 = max(angle01-10,0)
}
if distance_to_object(obj_potato_overw) > 20
{
    angle01 = max(angle01-10,0)
}

///Text Control
if active = 1
{
    if keyboard_check_pressed(ord("Z"))
    if string_length(strings[text]) < pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        if text != 6 or text != 7 or text != 9
        {
            //Unzoom
            if text = 9
            {
                if obj_camera_ow.zoom = 130
                {
                    pos = 0
                    text = text+1
                }
            }
            else
            {
                pos = 0
                text = text+1
            }
        }
        //Zoom
        if text = 6
        {
            if obj_camera_ow.zoom = 80
            {
                pos = 0
                text = text+1
            }
        }
        //Fade
        if text = 7
        {
            if alpha = 0
            {
                pos = 0
                text = text+1
            }
        }
        if (text > maxstrings)
        {
            pos = 0
            text = 0
            active = 0
            pause = 0
            instance_destroy();
        }
    }
    maxstrings = 10
}

///Events
if active = 1
{
    pause = 1
}
if text = 6
{
    with obj_camera_ow
    {
        x = obj_potato_overw.x+zoom
        obj_camera_ow.zoom = max(obj_camera_ow.zoom-(obj_camera_ow.zoom)*0.05,80)
    }
}
if text = 7
{
    alpha = max(alpha-0.05,0)
}
if text >= 8
{
    with obj_camera_ow
    {
        x = obj_potato_overw.x+zoom
        obj_camera_ow.zoom = min(obj_camera_ow.zoom+(obj_camera_ow.zoom)*0.01,130)
    }
}
//Portraits
if string_char_at(strings[text], 1) = "M"
{
    port = spr_potato_portrait_text
}
else
{
    port = spr_npc_spirit01_port
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

