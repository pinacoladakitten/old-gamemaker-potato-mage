if active = 1
{
    with obj_camera_ow
    {
        if zoom > 140
        {
            zoom = max(zoom-(zoom-140)*0.05,140)
        }
        if zoom < 140
        {
            zoom = min(zoom+(140-zoom)*0.05,140)
        }
    }
}

