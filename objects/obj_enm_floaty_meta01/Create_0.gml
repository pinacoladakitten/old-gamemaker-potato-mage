/// @description Setup
image_speed = 0.125
draw_set_alpha_test(true)
draw_set_alpha_test_ref_value(40)
hp = 210
attack = 0
time = 0
timemax = irandom_range(35,40);
explode = 0
enm_pause = 0
attk_numb = 0
spr = spr_enm_floaty_meta01
enm_numb += 1
xs = 1
ys = 1
zs = 1
r = 0
alarm[2] = 2*room_speed

///Defensive Vars
//(0.1 = 10% dmg reduction, so on...)
physical_defense = 0.12;
fire_defense = -0.05;
nature_defense = 0.1;
magic_defense = 0.1;
lightning_defense = 0;
dark_defense = 1;
earth_defense = 0;

