/// @description Event
if active = 1
{
    if camevt01 = 1
    {
        with obj_camera_ow
        {
            x = min(x+(obj_firespirit.x+450-x)*0.025,obj_firespirit.x+450)
            camx = max(camx-(camx-obj_firespirit.x)*0.025,obj_firespirit.x)
            camy = min(camy-(camy-obj_firespirit.y)*0.025,obj_firespirit.y)
            camzto = min(camzto+0.05,45)
            camzfrom = max(camzfrom-0.05,-3)
        }
    }
    if camevt01 = 2
    {
        with obj_camera_ow
        {
            camx = max(camx-(camx-obj_firespirit.x)*0.05,obj_firespirit.x)
            camy = min(camy-(camy-obj_firespirit.y)*0.05,obj_firespirit.y)
            camzto = min(camzto+(45-camzto)*0.05,45)
            camz = min(camz+(10-camz)*0.05,25)
            camzfrom = min(camzfrom+(35-camzfrom)*0.05,35)
        }
    }
    if camevt01 = 3
    {
        with obj_camera_ow
        {
            camx = max(camx-(camx-obj_firespirit.x)*0.05,obj_firespirit.x)
            camy = min(camy-(camy-obj_firespirit.y)*0.05,obj_firespirit.y)
            camzto = max(camzto-(camzto-0)*0.05,20)
            camz = max(camz-(camz-0)*0.05,0)
            zoom = min(zoom+(150-zoom)*0.05,150)
            x = obj_potato_overw.x+zoom
            y = obj_potato_overw.y
        }
    }
    if camevt01 = 4
    {
        with obj_camera_ow
        {
            zoom = max(zoom-1,110)
            camy = min(camy+(camy-obj_firespirit.y)*0.05,obj_firespirit.y)
            x = obj_potato_overw.x+zoom
            y = obj_firespirit.y
        }
    }
    if camevt01 = 5
    {
        with obj_camera_ow
        {
            zoom = 200
            camzto = 20
            camzfrom = 34
            camz = 0
            camxto = obj_potato_overw.x
            camyto = obj_potato_overw.y
            x = obj_potato_overw.x+zoom
            y = obj_potato_overw.y
        }
    }
    if fireend = 1
    {
        alarm[3] = 2*room_speed
        fireend = 0
    }
}

