/// @description Spawn Rain
if pause = 0
{
    active = 1
    obj_ow_enm_floaty01.active = 1
    obj_ow_enm_floaty02.active = 1
}
if active = 1
{
    if obj_potato_overw.z = 5
    if pause = 0
    {
        if overw_music != snd_dream_overw02
        {
            audio_sound_gain(overw_music,0,10*room_speed);
        }
        //Rain
        if !instance_exists(obj_raindrops)
        if !instance_exists(obj_dream_rain_evt01)
        {
            instance_create(obj_potato_overw.x,obj_potato_overw.y,obj_dream_rain_evt01);
        }
    }
}

