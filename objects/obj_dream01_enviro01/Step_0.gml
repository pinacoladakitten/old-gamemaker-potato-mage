if active = 1
if pause = 0
{
    if audio_sound_get_gain(overw_music) = 0
    {
        audio_stop_sound(overw_music);
        overw_music = snd_dream_overw02
    }
    if overw_music = snd_dream_overw02
    {
        if !audio_is_playing(overw_music)
        {
            audio_play_sound(overw_music,10,true);
            audio_sound_gain(overw_music,0.55,0);
            active = 0
        }
    }
}

