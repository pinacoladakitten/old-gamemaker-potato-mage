/// @description Room Check
if room = rm_overw_sewdung01
{
    pause = 1;
    obj_potato_overw.sprite_index = spr_potato_ow_idle_r;
    obj_potato_overw.x = -80;
    obj_potato_overw.y = 186;
    with obj_evt_sewerprev02
    {instance_destroy();}
    alarm[0] = 2*room_speed;
    with obj_camera_ow
    {
        vsp = obj_potato_overw.vsp;
        x = obj_potato_overw.x+zoom;
        y = obj_potato_overw.y;
        hsp = obj_potato_overw.hsp;
        camxto = obj_potato_overw.x;
        camyto = obj_potato_overw.y;
        //Cam Z Axis
        camz = 17;
    }
}

