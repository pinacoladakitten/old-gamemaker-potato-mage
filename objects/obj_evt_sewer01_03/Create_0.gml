/// @description Setup
active = 0;
pos = 1;
text = 0;
active = 0;
port = spr_port;
vol = 0;
potato_port = spr_potato_port_f;
kass_port = spr_kaas_port_default;
potato_xsc = 1;
kass_xsc = -1;
port_alpha = 0;
pause = 1;
//Camera
accel01 = 0;
accel02 = 0;
//Event Pause
evt = 0;

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Sewer Dweller:
Wait that all just happened!? So they're really all gone!?
"
strings[1] = @"Sewer Dweller, Marine:
Yes! I know I don't want to believe it either, but I was the only one who managed to escape.
"
strings[2] = @"Sewer Dweller:
This is...-holy cow, I-I can completely agree with you in not believing it, plus I was gone
for all of this. I didn't even want to leave, but now it's become more of a blessing in
disguise more than anything.
"
strings[3] = @"Sewer Dweller:
Still, the whole damned sewers, this is just absurd, who would do such a thing to us. 
Everyone, gone. I don't really feel anything yet, but I guess my feelings have yet to
hit me.
"
strings[4] = @"Sewer Dweller:
So, what happens now? We're all here, you guys said nobody else made it...uh-um we should
probably leave.
"
strings[5] = @"Mythor:
I-I can't leave, I made a promise to Jace, you guys need to leave-or at least stay out
here while I'm gone. I'm sure this is the only safe part of the sewers. 
"
strings[6] = @"Sewer Dweller, Marine:
Mythor, think about this, are you certain you'll be able to handle all of this. Maybe we
should get help.
"
strings[7] = @"Sewer Dweller:
Oh yes! I'm sure you can ask The Great Kassadin to help us! He can get this job done in
no problem for certain!
"
strings[8] = @"Sewer Dweller, Marine:
Mythor you know The Great Kassadin!? I've heard stories about him, didn't think he was
actually real, but if you know him I can believe he'll get the job done easily! Right?
"
strings[9] = @"Mythor:
No!...Sorry, I can't ask him, urm well, I won't ask him more like. It's complicated right now, so
I need to do this alone. Please guys, just let me do this, it's more of a personal thing
more than anything.
"
strings[10] = @"Sewer Dweller, Marine:
...Mythor...just come back to us safely then, I believe in you because Jace believed in
you. He obviously saw something in you.
"
strings[11] = @"Sewer Dweller:
I'm sure an apprentice of Kassadin is capable as well, he should proud of your
stride to aid us and take on such a big task.
"
strings[12] = @"Mythor:
Y-yeah, I guess you're right, well I'm gonna be taking my leave now. 
"
strings[13] = @"Sewer Dweller, Marine:
Be safe Mythor, again, come back safe ok?
"
strings[14] = @"Mythor:
I will, you guys be safe too. See you when I get back!
"

