/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if string_length(strings[text]) <= pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        if (text > maxstrings)
        {
            active = 0;
            alarm[1] = 1*room_speed;
            pause = 0;
        }
    }
    maxstrings = 14;
}

///Events
if (text < maxstrings)
{pause = 1;
evt = 1;}
//Portraits
if active = 1
if (text < maxstrings)
{
    if string_char_at(strings[text], 1) = "M"
    {port = spr_potato_portrait_text;}
    if string_char_at(strings[text], 1) = "S"
    {port = spr_port;}
}
//Events for reals

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false);
}

