/// @description Movement or Detected
if active = 1
if !instance_exists(obj_trans_fade_out)
if pause = 0
{
    if distance_to_object(obj_potato_overw) < 100
    if detected = 0
    if obj_inventory_ow.active = 0
    {
        detected = 1
        instance_create(x,y,obj_enm_detected);
        alarm[0] = 0.5*room_speed
    }
    
    if detected = 0
    {
        x += hsp
        y += vsp
    }
    
    if obj_inventory_ow.active = 1
    {
        move_towards_point(obj_potato_overw.x,obj_potato_overw.y,0)
    }
}
else
{
    hsp = 0;
    vsp = 0;
    motion_set(0, 0);
}

///Active Fade
if active = 1
{
    alpha = min(alpha+0.01,1)
}
else
{
    alpha = max(alpha-0.01,0)
}

///Collision
if place_meeting(x,y,obj_col)
{
    move_bounce_solid(false);
}
if place_meeting(x,y,obj_enm_col)
{
    move_bounce_solid(false);
}

