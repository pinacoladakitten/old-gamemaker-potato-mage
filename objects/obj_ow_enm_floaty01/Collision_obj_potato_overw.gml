/// @description Commence Battle
if obj_inventory_ow.active = 0
if active == 1
if pause == 0
if hit == false
{
    with(obj_spawn)
    {
        spawn1 = irandom_range(1,1);
        spawn2 = irandom_range(1,1);
        spawn3 = irandom_range(1,3);
        spawn4 = irandom_range(1,4);
        spawn5 = irandom_range(1,5);
        
        enemy[0] = obj_enm_floaty01;
        if spawn2 = 1 {enemy[1] = obj_enm_floaty01;}
        if spawn3 = 1 {enemy[2] = obj_enm_floaty01;}
        if spawn4 = 1 {enemy[3] = obj_enm_floaty01;}
        if spawn5 = 1 {enemy[4] = obj_enm_floaty01;}
    }
    ///Battle Trans
    potato_battle_room = rm_battle_dream3D;
    instance_create(512,384,obj_trans_battle);
    //Music
    combat_music = snd_dream_battle;
    combat_music_pt2 = snd_dream_battle;
    //Hit
    hit = true;
}

