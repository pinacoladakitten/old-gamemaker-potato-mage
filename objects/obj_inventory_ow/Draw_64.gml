/// @description Main Menu
d3d_set_lighting(false);
if active = 1
{
    if instance_exists(obj_potato_overw){draw_background_stretched(bg_pause,0,0,global.display_gui_width,global.display_gui_height);} //Background
    if skill_screen = 0{draw_sprite(spr_column_pause,-1,960+xx,0+yy);} //Large Column BG
    draw_set_font(font0);
    if pause_screen = 1
    {
        //Inventory Screen Selections
        draw_set_halign(fa_center);
        draw_text(960+xx01,320+yy01,string_hash_to_newline("Skills"));
        draw_text(960+xx01,410+yy01,string_hash_to_newline("Actions"));
        draw_text(960+xx01,500+yy01,string_hash_to_newline("Magic"));
        draw_text(960+xx01,590+yy01,string_hash_to_newline("Items"));
        draw_text(960+xx01,680+yy01,string_hash_to_newline("Exit"));
        //Char. Inv
        for (c = 0; c < ds_list_size(allies_ow); c += 1)
        {
            //Mythor
            if ds_list_find_value(allies_ow, c) = 0
            {
                draw_set_halign(fa_left);
                draw_sprite(spr_potato_portrait,-1,180+xx02,130+(150*c)+yy02);
                draw_text(260+xx02,70+(150*c)+yy02,string_hash_to_newline("Mythor:"));
                draw_text(260+xx02,110+(150*c)+yy02,string_hash_to_newline("HP:"));
                draw_text(310+xx02,110+(150*c)+yy02,string_hash_to_newline(string(hp_potato)+"  /"+string(hp_potato_max)));
                draw_text(260+xx02,150+(150*c)+yy02,string_hash_to_newline("MP:"));
                draw_text(310+xx02,150+(150*c)+yy02,string_hash_to_newline(string(mp_potato)+"  /"+string(mp_potato_max)));
                draw_text(430+xx02,110+(150*c)+yy02,string_hash_to_newline("XP:"));
                draw_text(480+xx02,110+(150*c)+yy02,string_hash_to_newline(string(round(potato_exp))+"  /"+string(round(potato_max_exp))));
                draw_set_colour(c_white);
                draw_text(430+xx02,150+(150*c)+yy02,string_hash_to_newline("Skill pts:"));
                draw_text(540+xx02,150+(150*c)+yy02,string_hash_to_newline(potato_skillp));
                //Equip
                draw_sprite(potato_equip_sprite,0,650+xx02,145+(150*c)+yy02);
                draw_sprite(spr_inv_equip_icon,-1,650+xx02,100+(150*c)+yy02);
            }
        }
    }
}

///Menu Cursor Positions
if active = 1
{
    if pause_screen = 1
    {
        if i = -1 //Exit
        {
            draw_sprite(spr_inventory_select,-1,990+xx01,695+yy01);
        }
        if i = 0 //Items
        {
            draw_sprite(spr_inventory_select,-1,990+xx01,605+yy01);
        }
        if i = 1 //Magic
        {
            draw_sprite(spr_inventory_select,-1,990+xx01,515+yy01);
        }
        if i = 2 //Actions
        {
            draw_sprite(spr_inventory_select,-1,990+xx01,425+yy01);
        }
        if i = 3 //Skills
        {
            draw_sprite(spr_inventory_select,-1,990+xx01,335+yy01);
        }
        if i = 4 //Equip
        {
            draw_sprite(spr_inventory_select,-1,680+xx02,165+yy02);
        }
    }
    if weapon_screen = 1
    {
        draw_sprite(spr_inventory_select,-1,200+xxw02,336+yyw02);
    }
    if item_screen > 0
    {
        draw_sprite(spr_inventory_select,-1,200+xxw02,336+yyw02);
    }
    if magic_screen = 1
    {
        draw_sprite(spr_inventory_select,-1,200+xxw02,336+yyw02);
    }
}

///Weapon Screen & Weapon Descriptions
if active = 1
{
    //Weapon slots draw
    if weapon_screen = 1
    {
    for (w = 0; w < maxwep; w += 1)
        {
            if inventorywep[w] = 0{draw_sprite(spr_wep_slot,0,160+xxw02,(w * 84)+336+scrolld+yyw02);}
            if inventorywep[w] = 1{draw_sprite(spr_inv_dagger,0,160+xxw02,(w * 84)+336+scrolld+yyw02);}
            if inventorywep[w] = 2{draw_sprite(spr_inv_dark_wood_stick,0,160+xxw02,(w * 84)+336+scrolld+yyw02);}
            if inventorywep[w] = 3{draw_sprite(spr_inv_handscythe01,0,160+xxw02,(w * 84)+336+scrolld+yyw02);}
        }
    }
    
    //Title
    if weapon_screen = 1
    {
    draw_set_halign(fa_center);
    draw_set_font(font1)
    draw_text(960+xxw,125+yyw,string_hash_to_newline("Equip"))
    draw_set_halign(fa_left);
    }
    //Weapon Description
    if weapon_screen = 1
    {
        if inventorywep[pw] = 1 //Dagger
        {
        draw_sprite(spr_dagger_item,0,440+xxw01,192+yyw01)
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Dagger"))
        draw_set_font(font0)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        A basic dagger for all of your dagger needs. 
        Daggers are used by every adventurer who would 
        like to add a small layer of protection just in-case."))
        
        draw_text(220+xxw01,580+yyw01,string_hash_to_newline("Physical Attack: +3#Magic attack: +0"))
        draw_text_colour(440+xxw01,580+yyw01,string_hash_to_newline("+" + string(round(potato_dexterity*1.5))),c_yellow,c_yellow,c_yellow,c_yellow,1)
        }
        
        if inventorywep[pw] = 2 //Dark Stick
        {
        draw_sprite(spr_dark_wood_stick_item,0,470+xxw01,192+yyw01)
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Dark Stick"))
        draw_set_font(font0)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        A wooden stick made from a worn lifeless tree.
        Due to this, it offers little energy to enhance 
        magic, however it still manages to do its job for 
        mages who don't have any other resource."))
        
        draw_text(220+xxw01,580+yyw01,string_hash_to_newline("Physical Attack: +0#Magic attack: +5"))
        draw_text_colour(440+xxw01,580+yyw01,string_hash_to_newline("#+" + string(round(potato_intellect*1.5))),c_yellow,c_yellow,c_yellow,c_yellow,1)
        }
        
        if inventorywep[pw] = 3 //Hand Scythe
        {
        draw_sprite(spr_handscythe_item01,0,440+xxw01,192+yyw01)
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Hand Scythe"))
        draw_set_font(font0)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        A tool used for gardening, not expected to be
        used in combat. However this scythe possesses
        an unusual aura, hence the blade having a faded
        green tint. It could be imbued from the nature of
        its use in the past.
         
        Effect: Slightly increases Haste."))
        
        draw_text(220+xxw01,590+yyw01,string_hash_to_newline("Physical Attack: +3#Magic attack: +2#Haste: +4"))
        draw_text_colour(440+xxw01,590+yyw01,string_hash_to_newline("+" + string(round(3+(potato_dexterity*0.25)+(potato_haste*1)))),c_yellow,c_yellow,c_yellow,c_yellow,1)
        draw_text_colour(440+xxw01,590+yyw01,string_hash_to_newline("#+" + string(round(2+(potato_intellect*0.25)))),c_yellow,c_yellow,c_yellow,c_yellow,1)
        }
    }
}

///Weapon Equipped
if weapon_screen = 1
{
    draw_sprite(spr_wep_equip,0,160+xxw02,336+scrolld-equipped+yyw02);
}
if pause_screen = 1
{
    pw = pw_equip;
    scrolld = scroll_equip;
}

///Skill Tree Screen
if active = 1
{
    //Title
    if skill_screen = 1
    {
        //Mythor
        draw_set_halign(fa_center);
        draw_set_font(font1)
        draw_set_halign(fa_left);
        //Stats
        draw_sprite(spr_column_pause03,-1,1060,0)
        draw_set_halign(fa_center)
        draw_set_font(font1)
        draw_text(1060,125,string_hash_to_newline("Level Up"))
        draw_set_font(font0)
        draw_text(1060,200,string_hash_to_newline("Mythor: " + string(potato_skillpredict)))
        draw_sprite(spr_potato_portrait,-1,1060,320)
        draw_set_halign(fa_center)
        //Stat Fonts
        if potato_vitality = potato_vit_predict{draw_text(1060,400,string_hash_to_newline("Vitality: " + string(potato_vit_predict)))}
        else{draw_text_colour(1060,400,string_hash_to_newline("Vitality: " + string(potato_vit_predict)),c_yellow,c_yellow,c_yellow,c_yellow,1)}

        if potato_haste = potato_haste_predict{draw_text(1060,450,string_hash_to_newline("Haste: " + string(potato_haste_predict)))}
        else{draw_text_colour(1060,450,string_hash_to_newline("Haste: " + string(potato_haste_predict)),c_yellow,c_yellow,c_yellow,c_yellow,1)}

        if potato_dexterity = potato_dex_predict{draw_text(1060,500,string_hash_to_newline("Dexterity: " + string(potato_dex_predict)))}
        else{draw_text_colour(1060,500,string_hash_to_newline("Dexterity: " + string(potato_dex_predict)),c_yellow,c_yellow,c_yellow,c_yellow,1)}

        if potato_intellect = potato_int_predict{draw_text(1060,550,string_hash_to_newline("Intellect: " + string(potato_int_predict)))}
        else{draw_text_colour(1060,550,string_hash_to_newline("Intellect: " + string(potato_int_predict)),c_yellow,c_yellow,c_yellow,c_yellow,1)}
        draw_text(1060,600,string_hash_to_newline("Confirm"))
        //Stat Icons
        draw_sprite(spr_lvl_vit,-1,948,400)
        draw_sprite(spr_lvl_haste,-1,948,450)
        draw_sprite(spr_lvl_dex,-1,948,500)
        draw_sprite(spr_lvl_int,-1,948,550)
        draw_set_halign(fa_left)
        //Trees
        draw_sprite(spr_column_tree_pure,-1,190,0)
        draw_sprite(spr_column_tree_dark,-1,770,0)   
        draw_sprite(spr_column_tree_sorcerer,-1,480,0)
        draw_set_font(font2)
        draw_set_halign(fa_center)
        draw_text(480,75,string_hash_to_newline("Volatile"))
        draw_text(770,75,string_hash_to_newline("Gloom"))
        draw_text(190,75,string_hash_to_newline("Pure Potato"))
        draw_set_halign(fa_left)
        //Skills
        //Controls
        draw_set_font(font7)
        draw_sprite(spr_ord_a,0,255,700)
        draw_sprite(spr_ord_d,0,295,700)
        draw_text(315,690,string_hash_to_newline(":Change column"))
        //Volatile
        draw_sprite(spr_skill_potato_icon01,potato_fire_skill01,480,150)
        draw_sprite(spr_skill_potato_icon02,potato_fire_skill02,512,220)
        draw_sprite(spr_skill_potato_icon03,potato_fire_skill03,460,290)
        draw_set_halign(fa_center)
        draw_set_font(font4)
        draw_text(480,170,string_hash_to_newline("Int: 13"))
        draw_text(512,240,string_hash_to_newline("Int: 18"))
        draw_text(460,310,string_hash_to_newline("Int: 23"))
        draw_set_halign(fa_left)
        //Pure Potato
        draw_sprite(spr_skill_purepotato_icon01,potato_pure_skill01,190,150)
        draw_sprite(spr_skill_purepotato_icon02,potato_pure_skill02,240,220)
        draw_sprite(spr_skill_purepotato_icon03,potato_pure_skill03,170,290)
        draw_set_halign(fa_center)
        draw_set_font(font4)
        draw_text(190,170,string_hash_to_newline("Haste: 6#Int: 9"))
        draw_text(240,240,string_hash_to_newline("Haste: 10#Int: 10"))
        draw_text(170,310,string_hash_to_newline("Haste: 13#Int: 12"))
        draw_set_halign(fa_left)
        //Gloom
        draw_sprite(spr_skill_gloompotato_icon01,potato_gloom_skill01,770,150)
        draw_sprite(spr_skill_gloompotato_icon02,potato_gloom_skill02,720,220)
        draw_sprite(spr_skill_gloompotato_icon03,potato_gloom_skill03,800,290)
        draw_set_halign(fa_center)
        draw_set_font(font4)
        draw_text(770,170,string_hash_to_newline("Dex: 9#Int: 9"))
        draw_text(720,240,string_hash_to_newline("Dex: 13#Int: 10"))
        draw_text(800,310,string_hash_to_newline("Dex: 16#Int: 12"))
        draw_set_halign(fa_left)
    }
}

///Skill Tree Cursor
if active = 1
{
    if skill_screen = 1
    {
        //Mythor
        //Volatile Tree
        if s = 0 //Fire_Skill01
        if t = 1
        {
            draw_sprite(spr_inventory_select,-1,500,155)
            draw_set_font(font4)
            draw_sprite(spr_skill_descr,-1,250,120)
            draw_text(250,120,string_hash_to_newline("Ember's MP cost reduced#by 40% & damage#increased by 20%"))
        }
        if s = 1 //Fire_Skill02
        if t = 1
        {
            draw_sprite(spr_inventory_select,-1,532,225)
            draw_set_font(font4)
            draw_sprite(spr_skill_descr,-1,250,120)
            draw_text(250,120,string_hash_to_newline("Using Ember burns the #target, dealing 10% of the#damage ember dealt#for 10 seconds."))
        }
        if s = 2 //Fire_Skill03
        if t = 1
        {
            draw_sprite(spr_inventory_select,-1,480,295)
            draw_set_font(font4)
            draw_sprite(spr_skill_descr,-1,250,120)
            draw_text(250,120,string_hash_to_newline("Mana refresh rate increased#by 50% and taking any#damage will restore 10% of#your mana."))
        }
        //Gloom Tree
        if s = 0 //Gloom_Skill01
        if t = 2
        {
            draw_sprite(spr_inventory_select,-1,790,155)
            draw_set_font(font4)
            draw_sprite(spr_skill_descr,-1,500,120)
            draw_text(500,120,string_hash_to_newline("Melee damage increased#by 25%; if at max health#damage is increased by 35%"))
        }
        if s = 1 //Gloom_Skill02
        if t = 2
        {
            draw_sprite(spr_inventory_select,-1,740,225)
            draw_set_font(font4)
            draw_sprite(spr_skill_descr,-1,500,120)
            draw_text(500,120,string_hash_to_newline("Drain a small fraction of#the enemy's soul, healing#you for 5% of the damage#dealt with melee attacks."))
        }
        if s = 2 //Gloom_Skill03
        if t = 2
        {
            draw_sprite(spr_inventory_select,-1,820,295)
            draw_set_font(font4)
            draw_sprite(spr_skill_descr,-1,500,120)
            draw_text(500,120,string_hash_to_newline("At max health your melee#damage is increased by 45%#and your melee attacks have#a 10% chance to deal critical#damage."))
        }
        //Pure Potato Tree
        if s = 0 //Pure_Skill01
        if t = 0
        {
            draw_sprite(spr_inventory_select,-1,210,155)
            draw_set_font(font4)
            draw_sprite(spr_skill_descr01,-1,270,120)
            draw_text(270,120,string_hash_to_newline("Solanum's mana cost#decreased by 35% and#potency increased by 5%.#Your mana will now also#replenish slowly at full#stamina."))
        }
        if s = 1 //Pure_Skill02
        if t = 0
        {
            draw_sprite(spr_inventory_select,-1,260,225)
            draw_set_font(font4)
            draw_sprite(spr_skill_descr,-1,270,120)
            draw_text(270,120,string_hash_to_newline("Solanum's potency increased#by +10% and casting it will#increase stamina refresh#rate by 40% for 20 seconds"))
        }
        if s = 2 //Pure_Skill03
        if t = 0
        {
            draw_sprite(spr_inventory_select,-1,190,295)
            draw_set_font(font4)
            draw_sprite(spr_skill_descr01,-1,270,120)
            draw_text_colour(270,120,string_hash_to_newline("Learn the spell Asterid!"),c_yellow,c_yellow,c_yellow,c_yellow,1);
            draw_text(270,120,string_hash_to_newline("#Conjure a potato infused#with spirited energy that#deals damage and increases#STM refresh by 50% for#8 seconds."))
        }
        //Skill Select
        if s = 4 //Potato_Vitality
        if t = 3
        {
            draw_sprite(spr_inventory_select,-1,1150,410)
            draw_set_font(font4)
            if potato_skillpredict > 0
            {
                draw_sprite(spr_point_level,-1,1060+string_width(string_hash_to_newline("Vitality: " + string(potato_vitality))),415)
            }
        }
        if s = 3 //Potato_Haste
        if t = 3
        {
            draw_sprite(spr_inventory_select,-1,1150,460)
            draw_set_font(font4)
            if potato_skillpredict > 0
            {
                draw_sprite(spr_point_level,-1,1060+string_width(string_hash_to_newline("Haste: " + string(potato_haste))),465)
            }
        }
        if s = 2 //Potato_Dexterity
        if t = 3
        {
            draw_sprite(spr_inventory_select,-1,1150,510)
            draw_set_font(font4)
            if potato_skillpredict > 0
            {
                draw_sprite(spr_point_level,-1,1060+string_width(string_hash_to_newline("Dexterity: " + string(potato_dexterity))),515)
            }
        }
        if s = 1 //Potato_Intellect
        if t = 3
        {
            draw_sprite(spr_inventory_select,-1,1150,560)
            draw_set_font(font4)
            if potato_skillpredict > 0
            {
                draw_sprite(spr_point_level,-1,1060+string_width(string_hash_to_newline("Intellect: " + string(potato_intellect))),565)
            }
        }
        if s = 0 //Potato_Confirm
        if t = 3
        {
            draw_sprite(spr_inventory_select,-1,1120,610)
            draw_set_font(font4)
        }
    }
}

///Actions Screen
d3d_set_lighting(false);
if active = 1
{
    if actions_screen = 1
    {
        //Title
        draw_set_halign(fa_center);
        draw_set_font(font1);
        draw_text(960+xxw,125+yyw,string_hash_to_newline("Actions"));
        draw_set_halign(fa_left);
        //Window Coords
        var window_x = global.display_gui_width;
        var window_y = global.display_gui_height;
        var mouse_xx = window_mouse_get_x();
        var mouse_yy = window_mouse_get_y();
        //Create Bar
        if !instance_exists(obj_potato_bactions){
            instance_create(window_x*0.2, window_y*0.8, obj_potato_bactions);
        }
        //~Actions (Potato)~
        var act = 0;
        for(act = 0 ; act < ds_list_size(global.actions_potato) ; ++act){
            //Attack
            if (ds_list_find_value(global.actions_potato, act) = "attack"){
                //Set Coords
                var attack_x = window_x*0.2;
                var attack_y = window_y*0.4;
                //Draw Icon Box
                draw_sprite(spr_inv_dagger, 0, attack_x, attack_y);
                //Check if clicked
                 if point_in_rectangle(obj_cursor.x, obj_cursor.y, attack_x-42, attack_y-42, attack_x+42, attack_y+42){//Check area
                    if mouse_check_button_pressed(mb_left) //Check Button
                    if !instance_exists(obj_act_pot_attack){ //Check if instance exists
                        instance_create(mouse_xx, mouse_yy, obj_act_pot_attack); //Create action to drag
                        obj_act_pot_attack.attack_type = 1; //Action Type
                    }
                }
            }
            //Solanum
            if (ds_list_find_value(global.actions_potato, act) = "solanum"){
                //Set Coords
                var solanum_x = window_x*0.3;
                var solanum_y = window_y*0.4;
                //Draw Icon Box
                draw_sprite(spr_mag_psolanum, 0, solanum_x, solanum_y);
                //Check if clicked
                 if point_in_rectangle(obj_cursor.x, obj_cursor.y, solanum_x-42, solanum_y-42, solanum_x+42, solanum_y+42){//Check area
                    if mouse_check_button_pressed(mb_left) //Check Button
                    if !instance_exists(obj_act_pot_attack){ //Check if instance exists
                        instance_create(mouse_xx, mouse_yy, obj_act_pot_attack); //Create action to drag
                        obj_act_pot_attack.attack_type = 2; //Action Type
                    }
                }
            }
            //Ember
            if (ds_list_find_value(global.actions_potato, act) = "ember"){
                //Set Coords
                var ember_x = window_x*0.4;
                var ember_y = window_y*0.4;
                //Draw Icon Box
                draw_sprite(spr_mag_pfire, 0, ember_x, ember_y);
                //Check if clicked
                 if point_in_rectangle(obj_cursor.x, obj_cursor.y, ember_x-42, ember_y-42, ember_x+42, ember_y+42){//Check area
                    if mouse_check_button_pressed(mb_left) //Check Button
                    if !instance_exists(obj_act_pot_attack){ //Check if instance exists
                        instance_create(mouse_xx, mouse_yy, obj_act_pot_attack); //Create action to drag
                        obj_act_pot_attack.attack_type = 3; //Action Type
                    }
                }
            }
            //Asterid
            if (ds_list_find_value(global.actions_potato, act) = "asterid"){
                //Set Coords
                var asterid_x = window_x*0.5;
                var asterid_y = window_y*0.4;
                //Draw Icon Box
                draw_sprite(spr_mag_pasterid, 0, asterid_x, asterid_y);
                //Check if clicked
                 if point_in_rectangle(obj_cursor.x, obj_cursor.y, asterid_x-42, asterid_y-42, asterid_x+42, asterid_y+42){//Check area
                    if mouse_check_button_pressed(mb_left) //Check Button
                    if !instance_exists(obj_act_pot_attack){ //Check if instance exists
                        instance_create(mouse_xx, mouse_yy, obj_act_pot_attack); //Create action to drag
                        obj_act_pot_attack.attack_type = 4; //Action Type
                    }
                }
            }
            //Tremmor
            if (ds_list_find_value(global.actions_potato, act) = "tremmor"){
                //Set Coords
                var tremmor_x = window_x*0.6;
                var tremmor_y = window_y*0.4;
                //Draw Icon Box
                draw_sprite(spr_mag_tremmor, 0, tremmor_x, tremmor_y);
                //Check if clicked
                 if point_in_rectangle(obj_cursor.x, obj_cursor.y, tremmor_x-42, tremmor_y-42, tremmor_x+42, tremmor_y+42){//Check area
                    if mouse_check_button_pressed(mb_left) //Check Button
                    if !instance_exists(obj_act_pot_attack){ //Check if instance exists
                        instance_create(mouse_xx, mouse_yy, obj_act_pot_attack); //Create action to drag
                        obj_act_pot_attack.attack_type = 5; //Action Type
                    }
                }
            }
            //Parasite
            if (ds_list_find_value(global.actions_potato, act) = "parasite"){
                //Set Coords
                var parasite_x = window_x*0.2;
                var parasite_y = window_y*0.5;
                //Draw Icon Box
                draw_sprite(spr_mag_parasite, 0, parasite_x, parasite_y);
                //Check if clicked
                 if point_in_rectangle(obj_cursor.x, obj_cursor.y, parasite_x-42, parasite_y-42, parasite_x+42, parasite_y+42){//Check area
                    if mouse_check_button_pressed(mb_left) //Check Button
                    if !instance_exists(obj_act_pot_attack){ //Check if instance exists
                        instance_create(mouse_xx, mouse_yy, obj_act_pot_attack); //Create action to drag
                        obj_act_pot_attack.attack_type = 6; //Action Type
                    }
                }
            }
            //Deathspike
            if (ds_list_find_value(global.actions_potato, act) = "deathspike"){
                //Set Coords
                var deathspike_x = window_x*0.3;
                var deathspike_y = window_y*0.5;
                //Draw Icon Box
                draw_sprite(spr_mag_deathspike, 0, deathspike_x, deathspike_y);
                //Check if clicked
                 if point_in_rectangle(obj_cursor.x, obj_cursor.y, deathspike_x-42, deathspike_y-42, deathspike_x+42, deathspike_y+42){//Check area
                    if mouse_check_button_pressed(mb_left) //Check Button
                    if !instance_exists(obj_act_pot_attack){ //Check if instance exists
                        instance_create(mouse_xx, mouse_yy, obj_act_pot_attack); //Create action to drag
                        obj_act_pot_attack.attack_type = 7; //Action Type
                    }
                }
            }
        }//End of Loop
        
    }
    else{
        if instance_exists(obj_potato_bactions){
            with (obj_potato_bactions){instance_destroy();}
        }
        if instance_exists(obj_act_pot_attack){
            with (obj_act_pot_attack){instance_destroy();}
        }
    }
}

///Magic Screen
if active = 1
{
    //Magic slots draw
    if magic_screen = 1
    {
    //Shade
    if instance_exists(obj_comm_wheel_potato)
    {
        draw_set_alpha(0.25)
        draw_rectangle_colour(0,0,global.display_gui_width,global.display_gui_height,c_black,c_black,c_black,c_black,0)
        draw_set_alpha(1)
    }
    //Slot Icons
    for (m = 0; m < maxmagic; m += 1)
        {
            if ds_list_find_value(inventorymagic, m) = 0{draw_sprite(spr_wep_slot,0,160+xxw02,(m * 84)+336+scrolldm+yyw02);}
            if ds_list_find_value(inventorymagic, m) = 1{draw_sprite(spr_mag_pfire,0,160+xxw02,(m * 84)+336+scrolldm+yyw02);}
            if ds_list_find_value(inventorymagic, m) = 2{draw_sprite(spr_mag_psolanum,0,160+xxw02,(m * 84)+336+scrolldm+yyw02);}
            if ds_list_find_value(inventorymagic, m) = 3{draw_sprite(spr_mag_pasterid,0,160+xxw02,(m * 84)+336+scrolldm+yyw02);}
            if ds_list_find_value(inventorymagic, m) = "tremmor"{draw_sprite(spr_mag_tremmor,0,160+xxw02,(m * 84)+336+scrolldm+yyw02);}
            if ds_list_find_value(inventorymagic, m) = "parasite"{draw_sprite(spr_mag_parasite,0,160+xxw02,(m * 84)+336+scrolldm+yyw02);}
            if ds_list_find_value(inventorymagic, m) = "deathspike"{draw_sprite(spr_mag_deathspike,0,160+xxw02,(m * 84)+336+scrolldm+yyw02);}
        }
    }
    
    //Title
    if magic_screen = 1
    {
    draw_set_halign(fa_center);
    draw_set_font(font1)
    draw_text(960+xxw,125+yyw,string_hash_to_newline("Magic"))
    draw_set_halign(fa_left);
    }
    //Magic Description
    if magic_screen = 1
    {
        //Ember
        if ds_list_find_value(inventorymagic, pm) = 1
        {
        draw_sprite(spr_mag_pfire_big,0,440+xxw01,192+yyw01)
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Ember"))
        draw_set_font(font3)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        A small flame that deals exceptional fire damage.
        
        This flame however seems to remain faded, 
        almost as if it's always on the brink of fizzing out.
        For such a simple spell known by many sorcerers 
        of all skill, it should be relatively easy to cast a 
        normal flame.
        "))
        draw_text_colour(220+xxw01,620+yyw01,string_hash_to_newline("Greatly scales with Intellect."),c_yellow,c_yellow,c_yellow,c_yellow,1);
        draw_text_colour(224+xxw01,649+yyw01,string_hash_to_newline("Willpower COST: " + string(pot_fire_cost)),c_black,c_black,c_black,c_black,1); //Willpower Shadow
        draw_text_colour(220+xxw01,645+yyw01,string_hash_to_newline("Willpower COST: " + string(pot_fire_cost)),c_blue,c_blue,c_white,c_white,1); //Willpower
        draw_text_colour(520+xxw01,645+yyw01,string_hash_to_newline("Type: " + "Action"), c_white, c_white, c_white, c_white, 1);
        draw_set_font(font0)
        }
        //Solanum
        if ds_list_find_value(inventorymagic, pm) = 2
        {
        draw_sprite(spr_mag_psolanum_big,0,440+xxw01,192+yyw01)
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Solanum"))
        draw_set_font(font3)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        Conjure a potato to restore a small amount of 
        health. 
        
        Potatoes can be boiled, mashed, stuck 
        into stew, you name it. This vegetable's level of 
        versatility is commonly underrated due to how 
        simple it appears to look.
        "))
        draw_text_colour(220+xxw01,620+yyw01,string_hash_to_newline("Slightly scales with Intellect and Haste"),c_yellow,c_yellow,c_yellow,c_yellow,1);
        draw_text_colour(224+xxw01,649+yyw01,string_hash_to_newline("Willpower COST: " + string(pot_solanum_cost)),c_black,c_black,c_black,c_black,1); //Willpower Shadow
        draw_text_colour(220+xxw01,645+yyw01,string_hash_to_newline("Willpower COST: " + string(pot_solanum_cost)),c_blue,c_blue,c_white,c_white,1); //Willpower
        draw_text_colour(520+xxw01,645+yyw01,string_hash_to_newline("Type: " + "Action"), c_white, c_white, c_white, c_white, 1);
        draw_set_font(font0)
        }
        //Asterid
        if ds_list_find_value(inventorymagic, pm) = 3
        {
        draw_sprite(spr_mag_pasterid_big,0,440+xxw01,192+yyw01)
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Asterid"))
        draw_set_font(font3)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        Conjure a potato blessed by the well-being of
        one's soul dealing nature damage to the enemy.
        Also gifts the caster with a boost of speed,
        increasing Willpower regeneration rate.
        
        The idea of a 'good natured' attack may
        sound contradictory, but doing what is looked at
        as 'good' isn't always what it seems. Perhaps
        this power may stem from somewhere else...
        "))
        draw_text_colour(220+xxw01,620+yyw01,string_hash_to_newline("Moderately scales with Intellect and Haste"),c_yellow,c_yellow,c_yellow,c_yellow,1);
        draw_text_colour(224+xxw01,649+yyw01,string_hash_to_newline("Willpower COST: " + string(pot_asterid_cost)),c_black,c_black,c_black,c_black,1); //Willpower Shadow
        draw_text_colour(220+xxw01,645+yyw01,string_hash_to_newline("Willpower COST: " + string(pot_asterid_cost)),c_blue,c_blue,c_white,c_white,1); //Willpower
        draw_text_colour(520+xxw01,645+yyw01,string_hash_to_newline("Type: " + "Action"), c_white, c_white, c_white, c_white, 1);
        draw_set_font(font0)
        }
        //Tremmor
        if ds_list_find_value(inventorymagic, pm) = "tremmor"
        {
        draw_sprite_ext(spr_mag_tremmor_big, 0, 460+xxw01, 175+yyw01, 1.25, 1.25, 0, -1, 1);
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Tremmor"))
        draw_set_font(font3)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        Create a massive boulder that falls from the sky
        and strikes the ground! Causing a shockwave that
        deals earth damage to all enemies within the 
        vacinity! 
        
        Earth-based magic is usually rare for the weak 
        willed to learn since they require great amounts 
        of strength within one's self to utilize. Not many 
        were successful in mastering this school of magic.
        "))
        draw_text_colour(220+xxw01,620+yyw01,string_hash_to_newline("Moderately scales with Intellect and Haste"),c_yellow,c_yellow,c_yellow,c_yellow,1);
        draw_text_colour(224+xxw01,649+yyw01,string_hash_to_newline("Willpower COST: " + string(global.pot_tremmor_cost)),c_black,c_black,c_black,c_black,1); //Willpower Shadow
        draw_text_colour(220+xxw01,645+yyw01,string_hash_to_newline("Willpower COST: " + string(global.pot_tremmor_cost)),c_blue,c_blue,c_white,c_white,1); //Willpower
        draw_text_colour(520+xxw01,645+yyw01,string_hash_to_newline("Type: " + "Action"), c_white, c_white, c_white, c_white, 1);
        draw_set_font(font0);
        }
        //Parasite
        if ds_list_find_value(inventorymagic, pm) = "parasite"
        {
        draw_sprite_ext(spr_mag_parasite_big, 0, 460+xxw01, 175+yyw01, 1.25, 1.25, 0, -1, 1);
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Parasite"))
        draw_set_font(font3)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        Infect the target with a parasite, dealing nature
        damage over 6 seconds, the disease can stack upon 
        itself an unlimited number of times if casted again.
        Potency also increases by 10% per tick.
        
        Hexers were mentally disturbed, many believed that
        they were simply born this way, thus are incurable of
        their discontent. They often used their magics for 
        such vile acts, and so they became targets to avoid.
        "))
        draw_text_colour(220+xxw01,620+yyw01,string_hash_to_newline("Moderately scales with Intellect"),c_yellow,c_yellow,c_yellow,c_yellow,1);
        draw_text_colour(224+xxw01,649+yyw01,string_hash_to_newline("Willpower COST: " + string(global.pot_parasite_cost)),c_black,c_black,c_black,c_black,1); //Willpower Shadow
        draw_text_colour(220+xxw01,645+yyw01,string_hash_to_newline("Willpower COST: " + string(global.pot_parasite_cost)),c_blue,c_blue,c_white,c_white,1); //Willpower
        draw_text_colour(520+xxw01,645+yyw01,string_hash_to_newline("Type: " + "Bonus Action"), c_lime, c_lime, c_white, c_white, 1);
        draw_set_font(font0);
        }
        //Death Spike
        if ds_list_find_value(inventorymagic, pm) = "deathspike"
        {
        draw_sprite_ext(spr_mag_deathspike_big, 0, 460+xxw01, 175+yyw01, 1.25, 1.25, 0, -1, 1);
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01);
        draw_set_font(font1);
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Death's Spike"));
        draw_set_font(font3);
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        Deals dark damage to the target. If you are moving 
        left or right while casting you will perform a roll in 
        the respective direction. Potency increases by 150% if 
        you melee attacked before casting.
        
        The Bringer of Ultima would entice rogues to live on the
        edges of life. If they were to put themselves at greater
        risks, they would be rewarded accordingly with special
        power.
        "))
        draw_text_colour(220+xxw01,620+yyw01,string_hash_to_newline("Moderately scales with Intellect and Dexterity"),c_yellow,c_yellow,c_yellow,c_yellow,1);
        draw_text_colour(224+xxw01,649+yyw01,string_hash_to_newline("Willpower COST: " + string(global.pot_deathspike_cost)),c_black,c_black,c_black,c_black,1); //Willpower Shadow
        draw_text_colour(220+xxw01,645+yyw01,string_hash_to_newline("Willpower COST: " + string(global.pot_deathspike_cost)),c_blue,c_blue,c_white,c_white,1); //Willpower
        draw_text_colour(520+xxw01,645+yyw01,string_hash_to_newline("Type: " + "Action"), c_white, c_white, c_white, c_white, 1);
        draw_set_font(font0);
        }
    }
}

///Items Screen
if active = 1
{
    //Key Items or Items Tab
    if item_screen > 0
    {
        draw_set_font(font2);
        draw_set_halign(fa_center);
        //Items Selected Shadow
        if item_screen = 1{draw_text_colour(304+xxw,68+yyw,string_hash_to_newline("Items"),c_black,c_black,c_black,c_black,1);}
        //Items-------------------------
        draw_text(300+xxw,64+yyw,string_hash_to_newline("Items"));
        //Key Items Selected Shadow
        if item_screen = 2{draw_text_colour(604+xxw,68+yyw,string_hash_to_newline("Key Items"),c_black,c_black,c_black,c_black,1);}
        //Key Items---------------------
        draw_text(600+xxw,64+yyw,string_hash_to_newline("Key Items"));
        draw_set_halign(fa_left);
        draw_set_font(font0);
    }
    //Item slots draw
    if item_screen = 1
    {
    //Shade
    if instance_exists(obj_comm_wheel_potato)
    {
        draw_set_alpha(0.25)
        draw_rectangle_colour(0,0,global.display_gui_width,global.display_gui_height,c_black,c_black,c_black,c_black,0)
        draw_set_alpha(1)
    }
    //Slot Icons
    for (s = 0; s < maxitem; s += 1)
        {
        if ds_list_find_value(global.inventoryitems, s) = 0{draw_sprite(spr_wep_slot,0,160+xxw02,(s * 84)+336+scrolldi+yyw02)}
        }
    }
    
    //Title
    if item_screen = 1
    {
    draw_set_halign(fa_center);
    draw_set_font(font1)
    draw_text(960+xxw,125+yyw,string_hash_to_newline("Items"))
    draw_set_halign(fa_left);
    }
    //Item Description
    if item_screen = 1
    {
        if ds_list_find_value(global.inventoryitems, item) = 1
        {
        draw_sprite_ext(spr_key01,0,440+xxw01,192+yyw01,2.5,2.5,0,-1,1)
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Key Shard \\#1"))
        draw_set_font(font0)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        It looks like a piece of some kind of plate. Perhaps 
        there should be another piece to go with it, maybe? 
        I don't know, you tell me, does it look like some kind 
        of key because from what I can tell, based off of my 
        opinion, it looks like a key to something. It's only 
        half of it however, so...get the other half.
        "))
        draw_set_font(font0)
        }
    }
}

///Key Items Screen
if active = 1
{
    //Item slots draw
    if item_screen = 2
    {
    //Shade
    if instance_exists(obj_comm_wheel_potato)
    {
        draw_set_alpha(0.25)
        draw_rectangle_colour(0,0,global.display_gui_width,global.display_gui_height,c_black,c_black,c_black,c_black,0)
        draw_set_alpha(1)
    }
    //Slot Icons
    for (s = 0; s < maxkeyitem; s += 1)
        {
        if ds_list_find_value(global.inventorykeyitems, s) = 0{draw_sprite(spr_wep_slot,0,160+xxw02,(s * 84)+336+scrolldi+yyw02)}
        if ds_list_find_value(global.inventorykeyitems, s) = 1{draw_sprite(spr_key01_inv,0,160+xxw02,(s * 84)+336+scrolldi+yyw02)}
        if ds_list_find_value(global.inventorykeyitems, s) = 2{draw_sprite(spr_key03_inv,0,160+xxw02,(s * 84)+336+scrolldi+yyw02)}
        }
    }
    
    //Title
    if item_screen = 2
    {
    draw_set_halign(fa_center);
    draw_set_font(font1)
    draw_text(960+xxw,125+yyw,string_hash_to_newline("Key Items"))
    draw_set_halign(fa_left);
    }
    //Item Description
    if item_screen = 2
    {
        if ds_list_find_value(global.inventorykeyitems, item) = 1
        {
        draw_sprite_ext(spr_key01,0,440+xxw01,192+yyw01,2.5,2.5,0,-1,1)
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Key Shard \\#1"))
        draw_set_font(font0)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        It looks like a piece of some kind of plate. Perhaps 
        there should be another piece to go with it, maybe? 
        I don't know, you tell me, does it look like some kind 
        of key because from what I can tell, based off of my 
        opinion, it looks like a key to something. It's only 
        half of it however, so...get the other half.
        "))
        draw_set_font(font0)
        }
        if ds_list_find_value(global.inventorykeyitems, item) = 2
        {
        draw_sprite_ext(spr_key03,0,440+xxw01,192+yyw01,2.5,2.5,0,-1,1)
        draw_sprite(spr_wep_descr,0,205+xxw01,375+yyw01)
        draw_set_font(font1)
        draw_text(370+xxw01,300+yyw01,string_hash_to_newline("Key Plate"))
        draw_set_font(font0)
        draw_text(180+xxw01,360+yyw01,
        string_hash_to_newline(@"
        The restored plate, a key to the unknown that is 
        perhaps the only way out of this dream-like world. 
        One not aware of the stigmas this world offers would 
        question the importance of such a dull looking plate, 
        and as to why the children of this world desire it so. 
        Upon questioning this, you can almost see a 
        reflection of ones-self within the plate.
        "))
        draw_set_font(font0)
        }
    }
}

///*Debug*
if instance_exists(obj_global_overw)
{
    //if obj_global_overw.draw_coords = 1
    //{
        //draw_text(0,0,"xx: " + string(xxw02) + "yy: " + string(yyw02));
        //yyw02 -= keyboard_check(vk_up)*0.5;
        //yyw02 += keyboard_check(vk_down)*0.5;
        //xxw02 += keyboard_check(vk_right)*0.5;
        //xxw02 -= keyboard_check(vk_left)*0.5;
   //}
}

/* */
/*  */
