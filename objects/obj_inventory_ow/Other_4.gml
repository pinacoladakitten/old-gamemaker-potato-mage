/// @description Weapon Check
for (w = 0; w < maxwep; w += 1)
{
    if inventorywep[w] = potato_equip
    {
        pw = w
        pw_equip = pw
    }
}

///Stats Check
if instance_exists(obj_potato_overw)
{
    potato_vit_predict = potato_vitality
    potato_haste_predict = potato_haste
    potato_dex_predict = potato_dexterity
    potato_int_predict = potato_intellect
    potato_skillpredict = potato_skillp
}

