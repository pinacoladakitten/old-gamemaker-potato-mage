/// @description Confirming and Max Stuff
//Max Stuff
/*maxitem = ds_list_size(global.inventoryitems);
maxkeyitem = ds_list_size(global.inventorykeyitems);
maxmagic = ds_list_size(inventorymagic);
maxwep = ds_list_size(inventorywep);*/

//Confirming
if keyboard_check_pressed(global.keyb_confirm)
or gamepad_button_check_pressed(0, global.gpi_confirm)
{
    //Applying Weapon--------------------------
    equipped = scrolld
    scroll_equip = scrolld
    pw_equip = pw
    //Dagger
    if weapon_screen = 1
    if inventorywep[pw] = 1 //Dagger
    if !instance_exists(obj_dagger)
    {
        instance_create(0,0,obj_dagger);
    }
    //Dark Stick
    if weapon_screen = 1
    if inventorywep[pw] = 2 //Dark Stick
    if !instance_exists(obj_dark_wood_stick)
    {
        instance_create(0,0,obj_dark_wood_stick);
    }
    //Hand Scythe
    if weapon_screen = 1
    if inventorywep[pw] = 3 //Hand Scythe
    if !instance_exists(obj_handscythe01)
    {
        instance_create(0,0,obj_handscythe01);
    }
    //Weapon Select--------------------------
    if active = 1
    {
        if weapon_screen = 1
        {
            potato_equip = inventorywep[pw]
        }
    }
    ///Magic Select--------------------------
    if active = 1
    if magic_screen = 1
    {
        //Magic Use
        if instance_exists(obj_comm_wheel_potato)
        {
            //Ember
            if ds_list_find_value(inventorymagic, pm) = 1
            if mp_potato >= pot_fire_cost
            {
                with obj_target
                {
                    draw = 1
                    active = 1
                }
                with obj_comm_wheel_potato
                {
                    change = 0
                    confirm = 1
                    mag01 = 1
                }
            }
            //Solanum
            if ds_list_find_value(inventorymagic, pm) = 2
            if mp_potato >= pot_solanum_cost
            {
                with obj_target_ally
                {
                    draw = 1
                    active = 1
                }
                with obj_comm_wheel_potato
                {
                    change = 0
                    confirm = 1
                    mag02 = 1
                }
            }
            //Asterid
            if ds_list_find_value(inventorymagic, pm) = 3
            if mp_potato >= pot_asterid_cost
            {
                with obj_target
                {
                    draw = 1
                    active = 1
                }
                with obj_comm_wheel_potato
                {
                    change = 0
                    confirm = 1
                    magpotato = 3
                }
            }
        }
    }
    ///Item Select--------------------------
    if active = 1
    {
        if item_screen = 1
        {
            //Nothing Yet...
        }
    }
    if instance_exists(obj_comm_wheel_potato)
    if active = 1
    {
        active = 1 - active
        weapon_screen = 0
        skill_screen = 0
        magic_screen = 0
        item_screen = 0
        pause_screen = 1
    }
}

