/// @description KeyInputs or GamePad
//Activate Menu//--------------------------------------------------
if keyboard_check_pressed(global.keyb_menu)
or gamepad_button_check_pressed(0, global.gpi_menu)
{
    if (instance_exists(obj_potato_overw) and global.inventoryDisable == false and pause == 0)
    {
        active = 1 - active;
        if active = 0
        {
            with obj_potato_overw {pause = 0}
        }
        if active = 1
        {
            with obj_potato_overw {pause = 1}
            inventory_screen = 0
            item_screen = 0
            weapon_screen = 0
            magic_screen = 0
            skill_screen = 0
            pause_screen = 1
        }
    }
}
///Back-out//--------------------------------------------------
if keyboard_check_pressed(global.keyb_cancel)
or gamepad_button_check_pressed(0, global.gpi_cancel)
{
    if instance_exists(obj_comm_wheel_potato)
    if active = 1
    {
        active = 1 - active
        weapon_screen = 0
        magic_screen = 0
        item_screen = 0
        skill_screen = 0
        pause_screen = 1
    }
}

///Menu Control
if active = 1
{
    //Movement
    //Pause Screen------------------------------------------------
    if pause_screen = 1
    {
        //Move Up
        if keyboard_check_pressed(global.keyb_up)
        or gamepad_button_check_pressed(0, global.gpi_up)
        {i = min(i+1,4);}
        //Move Down
        if keyboard_check_pressed(global.keyb_down)
        or gamepad_button_check_pressed(0, global.gpi_down)
        {i = max(i-1,-1);}
    }
    //Skill Tree Screen------------------------------------------------
    if skill_screen = 1
    {
        //Level Select
        if t = 3
        {
            //Move Up
            if keyboard_check_pressed(global.keyb_up)
            or gamepad_button_check_pressed(0, global.gpi_up)
            {s = min(s+1,4);}
            //Move Down
            if keyboard_check_pressed(global.keyb_down)
            or gamepad_button_check_pressed(0, global.gpi_down)
            {s = max(s-1,0);}
            if s = 0
            {
                //Move Right
                if keyboard_check_pressed(global.keyb_right)
                or gamepad_button_check_pressed(0, global.gpi_right)
                {t = min(t+1,3);}
                //Move Left
                if keyboard_check_pressed(global.keyb_left)
                or gamepad_button_check_pressed(0, global.gpi_left)
                {t = max(t-1,0);}
            }
        }
        //Skill Select
        if t != 3
        {
            //Movement
            //Move Up
            if keyboard_check_pressed(global.keyb_up)
            or gamepad_button_check_pressed(0, global.gpi_up)
            {s = max(s-1,0)}
            //Move Down
            if keyboard_check_pressed(global.keyb_down)
            or gamepad_button_check_pressed(0, global.gpi_down)
            {s = min(s+1,2)}
            //Move Right
            if keyboard_check_pressed(global.keyb_right)
            or gamepad_button_check_pressed(0, global.gpi_right)
            {
                t = min(t+1,3)
                if t > 2
                {s = 0}
            }
            //Move Left
            if keyboard_check_pressed(global.keyb_left)
            or gamepad_button_check_pressed(0, global.gpi_left)
            {t = max(t-1,0)}
        }
        //Column Change
        //Tab Right
        if keyboard_check_pressed(global.keyb_column_scrollR)
        or gamepad_button_check_pressed(0, global.gpi_column_scrollR)
        {
            t = min(t+1,3)
            s = 0
        }
        //Tab Left
        if keyboard_check_pressed(global.keyb_column_scrollL)
        or gamepad_button_check_pressed(0, global.gpi_column_scrollL)
        {
            t = max(t-1,0)
            s = 0
        }
    }
    //Wep Screen------------------------------------------------
    if weapon_screen = 1
    {
        //Move Up
        if keyboard_check_pressed(global.keyb_up)
        or gamepad_button_check_pressed(0, global.gpi_up)
        {
            pw = max(pw-1,0)
            scrolld = min(scrolld+84,0)
        }
        //Move Down
        if keyboard_check_pressed(global.keyb_down)
        or gamepad_button_check_pressed(0, global.gpi_down)
        {
            pw = min(pw+1,maxwep-1)
            scrolld = max(scrolld-84,(-maxwep+1)*84)
        }
        //Round in-betweens
        pw = round(pw);
    }
    //Item Screen------------------------------------------------
    if item_screen > 0
    {
        //Items
        if item_screen = 1
        {
            //Move Up
            if keyboard_check_pressed(global.keyb_up)
            or gamepad_button_check_pressed(0, global.gpi_up)
            {
                item = max(item-1,0)
                scrolldi = min(scrolldi+84,0)
            }
            //Move Down
            if keyboard_check_pressed(global.keyb_down)
            or gamepad_button_check_pressed(0, global.gpi_down)
            {
                item = min(item+1,maxitem-1)
                scrolldi = max(scrolldi-84,(-maxitem+1)*84)
            }
        }
        //Key Items
        if item_screen = 2
        {
            //Move Up
            if keyboard_check_pressed(global.keyb_up)
            or gamepad_button_check_pressed(0, global.gpi_up)
            {
                item = max(item-1,0)
                scrolldi = min(scrolldi+84,0)
            }
            //Move Down
            if keyboard_check_pressed(global.keyb_down)
            or gamepad_button_check_pressed(0, global.gpi_down)
            {
                item = min(item+1,maxkeyitem-1)
                scrolldi = max(scrolldi-84,(-maxkeyitem+1)*84)
            }
        }
        //Change Between Items and Key Items----------------
        //Go to Key Items
        if keyboard_check_pressed(global.keyb_column_scrollR)
        or gamepad_button_check_pressed(0, global.gpi_column_scrollR)
        {item_screen = min(item_screen+1,2);}
        //Go to Items
        if keyboard_check_pressed(global.keyb_column_scrollL)
        or gamepad_button_check_pressed(0, global.gpi_column_scrollL)
        {item_screen = max(item_screen-1,1);}
        //Round Selection (Avoid in-betweens)
        item = round(item);
    }
    //Magic Screen------------------------------------------------
    if magic_screen = 1
    {
        //Move Up
        if keyboard_check_pressed(global.keyb_up)
        or gamepad_button_check_pressed(0, global.gpi_up)
        {
            pm = max(pm-1,0)
            scrolldm = min(scrolldm+84,0)
        }
        //Move Down
        if keyboard_check_pressed(global.keyb_down)
        or gamepad_button_check_pressed(0, global.gpi_down)
        {
            pm = min(pm+1,maxmagic-1)
            scrolldm = max(scrolldm-84,(-maxmagic+1)*84)
        }
        pm = round(pm);
    }
    
//Selecting Stuff-------------------------------------------------
    if pause_screen = 1
    {
        if keyboard_check_pressed(global.keyb_confirm)
        or gamepad_button_check_pressed(0, global.gpi_confirm)
        {
            //Exit
            if i = -1
            {
                with obj_potato_overw
                {pause = 0;}
                active = 0;
                pause_screen = 1;
            }
            //Item Screen
            if i = 0
            {
                item_screen = 1;
                pause_screen = 0;     
            }
            //Magic Screen
            if i = 1
            {
                magic_screen = 1;
                pause_screen = 0;
            }
            //Actions
            if i = 2
            {
               actions_screen = 1;
               pause_screen = 0;
            }
            //Skill Screen
            if i = 3
            {
                skill_screen = 1 ;
                pause_screen = 0 ;
                potato_skillpredict = potato_skillp;
                potato_vit_predict = potato_vitality;
                potato_haste_predict = potato_haste;
                potato_dex_predict = potato_dexterity;
                potato_int_predict = potato_intellect;
            }
            //Weapon Screen
            if i = 4
            {
                weapon_screen = 1;
                pause_screen = 0;
            }
        }
    }
    
//Go Back
    if keyboard_check_pressed(global.keyb_cancel)
    or gamepad_button_check_pressed(0, global.gpi_cancel)
    {
        if pause_screen = 1 //Main Screen
        {
            with obj_potato_overw
            {pause = 0;}
             active = 0;
             pause_screen = 1;
        }
        if weapon_screen = 1 //Weapon Screen
        {
            pause_screen = 1;
            weapon_screen = 0;
        }
        if item_screen > 0 //Item Screen
        {
            pause_screen = 1;
            item_screen = 0;
        }
        if magic_screen = 1 //Magic Screen
        {
            pause_screen = 1;
            magic_screen = 0;
        }
        if actions_screen = 1 //Actions Screen
        {
            pause_screen = 1;
            actions_screen = 0;
        }
        if skill_screen = 1 //Skill Screen
        {
            pause_screen = 1;
            skill_screen = 0;
            potato_skillpredict = potato_skillp;
            potato_vit_predict = potato_vitality;
            potato_haste_predict = potato_haste;
            potato_dex_predict = potato_dexterity;
            potato_int_predict = potato_intellect;
        }
    }
}

///Level Up Potato
if active = 1
if skill_screen = 1
{
    //Selecting Stuff
    //Vitality
    if s = 4
    if t = 3
    {
        if keyboard_check_pressed(global.keyb_right) or gamepad_button_check_pressed(0, global.gpi_right)
        if potato_skillpredict > 0
        {
            potato_skillpredict -= 1
            potato_vit_predict += 1
            audio_play_sound(snd_menu_equip,10,false)
        }
        if keyboard_check_pressed(global.keyb_left) or gamepad_button_check_pressed(0, global.gpi_left)
        if potato_vit_predict > potato_vitality
        {
            potato_skillpredict += 1
            potato_vit_predict = max(potato_vit_predict-1,potato_vitality)
            audio_play_sound(snd_menu_equip,10,false)
        }
    }
    //Haste
    if s = 3
    if t = 3
    {
        if keyboard_check_pressed(global.keyb_right) or gamepad_button_check_pressed(0, global.gpi_right)
        if potato_skillpredict > 0
        {
            potato_skillpredict -= 1
            potato_haste_predict += 1
            audio_play_sound(snd_menu_equip,10,false)
        }
        if keyboard_check_pressed(global.keyb_left) or gamepad_button_check_pressed(0, global.gpi_left)
        if potato_haste_predict > potato_haste
        {
            potato_skillpredict += 1
            potato_haste_predict = max(potato_haste_predict-1,potato_haste)
            audio_play_sound(snd_menu_equip,10,false)
        }
    }
    //Dexterity
    if s = 2
    if t = 3
    {
        if keyboard_check_pressed(global.keyb_right) or gamepad_button_check_pressed(0, global.gpi_right)
        if potato_skillpredict > 0
        {
            potato_skillpredict -= 1
            potato_dex_predict += 1
            audio_play_sound(snd_menu_equip,10,false)
        }
        if keyboard_check_pressed(global.keyb_left) or gamepad_button_check_pressed(0, global.gpi_left)
        if potato_dex_predict > potato_dexterity
        {
            potato_skillpredict += 1
            potato_dex_predict = max(potato_dex_predict-1,potato_dexterity)
            audio_play_sound(snd_menu_equip,10,false)
        }
    }
    //Intellect
    if s = 1
    if t = 3
    {
        if keyboard_check_pressed(global.keyb_right) or gamepad_button_check_pressed(0, global.gpi_right)
        if potato_skillpredict > 0
        {
            potato_skillpredict -= 1
            potato_int_predict += 1
            audio_play_sound(snd_menu_equip,10,false)
        }
        if keyboard_check_pressed(global.keyb_left) or gamepad_button_check_pressed(0, global.gpi_left)
        if potato_int_predict > potato_intellect
        {
            potato_skillpredict += 1
            potato_int_predict = max(potato_int_predict-1,potato_intellect)
            audio_play_sound(snd_menu_equip,10,false)
        }
    }
    //Confirm
    if keyboard_check_pressed(global.keyb_confirm) or gamepad_button_check_pressed(0, global.gpi_confirm)
    if s = 0
    if t = 3
    {
        potato_skillp = potato_skillpredict
        potato_vitality = potato_vit_predict
        potato_haste = potato_haste_predict
        potato_dexterity = potato_dex_predict
        potato_intellect = potato_int_predict
    }
}

///Command Inactive
if instance_exists(obj_comm_wheel_potato)
{
    if active = 1
    {with obj_comm_wheel_potato{pause_com = 1}}
    else
    {with obj_comm_wheel_potato{pause_com = 0}}
}

///Weapon Sprites
if potato_equip = 0{potato_equip_sprite = spr_wep_slot}
if potato_equip = 1{potato_equip_sprite = spr_inv_dagger}
if potato_equip = 2{potato_equip_sprite = spr_inv_dark_wood_stick}
if potato_equip = 3{potato_equip_sprite = spr_inv_handscythe01}

///Sounds
if active = 1
{
    if skill_screen = 1
    {
        if t != 3
        {
            if keyboard_check_pressed(global.keyb_left) or keyboard_check_pressed(global.keyb_right)
            or gamepad_button_check_pressed(0, global.gpi_left) or gamepad_button_check_pressed(0, global.gpi_right)
            {audio_play_sound(snd_move_menu,10,false)}
        }
        if keyboard_check_pressed(global.keyb_column_scrollL) or keyboard_check_pressed(global.keyb_column_scrollR)
        or gamepad_button_check_pressed(0, global.gpi_column_scrollL) or gamepad_button_check_pressed(0, global.gpi_column_scrollR)
        {audio_play_sound(snd_move_menu,10,false)}
    }
    //Sound
    //Move
    if keyboard_check_pressed(global.keyb_up) or keyboard_check_pressed(global.keyb_down)
    or gamepad_button_check_pressed(0, global.gpi_up) or gamepad_button_check_pressed(0, global.gpi_down)
    {audio_play_sound(snd_move_menu,10,false)}
    //Confirm
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    {audio_play_sound(snd_select,10,false)}
    //Cancel
    if keyboard_check_pressed(global.keyb_cancel)
    or gamepad_button_check_pressed(0, global.gpi_cancel)
    {audio_play_sound(snd_menu_back,10,false)}
}

