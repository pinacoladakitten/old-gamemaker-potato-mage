/// @description Vars
i = 0;
s = 0;
t = 3;
//Scroll Screen Types
scrolld = 0;
scrolldi = 0;
scrolldm = 0;
//Screens
inventory_screen = 0;
weapon_screen = 0;
magic_screen = 0;
item_screen = 0;
pause_screen = 1;
skill_screen = 0;
actions_screen = 0;
//Active
active = 0;
//Equip
equipped = 0;
scroll_equip = 0;
pw_equip = 0;
image_speed = 0.5;
//Char Vars
potato_equip_sprite = spr_wep_slot;

///Weapon Key
//0 = spr_wep_slot
//1 = spr_inv_dagger
//2 = spr_inv_dark_wood_stick
//3 = spr_handscythe

///Magic Key
//0 = spr_wep_slot
//1 = spr_mag_pfire

///Inventory Weapons Slots
w = 0
pw = 0
maxwep = 1

for (w = 0; w < maxwep; w += 1)
{
    inventorywep[w] = 0;
}

///Inventory Item Slots
i = 0;
item = 0;
maxitem = 0;
maxkeyitem = 0;
global.inventoryitems = ds_list_create();
global.inventorykeyitems = ds_list_create();

///Inventory Magic Slots
i = 0
magic = 0
pm = 0
maxmagic = 0
globalvar inventorymagic;
inventorymagic = ds_list_create();

///Misc

