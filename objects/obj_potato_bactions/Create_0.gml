/// @description Setup
alpha = 0.5;
alpha01 = 1;
active = false;
bonus_action_type = 0;
b = 0;
equip_spr = obj_inventory_ow.potato_equip_sprite
if obj_inventory_ow.potato_equip_sprite = spr_wep_slot
{equip_spr = spr_mag_psolanum;}
alarm[0] = 0.1*room_speed;

//**Times**\\
timer_glob_max = 3 - (potato_haste*0.005); //Global Timer Initial Constant
timer_glob = timer_glob_max; //Global Timer
timer_reduce = 0; // Timer reduction status effect
//Spell Specific Times
timer_para_max = 1 - (potato_haste*0.005);
timer_para = timer_para_max;
timerParaCount = 0;
timerParaCount_max = 0.1*room_speed;
ParaAlpha = 1;
activePara = false;

//**Buttons Keyboard**\\
button_map_key = ds_list_create();
ds_list_add(button_map_key, global.bonus_action01);
ds_list_add(button_map_key, global.bonus_action02);
ds_list_add(button_map_key, global.bonus_action03);
ds_list_add(button_map_key, global.bonus_action04);
ds_list_add(button_map_key, global.bonus_action05);
ds_list_add(button_map_key, global.bonus_action06);
ds_list_add(button_map_key, global.bonus_action07);

//Buttons Gamepad
button_map_key_gpi = ds_list_create();
ds_list_add(button_map_key_gpi, global.bonus_action01);
ds_list_add(button_map_key_gpi, global.bonus_action02);
ds_list_add(button_map_key_gpi, global.bonus_action03);
ds_list_add(button_map_key_gpi, global.bonus_action04);
ds_list_add(button_map_key_gpi, global.bonus_action05);
ds_list_add(button_map_key_gpi, global.bonus_action06);
ds_list_add(button_map_key_gpi, global.bonus_action07);

/* */
/*  */
