/// @description Timer Checks and **Executing Actions**
if instance_exists(obj_potato_combat)
{
    //Committing Actions
    if (timer_glob = 0 and obj_potato_combat.pause_combat = 0 and obj_potato_combat.dodging = false)
    {
        for (b = 0; b < ds_list_size(global.actions_pot_equip); ++b)
        {
            if (ds_list_find_value(global.actions_pot_equip, b) = "attack"){ //Find Action Position
                var attack_button = (ds_list_find_value(button_map_key, b)); //Keyboard Input Based on pos in list
                var attack_button_gpi = (ds_list_find_value(button_map_key_gpi, b)); //Gamepad Input Based on pos in list
                //Melee Attack//-----------------------------------------------------------------------------------------------
                if keyboard_check_pressed(attack_button)
                or gamepad_button_check_pressed(0, attack_button_gpi)
                {
                    if mp_potato >= global.pot_attack_cost
                    {
                        with (obj_potato_combat){
                            //WP Cost
                            mp_potato -= global.pot_attack_cost
                            //Attack Type
                            action_attk = 0;
                            //Set Sprite
                            spr = spr_potato_attack01;
                            //Set Frame to 0
                            image_index = 0;
                            //Frame Speed
                            image_speed = 0.25;
                            //Set WP Regen Timer
                            wp_regen_pause = true;
                            alarm[0] = 1*room_speed;
							
                            //Set Death Spike Bns to true and add icon to buffs
							if(ds_list_find_index(global.actions_pot_equip, "deathspike")!=-1){
	                            deathSpikeAttkBns = true;
								with(obj_combat_hud_all){
									if(ds_list_find_index(pbufftype, "deathspike")==-1){
									    pbuffs += 1
									    ds_list_add(pbufftype, "deathspike");
									}
								}
							}
                        }
                        //Wep Durability Cost
                        if potato_equip != 0{global.wep_durability -= 10;}
                        //Timer
                        timer_glob = timer_glob_max - (timer_glob_max*timer_reduce);
                        active = true;
                    }
                }
            }
            if (ds_list_find_value(global.actions_pot_equip, b) = "solanum"){ //Find Action Position
            var solanum_button = (ds_list_find_value(button_map_key, b)); //Keyboard Input Based on pos in list
            var solanum_button_gpi = (ds_list_find_value(button_map_key_gpi, b)); //Gamepad Input Based on pos in list
                //Solanum Action//-----------------------------------------------------------------------------------------------
                if keyboard_check_pressed(solanum_button)
                or gamepad_button_check_pressed(0, solanum_button_gpi)
                {
                    if mp_potato >= pot_solanum_cost
                    {
                        with (obj_potato_combat){
                            //WP Cost
                            mp_potato -= pot_solanum_cost;
                            //Attack Type
                            action_attk = 1;
                            //Set Sprite
                            spr = spr_potato_magic01;
                            //Set Frame to 0
                            image_index = 0;
                            //Frame Speed
                            image_speed = 0.25;
                            //Set WP Regen Timer
                            wp_regen_pause = true;
                            alarm[0] = 1*room_speed;
                        }
                        timer_glob = timer_glob_max - (timer_glob_max*timer_reduce);
                        active = true;
                    }
                }
            }
            if (ds_list_find_value(global.actions_pot_equip, b) = "ember"){ //Find Action Position
            var ember_button = (ds_list_find_value(button_map_key, b)); //Keyboard Input Based on pos in list
            var ember_button_gpi = (ds_list_find_value(button_map_key_gpi, b)); //Gamepad Input Based on pos in list
                //Ember Action//-----------------------------------------------------------------------------------------------
                if keyboard_check_pressed(ember_button)
                or gamepad_button_check_pressed(0, ember_button_gpi)
                {
                    if mp_potato >= pot_fire_cost
                    {
                        with (obj_potato_combat){
                            //WP Cost
                            mp_potato -= pot_fire_cost;
                            //Attack Type
                            action_attk = 2;
                            //Set Sprite
                            spr = spr_potato_magic01;
                            //Set Frame to 0
                            image_index = 0;
                            //Frame Speed
                            image_speed = 0.25;
                            //Set WP Regen Timer
                            wp_regen_pause = true;
                            alarm[0] = 1*room_speed;
                        }
                        timer_glob = timer_glob_max - (timer_glob_max*timer_reduce);
                        active = true;
                    }
                }
            }
            if (ds_list_find_value(global.actions_pot_equip, b) = "asterid"){ //Find Action Position
            var asterid_button = (ds_list_find_value(button_map_key, b)); //Keyboard Input Based on pos in list
            var asterid_button_gpi = (ds_list_find_value(button_map_key_gpi, b)); //Gamepad Input Based on pos in list
                //Asterid Action//-----------------------------------------------------------------------------------------------
                if keyboard_check_pressed(asterid_button)
                or gamepad_button_check_pressed(0, asterid_button_gpi)
                {
                    if mp_potato >= pot_asterid_cost
                    {
                        with (obj_potato_combat){
                            //WP Cost
                            mp_potato -= pot_asterid_cost;
                            //Attack Type
                            action_attk = 3;
                            //Set Sprite
                            spr = spr_potato_attack01;
                            //Set Frame to 0
                            image_index = 0;
                            //Frame Speed
                            image_speed = 0.25;
                            //Set WP Regen Timer
                            wp_regen_pause = true;
                            alarm[0] = 1*room_speed;
                        }
                        timer_glob = timer_glob_max - (timer_glob_max*timer_reduce);
                        active = true;
                    }
                }
            }
            if (ds_list_find_value(global.actions_pot_equip, b) = "tremmor"){ //Find Action Position
            var tremmor_button = (ds_list_find_value(button_map_key, b)); //Keyboard Input Based on pos in list
            var tremmor_button_gpi = (ds_list_find_value(button_map_key_gpi, b)); //Gamepad Input Based on pos in list
                //Tremmor Action//-----------------------------------------------------------------------------------------------
                if keyboard_check_pressed(tremmor_button)
                or gamepad_button_check_pressed(0, tremmor_button_gpi)
                {
                    if mp_potato >= global.pot_tremmor_cost
                    {
                        with (obj_potato_combat){
                            //WP Cost
                            mp_potato -= global.pot_tremmor_cost;
                            //Attack Type
                            action_attk = 4;
                            //Set Sprite
                            spr = spr_potato_magic01;
                            //Set Frame to 0
                            image_index = 0;
                            //Frame Speed
                            image_speed = 0.25;
                            //Set WP Regen Timer
                            wp_regen_pause = true;
                            alarm[0] = 1*room_speed;
                        }
                        timer_glob = timer_glob_max - (timer_glob_max*timer_reduce);
                        active = true;
                    }
                }
            }
            if (ds_list_find_value(global.actions_pot_equip, b) = "deathspike"){ //Find Action Position
            var deathsp_button = (ds_list_find_value(button_map_key, b)); //Keyboard Input Based on pos in list
            var deathsp_button_gpi = (ds_list_find_value(button_map_key_gpi, b)); //Gamepad Input Based on pos in list
                //Death Spike Action//-----------------------------------------------------------------------------------------------
                if keyboard_check_pressed(deathsp_button)
                or gamepad_button_check_pressed(0, deathsp_button_gpi)
                {
                    if (mp_potato >= global.pot_deathspike_cost)
                    {
                        with (obj_potato_combat){
                            //WP Cost
                            mp_potato -= global.pot_deathspike_cost;
                            //Attack Type
                            action_attk = 6;
                            //Set Sprite
                            spr = spr_potato_magic01;
                            //Set Frame to 0
                            image_index = 0;
                            //Frame Speed
                            image_speed = 0.25;
                            //Set WP Regen Timer
                            wp_regen_pause = true;
                            alarm[0] = 1*room_speed;
							//Erase Buff Icon from list
							if(deathSpikeAttkBns){
								with(obj_combat_hud_all){
								    ds_list_delete(pbufftype, ds_list_find_index(pbufftype, "deathspike"));
								    pbuffs -= 1;
								}
							}
                        }
                        timer_glob = timer_glob_max - (timer_glob_max*timer_reduce);
                        active = true;
                    }
                }
            }
            alpha = 1;
        }
    }
    //Timer Global Reset
    if (timer_glob > 0){
        if (active = true){
            alarm[0] = 0.1*room_speed;
            alpha = 0.5;
            active = false;
        }
    }
    //Pause Check
    if instance_exists(obj_victory)
    or instance_exists(obj_gameover)
    or obj_potato_combat.pause_combat = 1
    {alpha01 = max(alpha01-0.1,0);}
    
    
    //Potato Time Check (Outdated)
    //if instance_exists(obj_potato_combat)
    //if active = false
    //{
        //if obj_potato_combat.time = 100
        //{alarm[0] = 0.1*room_speed;}
    //}
    
    //*******UNIQUE ACTIONS******\\
    //Parasite
    if (timer_para = 0 and obj_potato_combat.pause_combat = 0)
    {
        for (b = 0; b < ds_list_size(global.actions_pot_equip); ++b)
        {
            if (ds_list_find_value(global.actions_pot_equip, b) = "parasite"){ //Find Action Position
                var parasite_button = (ds_list_find_value(button_map_key, b)); //Keyboard Input Based on pos in list
                var parasite_button_gpi = (ds_list_find_value(button_map_key_gpi, b)); //Gamepad Input Based on pos in list
                    //Parasite Action//-----------------------------------------------------------------------------------------------
                    if keyboard_check_pressed(parasite_button)
                    or gamepad_button_check_pressed(0, parasite_button_gpi)
                    {
                        if mp_potato >= global.pot_parasite_cost
                        {
                            with (obj_potato_combat){
                                //WP Cost
                                mp_potato -= global.pot_parasite_cost;
                                //Attack Type
                                action_attk = 5;
                                //Set Sprite
                                spr = spr_potato_attack01;
                                //Set Frame to 0
                                image_index = 0;
                                //Frame Speed
                                image_speed = 0.25;
                                //Set WP Regen Timer
                                wp_regen_pause = true;
                                alarm[0] = 1*room_speed;
                            }
                            timer_para = timer_para_max - (timer_para_max*timer_reduce);
                            activePara = true; //Start Timer
                        }
                    }
                }
        }
    }
}

/* */
///Timer Countdowns UNIQUE
//Parasite
if (timer_para > 0){
    if (activePara == true){
        timerParaCount = 0;
        activePara = false;
    }
    //Set Alpha
    ParaAlpha = 0.5;
    //Timer Event CD
    timerParaCount += 1;
    //Once timer hits max
    if (timerParaCount > timerParaCount_max){
        //Clamp timer CD to reach 0
        timer_para = max(timer_para-0.1, 0);
        //Reset tick timer
        timerParaCount = 0;
    }
}else{
    ParaAlpha = 1;
}

/* */
///Drag Out Actions
var xx = global.display_gui_width; //X Based on screen size
var yy = global.display_gui_height; //Y Based on screen size
var b = 0;
//Attacks (Potato)
for (b = 0; b < ds_list_size(global.actions_pot_equip); ++b)
{
    //Basic Attack
    if mouse_check_button_pressed(mb_left) //Check Mouse Pressed
    if point_in_rectangle(obj_cursor.x, obj_cursor.y, (xx*0.3+88*b)-42, (yy*0.8)-42, (xx*0.3+88*b)+42, (yy*0.8)+42){ //Check Mouse Pos
        //Create action icon to drag
        instance_create(obj_cursor.x, obj_cursor.y, obj_act_pot_attack);
        //Action icon type
        if (ds_list_find_value(global.actions_pot_equip, b) = "attack"){ //Find value Attack
                obj_act_pot_attack.attack_type = 1;
                ds_list_replace(global.actions_pot_equip, b, 0);
        }
        if (ds_list_find_value(global.actions_pot_equip, b) = "solanum"){ //Find value Solanum
                obj_act_pot_attack.attack_type = 2;
                ds_list_replace(global.actions_pot_equip, b, 0);
        }
        if (ds_list_find_value(global.actions_pot_equip, b) = "ember"){ //Find value Ember
                obj_act_pot_attack.attack_type = 3;
                ds_list_replace(global.actions_pot_equip, b, 0);
        }
        if (ds_list_find_value(global.actions_pot_equip, b) = "asterid"){ //Find value Asterid
                obj_act_pot_attack.attack_type = 4;
                ds_list_replace(global.actions_pot_equip, b, 0);
        }
        if (ds_list_find_value(global.actions_pot_equip, b) = "tremmor"){ //Find value Asterid
                obj_act_pot_attack.attack_type = 5;
                ds_list_replace(global.actions_pot_equip, b, 0);
        }
        if (ds_list_find_value(global.actions_pot_equip, b) = "parasite"){ //Find value Asterid
                obj_act_pot_attack.attack_type = 6;
                ds_list_replace(global.actions_pot_equip, b, 0);
        }
        if (ds_list_find_value(global.actions_pot_equip, b) = "deathspike"){ //Find value Asterid
                obj_act_pot_attack.attack_type = 7;
                ds_list_replace(global.actions_pot_equip, b, 0);
        }
    }
}

/* */
/*  */
