/// @description Draw Event
//Setup
d3d_set_lighting(false);
d3d_set_fog(false, -1, 0, 10000);
draw_set_halign(fa_center);
draw_set_font(font0);
draw_set_colour(-1);
draw_set_alpha(alpha01);
draw_set_alpha(alpha);
//Vars
var xx = global.display_gui_width; //X Based on screen size
var yy = global.display_gui_height; //Y Based on screen size
//Draw Stuff
//List of Attacks (Potato)
for (b = 0; b < ds_list_size(global.actions_pot_equip); ++b)
{
    //Empty Space
    if (ds_list_find_value(global.actions_pot_equip, b) = 0){ //Find value
        draw_sprite(spr_wep_slot, 0 ,xx*0.3 + 88*b, yy*0.8); //Draw sprite accordingly
    }
    //Basic Attack
    if (ds_list_find_value(global.actions_pot_equip, b) = "attack"){ //Find value
        draw_sprite(equip_spr,0,xx*0.3 + 88*b,yy*0.8); //Draw sprite accordingly
        if timer_glob > 0{ //Timer (Uses Global)
            draw_text_colour(xx*0.3+88*b+4, yy*0.8 - 11,string_hash_to_newline(string_format(timer_glob,1,1)),c_black,c_black,c_black,c_black,1);
            draw_text(xx*0.3 + 88*b, yy*0.8 - 15,string_hash_to_newline(string_format(timer_glob,1,1)));
        }
        //Willpower Cost
        draw_text_colour(xx*0.3 + 88*b+4, yy*0.8 + 18, string_hash_to_newline(global.pot_attack_cost),c_black,c_black,c_black,c_black,1);
        draw_text(xx*0.3 + 88*b, yy*0.8 + 14, string_hash_to_newline(global.pot_attack_cost));
    }
    //Solanumn
    if (ds_list_find_value(global.actions_pot_equip, b) = "solanum"){ //Find value
        draw_sprite(spr_mag_psolanum,0,xx*0.3 + 88*b,yy*0.8); //Draw sprite accordingly
        if timer_glob > 0{ //Timer (Uses Global)
            draw_text_colour(xx*0.3+88*b+4, yy*0.8 - 11,string_hash_to_newline(string_format(timer_glob,1,1)),c_black,c_black,c_black,c_black,1);
            draw_text(xx*0.3 + 88*b, yy*0.8 - 15,string_hash_to_newline(string_format(timer_glob,1,1)));
        }
        //Willpower Cost
        draw_text_colour(xx*0.3 + 88*b+4, yy*0.8 + 18, string_hash_to_newline(pot_solanum_cost),c_black,c_black,c_black,c_black,1);
        draw_text(xx*0.3 + 88*b, yy*0.8 + 14, string_hash_to_newline(pot_solanum_cost));
    }
    //Ember
    if (ds_list_find_value(global.actions_pot_equip, b) = "ember"){ //Find value
        draw_sprite(spr_mag_pfire,0,xx*0.3 + 88*b,yy*0.8); //Draw sprite accordingly
        if timer_glob > 0{ //Timer (Uses Global)
            draw_text_colour(xx*0.3+88*b+4, yy*0.8 - 11,string_hash_to_newline(string_format(timer_glob,1,1)),c_black,c_black,c_black,c_black,1);
            draw_text(xx*0.3 + 88*b, yy*0.8 - 15,string_hash_to_newline(string_format(timer_glob,1,1)));
        }
        //Willpower Cost
        draw_text_colour(xx*0.3 + 88*b+4, yy*0.8 + 18, string_hash_to_newline(pot_fire_cost),c_black,c_black,c_black,c_black,1);
        draw_text(xx*0.3 + 88*b, yy*0.8 + 14, string_hash_to_newline(pot_fire_cost));
    }
    //Asterid
    if (ds_list_find_value(global.actions_pot_equip, b) = "asterid"){ //Find value
        draw_sprite(spr_mag_pasterid,0,xx*0.3 + 88*b,yy*0.8); //Draw sprite accordingly
        if timer_glob > 0{ //Timer (Uses Global)
            draw_text_colour(xx*0.3+88*b+4, yy*0.8 - 11,string_hash_to_newline(string_format(timer_glob,1,1)),c_black,c_black,c_black,c_black,1);
            draw_text(xx*0.3 + 88*b, yy*0.8 - 15,string_hash_to_newline(string_format(timer_glob,1,1)));
        }
        //Willpower Cost
        draw_text_colour(xx*0.3 + 88*b+4, yy*0.8 + 18, string_hash_to_newline(pot_asterid_cost),c_black,c_black,c_black,c_black,1);
        draw_text(xx*0.3 + 88*b, yy*0.8 + 14, string_hash_to_newline(pot_asterid_cost));
    }
    //Tremmor
    if (ds_list_find_value(global.actions_pot_equip, b) = "tremmor"){ //Find value
        draw_sprite(spr_mag_tremmor,0,xx*0.3 + 88*b,yy*0.8); //Draw sprite accordingly
        if timer_glob > 0{ //Timer (Uses Global)
            draw_text_colour(xx*0.3+88*b+4, yy*0.8 - 11,string_hash_to_newline(string_format(timer_glob,1,1)),c_black,c_black,c_black,c_black,1);
            draw_text(xx*0.3 + 88*b, yy*0.8 - 15,string_hash_to_newline(string_format(timer_glob,1,1)));
        }
        //Willpower Cost
        draw_text_colour(xx*0.3 + 88*b+4, yy*0.8 + 18, string_hash_to_newline(global.pot_tremmor_cost),c_black,c_black,c_black,c_black,1);
        draw_text(xx*0.3 + 88*b, yy*0.8 + 14, string_hash_to_newline(global.pot_tremmor_cost));
    }
    draw_set_alpha(ParaAlpha);
    //Parasite
    if (ds_list_find_value(global.actions_pot_equip, b) = "parasite"){ //Find value
        draw_sprite(spr_mag_parasite,0,xx*0.3 + 88*b,yy*0.8); //Draw sprite accordingly
        if (timer_para > 0){ //Timer (Uses Unique)
            draw_text_colour(xx*0.3+88*b+4, yy*0.8 - 11,string_hash_to_newline(string_format(timer_para, 1, 1)),c_black,c_black,c_black,c_black,1);
            draw_text(xx*0.3 + 88*b, yy*0.8 - 15,string_hash_to_newline(string_format(timer_para, 1, 1)));
        }
        //Willpower Cost
        draw_text_colour(xx*0.3 + 88*b+4, yy*0.8 + 18, string_hash_to_newline(global.pot_parasite_cost),c_black,c_black,c_black,c_black,1);
        draw_text(xx*0.3 + 88*b, yy*0.8 + 14, string_hash_to_newline(global.pot_parasite_cost));
    }
    draw_set_alpha(alpha);
    //Death Spike
    if (ds_list_find_value(global.actions_pot_equip, b) = "deathspike"){ //Find value
    
        //Check to see if BNS is active
        if (instance_exists(obj_potato_combat)){
            if (obj_potato_combat.deathSpikeAttkBns == true){
                image_speed = 0.009;
                draw_sprite(spr_mag_deathspike, -1, xx*0.3 + 88*b, yy*0.8); //Draw sprite accordingly animated
            }
            else{
                draw_sprite(spr_mag_deathspike, 0, xx*0.3 + 88*b, yy*0.8); //Draw sprite accordingly default
            }
        }
        else{
            draw_sprite(spr_mag_deathspike, 0, xx*0.3 + 88*b, yy*0.8); //Draw sprite accordingly default
        }
        
        if timer_glob > 0{ //Timer (Uses Global)
            draw_text_colour(xx*0.3+88*b+4, yy*0.8 - 11,string_hash_to_newline(string_format(timer_glob,1,1)),c_black,c_black,c_black,c_black,1);
            draw_text(xx*0.3 + 88*b, yy*0.8 - 15,string_hash_to_newline(string_format(timer_glob,1,1)));
        }
        //Willpower Cost
        draw_text_colour(xx*0.3 + 88*b+4, yy*0.8 + 18, string_hash_to_newline(global.pot_deathspike_cost),c_black,c_black,c_black,c_black,1);
        draw_text(xx*0.3 + 88*b, yy*0.8 + 14, string_hash_to_newline(global.pot_deathspike_cost));
    }
    draw_set_alpha(alpha);
    //Hotkey Draw
    if (b = 0){draw_text(xx*0.3 - 32 + 88*b, yy*0.8 - 42, string_hash_to_newline("Z"));}
    if (b = 1){draw_text(xx*0.3 - 32 + 88*b, yy*0.8 - 42, string_hash_to_newline("X"));}
    if (b = 2){draw_text(xx*0.3 - 32 + 88*b, yy*0.8 - 42, string_hash_to_newline("C"));}
    if (b = 3){draw_text(xx*0.3 - 32 + 88*b, yy*0.8 - 42, string_hash_to_newline("V"));}
    if (b = 4){draw_text(xx*0.3 - 32 + 88*b, yy*0.8 - 42, string_hash_to_newline("B"));}
    if (b = 5){draw_text(xx*0.3 - 32 + 88*b, yy*0.8 - 42, string_hash_to_newline("N"));}
    if (b = 6){draw_text(xx*0.3 - 32 + 88*b, yy*0.8 - 42, string_hash_to_newline("M"));}
}

//Set Default Stuff
draw_set_alpha(1);
draw_set_halign(fa_left);

///Debug

