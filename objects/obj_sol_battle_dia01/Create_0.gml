/// @description Start up
pos = 1
text = 0
active = 0
draw_scene = 0
port = spr_port
alarm[0] = 1*room_speed

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Sol:
Ack! Ugh! *Cough* *Cough*...How could I be defeated so easily!...
"
strings[1] = @"Kassadin:
Wow Myth you did it! You won the match! Great job! (Oh I hope this was convincing
enough...)
"
strings[2] = @"Mythor:
Oh...sweet.
"
strings[3] = @"Sol:
UGH! I-I'm so mad I lost! Erk! OOF!
"
strings[4] = @"Kassadin:
Yeah I'm sure you are...ugh. Alright we're done.
"

