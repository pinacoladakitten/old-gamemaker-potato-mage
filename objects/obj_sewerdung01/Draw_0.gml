/// @description Draw Model
if room = rm_overw_sewdung01
{
    draw_set_alpha(1);
    d3d_set_lighting(true); 
    texture_set_repeat(true);
    d3d_transform_add_scaling(1,1,1)
    d3d_transform_add_rotation_z(0)
    d3d_transform_add_translation(x,y,0)
    d3d_set_lighting(false); 
    d3d_model_draw(model0,0,0,0,background_get_texture(bg_castle01_light02));
    d3d_set_lighting(true); 
    d3d_model_draw(model1,0,0,0,background_get_texture(bg_castle01_nolight));
    d3d_model_draw(model3,0,0,0,background_get_texture(lamp_tex));
    d3d_model_draw(model4,0,0,0,background_get_texture(sew_black01));
    d3d_model_draw(model5,0,0,0,background_get_texture(sew_brick01));
    d3d_model_draw(model6,0,0,0,background_get_texture(sew_brick02));
    d3d_model_draw(model7,0,0,0,background_get_texture(sew_brick03));
    d3d_model_draw(model8,0,0,0,background_get_texture(sew_grate01));
    d3d_model_draw(model9,0,0,0,background_get_texture(sew_grate03));
    d3d_model_draw(model10,0,0,0,background_get_texture(sew_pipe01));
    d3d_model_draw(model11,0,0,0,background_get_texture(sew_pipe02));
    d3d_model_draw(model12,0,0,0,background_get_texture(sew_tile01)); 
    d3d_model_draw(model13,0,0,0,background_get_texture(sew_tile02));
    d3d_model_draw(model14,0,0,0,sprite_get_texture(sew_waterfall01,-1));
    d3d_model_draw(model15,0,0,0,background_get_texture(stone01));
    d3d_set_lighting(true); 
    d3d_transform_set_identity();
    //Fog and Other Settings
    d3d_set_fog(true, __background_get_colour( ), 1000, 4200);
}

