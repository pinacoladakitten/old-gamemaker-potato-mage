audio_play_sound(snd_encounter, 10, false);
audio_play_sound(snd_darkness02, 10, false);
audio_sound_gain(snd_darkness02, 0.25, 0);
if battle_music = 1
{
    audio_pause_sound(overw_music);
    audio_sound_gain(overw_music, 0, 0);
    audio_stop_sound(combat_music);
    audio_play_sound(combat_music, 10, true);
}

