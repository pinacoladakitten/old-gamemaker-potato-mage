//Preset Stuff
d3d_set_lighting(false);
draw_set_alpha(alpha);
draw_sprite(spr_combat_panel02,-1,xx*0.5,yy*0.85)
draw_sprite(item,0,(xx*0.5)-100,yy*0.86)
draw_set_font(font00);
//Obtained!
draw_text_colour((xx*0.5)-52,(yy*0.81)+20,string_hash_to_newline("Acquired!:"),c_black,c_black,c_black,c_black,alpha)
draw_text((xx*0.5)-50,(yy*0.8)+20,string_hash_to_newline("Acquired!:"))
//Weapons
draw_set_font(font0);
if item = spr_inv_dagger
{
    draw_text_colour((xx*0.5)-52,yy*0.86,string_hash_to_newline("Dagger"),c_black,c_black,c_black,c_black,alpha)
    draw_text((xx*0.5)-50,yy*0.85,string_hash_to_newline("Dagger"))
}
if item = spr_inv_dark_wood_stick
{
    draw_text_colour((xx*0.5)-52,yy*0.86,string_hash_to_newline("Dark Stick"),c_black,c_black,c_black,c_black,alpha)
    draw_text((xx*0.5)-50,yy*0.85,string_hash_to_newline("Dark Stick"))
}
if item = spr_inv_handscythe01
{
    draw_text_colour((xx*0.5)-52,yy*0.86,string_hash_to_newline("Hand Scythe"),c_black,c_black,c_black,c_black,alpha)
    draw_text((xx*0.5)-50,yy*0.85,string_hash_to_newline("Hand Scythe"))
}
//Magic
if item = spr_mag_pfire
{
    draw_text_colour((xx*0.5)-52,yy*0.86,string_hash_to_newline("Ember"),c_black,c_black,c_black,c_black,alpha)
    draw_text((xx*0.5)-50,yy*0.85,string_hash_to_newline("Ember"))
}
if item = spr_mag_psolanum
{
    draw_text_colour((xx*0.5)-52,yy*0.86,string_hash_to_newline("Solanum"),c_black,c_black,c_black,c_black,alpha)
    draw_text((xx*0.5)-50,yy*0.85,string_hash_to_newline("Solanum"))
}
//Items
if item = spr_key01
{
    draw_text_colour((xx*0.5)-52,yy*0.86,string_hash_to_newline("Key Shard \\#1"),c_black,c_black,c_black,c_black,alpha)
    draw_text((xx*0.5)-50,yy*0.85,string_hash_to_newline("Key Shard \\#1"))
}
if item = spr_key03
{
    draw_text_colour((xx*0.5)-52,yy*0.86,string_hash_to_newline("Key Plate"),c_black,c_black,c_black,c_black,alpha)
    draw_text((xx*0.5)-50,yy*0.85,string_hash_to_newline("Key Plate"))
}
//Reset Stuff
draw_set_halign(fa_left);
draw_set_colour(-1);
draw_set_alpha(1);

