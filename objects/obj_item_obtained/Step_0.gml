if fade = 0
{
    alpha = min(alpha+0.1,1)
}
if fade = 1
{
    alpha = max(alpha-0.1,0)
    if alpha <= 0
    {
        instance_destroy();
    }
}

