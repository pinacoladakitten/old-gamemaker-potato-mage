/// @description Angles
angle+=2
if angle = 360
{
    angle = 0
}
if (obj_potato_overw.y < y)
if distance_to_object(obj_potato_overw) < 20
{
    angle01 = min(angle01+10,180)
}
if (obj_potato_overw.y > y)
if distance_to_object(obj_potato_overw) < 20
{
    angle01 = max(angle01-10,0)
}
if distance_to_object(obj_potato_overw) > 20
{
    angle01 = max(angle01-10,0)
}

///Text Control
if active = 1
if !instance_exists(obj_trans_fade_in_out)
{
    if keyboard_check_pressed(ord("Z"))
    if string_length(strings[text]) < pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        if (text = 9)
        {
            pos = 0
            active = 0
            pause = 0
            text = 9
        }
        if (text = 10)
        {
            pos = 0
            active = 0
            pause = 0
            text = 9
        }
        if (text = 19)
        {
            pos = 0
            active = 0
            pause = 1
            //Panic Music Queue
            audio_play_sound(snd_darkness01,10,false);
            if audio_is_playing(overw_music)
            {
                audio_stop_sound(overw_music);
            }
            if !audio_is_playing(snd_panic01)
            {
                audio_play_sound(snd_panic01,10,true);
                audio_sound_gain(snd_panic01,0.6,0);
            }
        }
        if (text = 20)
        {
            obj_potato_overw.sprite_index = spr_potato_ow_idle_look
            obj_potato_overw.image_index = 1
        }
        if (text = 25)
        {
            obj_potato_overw.image_index = 2
        }
        if (text = 25)
        {
            instance_create(x-2,y,obj_key_shard02);
        }
        if text != 30 or text != 31
        {
            pos = 0
            text = text+1
        }
        else
        {
            if z > 90
            if obj_camera_ow.zoom = 130
            {
                pos = 0
                text = text+1
            }
        }
        if text = 31
        {
            if !audio_is_playing(snd_panic01)
            {
                pos = 0
                text = text+1
            }
        }
        if (text > maxstrings)
        {
            pos = 0
            active = 0
            pause = 0
            obj_potato_overw.sprite_index = spr_potato_ow_idle_l
            audio_play_sound(overw_music,10,true);
            audio_sound_gain(overw_music,1,0);
            instance_destroy();
        }
    }
    maxstrings = 39
}

///Events
if active = 1
{
    pause = 1
}
//Portraits
if string_char_at(strings[text], 1) = "M"
{
    port = spr_potato_portrait_text
}
else
{
    port = spr_npc_spirit02_port
}
//Cam Zoom
if text >= 21
if text < 31
{
    with obj_camera_ow
    {
        x = obj_potato_overw.x+zoom
        zoom = max(zoom-(zoom)*0.05,100)
    }
}
else
{
    with obj_camera_ow
    {
        x = obj_potato_overw.x+zoom
        zoom = min(zoom+(zoom)*0.01,130)
    }
}
//Drop
if text < 24
{
    if distance_to_object(obj_potato_overw) < 50
    {
        z = max(z-(z-0)*0.05,0)
    }
    if distance_to_object(obj_potato_overw) > 50
    {
        z = min(z+1,100)
    }
}
//Hand Drop
if text >= 20
if text < 29
{
    zhand = max(zhand-(zhand-0)*0.05,0)
    if handspeed = 0
    {
        image_index = 0
    }
    if zhand <= 5
    {
        if image_index > 2
        if image_index < 3
        {
            active = 1
        }
        if image_index >= 3
        {
            handspeed = 3
            //Play Grab Sound
            if image_speed != 0
            {
                audio_play_sound(snd_darkness02,10,false);
            }
            image_speed = 0
        }
        else
        {
            handspeed = -1
        }
    }
    //Camera Drop
    if zhand <= obj_camera_ow.camz
    if obj_camera_ow.camz > 0
    {
        obj_camera_ow.camz = zhand
    }
}
else
{
    //Play Sound
    if z = 0
    if !audio_is_playing(snd_darkness02)
    {
        audio_play_sound(snd_darkness02,10,false);
    }
    if z >= 5
    if z < 10
    if !audio_is_playing(snd_darkness01)
    {
        audio_play_sound(snd_darkness01,10,false);
    }
    //Hand Move Up
    zhand = min(zhand+(zhand+0)*0.5,100)
    z = zhand
}
//Camera
if text >= 17
if text < 20
{
    obj_camera_ow.camz = min(obj_camera_ow.camz+(120-obj_camera_ow.camz)*0.005,120)
    //Music Fade Out
    audio_sound_gain(overw_music,0,10*room_speed)
    if audio_sound_get_gain(overw_music) = 0
    {
        audio_stop_sound(overw_music);
    }
}
//Panic Music Fade Out
if text >= 31
{
    audio_sound_gain(snd_panic01,0,10*room_speed)
    if audio_sound_get_gain(snd_panic01) = 0
    {
        audio_stop_sound(snd_panic01);
    }
}
//Misc
if instance_exists(obj_trans_fade_in_out)
{
obj_trans_fade_in_out.fadespd = 0.0075
    if obj_trans_fade_in_out.alpha >= 1
    {
        //Potato
        obj_potato_overw.x = x
        obj_potato_overw.y = 1835
        obj_potato_overw.sprite_index = spr_potato_ow_idle_l
        //Cam
        with obj_camera_ow
        {
            vsp = obj_potato_overw.vsp
            hsp = obj_potato_overw.hsp
            camxto = obj_potato_overw.x
            camyto = obj_potato_overw.y
            camz = 0
            x = obj_potato_overw.x+zoom
            y = obj_potato_overw.y
        }
    }
}

///Sounds
if active = 1
if !instance_exists(obj_trans_fade_in_out)
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

