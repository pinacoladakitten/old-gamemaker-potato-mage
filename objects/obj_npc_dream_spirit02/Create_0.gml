/// @description Vars
image_speed = 0.05
angle = 0
angle01 = 0
alpha = 1
port = spr_port
z = 100
zhand = 200
handspeed = 0

///Start up
pos = 1
text = 0
active = 0
draw_scene = 0

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
Th-the shard man! I need to find it!
"
strings[1] = @"???:
It's the only way out, the only way out...the...o-n-l-y
"
strings[2] = @"Mythor:
Um...ok?-
"
strings[3] = @"???:
You must find it! I don't remember anything, but from what
I can gather the shard will lead the way!
"
strings[4] = @"???:
Please! This is only what I ask of you! Hurry! Before he
comes-...
"
strings[5] = @"Mythor:
Before who c-comes?...
"
strings[6] = @"???:
Who? Wait...what was I talking about again...um...gah! I
can't remember anything! Oh that's right! The shard!
Get it please! It's the only way!
"
strings[7] = @"Mythor:
...I-I'll look out for it.
"
strings[8] = @"???:
No No! Don't just 'look out for it', look very very hard
for it! Look for it as if your life is on the line! You
understand me!!?? 
"
strings[9] = @"Mythor:
Ok ok! I'll get right on it!
"
strings[10] = @"???:
Don't just stand there! Go along and find that 'shard'!
"
strings[11] = @"???:
Oh! You found it! Good good, give it here!
"
strings[12] = @"???:
Luckily I have already attained the other half as well!!
"
strings[13] = @"???:
Thank you! Thank you!! Thank-you!!! Now I can finally make
my escape!!
"
strings[14] = @"Mythor:
Great! Now we can both get out of here together! Right?
"
strings[15] = @"???:
...PFFFFFFFFFFTTTT!!! HAHAHAHAHAHAHAHAHA!!
"
strings[16] = @"Mythor:
B-but!?-
"
strings[17] = @"???:
NONSENSE! Don't you see it yet!? There's only room for one of
us to escape! That being me! Dragging you along would only
make everything more difficult!
"
strings[18] = @"???:
I deeply appreciate your help! Even though you only had to
walk to the other side and grab it for me, which doesn't
take much effort anyway since, out of sheer luck, 'they'
haven't come after you yet!
"
strings[19] = @"???:
Anyways I'll be sure to always remember you for helping my
escape. Sorry it had to end this way, but in dire situations
it's every spirit for themselves.
"
strings[20] = @"???:
UGH! AAAAAAAAAAAAAAGGGGHHHH!! NONONONONO!! NOT RIGHT NOW!!
NOT RIGHT NOW!! NOT WHILE I WAS SO CLOSE!!
"
strings[21] = @"Mythor:
....!!!!????-
"
strings[22] = @"???:
I THOUGHT I COULD ESCAPE YOU IN TIME!! WHAT DID I DO TO 
DESERVE THIS!!!!
"
strings[23] = @"???:
OK I ADMIT IT! I'VE DONE SOMETHING WRONG! I REGRET IT TRULY!
"
strings[24] = @"???:
BUT IT WAS MY ONLY CHANCE AT FINALLY GETTING OUT OF HERE!!!
"
strings[25] = @"Mythor:
Take my hand!-
"
strings[26] = @"???:
No!...Here! The stone! Take it! I at least want to do one good
deed before I leave!
"
strings[27] = @"???:
...Hehe...I've done so many regretful things, hurting other 
people in the process. I can't believe I'm saying this, but 
seeing you has sparked me to realize this remorse. Now I can
finally serve for what I've caused.
"
strings[28] = @"Mythor:
Wait! I almost got you! Just take my hand please! We can do this
together!
"
strings[29] = @"???:
Heed my word as my last plea! Go! Get out of here! Worry about 
yourself!
"
strings[30] = @"???:
Farewell young spirit!
"
strings[31] = @"???:
UGH!! AAAAAAGGGHHHH!!
"
strings[32] = @"Mythor:
........
"
strings[33] = @"Mythor:
........no...
"
strings[34] = @"Mythor:
........why...
"
strings[35] = @"Mythor:
Why is this happening!!?
"
strings[36] = @"Mythor:
I don't know whether to be terrified or sorry...
"
strings[37] = @"Mythor:
Perhaps both, but I don't feel either...
"
strings[38] = @"Mythor:
It all just happened so sudden.
"
strings[39] = @"Mythor:
I guess all that's left....is the key....
"
strings[40] = @"Mythor:
I guess all that's left....is the key....
"

