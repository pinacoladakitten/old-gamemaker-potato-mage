d3d_set_lighting(false);
var ss,cc,tex;
tex = sprite_get_texture(spr_npc_spirit02,-1);
tex02 = sprite_get_texture(spr_npc_spirit02_web,-1);
tex03 = sprite_get_texture(spr_talk,-1);
tex01 = sprite_get_texture(spr_ow_shadow,-1);
tex04 = sprite_get_texture(spr_npc_spirit02_eff_hand01,handspeed);
tex05 = sprite_get_texture(spr_npc_spirit02_eff_hand02,-1);
ss = sin(obj_camera_ow.direction*pi/180);
cc = cos(obj_camera_ow.direction*pi/180);

draw_set_alpha(alpha);
d3d_transform_add_rotation_z(angle01)
d3d_transform_add_translation(x,y,0)
d3d_draw_wall(0+15*ss,0+15*cc,z+35,0-15*ss,0-15*cc,z+5,tex,1,1)
d3d_draw_wall(0+15*ss,0+15*cc,5,0-15*ss,0-15*cc,0,tex01,1,1)
d3d_transform_set_identity();
d3d_draw_wall(x-1,y+0.25,z+200,x-1,y-0.25,z+10,tex02,1,1)

if place_meeting(x,y,obj_potato_overw)
if active = 0
{
    d3d_set_lighting(false);
    d3d_transform_add_rotation_z(angle)
    d3d_transform_add_translation(x,y,0)
    d3d_draw_wall(0,0+3,40,0,0-3,34,tex03,1,1)
    d3d_transform_set_identity();
}
if text >= 19
{
    draw_set_alpha(alpha);
    d3d_transform_add_translation(x,y,0)
    d3d_draw_wall(0+4,0+18,zhand+200,0+4,0-18,zhand+20,tex05,1,5)
    d3d_draw_wall(0+5,0+18,zhand+35,0+5,0-18,zhand+5,tex04,1,1)
    d3d_transform_set_identity();
}
draw_set_alpha(1);

