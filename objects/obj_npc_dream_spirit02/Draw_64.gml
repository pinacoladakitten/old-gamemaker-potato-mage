/// @description Main event
if active = 1
if !instance_exists(obj_trans_fade_in_out)
{
    if draw_scene = 0
    if text < 20
    {
        draw_set_alpha(0.5)
        draw_rectangle_colour(0,0,1280,720,c_black,c_black,c_black,c_black,0)
    }
    //Setup
    draw_set_font(font0);
    draw_set_halign(fa_left);
    draw_set_color(c_white);
    draw_set_alpha(1)
    //Textbox
    draw_sprite(spr_textbox,0,0,550)
    draw_sprite(port,0,40,560)
    //The Text
    cstr = string_copy(strings[text],1,pos)
    draw_text_ext_transformed(250,560,string_hash_to_newline(cstr),-1,-1,1,1,0)
    pos = min(pos+2,255)
    if keyboard_check(ord("X"))
    {
        pos = min(pos+5,255)
    }
}

