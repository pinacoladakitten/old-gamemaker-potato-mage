/// @description Start up
pos = 1
text = 0
active = 1
draw_scene = 0
port = spr_port

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
I don't understand! What makes you so strong! What do I lack
against the likes of you!?
"
strings[1] = @"???:
No! NO! NOO! This is mutiny! A rebellion against the very nature
of my motto!
"
strings[2] = @"Mythor:
I'm not sure what you really mean by that, but I think we should call 
it a truce and simply move on...right?
"
strings[3] = @"???:
Now you dare tell me to simply 'move on' and forget about
this moment?
"
strings[4] = @"Mythor:
I guess you could say that, look we're all friends here...or 
atleast we could start being friends as of now-
"
strings[5] = @"???:
I would never befriend such weak spirited scum like you! My heart
burns with rage over this tragedy! I WILL NEVER LOSE!
"
strings[6] = @"???:
BURN TO CINDER!!! RETURN TO DUST AND LEAVE MY SIGHT!!!
"
strings[7] = @"Mythor:
!?...
"

