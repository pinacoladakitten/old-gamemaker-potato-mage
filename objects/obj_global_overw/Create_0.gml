/// @description Vars **Music, SFX, Player XY coords**
globalvar enm_encounter;
enm_encounter = 0
globalvar event;
event = 0
globalvar potato_room;
potato_room = rm_overw_home01
globalvar ruum;
ruum= 0
globalvar overw_music;
overw_music = 0
globalvar potato_room_name;
potato_room_name = "Home"
globalvar activee;
activee = 0
globalvar party;
party = 1
globalvar file;
file = 1
globalvar potato_x;
potato_x = 0
globalvar potato_y;
potato_y = 0
globalvar potato_z;
potato_z = 0
globalvar dead;
dead = 0
globalvar controller;
controller = 0
globalvar pause;
pause = 0
//Sound
global.music_volume = 1; //Outdated
audio_group_load(sfx);
//For Gain check Room Start

///P3DC
p3dc_init();
//Vars
globalvar p3dc;
p3dc = 0
globalvar p3dc_floor;
p3dc_floor = 0
globalvar map_type;
map_type = 0
globalvar map_x;
map_x = 0
globalvar map_y;
map_y = 0
globalvar collision_player;
collision_player = 0
globalvar collision_camera;
collision_camera = 0
globalvar collision_map;
collision_map = 0
//Models
//Player
collision_player = p3dc_begin_model();
p3dc_add_block(-2,-2,0,2,2,10);
p3dc_end_model();
//Map
collision_map = p3dc_begin_model();
p3dc_add_model(map_type,0,0,0);
p3dc_end_model();

///Debug Vars
draw_coords = -1;
draw_color = -1;

///Setup Controls
//Keyboard
global.keyb_up = vk_up;
global.keyb_down = vk_down;
global.keyb_left = vk_left;
global.keyb_right = vk_right;
global.keyb_confirm = ord("Z");
global.keyb_cancel = ord("X");
global.keyb_action01 = vk_space;
global.keyb_menu = ord("A");
global.keyb_column_scrollR = ord("D");
global.keyb_column_scrollL = ord("A");
global.keyb_combat_moveU = vk_up;
global.keyb_combat_moveD = vk_down;
global.keyb_combat_moveL = vk_left;
global.keyb_combat_moveR = vk_right;
global.keyb_jump = vk_space;
global.keyb_help = vk_backspace;
global.keyb_targetL = ord("Q");
global.keyb_targetR = ord("E");
global.keyb_die = ord("P");
//Action Hotkeys
global.bonus_action01 = ord("Z");
global.bonus_action02 = ord("X");
global.bonus_action03 = ord("C");
global.bonus_action04 = ord("V");
global.bonus_action05 = ord("B");
global.bonus_action06 = ord("N");
global.bonus_action07 = ord("M");
//Gamepad
global.gpi_up = gp_padu;
global.gpi_down = gp_padd;
global.gpi_left = gp_padl;
global.gpi_right = gp_padr;
global.gpi_confirm = gp_face1;
global.gpi_cancel = gp_face2;
global.gpi_jump = gp_face4;
global.gpi_action01 = gp_shoulderr;
global.gpi_menu = gp_start;
global.gpi_column_scrollR = gp_shoulderr;
global.gpi_column_scrollL = gp_shoulderl;
global.gpi_help = gp_select;
global.gpi_targetL = gp_padl;
global.gpi_targetR = gp_padr;
//Action Hotkeys
global.gpi_bonus_action01 = gp_face4;
global.gpi_bonus_action02 = gp_face4;
global.gpi_bonus_action03 = gp_face4;
global.gpi_bonus_action04 = gp_face4;
global.gpi_bonus_action05 = gp_face4;
global.gpi_bonus_action06 = gp_face4;
global.gpi_bonus_action07 = gp_face4;
reset = 0
//Misc
any_pressed = 0
i = 0
//Controller Setup
gamepad_set_axis_deadzone(0, 0.5);
gamepad_set_button_threshold(0, 0.1);
//Cursor
instance_create(0,0,obj_cursor);

///Lists
globalvar allies_ow;
allies_ow = ds_list_create();
ds_list_add(allies_ow,0);
gpu_set_cullmode(cull_counterclockwise);

//D3D Unload All
global.unloadAll = false;

//Inventory Can Use
global.inventoryDisable = false;
