/// @description Collected Objects
global.o = 0;
global.maxobject = 0;
for (global.o = 0; global.o < global.maxobject; global.o += 1)
{
    global.objectcollected[global.o] = 0;
}

///Randomize
randomize();

///Overworld Events
//Intro
global.intro_umbra_gone01 = 0;
//Sewer01
global.sewer01_jace_evt01 = 0;

///**Resolution Settings**
//Anti-aliasing & Vsync
aa = 0;
vsync = 1;
display_reset(aa, vsync);
//Display Size
global.display_width = display_get_width();
global.display_height = display_get_height();
//GUI Size
global.display_gui_width = 1920;
global.display_gui_height = 1080;

/* */
/*  */
