/// @description Start of every room **AUDIO GAIN, COLLECTED OBJs**

//Set Audio Volume
audio_group_set_gain(sfx, 0.35, 0); //SFX
audio_group_set_gain(audiogroup_default, 0.6, 0); //BGM default: 0.4

//Delete Collected Objects
for (global.o = 0; global.o < global.maxobject; global.o += 1)
{
    with(global.objectcollected[global.o])
    {instance_destroy();}
}

///P3DC
if instance_exists(obj_potato_overw)
if p3dc = 1 //p3dc check//
{
    x = obj_potato_overw.x
    y = obj_potato_overw.y
    //Change Map
    p3dc_clear_model(collision_map);
    collision_map = p3dc_begin_model();
    p3dc_add_model(map_type,0,0,0);
    p3dc_end_model();
}

