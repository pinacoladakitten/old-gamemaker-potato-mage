/// @description Inventory Active Check
if instance_exists(obj_inventory_ow)
{
    if obj_inventory_ow.active = 1{
        pause = 1;
    }
}

///Controller Check
for (i=gp_face1; i<gp_axisrv; i++){
    if gamepad_button_check(0, i){
        any_pressed = 1;
        exit;
        }
    }
if gamepad_axis_value(0, gp_axislh) != 0 or gamepad_axis_value(0, gp_axislv) != 0
{any_pressed = 1;}
if any_pressed = 1
{controller = 1}
if keyboard_check_pressed(vk_anykey)
{controller = 0
any_pressed = 0}

