/// @description Debug Draw
d3d_set_fog(false, -1, 0, 10000);
if instance_exists(obj_potato_overw)
if draw_coords = 1
{
	draw_set_color(draw_color);
    draw_set_font(font3);
    draw_text(0,0,string_hash_to_newline("X:"+string(obj_potato_overw.x)+" "+"Y:"+string(obj_potato_overw.y)+" "+"Z:"+string(obj_potato_overw.z-5)+" "+"Zspeed:"+string(obj_potato_overw.zspeed)+" "+"Room:"+string(room)));
}

///GUI Size
display_set_gui_size(global.display_gui_width, global.display_gui_height);

