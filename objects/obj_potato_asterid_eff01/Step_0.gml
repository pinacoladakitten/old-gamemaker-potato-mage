/// @description Destroy
alpha -= 0.01
x += xdir
y += ydir
z += zdir
xsc -= 0.01
ysc -= 0.01
zsc -= 0.01
if alpha <= 0 
or xsc <= 0
{
    instance_destroy();
}

