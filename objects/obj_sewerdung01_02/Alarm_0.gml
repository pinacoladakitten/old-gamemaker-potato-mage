/// @description Loading Model
obj_loading.image_index += 0.25
if modelload = 0
{
    d3d_model_load(model0_01, "maps/mp_sewdung01/sect02/bg_castle01_light02.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 1
{
    d3d_model_load(model1_01, "maps/mp_sewdung01/sect02/bg_castle01_nolight.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 2
{
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 3
{
    d3d_model_load(model3_01, "maps/mp_sewdung01/sect02/lamp_tex.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 4
{
    d3d_model_load(model4_01, "maps/mp_sewdung01/sect02/sew_black01.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 5
{
    d3d_model_load(model5_01, "maps/mp_sewdung01/sect02/sew_brick01.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 6
{
    d3d_model_load(model6_01, "maps/mp_sewdung01/sect02/sew_brick02.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 7
{
    d3d_model_load(model7_01, "maps/mp_sewdung01/sect02/sew_brick03.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 8
{
    d3d_model_load(model8_01, "maps/mp_sewdung01/sect02/sew_grate01.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 9
{
    d3d_model_load(model9_01, "maps/mp_sewdung01/sect02/sew_grate03.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 10
{
    d3d_model_load(model10_01, "maps/mp_sewdung01/sect02/sew_pipe01.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 11
{
    d3d_model_load(model11_01, "maps/mp_sewdung01/sect02/sew_pipe02.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 12
{
    d3d_model_load(model12_01, "maps/mp_sewdung01/sect02/sew_tile01.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 13
{
    d3d_model_load(model13_01, "maps/mp_sewdung01/sect02/sew_tile02.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 14
{
    d3d_model_load(model14_01, "maps/mp_sewdung01/sect02/sew_waterfall01.d3d");
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 15
{
    d3d_model_load(model15_01, "maps/mp_sewdung01/sect02/stone01.d3d");
    if !audio_is_playing(overw_music)
    {audio_play_sound(overw_music,10,true);}
    obj_loading.draw = 0
}

