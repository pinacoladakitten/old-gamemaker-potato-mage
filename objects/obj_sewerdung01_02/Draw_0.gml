/// @description Draw Model
if room = rm_overw_sewdung01_02
{
    draw_set_alpha(1);
    d3d_set_lighting(true); 
    texture_set_repeat(true);
    d3d_transform_add_scaling(1,1,1)
    d3d_transform_add_rotation_z(0)
    d3d_transform_add_translation(x+711,y+387,0)
    d3d_set_lighting(false); 
    d3d_model_draw(model0_01,0,0,0,background_get_texture(bg_castle01_light02));
    d3d_set_lighting(true); 
    d3d_model_draw(model1_01,0,0,0,background_get_texture(bg_castle01_nolight));
    d3d_model_draw(model3_01,0,0,0,background_get_texture(lamp_tex));
    d3d_model_draw(model4_01,0,0,0,background_get_texture(sew_black01));
    d3d_model_draw(model5_01,0,0,0,background_get_texture(sew_brick01));
    d3d_model_draw(model6_01,0,0,0,background_get_texture(sew_brick02));
    d3d_model_draw(model7_01,0,0,0,background_get_texture(sew_brick03));
    d3d_model_draw(model8_01,0,0,0,background_get_texture(sew_grate01));
    d3d_model_draw(model9_01,0,0,0,background_get_texture(sew_grate03));
    d3d_model_draw(model10_01,0,0,0,background_get_texture(sew_pipe01));
    d3d_model_draw(model11_01,0,0,0,background_get_texture(sew_pipe02));
    d3d_model_draw(model12_01,0,0,0,background_get_texture(sew_tile01)); 
    d3d_model_draw(model13_01,0,0,0,background_get_texture(sew_tile02));
    d3d_model_draw(model14_01,0,0,0,sprite_get_texture(sew_waterfall01,-1));
    d3d_model_draw(model15_01,0,0,0,background_get_texture(stone01));
    d3d_set_lighting(true); 
    d3d_transform_set_identity();
    //Fog and Other Settings
    d3d_set_fog(true, __background_get_colour( ), 1000, 4200);
}

