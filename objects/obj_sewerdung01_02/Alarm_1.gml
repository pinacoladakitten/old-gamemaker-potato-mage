/// @description Real Lighting
if room = rm_overw_sewdung01_02
{
    with obj_camera_ow{
        d3d_light_define_direction(1,1,0,-0.25,c_white);
        d3d_light_enable(1,true);
        d3d_light_define_direction(7,-0.1,0,-0.1,c_orange);
        d3d_light_enable(7,true);
        zoom = 130
    }
}

