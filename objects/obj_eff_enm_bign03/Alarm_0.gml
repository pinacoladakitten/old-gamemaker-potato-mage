/// @description Create Aoe Marker
choice = choose(1,1);
dir  = point_direction(x,y,obj_potato_combat.x,obj_potato_combat.y)+30;
if choice = 1
{
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -10+dir
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -20+dir
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -30+dir
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -40+dir
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -50+dir
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -60+dir
}
if choice = 2
{
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -15+dir
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -25+dir
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -35+dir
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -45+dir
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -55+dir
    instance_create(x,y,obj_aoemark_bign01);
    var targetaoe = instance_find(obj_aoemark_bign01,instance_number(obj_aoemark_bign01)-1);
    targetaoe.r = -65+dir
}

