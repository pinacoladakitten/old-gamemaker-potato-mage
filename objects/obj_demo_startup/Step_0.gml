/// @description Press Confirm
if keyboard_check_pressed(global.keyb_confirm)
or gamepad_button_check_pressed(0, global.gpi_confirm)
{
    if !instance_exists(obj_trans_fade_in)
    {
        instance_create(0,0,obj_trans_fade_in);
        obj_trans_fade_in.roomgoto = rm_BELFRY_title;
    }
}

