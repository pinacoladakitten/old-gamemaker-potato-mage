/// @description Press Z prompt
draw_set_font(font0);
draw_set_halign(fa_left);
draw_set_color(c_white);
draw_text(50+xx,50+yy,
string_hash_to_newline(@"
NOTE: This game is only meant to be showcased for personal reasons, so yes, yes, I know, 
all of the music in this game is obviously not by me since I haven't got a clue how to produce 
music. Anyways, I hope you enjoy what I have in store this time. There are probably a couple 
bugs since I'm the only one play-testing this stuff, so if you find any or have a suggestion 
about anything or if you have any cool ideas lemme know I'd love to hear what you have to say!

~Credits~
Music: Square Enix, Capcom
P3DC: Brett Binnersley, Samuel Hanson, Snake_PL, Thomas Moller
SFX: All created in BFXR and Audacity

And of course, The Squad of Cuties <3"));
draw_set_font(font0);

///Debug
if instance_exists(obj_global_overw)
{
    if obj_global_overw.draw_coords = 1
    {
        draw_text(0,0,string_hash_to_newline("xx: " + string(xx) + "yy: " + string(yy)));
        yy -= keyboard_check(vk_up)*0.5;
        yy += keyboard_check(vk_down)*0.5;
        xx += keyboard_check(vk_right)*0.5;
        xx -= keyboard_check(vk_left)*0.5;
    }
}

