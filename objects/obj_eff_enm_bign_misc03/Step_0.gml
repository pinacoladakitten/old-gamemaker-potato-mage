/// @description Destroy
alpha -= 0.02
x += xdir
y += ydir
z += zdir
xsc -= 0.02
ysc -= 0.02
zsc -= 0.02
if alpha <= 0 
{
    instance_destroy();
}

