/// @description Start up
pos = 1
text = 0
active = 0
alarm[0] = 3*room_speed

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
Ok...so...um...
"
strings[1] = @"???:
Uhhhh...I kinda forgot how this went...
"
strings[2] = @"???:
Give me a sec...I am so sorry seriously...I wanted to start out strong with this,
....ummmm, oh great....
"
strings[3] = @"???:
Ok, so, there was a-building! Yes!...
"
strings[4] = @"???:
...and it looks like this!
"
strings[5] = @"???:
Mhmm, alright, now since there is a building...uh, there must be a town right? I mean
why would a building be in the middle of nowhere? So let's add that too...yes...yesssss
"
strings[6] = @"???:
Great-great...awesome...outstanding...oh how amazing it feels to remember more and
more...
"
strings[7] = @"???:
Ah! Yes! The city was by the ocean too, so...
"
strings[8] = @"???:
There we go...great...great...
"
strings[9] = @"???:
Alright, alright...I remember everything now cool! Yeah this little exercise basically
warmed up my head and now everything just popped into place. Awesome!
"
strings[10] = @"???:
Sooorrrrryyyy! I just had a massive brain fart, now this is how it's all supposed to go I 
swear, please forgive me...
"
strings[11] = @"???:
It's around 3am, not to say it's your fault for arriving so late,(literally 10 minutes
ago), but I'm kind of exausted for standing out here, seriously, this dreary scenery 
gets to you.
"
strings[12] = @"???:
Oh who am I kidding! I'm wasting even more time rambling on without your consent!
Terribly sorry again. Anyway, let's take this from the top! 
"
strings[13] = @"???:
Wait...what's that? You still don't know my name!?...*Sigh*...Oh my gosh! I'm soooo sorry! 
It's Umbra by the way. Oh this night just keeps getting worse and worse, sorry again...
"
strings[14] = @"Umbra:
Oh! And sorry for that scare back there with the hand, I tend to just do that sometimes,
I'm such a jerk I know. ;~;
"
strings[15] = @"Umbra:
OK! I think that's all I needed to cover right? Alright, Alright, lets get on with this.
Buckle Up! Here we...GO!!!!!!
"
strings[16] = @"Umbra:
Oh and my bad for saying sorry a lot, I tend to do that as well. Yeah me being so repititious
is NOT good at all. Forgive me. OK, alright! Now let's start this!
"

