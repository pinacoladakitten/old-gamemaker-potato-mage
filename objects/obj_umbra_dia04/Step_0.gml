/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(ord("Z"))
    if string_length(strings[text]) < pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        if text = 9
        {
            instance_create(x,y,obj_trans_fade_in_out)
        }
        if text = 16
        {
            instance_create(x,y,obj_trans_fade_in_next_white)
        }
        pos = 0
        text = text+1
    }
        
        if (text > maxstrings)
        {
        instance_destroy();
        instance_create(0,0,obj_trans_fade_in_next_white)
        }
    maxstrings = 16
}

///Events
if text = 4
{
    __background_set( e__BG.Index, 0, bg_intro_white_town01 )
}
if text = 6
{
    __background_set( e__BG.Index, 0, bg_intro_white_town02 )
}
if text = 8
{
    __background_set( e__BG.Index, 0, bg_intro_white_town03 )
}
if instance_exists(obj_trans_fade_in_out)
{
    if obj_trans_fade_in_out.alpha = 1
    if text = 10
    {
        __background_set( e__BG.Index, 0, bg_intro_cutscene03 )
    }
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
audio_play_sound(snd_text_speech,10,false)
}

