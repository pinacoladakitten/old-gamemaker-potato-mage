/// @description Draw
var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
d3d_set_lighting(true);
draw_set_alpha(alpha*0.8)
d3d_transform_add_scaling(xsc,ysc,zsc)
d3d_transform_add_rotation_x(r);
d3d_transform_add_translation(x,y,z)
d3d_draw_wall(0+2*ss,0+2*cc,0+4,0+2*ss,0-2*cc,0,tex,1,1)
d3d_set_lighting(false);
d3d_transform_set_identity();
draw_set_alpha(1)

