/// @description Setup
x += random_range(0, 3);
y += random_range(-5, 5);
spr = sprite_index;
image_speed = 0.25;
xscmax = irandom_range(2,3);
xsc = irandom_range(1,2);
ysc = xsc;
zsc = xsc;
z = 5;
r = 0;
xdir = random_range(-0.1, 0.1);
ydir = random_range(-0.1, 0.1);
zdir = random_range(-0.1, 0.1);
alpha = 0;
phase = 0;
//Number Check
if instance_number(obj_potato_deathspike_smoke) > 200
{instance_destroy();}

