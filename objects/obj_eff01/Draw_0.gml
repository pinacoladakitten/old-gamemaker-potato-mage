var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);

//On Enemy
if spr = spr_eff_hit01
{
d3d_draw_wall(x+5*ss,y+5*cc,z,x-5*ss,y-5*cc,z-10,tex,1,1)
}
if spr = spr_fire
{
d3d_draw_wall(x+10*ss,y+10*cc,z,x-10*ss,y-10*cc,z-20,tex,1,1)
}
if spr = spr_slash01
{
d3d_draw_wall(x+20*ss,y+20*cc,40,x-20*ss,y-20*cc,0,tex,1,1)
}

