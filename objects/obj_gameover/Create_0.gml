/// @description Setup
if instance_exists(obj_potato_combat)
{
    x = obj_potato_combat.x+60
    y = obj_potato_combat.y+5
}
speedx = random_range(0.5,0.75)
speedy = random_range(-0.5,0.5)
drawgui = 0
width = display_get_gui_width();
height = display_get_gui_height();
portx = width
porty = height/2
alpha01 = 0
alpha02 = 0
music_stop = 0
active = 0
retry = 0
option = 0
image_speed = 0.5
alarm[1] = 2*room_speed

