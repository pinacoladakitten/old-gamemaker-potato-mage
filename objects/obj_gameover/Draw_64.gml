/// @description Draw GameOver Screen
if drawgui = 1
{
    speedx = 0
    speedy = 0
    draw_set_alpha(alpha01);
    draw_rectangle_colour(0,0,display_get_gui_width(),display_get_gui_height(),c_black,c_black,c_black,c_black,0);
    draw_set_alpha(alpha02);
    draw_set_halign(fa_center);
    draw_set_font(font1);
    draw_text(width/2, height/2,string_hash_to_newline("Game Over"));
    draw_set_halign(fa_left);
    draw_set_alpha(1);
}
if retry = 1
{
    draw_set_font(font0);
    draw_sprite(spr_text_option,0,width/2,(height/2)+200)
    draw_set_halign(fa_center);
    draw_text((width/2),(height/2)+140,string_hash_to_newline("Retry Battle?"))
    draw_set_halign(fa_left);
    draw_text((width/2)-270,(height/2)+160,string_hash_to_newline("Yes please..."))
    draw_text((width/2)-270,(height/2)+220,string_hash_to_newline("Send me back now!"))
    if option = 0
    {
        draw_sprite_ext(spr_inventory_select,-1,370,(height/2)+170,-1,1,0,-1,1);
    }
    if option = 1
    {
        draw_sprite_ext(spr_inventory_select,-1,370,(height/2)+230,-1,1,0,-1,1);
    }
}

