/// @description Start Results
drawgui = 1
if !audio_is_playing(snd_gameover)
{
    audio_play_sound(snd_gameover,10,false);
    audio_sound_gain(snd_gameover,0.55,0);
    audio_stop_sound(combat_music);
    audio_stop_sound(combat_music_pt2);
}

