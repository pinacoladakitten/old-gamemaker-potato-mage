/// @description Main Event
if active = 1
{
    //Movement
    x += speedx
    y += speedy
    //Camera Movement
    with obj_camera
    {
        x = obj_gameover.x
        y = obj_gameover.y
        camx = 0
        camy = 0
        camxto = obj_potato_combat.x
        camyto = obj_potato_combat.y
        camzfrom = 5
    }
}
//Portrait Fade
if drawgui = 1
{
    alpha01 = min(alpha01+0.006,0.65)
    alpha02 = min(alpha02+0.01,1)
}

///Pause
enm_pause = 1
if instance_exists(obj_potato_combat)
{
    obj_potato_combat.pause_combat = 1
}
if instance_exists(obj_comm_wheel_potato)
{
    obj_comm_wheel_potato.pause_com = 1
}

///Retry Option
if retry = 1
{
    if keyboard_check_pressed(global.keyb_up)
    or gamepad_button_check_pressed(0, global.gpi_up)
    {
        option = max(option-1,0);
    }
    if keyboard_check_pressed(global.keyb_down)
    or gamepad_button_check_pressed(0, global.gpi_down)
    {
        option = min(option+1,1);
    }
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    {
        if option = 0
        {
            instance_create(0,0,obj_trans_fade_in);
            obj_trans_fade_in.roomgoto = room;
            dead = 1;
        }
        if option = 1
        {
            instance_create(0,0,obj_trans_fade_in_dung);
            dead = 1;
        }
    }
}

///Misc
if instance_exists(obj_victory)
{
    with(obj_victory)
    {
        instance_destroy();
    }
}

