/// @description Insert description here
// You can write your code in this editor
/// @description Draw
d3d_set_lighting(false);
var ss,cc,tex;
tex = sprite_get_texture(sprite_index,-1);
ss = sin(obj_camera_ow.direction*pi/180);
cc = cos(obj_camera_ow.direction*pi/180);
draw_set_alpha(1)
d3d_transform_add_translation(x,y,-5);
d3d_draw_wall(0+50*ss,0+50*cc,z+50,0-50*ss,0-50*cc,z+0,tex,1,1);
d3d_transform_set_identity();
draw_set_alpha(1)
d3d_set_lighting(false);