/// @description Create Effect
if active = 1
{
    instance_create(-880,103,obj_water_vapor);
    instance_create(-880,103,obj_water_vapor);
    instance_create(-880,103,obj_water_vapor);
    instance_create(-880,103,obj_water_vapor);
    //Mid
    instance_create(-880,103,obj_water_vapor);
    var targetpart = instance_find(obj_water_vapor,instance_number(obj_water_vapor)-1);
    targetpart.z = 309;
    instance_create(-880,103,obj_water_vapor);
    var targetpart = instance_find(obj_water_vapor,instance_number(obj_water_vapor)-1);
    targetpart.z = 309;
    //Top
    instance_create(-880,103,obj_water_vapor);
    var targetpart = instance_find(obj_water_vapor,instance_number(obj_water_vapor)-1);
    targetpart.z = 610;
    instance_create(-880,103,obj_water_vapor);
    var targetpart = instance_find(obj_water_vapor,instance_number(obj_water_vapor)-1);
    targetpart.z = 610;
}
alarm[0] = 0.1*room_speed;

