/// @description Draw Something
if active = 1
{
    var tex;
    tex = sprite_get_texture(sew_waterfall01,-1);
    draw_set_alpha(0.75);
    d3d_set_lighting(false);
    d3d_transform_add_scaling(1.3,1.3,1);
    d3d_transform_add_rotation_z(270);
    d3d_transform_add_translation(-880,103,0);
    //Cylinders
    d3d_draw_cylinder(13,13,z*1.45,-13,-13,-10,tex,1,1,true,18);
    d3d_draw_cylinder(14,14,z*1.25,-14,-14,-10,tex,1,1,true,18);
    d3d_draw_cylinder(15,15,z*1,-15,-15,-10,tex,1,1,true,18);
    d3d_draw_cylinder(17,17,z*0.75,-17,-17,-10,tex,1,1,true,18);
    d3d_draw_cylinder(19,19,z*0.5,-19,-19,-10,tex,1,1,true,18);
    d3d_draw_cylinder(21,21,z*0.25,-21,-21,-10,tex,1,1,true,18);
    d3d_draw_cylinder(23,23,z*0.1,-23,-23,-10,tex,1,1,true,18);
    d3d_draw_cylinder(25,25,z*0.05,-23,-23,-10,tex,1,1,true,18);
    d3d_transform_set_identity();
    draw_set_alpha(1);
    d3d_set_lighting(false);
}

