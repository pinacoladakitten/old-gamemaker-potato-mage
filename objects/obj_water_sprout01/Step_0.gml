/// @description Main Event
if place_meeting(x,y,obj_potato_overw){
    //Collide Check In
    if collide = 0
    {
        //Sound Check
        if !audio_is_playing(snd_water_enter)
        {audio_play_sound(snd_water_enter,10,false);}
    }
    collide = 1;
    //Everything Else doing with Player
    with obj_potato_overw
    {
        if z <= 619
        {
            zspeed = -2;
            on_floor = 1;
            if sprite_index != spr_potato_ow_carried
            {sprite_index = spr_potato_ow_carried;}
            image_speed = 0.1;
            movemax = 1;
        }
    }
}
else{
    //Collide Check Out
    if collide = 1
    {
        //Sound Check
        if !audio_is_playing(snd_water_enter)
        {audio_play_sound(snd_water_enter,10,false);}
    }
    collide = 0;
}
//Z Max
if active = 1
{z = min(z+50,619);}

