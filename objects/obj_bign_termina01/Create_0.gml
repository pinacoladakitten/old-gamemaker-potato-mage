/// @description Set Up
alpha = 0
countdown = 15
count_pause = 0
r = 0
xsc = 0
ysc = 0
zsc = 0
grow = 1
target = 0
fade = 0
//Start Countdown
alarm[0] = 1*room_speed
alarm[1] = 10
//Smoke Sound
audio_play_sound(snd_darkness02,10,false);
audio_play_sound(snd_sea,10,false);
//Pause During Buff
enm_pause = 1
//Add Buff
with obj_combat_hud_all
{
    pbuffs += 1
    ds_list_add(pbufftype, "doom");
}

