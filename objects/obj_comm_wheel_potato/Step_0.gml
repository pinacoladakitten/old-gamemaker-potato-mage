/// @description Controls
if pause_com = 0
if alpha = 1
{
    if change = 1
    {
        //Pick Control
        if keyboard_check_pressed(global.keyb_up)
        or gamepad_button_check_pressed(0, global.gpi_up)
        {
            audio_play_sound(snd_wheel_move,10,false)
            numb = 1
            pick += numb
        }
        if keyboard_check_pressed(global.keyb_down)
        or gamepad_button_check_pressed(0, global.gpi_down)
        {
            audio_play_sound(snd_wheel_move,10,false)
            numb = -1
            pick += numb
        }
        //Pick type
        if pick > 4
        {
            pick = 1
            numb = numb*-1
        }
        if pick < 1
        {
            pick = 4
            numb = numb*-1
        }
    }
}

///Command Pick Rotation
if pause_com = 0
if alpha = 1
{
    //Pick 1
    if pick = 1
    {
        if numb > 0
        {
            rot = min(rot+5,0)
        }
        if numb < 0
        {
            rot = max(rot-5,0)
        }
    }
    //Pick 2
    if pick = 2
    {
        if numb > 0
        {
            rot = min(rot+5,90)
        }
        if numb < 0
        {
            rot = max(rot-5,90)
        }
    }
    //Pick 3
    if pick = 3
    {
        if numb > 0
        {
            rot = min(rot+5,180)
        }
        if numb < 0
        {
            rot = max(rot-5,180)
        }
    }
    //Pick 4
    if pick = 4
    {
        if numb > 0
        {
            rot = min(rot+5,270)
        }
        if numb < 0
        {
            rot = max(rot-5,270)
        }
    }
}

///Use Command
if pause_com = 0
if alpha = 1
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if confirm = 1
    {
        audio_play_sound(snd_select,10,false)
        //Pick 1 Attack
        if pick = 1
        {
            with obj_potato_combat
            {
                attack = 1
                attack_type01 = 1
            }
        }
        //Magic: Ember
        if mag01 = 1
        {
            with obj_potato_combat
                {
                    magic01 = 1
                    commence = 1
                }
        }
        //Magic: Solanum
        if mag02 = 1
        {
            with obj_potato_combat
                {
                    magic02 = 1
                    commence = 1
                }
        }
        //Magic: Asterid
        if magpotato = 3
        {
            with obj_potato_combat
                {
                    magictype = 3
                    commence = 1
                }
        }
        //Target Active Off
        with obj_target
        {
            active = 0
        }
        //Target Ally Active Off
        with obj_target_ally
        {
            active = 0
        }
    }
    //Confirmed
    if confirm = 0
    {
        with obj_target
        {
            draw = 0
            active = 0
        }
        with obj_target_ally
        {
            draw = 0
            active = 0
        }
        change = 1
    }
    
    //Pick Check
    //Pick 1 Attack
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if pick = 1
    {
        if !audio_is_playing(snd_select)
        {
            audio_play_sound(snd_select,10,false)
        }
        with obj_target
        {
            draw = 1
            active = 1
        }
        change = 0
        confirm = 1
    }
    //Pick 2 Magic
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if pick = 2
    {
        if confirm = 0
            {
                obj_inventory_ow.active = 1
                obj_inventory_ow.magic_screen = 1
                obj_inventory_ow.pause_screen = 0
                pause_com = 1
            }
    }
    //Pick 3 Items
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if pick = 3
    {
        obj_inventory_ow.active = 1
        obj_inventory_ow.item_screen = 1
        obj_inventory_ow.pause_screen = 0
        pause_com = 1
    }
}
//Back-out
if keyboard_check_pressed(global.keyb_cancel)
or gamepad_button_check_pressed(0, global.gpi_cancel)
{
    audio_play_sound(snd_menu_back,10,false)
    change = 1
    confirm = 0
    mag01 = 0
    mag02 = 0
}

///Misc
if obj_potato_combat.time = 100
{
    alpha = min(alpha+0.1,1)
}
if alpha = 0
{
    instance_destroy();
}

