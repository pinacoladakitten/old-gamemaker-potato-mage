/// @description Draw
d3d_set_lighting(false); 
draw_sprite_ext(spr_comm_wheel,0,1820,860,1,1,rot,-1,alpha*1);
draw_sprite_ext(spr_commands,0,1820,860,1,1,rot,-1,alpha*1);

///Debug
if instance_exists(obj_global_overw)
{
    if obj_global_overw.draw_coords = 1
    {
        draw_text(0,0,string_hash_to_newline("xx: " + string(xx) + "yy: " + string(yy)));
        yy -= keyboard_check(vk_up)*0.5;
        yy += keyboard_check(vk_down)*0.5;
        xx += keyboard_check(vk_right)*0.5;
        xx -= keyboard_check(vk_left)*0.5;
    }
}

