/// @description Main Event
if draw = 0
{
    alpha-=0.025
    pause = 0
}
else
{
    alpha = min(alpha+0.01, 1);
}
if alpha <= 0
{
    instance_destroy();
}

