/// @description Setup
alpha = 0
draw = 1
image_speed = 0
pause = 1
hints = 0
//Loading Picture
loading_bg = choose(mythor_loading01,mythor_loading02,kaas_loading01,raime_loading01,
sol_loading01,bign_loading01);

///Hints
maxstrings = 7
d = irandom_range(0,maxstrings);
for (i = 0; i < maxstrings; i += 1)
{
    hint[i] = -1;
}
//Text
hint[0] = @"In a sticky situation? Press 'R' to respawn at the nearest save station.
NOTE: Only works out of combat, you're on your own if this doesn't apply to you, sorry..."

hint[1] = @"Found a bug? Something doesn't seem right? Have a suggestion? Tell the
developer of this game, I'm sure they're willing to listen. Yes this is a hint, 
I'm just that incompetent ok?"

hint[2] = @"Certain enemies are weak to certain elemental types, it may seem obvious to
determine what an enemy is weak to or what they resist...most of the time. 
There are exceptions...like some could be the opposite of what you would 
expect."

hint[3] = @"Mythor's 'Gloom' skill tree is primarily melee based with dark arts, so if 
you favor melee attacks and uh...darkness, then this might be the skill tree 
for you. Stems from 'Dexterity' and some 'Intellect'."

hint[4] = @"Mythor's 'Pure Potato' skill tree primarily focuses on healing you and your
allies while also granting a boost of speed. Thus making you a quick healer,
yet the potency of each heal isn't too strong. Stems from 'Haste' and some 
'Intellect'."

hint[5] = @"Mythor's 'Volatile' skill tree favors elemental magic based combat, thus
granting you benefits to spells that deal considerable amounts of damage 
while also exposing the weaknesses of enemies. Be warned, enemies can also 
be resistant to these spells as well. Stems from 'Intellect' only."

hint[6] = @"Mythor can dodge attacks when pressing 'C' in combat, this will negate all 
damage for that attack as long as you have correct timing. However, some attacks
cannot be dodged (which are displayed by a 'Red' marker), these attacks cannot KO 
anyone though."

hint[7] = @"You can move left or right by pressing 'Q' or 'E' respectively in combat. 
Allowing you to dodge attacks that can't be dealt with any other way."

hint[8] = "I swear if you clip into the floor and not tell me you're gonna have a bad time."

