/// @description Draw
d3d_set_fog(false, -1, 0, 10000);
var xx,yy;
xx = window_get_width();
yy = window_get_height();
draw_set_halign(fa_left);
draw_set_alpha(alpha);
draw_rectangle_colour(0,0,xx,yy,c_black,c_black,c_black,c_black,false);
draw_background(loading_bg,0,0);
draw_sprite(spr_loading_scr,0,xx-150,yy-100);
draw_sprite(spr_loading_dots,-1,xx-200,yy-100);
draw_set_alpha(1);
//Draw Hints
if hints = 1
{
    draw_set_alpha(alpha);
    draw_set_font(font0);
    draw_text(75,yy-125,string_hash_to_newline(hint[d]));
    draw_set_alpha(1);
}

