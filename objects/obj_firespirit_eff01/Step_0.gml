/// @description Movement
r += 10
z += 0.5
//Size
if shrink = 0
{
    xsc += 0.1
    ysc += 0.1
}
else
{
    xsc -= 8
    ysc -= 8
    z += 20
    zsph += 20
}
if xsc > 85
{
    shrink = 1
    if !audio_is_playing(snd_fireexp02){audio_play_sound(snd_fireexp02,10,false);}
    audio_stop_sound(snd_fireexp01);
}
if xsc <= 0
{
    instance_destroy();
}
//Alpha
if fade = 1
{
    alpha -= 0.001
    obj_firespirit.alpha = min(obj_firespirit.alpha+0.01,1)
}

d3d_set_lighting(true);
d3d_light_define_point(3,x,y,0,200,c_yellow)
d3d_light_enable(3,true);
d3d_set_lighting(false);

