/// @description Hitbox
if place_meeting(x,y,obj_potato_combat)
if hit = 0
{
    if potato_dodge = 0
        {
            hp_potato -= 7
            obj_potato_combat.hit = 1
            hit = 1
        }
    if potato_dodge = 1
    if hit2 = 0
        {
            hp_potato -= 0
            if place_meeting(x,y,obj_potato_combat){instance_create(x,y,obj_encourage_words)}
            hit2 = 1
        }
}

///Movement
r += 10
z += 0.25
y += yspeed
if instance_exists(obj_potato_combat){
    if y < obj_potato_combat.y
    {
        yspeed = min(yspeed+0.025,1)
    }
    if y > obj_potato_combat.y
    {
        yspeed = max(yspeed-0.025,-1)
    }
    x = obj_potato_combat.x;
}
if yspeed = 0 
{
    hit = 0
    hit2 = 0
}
//Destroy
if destroy = 1
{
    xsc -= 0.25
    ysc -= 0.25
    r += 10
    z += 5
    if xsc <= 0
    {
        instance_destroy();
    }
}

///Lighting
d3d_set_lighting(true);
d3d_light_define_point(3,x,y,0,200,c_yellow)
d3d_light_enable(3,true);
d3d_set_lighting(false);

///Misc

