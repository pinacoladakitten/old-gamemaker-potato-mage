/// @description Sounds
if active = 1
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}
//Selecting Stuff
if text = 0
or text = 41
if string_length(strings[text]) < pos
{
    if keyboard_check_pressed(global.keyb_down)
    or gamepad_button_check_pressed(0, global.gpi_down)
    {audio_play_sound(snd_move_menu,10,false)}
    if keyboard_check_pressed(global.keyb_up)
    or gamepad_button_check_pressed(0, global.gpi_up)
    {audio_play_sound(snd_move_menu,10,false)}
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    {audio_play_sound(snd_select,10,false)}
}

///Text Control
if active = 1
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if string_length(strings[text]) < pos
    if text != 0
    if text != 41
        {
            if text = 4
            {
                active = 0
                obj_potato_combat.pause_combat = 0
                enm_pause = 0
            }
            if text = 13
            {
                active = 0
                obj_boss_sol.alarm[0] = 20
                obj_potato_combat.pause_combat = 0
                enm_pause = 0
            }
            if text = 14
            {
                active = 0
                obj_potato_combat.pause_combat = 0
                enm_pause = 0
            }
            if text = 19
            {
                active = 0
                obj_boss_sol.alarm[0] = 10
                enm_pause = 0
                obj_potato_combat.pause_combat = 0
            }
            if text = 22
            {
                active = 0
                obj_boss_sol.alarm[0] = 10
                enm_pause = 0
                obj_potato_combat.pause_combat = 0
            }
            if text = 26
            {
                active = 0
                obj_boss_sol.alarm[0] = 10
                enm_pause = 0
                obj_potato_combat.pause_combat = 0
            }
            if text = 30
            {
                text = 38
            }
            if text = 43
            {
                active = 0
                obj_potato_combat.pause_combat = 0
                enm_pause = 0
                obj_boss_sol.timemax = irandom_range(65, 80)
                mp_potato = mp_potato_max
                hp_potato = hp_potato_max
                instance_destroy();
            }
            if text = 50
            {
                instance_create(0,0,obj_trans_fade_in);
                obj_trans_fade_in.roomgoto = rm_boss_sol
                audio_sound_gain(combat_music,0.55,15*room_speed);
                enm_pause = 1
                obj_potato_combat.pause_combat = 1
                instance_destroy();
            }
            if text < 50
            {
                pos = 0
                text = text+1
            }
        }
}

///Events
if active = 1
{   obj_potato_combat.pause_combat = 1
    enm_pause = 1
}
if text = 0
or text = 41
if string_length(strings[text]) < pos
{
    if keyboard_check_pressed(global.keyb_down)
    or gamepad_button_check_pressed(0, global.gpi_down)
    {
        pick = min(pick+1,1)
    }
    if keyboard_check_pressed(global.keyb_up)
    or gamepad_button_check_pressed(0, global.gpi_up)
    {
        pick = max(pick-1,0)
    }
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    {
        if text = 0
        {
            if pick = 0
            {
                pos = 0
                text = text+1
            }
            if pick = 1
            {
                pos = 0
                text = 39
            }
        }
        if text = 41
        {
            if pick = 0
            {
                pos = 0
                text = text+1
            }
            if pick = 1
            {
                pos = 0
                text = 50
                obj_potato_combat.time = 0
            }
        }
    }
}
//Tut attk 1
if text = 5
{obj_boss_sol.attack_type = 1}
//Tut attk 2
if text = 15
or text = 19
{obj_boss_sol.attack_type = 2}
//Tut attk 3
if text = 22
or text = 26
{obj_boss_sol.attack_type = 4}

///Misc.
if obj_potato_combat.time <= 10
{
    obj_potato_combat.time += 10
}
if active = 1
if obj_potato_combat.time > 90
{
    obj_potato_combat.time -= 10
}
if hp_potato < 30
{
    if !instance_exists(obj_potato_heal)
    {instance_create(obj_potato_combat.x,obj_potato_combat.y,obj_potato_heal);}
}
//Sol Reset
if active = 1
{
    obj_boss_sol.time = 0
}
//Portraits
if string_char_at(strings[text], 1) = "M"
{port = spr_potato_portrait_text}
if string_char_at(strings[text], 1) = "?"
{port = spr_sol_portrait}
if string_char_at(strings[text], 1) = "K"
{port = spr_kaas_portrait}
if string_char_at(strings[text], 1) = "U"
{port = spr_umbra_portrait}
//Umbra Talking
if text = 39 or text = 40 or text = 41 or text = 42
{
    alpha = min(alpha+0.01,0.85);
    audio_sound_gain(combat_music,0.25,15*room_speed);
}
if text = 42 or text = 43
{
    alpha = max(alpha-0.01,0.5);
    audio_sound_gain(combat_music,0.55,15*room_speed);
}

