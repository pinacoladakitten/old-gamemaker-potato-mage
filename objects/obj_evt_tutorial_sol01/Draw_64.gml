/// @description Main event
if active = 1
{
    draw_set_alpha(alpha)
    draw_rectangle_colour(0,0,1280,720,c_black,c_black,c_black,c_black,0)
    //Setup
    draw_set_font(font0);
    draw_set_halign(fa_left);
    draw_set_color(c_white);
    draw_set_alpha(1)
    //Textbox
    draw_sprite(spr_textbox,0,0,550)
    draw_sprite(port,0,40,560)
    //Text Effects
    //Blue Diamond Tut
    if text = 9
    {
        draw_sprite(spr_cast_eff,0,640,360)
        draw_sprite(spr_arrow,-1,720,360)
        draw_sprite_ext(spr_arrow,-1,560,360,-1,1,0,-1,1)
    }
    //Dodge Tut
    if text = 10 or text = 11 or text = 12
    {
        draw_sprite(spr_ord_c,0,480,360)
        draw_sprite(spr_arrow,-1,560,360)
        draw_sprite_ext(spr_arrow,-1,400,360,-1,1,0,-1,1)
        draw_sprite(spr_equals,0,660,360)
        draw_sprite(spr_dodged,0,790,370)
    }
    //Yellow Diamond Tut
    if text = 15 or text = 16
    {
        draw_sprite(spr_cast_eff01,0,640,360)
        draw_sprite(spr_arrow,-1,720,360)
        draw_sprite_ext(spr_arrow,-1,560,360,-1,1,0,-1,1)
    }
    //Purple Diamond Tut
    if text = 24
    {
        draw_sprite(spr_cast_eff03,0,640,360)
        draw_sprite(spr_arrow,-1,720,360)
        draw_sprite_ext(spr_arrow,-1,560,360,-1,1,0,-1,1)
    }
    //Aoe Dodging Tut
    if text = 25 or text = 26
    {
        draw_sprite(spr_ord_wasd,0,480,328)
        draw_sprite(spr_ord_wasd,1,416,392)
        draw_sprite(spr_ord_wasd,2,480,392)
        draw_sprite(spr_ord_wasd,3,544,392)
        draw_sprite(spr_arrow,-1,600,360)
        draw_sprite_ext(spr_arrow,-1,360,360,-1,1,0,-1,1)
    }
    if text = 26
    {
        draw_sprite(spr_mouse_wheel01,0,790,370);
        draw_sprite(spr_zoom01,0,853,370);
    }
    //Text Options
    if text = 0 or text = 41
    if string_length(strings[text]) < pos
    {
        draw_sprite(spr_text_option,0,640,360)
        if text = 0
        {
            //Shadow
            draw_set_colour(c_black);
            draw_text(424,329,string_hash_to_newline("Yes please..."))
            draw_text(424,377,string_hash_to_newline("Not Interested..."))
            draw_set_colour(-1);
            //Normal
            draw_text(420,325,string_hash_to_newline("Yes please..."))
            draw_text(420,373,string_hash_to_newline("Not Interested..."))
        }
        if text = 41
        {
            //Shadow
            draw_set_colour(c_black);
            draw_text(424,329,string_hash_to_newline("I'm pretty sure I've got everything down."))
            draw_text(424,377,string_hash_to_newline("I have no idea what just happened Umbra..."))
            draw_set_colour(-1);
            //Normal
            draw_text(420,325,string_hash_to_newline("I'm pretty sure I've got everything down."))
            draw_text(420,373,string_hash_to_newline("I have no idea what just happened Umbra..."))
        }
        if pick = 0 {draw_sprite_ext(spr_inventory_select,-1,415,335,-1,1,0,-1,1)}
        if pick = 1 {draw_sprite_ext(spr_inventory_select,-1,415,383,-1,1,0,-1,1)}
    }
    //The Text
    cstr = string_copy(strings[text],1,pos)
    draw_text_ext_transformed(250,560,string_hash_to_newline(cstr),32,-1,1,1,0)
    pos = min(pos+2,string_length(strings[text])+1)
    if keyboard_check(global.keyb_cancel)
    or gamepad_button_check_pressed(0, global.gpi_cancel)
    {
        pos = min(pos+5,string_length(strings[text])+1)
    }
}

