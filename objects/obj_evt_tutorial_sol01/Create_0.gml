/// @description Start up
pos = 1
text = 0
active = 0
enm_pause = 1
pick = 0
port = 0
alpha = 0.5
image_speed = 0.25
alarm[0] = 5*room_speed
obj_potato_combat.pause_combat = 1

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Kassadin:
Alright Myth, don't panic! I know you're terrified right now, but trust me, as long
as you follow what I tell you to do you'll be fine! Unless if you're already confident
in your ability to handle enemy encounters...in that case, do you need my guidance?
"
strings[1] = @"Kassadin:
Ok? So let's get started, now while in combat with a real enemy that will actually
attack you need to ALWAYS pay attention to your opponent. Consider when they 
will strike and what attacks they have planned up their sleeve.
"
strings[2] = @"Kassadin:
Now since you need to wait for your stamina in order to attack, while waiting you 
NEED TO BE DEFENSIVE! The enemy should be your main priority, watch 
carefully for any attacks coming!
"
strings[3] = @"???:
Hey hey! This is supposed to be a duel between me and this kid that mocked me! 
Why are you butting in and aiding him, he doesn't deserve any help!
"
strings[4] = @"Kassadin:
Eh...sorry...(Psst! Myth you got this.)
"
strings[5] = @"Kassadin:
Myth! Did you see that blue diamond fly up from him before disappearing?
"
strings[6] = @"Mythor:
Uh..yeah I guess, am I supposed to see this?
"
strings[7] = @"???:
Wait...did you see something? What's blue that you see?
"
strings[8] = @"Kassadin:
No no, I was just talking about your...uh...fire! Yes the flames around you 
are so intimidating! I can almost see a hint of blue in them.
"
strings[9] = @"Kassadin:
(Psst! Myth! That diamond represents his intention to strike you, now you can
deal with this situation one of two ways. You can either take the hit and lose 
hp, but conserve stamina. Or you can dodge his attack, but lose a little stamina.)
"
strings[10] = @"Kassadin:
I'd HIGHLY RECOMMEND DODGING, PLEASE...FOR ME! I really really 
don't want you to get hurt and suffer from lots of pain from every battle.
"
strings[11] = @"Kassadin:
Plus your Combo Multiplier will dissipate upon getting hit as well. So be prepared 
to hit 'C' as soon as the blue diamond disappears since he's going to attack.
"
strings[12] = @"Mythor:
Ok, got it! I'll just hit 'C' to dodge...wait what do you mean by hit 'C'?
"
strings[13] = @"???:
Wow, you're doing a really bad job at covering up your aid for this kid. 
No matter! Get a load of this!
"
strings[14] = @"Kassadin:
Yes! Just like that Myth, keep it up!
"
strings[15] = @"Kassadin:
Oh! Myth! Did you see that yellow diamond? It represents a 'delayed attack' in which
you may have to wait a little longer to dodge the attack. These are VERY tricky to
dodge since these attacks differ in how long you wait depending on the attack.
"
strings[16] = @"Kassadin:
Usually it is easy to tell when to dodge them, but there are attacks that are harder
to see coming. I'd recommend waiting about a second after the yellow diamond 
disappears, then try dodging the attack. Becoming more familiar with the enemy's 
attack pattern WILL greatly help you achieve the status of a 'dodging master'.
"
strings[17] = @"???:
Ok I don't know what these diamonds are and why you keep bringing them up, 
but no matter, I'm fed up with having to wait this long just to prove such 
a meaningless point.
"
strings[18] = @"Kassadin:
That point is exactly?
"
strings[19] = @"???:
The fact that no one should mess with me!
"
strings[20] = @"???:
Alright! Are you done yet? Please tell me you are!
"
strings[21] = @"Kassadin:
Uh-um...I'm not too sure, pretty sure I forgot something, I just can't figure
it out.
"
strings[22] = @"???:
Well whatever, hey, um, Myth was it? See if you can dodge this!
"
strings[23] = @"Kassadin:
Hey! That's it! Myth did you see it!?
"
strings[24] = @"Kassadin:
That purple diamond! These attacks are very special, they can't be dodged normally,
rather you need to move out of the way of them entirely to dodge them. Using your
dodging ability won't work this time.
"
strings[25] = @"Kassadin:
You can move by pressing 'WASD' respectively in each direction. Be careful though,
as you move your stamina will drain slowly leaving a greater attack period for your
enemy.
"
strings[26] = @"Kassadin:
You can also zoom the camera out using the mouse wheel if you want to see from a
greater perspective. Myth look out he's about to attack!
"
strings[27] = @"???:
Uh...Kassadin are you do-...uh oh...
"
strings[28] = @"Kassadin:
Shhhhhhh! (Oh my god, he blew it)
"
strings[29] = @"Mythor:
Kass, he just said your-
"
strings[30] = @"Kassadin:
MYTH! So yeah hahaha...um looks like that wraps up everything for you to learn. You
should be ready to fight him now. (Good job Sol, you really had this one)
"
strings[39] = @"Umbra:
Sorry to interrupt the story friend, but this is vital information you must 
grasp in order to continue the journey smoothly.
"
strings[40] = @"Umbra:
Thus if you do not understand this information I would advise that I should re-tell
you this section of the story.
"
strings[41] = @"Umbra:
My dear friend, is everything up to this point understandable to you?
"
strings[42] = @"Umbra:
Then let us get back to this journey.
"
strings[43] = @"Kassadin:
Alright Myth I'm counting on you! Show him that you've got what it takes! Also don't
get hurt too badly please!
"
strings[50] = @"Umbra:
Your choice has been made, let's go back.
"

