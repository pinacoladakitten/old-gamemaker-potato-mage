d3d_set_lighting(true); 
texture_set_repeat(true);
if instance_exists(obj_floor)
{
    d3d_transform_add_scaling(obj_floor.xs,obj_floor.ys,obj_floor.zs)
}
d3d_transform_add_translation(x,y,z)
d3d_model_draw(model0,0,0,0,background_get_texture(bg_castle01_brick));
d3d_set_lighting(false); 
d3d_model_draw(model2,0,0,0,background_get_texture(bg_castle01_light));
d3d_set_lighting(true); 
d3d_model_draw(model1,0,0,0,background_get_texture(bg_castle01_brick2));
d3d_model_draw(model3,0,0,0,background_get_texture(bg_castle01_outline));
d3d_model_draw(model4,0,0,0,background_get_texture(bg_castle01_roof));
d3d_transform_set_identity();

d3d_set_lighting(false);

