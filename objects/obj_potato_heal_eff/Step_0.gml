/// @description Destroy
xsc -= 0.10
ysc -= 0.10
r += 15
z = min(z+0.5,12)
alpha -= 0.015
if image_index = 12
or xsc = 0
{
    instance_destroy();
}

