/// @description Movement
if active = 1
if keyboard_check_pressed(global.keyb_targetL)
or gamepad_button_check_pressed(0, global.gpi_targetL)
{
    if draw = 1{audio_play_sound(snd_wheel_click01,10,false)}
    if !is_undefined(ds_list_find_value(enemies,s+1))
    {
        do{s+=1}
        until(!is_undefined(ds_list_find_value(enemies,s)))
    }
    alpha = 0
}

if active = 1
if keyboard_check_pressed(global.keyb_targetR)
or gamepad_button_check_pressed(0, global.gpi_targetR)
{
    if draw = 1{audio_play_sound(snd_wheel_click01,10,false)}
    if !is_undefined(ds_list_find_value(enemies,s-1))
    {
        do{s-=1}
        until(!is_undefined(ds_list_find_value(enemies,s)))
    }
    alpha = 0
}

///Finding Objects
target = ds_list_find_value(enemies,s);
if enm_numb > 0
{
    if !is_undefined(ds_list_find_value(enemies,s))
    if !instance_exists(ds_list_find_value(enemies,s))
    {
        if is_undefined(ds_list_find_value(enemies,s+1))
        {
            do{s-=1}
            until(!is_undefined(ds_list_find_value(enemies,s)))
        }
        if is_undefined(ds_list_find_value(enemies,s-1))
        {
            do{s+=1}
            until(!is_undefined(ds_list_find_value(enemies,s)))
        }
    }
    
    if !is_undefined(ds_list_find_value(enemies,s))
    if instance_exists(ds_list_find_value(enemies,s))
    {
        x = ds_list_find_value(enemies,s).x;
        y = ds_list_find_value(enemies,s).y;
    }
}

///Controls
if enm_numb > 0
{
    if !is_undefined(ds_list_find_value(enemies,s))
    if !instance_exists(ds_list_find_value(enemies,s))
    {
        if active = 1
        {
            if keyboard_check(global.keyb_left) or keyboard_check(global.keyb_up)
            or gamepad_button_check(0, global.gpi_left) or gamepad_button_check(0, global.gpi_up)
            {
                s+=1
            }
            if keyboard_check(global.keyb_right) or keyboard_check(global.keyb_down)
            or gamepad_button_check(0, global.gpi_right) or gamepad_button_check(0, global.gpi_down)
            {
                s-=1
            }
        }
        if keyboard_check(vk_nokey)
            {
            s+=1
            }
    }
}

///Misc
alpha = min(alpha+0.03,1)
r = min(r+1,360)
if r >= 360
{
    r = 0
}

