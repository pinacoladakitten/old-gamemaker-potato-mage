/// @description Draw
if active = 1
{
    d3d_set_lighting(false);
    var ss,cc,tex,tex01;
    tex = sprite_get_texture(spr,-1);
    tex01 = sprite_get_texture(spr_target02,-1);
    ss = sin(obj_camera.direction*pi/180);
    cc = cos(obj_camera.direction*pi/180);
    //Draw Stuff
    draw_set_alpha(alpha);
    d3d_transform_add_translation(x,y,z);
    d3d_draw_wall(0+8, 0+8*cc, 0+56, 0+8, 0-8*cc, 0+40, tex, 1, 1);
    d3d_transform_set_identity();
    //Bottom
    d3d_transform_add_rotation_z(r);
    d3d_transform_add_translation(x,y,0);
    d3d_draw_floor(0+10,0+10,1,0-10,0-10,1,tex01,1,1)
    d3d_transform_set_identity();
    //Set Defaults
    draw_set_alpha(1);
    d3d_set_lighting(true);
}

