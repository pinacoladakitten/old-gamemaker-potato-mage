/// @description Draw All
if active != 0
{
    draw_set_font(font00);
    d3d_set_lighting(false);
    //Shadow Back
    draw_set_alpha(alpha*0.65);
    draw_set_color(c_black);
    draw_sprite(spr_column_pause03,-1,960,50);
    //Normal Back
    draw_set_alpha(alpha);
    draw_set_color(c_white);
    draw_sprite(spr_column_pause03,-1,950,40);
    draw_set_halign(fa_center);
    //Shadow Text
    draw_set_color(c_black);
    draw_text(954,204,string_hash_to_newline("Sol (Tutorial)"));
    draw_text(954,244,string_hash_to_newline("Llumina"));
    draw_text(954,284,string_hash_to_newline("Big N"));
    draw_text(954,324,string_hash_to_newline("Big N (Hard)"));
    //Normal Text
    draw_set_color(c_white);
    draw_text(950,200,string_hash_to_newline("Sol (Tutorial)"));
    draw_text(950,240,string_hash_to_newline("Llumina"));
    draw_text(950,280,string_hash_to_newline("Big N"));
    draw_text(950,320,string_hash_to_newline("Big N (Hard)"));
    draw_set_halign(fa_left);
    //Target
    if boss = 0
    {draw_sprite_ext(spr_inventory_select,-1,850,205,-1,1,1,-1,alpha);
    port = spr_sol_port_versus;}
    if boss = 1
    {draw_sprite_ext(spr_inventory_select,-1,850,245,-1,1,1,-1,alpha);
    port = spr_firesp_versus;}
    if boss = 2
    {draw_sprite_ext(spr_inventory_select,-1,850,285,-1,1,1,-1,alpha);
    port = spr_bign_versus;}
    if boss = 3
    {draw_sprite_ext(spr_inventory_select,-1,850,325,-1,1,1,-1,alpha);
    port = spr_bign_versus;}
    draw_set_alpha(alpha2);
    draw_sprite(port,0,xx,360);
    draw_set_alpha(1);
    //Confirm
    if confirm = 1
    {
        draw_set_font(font0);
        draw_sprite(spr_text_option,0,width/2,(height/2)+200)
        draw_set_halign(fa_center);
        draw_text((width/2),(height/2)+140,string_hash_to_newline("Commence Battle?"))
        draw_set_halign(fa_left);
        draw_text((width/2)-270,(height/2)+160,string_hash_to_newline("Let's go!"))
        draw_text((width/2)-270,(height/2)+220,string_hash_to_newline("Hold on a sec."))
        if option = 0
        {
            draw_sprite_ext(spr_inventory_select,-1,(width/2)-300,(height/2)+170,-1,1,0,-1,1);
        }
        if option = 1
        {
            draw_sprite_ext(spr_inventory_select,-1,(width/2)-300,(height/2)+230,-1,1,0,-1,1);
        }
    }
}

