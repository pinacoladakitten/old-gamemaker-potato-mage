/// @description Main
if active = 2
{
    alpha = max(alpha-0.05,0);
    alpha2 = max(alpha2-0.05,0);
    if alpha = 0
    {active = 0}
}
if active = 1
{
    alpha = min(alpha+0.05,1);
    alpha2 = min(alpha2+0.015,1);
    xx = min(xx+10,350);
    if keyboard_check_pressed(global.keyb_up)
    or gamepad_button_check_pressed(0, global.gpi_up)
    {
        if confirm = 0
        {
            boss = max(boss-1,0);
            alpha2 = 0
            xx = 0
        }
        else
        {
            option = 0
        }
    }
    if keyboard_check_pressed(global.keyb_down)
    or gamepad_button_check_pressed(0, global.gpi_down)
    {
        if confirm = 0
        {
            boss = min(boss+1,3);
            alpha2 = 0
            xx = 0
        }
        else
        {
            option = 1
        }
    }
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    {
        if confirm = 0
        {confirm = 1}
        else
        {
            if option = 0
            {
                instance_create(0,0,obj_trans_battle_notpersist);
                pause = 0
                if boss = 0
                {potato_battle_room = rm_boss_sol
                combat_music = snd_boss_sol
                obj_trans_battle_notpersist.gain = 0.48}
                if boss = 1
                {potato_battle_room = rm_boss_firesp_dream3D
                combat_music = snd_boss_firesp
                obj_trans_battle_notpersist.gain = 0.45}
                if boss = 2
                {potato_battle_room = rm_boss_big_n01
                combat_music = snd_battle_evt01}
                instance_destroy();
                if boss = 3
                {potato_battle_room = rm_boss_big_n01_hard;
                combat_music = snd_bign_battle_hard01;
                obj_trans_battle_notpersist.gain = 0.65;}
                instance_destroy();
            }
            if option = 1
            {confirm = 0}
        }
    }
}
//Back-out
if keyboard_check_pressed(global.keyb_cancel)
or gamepad_button_check_pressed(0, global.gpi_cancel)
{
    if confirm = 0
    {
        active = 2
        pause = 0
    }
    else
    {confirm = 0}
}

///Sounds
if active = 1
{
    if keyboard_check_pressed(global.keyb_up) or keyboard_check_pressed(global.keyb_down)
    or gamepad_button_check_pressed(0, global.gpi_up) or gamepad_button_check_pressed(0, global.gpi_down)
    {
        audio_play_sound(snd_move_menu,10,false);
    }
}

