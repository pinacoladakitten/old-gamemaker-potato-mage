/// @description Activate Part 1
if !instance_exists(obj_gameover)
if !instance_exists(obj_victory)
{
    if active = 1
    {
        if !audio_is_playing(combat_music)
        {
            audio_play_sound(combat_music, 10, false);
            active = 2;
        }
        else
        {active = 2;}
    }
    ///Activate Part 2
    if active = 2
    if !audio_is_playing(combat_music)
    {
        audio_play_sound(combat_music_pt2, 10, true);
        active = 3;
    }
}
else
{
    instance_destroy();
}

