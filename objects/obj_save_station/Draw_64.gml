/// @description Main Screen
d3d_set_lighting(false);
if activated = 1
if main_screen = 1
{
draw_sprite(spr_column_pause02,-1,640,0)
draw_set_halign(fa_center)
draw_set_font(font1)
draw_text(640,125,string_hash_to_newline("Save Stone"))
draw_set_font(font0)
draw_text(640,325,string_hash_to_newline("Save"))
draw_text(640,400,string_hash_to_newline("Level Up"))
draw_text(640,500,string_hash_to_newline("Exit"))
draw_set_halign(fa_left)
}

///Level Up Screen
d3d_set_lighting(false);
if activated = 1
if points_screen = 1
{
draw_sprite(spr_column_pause03,-1,640,0)
draw_set_halign(fa_center)
draw_set_font(font1)
draw_text(640,125,string_hash_to_newline("Level Up"))
draw_set_font(font0)
draw_text(640,200,string_hash_to_newline("Potato Mage: " + string(potato_skillpredict)))
draw_sprite(spr_potato_portrait,-1,640,320)
draw_set_halign(fa_left)
//Stat Fonts
if potato_vitality = potato_vit_predict{draw_text(568,400,string_hash_to_newline("Vitality: " + string(potato_vit_predict)))}
else{draw_text_colour(568,400,string_hash_to_newline("Vitality: " + string(potato_vit_predict)),c_yellow,c_yellow,c_yellow,c_yellow,1)}

if potato_haste = potato_haste_predict{draw_text(568,450,string_hash_to_newline("Haste: " + string(potato_haste_predict)))}
else{draw_text_colour(568,450,string_hash_to_newline("Haste: " + string(potato_haste_predict)),c_yellow,c_yellow,c_yellow,c_yellow,1)}

if potato_dexterity = potato_dex_predict{draw_text(568,500,string_hash_to_newline("Dexterity: " + string(potato_dex_predict)))}
else{draw_text_colour(568,500,string_hash_to_newline("Dexterity: " + string(potato_dex_predict)),c_yellow,c_yellow,c_yellow,c_yellow,1)}

if potato_intellect = potato_int_predict{draw_text(568,550,string_hash_to_newline("Intellect: " + string(potato_int_predict)))}
else{draw_text_colour(568,550,string_hash_to_newline("Intellect: " + string(potato_int_predict)),c_yellow,c_yellow,c_yellow,c_yellow,1)}

//Stat Icons
draw_sprite(spr_lvl_vit,-1,528,400)
draw_sprite(spr_lvl_haste,-1,528,450)
draw_sprite(spr_lvl_dex,-1,528,500)
draw_sprite(spr_lvl_int,-1,528,550)

draw_set_halign(fa_center)
draw_text(640,630,string_hash_to_newline("Confirm"))
draw_text(640,680,string_hash_to_newline("Exit"))
draw_set_halign(fa_left)
}

///Select
///Main Menu Select
if activated = 1
if main_screen = 1
{
    if a = 0 //Save
    {
    draw_sprite(spr_inventory_select,-1,673,335)
    }
    if a = 1 //Level Up
    {
    draw_sprite(spr_inventory_select,-1,683,410)
    }
    if a = 2 //Exit
    {
    draw_sprite(spr_inventory_select,-1,673,510)
    }
}
///Level Up
if activated = 1
if points_screen = 1
{
    if p = 0 //Vitality
    {
    draw_sprite(spr_inventory_select,-1,728,415)
        if potato_skillpredict > 0
        {
        draw_sprite(spr_point_level,-1,588+string_width(string_hash_to_newline("Vitality: " + string(potato_vitality))),415)
        }
    }
    if p = 1 //Haste
    {
    draw_sprite(spr_inventory_select,-1,728,465)
    if potato_skillpredict > 0
        {
        draw_sprite(spr_point_level,-1,588+string_width(string_hash_to_newline("Haste: " + string(potato_haste))),465)
        }
    }
    if p = 2 //Dexterity
    {
    draw_sprite(spr_inventory_select,-1,728,515)
    if potato_skillpredict > 0
        {
        draw_sprite(spr_point_level,-1,588+string_width(string_hash_to_newline("Dexterity: " + string(potato_dexterity))),515)
        }
    }
    if p = 3 //Intellect
    {
    draw_sprite(spr_inventory_select,-1,728,565)
    if potato_skillpredict > 0
        {
        draw_sprite(spr_point_level,-1,588+string_width(string_hash_to_newline("Intellect: " + string(potato_intellect))),565)
        }
    }
    if p = 4 //Confirm
    {
    draw_sprite(spr_inventory_select,-1,728,645)
    }
    if p = 5 //Exit
    {
    draw_sprite(spr_inventory_select,-1,728,695)
    }
}

