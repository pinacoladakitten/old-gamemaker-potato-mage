/// @description Activate
if keyboard_check_pressed(global.keyb_confirm)
or gamepad_button_check_pressed(0, global.gpi_confirm)
{
    ///Reset Stats and Activate
    if place_meeting(x+5,y+5,obj_potato_overw) or place_meeting(x-5,y-5,obj_potato_overw)
    if activated = 0
    if obj_inventory_ow.active = 0
    if pause = 0
    {
        with obj_potato_overw {pause = 1}
        activated = 1
        main_screen = 1
        points_screen = 0
        a = 0
        mp_potato = mp_potato_max
        hp_potato = hp_potato_max
        potato_vit_predict = potato_vitality
        potato_haste_predict = potato_haste
        potato_dex_predict = potato_dexterity
        potato_int_predict = potato_intellect
        potato_skillpredict = potato_skillp
        potato_x = x + 20
        potato_y = y
        potato_z = z+10
    }
    ///Selecting Stuff
    if activated = 1
    if select = 1
    if main_screen = 1
    {
        if a = 0
            {
                activated = 0
                instance_create(0,0,obj_save_menu)
                obj_save_menu.saving = 1
                audio_play_sound(snd_select,10,false)
            }
        if a = 1
            {
                main_screen = 0
                points_screen = 1
                p = 0
                audio_play_sound(snd_select,10,false)
            }
        if a = 2
            {
                activated = 0
                with obj_potato_overw{pause = 0}
                audio_play_sound(snd_menu_back,10,false)
            }
    }
}

