/// @description Main Menu
if activated = 1
if select = 1
if main_screen = 1
{
    //Exit Regardless
    if keyboard_check_pressed(global.keyb_cancel)
    or gamepad_button_check_pressed(0, global.gpi_cancel)
    {
        with obj_potato_overw
            {
                pause = 0
            }
        activated = 0
        audio_play_sound(snd_menu_back,10,false)
    }
    
    //Movement
    if keyboard_check_pressed(global.keyb_up)
    or gamepad_button_check_pressed(0, global.gpi_up)
    {
        a = max(a-1,0)
        audio_play_sound(snd_move_menu,10,false)
    }
    if keyboard_check_pressed(global.keyb_down)
    or gamepad_button_check_pressed(0, global.gpi_down)
    {
        a = min(a+1,2)
        audio_play_sound(snd_move_menu,10,false)
    }
}
if activated = 0
{
    select = 0
    a = 0
}
if activated = 1
{
    mp_potato = mp_potato_max
    hp_potato = hp_potato_max
}

///Level Up Potato
if activated = 1
if select = 1
if points_screen = 1
{
    //Exit Regardless
    if keyboard_check_pressed(global.keyb_cancel)
    or gamepad_button_check_pressed(0, global.gpi_cancel)
    {
    main_screen = 1
    points_screen = 0
    p = 0
    potato_skillpredict = potato_skillp
    potato_vit_predict = potato_vitality
    potato_haste_predict = potato_haste
    potato_dex_predict = potato_dexterity
    potato_int_predict = potato_intellect
    audio_play_sound(snd_menu_back,10,false)
    }
    
    //Movement
    if keyboard_check_pressed(global.keyb_up)
    or gamepad_button_check_pressed(0, global.gpi_up)
    {
    p = max(p-1,0)
    audio_play_sound(snd_move_menu,10,false)
    }
    if keyboard_check_pressed(global.keyb_down)
    or gamepad_button_check_pressed(0, global.gpi_down)
    {
    p = min(p+1,5)
    audio_play_sound(snd_move_menu,10,false)
    }
    
    //Selecting Stuff
    //Vitality
    if p = 0
    {
        if keyboard_check_pressed(global.keyb_right)
        or gamepad_button_check_pressed(0, global.gpi_right)
        if potato_skillpredict > 0
        {
        potato_skillpredict -= 1
        potato_vit_predict += 1
        audio_play_sound(snd_menu_equip,10,false)
        }
        if keyboard_check_pressed(global.keyb_left)
        or gamepad_button_check_pressed(0, global.gpi_left)
        if potato_vit_predict > potato_vitality
        {
        potato_skillpredict += 1
        potato_vit_predict = max(potato_vit_predict-1,potato_vitality)
        audio_play_sound(snd_menu_equip,10,false)
        }
    }
    //Haste
    if p = 1
    {
        if keyboard_check_pressed(global.keyb_right)
        or gamepad_button_check_pressed(0, global.gpi_right)
        if potato_skillpredict > 0
        {
        potato_skillpredict -= 1
        potato_haste_predict += 1
        audio_play_sound(snd_menu_equip,10,false)
        }
        if keyboard_check_pressed(global.keyb_left)
        or gamepad_button_check_pressed(0, global.gpi_left)
        if potato_haste_predict > potato_haste
        {
        potato_skillpredict += 1
        potato_haste_predict = max(potato_haste_predict-1,potato_haste)
        audio_play_sound(snd_menu_equip,10,false)
        }
    }
    //Dexterity
    if p = 2
    {
        if keyboard_check_pressed(global.keyb_right)
        or gamepad_button_check_pressed(0, global.gpi_right)
        if potato_skillpredict > 0
        {
        potato_skillpredict -= 1
        potato_dex_predict += 1
        audio_play_sound(snd_menu_equip,10,false)
        }
        if keyboard_check_pressed(global.keyb_left)
        or gamepad_button_check_pressed(0, global.gpi_left)
        if potato_dex_predict > potato_dexterity
        {
        potato_skillpredict += 1
        potato_dex_predict = max(potato_dex_predict-1,potato_dexterity)
        audio_play_sound(snd_menu_equip,10,false)
        }
    }
    //Intellect
    if p = 3
    {
        if keyboard_check_pressed(global.keyb_right)
        or gamepad_button_check_pressed(0, global.gpi_right)
        if potato_skillpredict > 0
        {
        potato_skillpredict -= 1
        potato_int_predict += 1
        audio_play_sound(snd_menu_equip,10,false)
        }
        if keyboard_check_pressed(global.keyb_left)
        or gamepad_button_check_pressed(0, global.gpi_left)
        if potato_int_predict > potato_intellect
        {
        potato_skillpredict += 1
        potato_int_predict = max(potato_int_predict-1,potato_intellect)
        audio_play_sound(snd_menu_equip,10,false)
        }
    }
    //Confirm
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if p = 4
    {
    potato_skillp = potato_skillpredict
    potato_vitality = potato_vit_predict
    potato_haste = potato_haste_predict
    potato_dexterity = potato_dex_predict
    potato_intellect = potato_int_predict
    audio_play_sound(snd_select,10,false)
    }
    //Exit
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if p = 5
    {
    main_screen = 1
    points_screen = 0
    potato_skillpredict = potato_skillp
    potato_vit_predict = potato_vitality
    potato_haste_predict = potato_haste
    potato_dex_predict = potato_dexterity
    potato_int_predict = potato_intellect
    audio_play_sound(snd_menu_back,10,false)
    }
}

