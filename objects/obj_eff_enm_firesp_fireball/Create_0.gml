/// @description Fireball Vars
image_speed = 0.125
spr = spr_sol_fireint01
draw = 1
z = 250
alpha = 1
xs = 10
ys = 15
xsc = 10
ysc = 10
xsc2 = 15
ysc2 = 15
xss = 3
yss = 3
zs = 50
r = 0
image_xscale = 1
image_yscale = 1
hit = 0
hit2 = 0
zz = 0
alpha2 = 1

