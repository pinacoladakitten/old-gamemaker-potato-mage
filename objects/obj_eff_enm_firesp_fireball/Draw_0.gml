d3d_set_lighting(false);
var tex,tex01,tex02;
tex = sprite_get_texture(spr,-1);
tex01 = sprite_get_texture(spr_sol_fireint02,-1);
tex02 = sprite_get_texture(spr_sol_fireint04,-1);
tex03 = sprite_get_texture(spr_sol_fireint01,-1);
tex04 = sprite_get_texture(spr_firesp_eff01,-1);

//Fireball
draw_set_alpha(alpha);
d3d_transform_add_rotation_z(r)
d3d_transform_add_translation(x,y,0)
d3d_draw_ellipsoid(0+(xs),0+(ys),z-7,0-(xs),0-(ys),z+(zs),tex,1,1,24)
d3d_transform_set_identity();
draw_set_alpha(1);
//Pillar 2
draw_set_alpha(alpha);
d3d_transform_add_rotation_z(r*2)
d3d_transform_add_translation(x,y,0)
d3d_draw_cylinder(0+(xsc2),0+(ysc2),z+100,0-(xsc2),0-(ysc2),z,tex04,2,1,0,24)
d3d_transform_set_identity();
draw_set_alpha(1);

if draw = 1
{
    //Pillar 1
    draw_set_alpha(alpha);
    d3d_transform_add_rotation_z(r)
    d3d_transform_add_translation(x,y,0)
    d3d_draw_cylinder(0+(xsc),0+(ysc),z+100,0-(xsc),0-(ysc),z+10,tex01,1,1,0,24)
    d3d_transform_set_identity();
    draw_set_alpha(1);
}
if draw = 2
{
    draw_set_alpha(alpha2);
    d3d_transform_add_scaling(image_xscale,image_yscale,1)
    d3d_transform_add_rotation_z(r)
    d3d_transform_add_translation(x,y,0)
    d3d_draw_cylinder(0+32,0+32,zz,0-32,0-32,0,tex03,round(image_xscale),1,0,24)
    d3d_draw_cylinder(0+33,0+33,zz*0.6,0-33,0-33,0,tex03,round(image_xscale),1,0,24)
    d3d_transform_set_identity();
    draw_set_alpha(1);
}

