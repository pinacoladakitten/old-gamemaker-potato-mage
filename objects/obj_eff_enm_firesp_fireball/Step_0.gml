/// @description Initial Fireball
r += 2.5
if (z > 0)
{
    z -= 5
}
if (z = 10)
{
    audio_play_sound(snd_fire_intro,10,false)
}
if (z = 0)
{
    alpha = max(alpha-0.005,0)
    xs += 0.25
    ys += 0.25
    xsc -= 0.25
    ysc -= 0.25
    zs -= 0.25
}
if (xsc = 0)
{
    draw = 2
}

///Ring of Fire
if draw = 2
{
    image_xscale += 0.04
    image_yscale += 0.04
    zz = min(zz+0.25,15)
    alpha2 = max(alpha2-0.005,0)
    if alpha2 = 0
    {
        instance_destroy();
    }
}

///Hitbox
if place_meeting(x,y,obj_potato_combat)
if hit = 0
{
    if potato_dodge = 0
        {
            hp_potato -= 15
            obj_potato_combat.hit = 1
            hit = 1
        }
    if potato_dodge = 1
    if hit2 = 0
        {
            hp_potato -= 0
            if place_meeting(x,y,obj_potato_combat){instance_create(x,y,obj_encourage_words)}
            hit2 = 1
            hit = 1
        }
}

