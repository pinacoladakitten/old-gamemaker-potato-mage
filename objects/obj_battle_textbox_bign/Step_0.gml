/// @description Text Control
if keyboard_check_pressed(global.keyb_confirm)
or gamepad_button_check_pressed(0, global.gpi_confirm)
if string_length(strings[text]) <= pos
if !instance_exists(obj_trans_fade_in_out)
{
    pos = 0
    text = text+1
    alarm[0] = 10*room_speed
    if (text > maxstrings)
    {
        pos = 0
        text = 0
        active = 0
        enm_pause = 0
        obj_potato_combat.pause_combat = 0
        instance_destroy();
    }
}

///Events
if active = 1
{
    enm_pause = 1;
    obj_potato_combat.pause_combat = 1;
}
//Portraits
if string_char_at(strings[text], 1) = "M"
{
    port = spr_potato_portrait_text
}
if string_char_at(strings[text], 1) = "?"
{
    port = spr_firespirit_portrait
}

///Sounds
if !instance_exists(obj_trans_fade_in_out)
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

