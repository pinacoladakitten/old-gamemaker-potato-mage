/// @description Main event
if active = 1
{
    draw_set_alpha(0.5)
    //Setup
    draw_set_font(font0);
    draw_set_halign(fa_left);
    draw_set_color(c_white);
    draw_set_alpha(1)
    //Textbox
    var width = global.display_gui_width;
    var height = global.display_gui_height;
    draw_sprite(spr_textbox_slim,0,(width*0.5)-640,0);
    //The Text
    cstr = string_copy(strings[text],1,pos);
    draw_text_ext_transformed((width*0.5)-590,10,string_hash_to_newline(cstr),-1,-1,1,1,0);
    pos = min(pos+1,255);
    //Text Speedup
    if keyboard_check(global.keyb_cancel)
    or gamepad_button_check(0, global.gpi_cancel)
    {pos = min(pos+5,255);}
}

