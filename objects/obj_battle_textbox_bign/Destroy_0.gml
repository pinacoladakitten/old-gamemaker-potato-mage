/// @description On Destroy
if obj_big_n.phase = 1
if maxstrings = 3
{
    ///Cast Termina
    if !instance_exists(obj_bign_termina01)
    {
        var i = 0;
        for (i = 0; i < ally_numb; i += 1)
        {
            instance_create(x,y,obj_bign_termina01);
            obj_bign_termina01.target = instance_find(ds_list_find_value(allies,i),0);
        }
    }
}
if obj_big_n.phase = 1
if maxstrings = 9
{
    ///Cast Termina
    if instance_exists(obj_bign_termina01)
    {
        obj_bign_termina01.fade = 1;
    }
    obj_big_n.phase = 2
    if instance_exists(obj_bign_sky_darken01)
    {
        obj_bign_sky_darken01.active = 1;
    }
    if instance_exists(obj_bign_sky_eyes01)
    {
        obj_bign_sky_eyes01.active = 1;
    }
    obj_bign_music01.active = 1
}


