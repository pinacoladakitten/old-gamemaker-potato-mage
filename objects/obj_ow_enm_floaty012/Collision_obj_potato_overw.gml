/// @description Commence Battle
if (obj_inventory_ow.active = 0 and active = 1 and pause = 0 and (instance_exists(obj_potato_overw))
and abs(z-obj_potato_overw.z) <= 5)
{
    with(obj_spawn)
    {
        spawn1 = irandom_range(1,1)
        spawn2 = irandom_range(1,1)
        spawn3 = irandom_range(1,3)
        spawn4 = irandom_range(1,4)
        spawn5 = irandom_range(1,5)
        
        enemy[0] = obj_enm_dummy01;
        if spawn2 = 1 {enemy[1] = obj_enm_dummy01;}
        if spawn3 = 1 {enemy[2] = obj_enm_dummy01;}
        if spawn4 = 1 {enemy[3] = obj_enm_dummy01;}
        if spawn5 = 1 {enemy[4] = obj_enm_dummy01;}
    }
    alarm[3] = 35*room_speed;
    ///Battle Trans
    potato_battle_room = rm_battle_sewer3D;
    instance_create(512,384,obj_trans_battle_notpersist);
    obj_trans_battle_notpersist.battle_music = true;
    //Music
    combat_music = snd_battle_meta;
    combat_music_pt2 = snd_battle_meta;
    active = 0;
}

