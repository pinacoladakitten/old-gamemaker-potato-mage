if fade = 0
{
    alpha = min(alpha+0.01,0.5)
}
if fade = 1
{
    alpha = max(alpha-0.01,0)
    if alpha = 0
    {
        instance_destroy();
    }
}

r = min(r+0.5,360)
if r = 360
{
    r = 0
}

