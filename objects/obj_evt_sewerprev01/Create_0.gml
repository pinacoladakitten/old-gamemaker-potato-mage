/// @description Start up
pos = 1
text = 0
active = 0
port = spr_port
vol = 0
alarm[0] = 1.5*room_speed

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Mythor:
Uh-um...
"
strings[1] = @"Kassadin:
...
"
strings[2] = @"Mythor:
Hey Kass, um...did you wanna do some training today?
"
strings[3] = @"Kassadin:
...
"
strings[4] = @"Mythor:
You know...to uh-get me ready, ready in terms of uh-defending myself,
because that's what you wanted right?
"
strings[5] = @"Kassadin:
...
"
strings[6] = @"Mythor:
I mean it IS a nice day to train, the weather outside is nice and warm,
not too cold or hot. Soooo...yeah?
"
strings[7] = @"Kassadin:
No.
"
strings[8] = @"Mythor:
Then why don't we do something else? How abouuut...-how 'bout we go for a walk?
"
strings[9] = @"Kassadin:
No.
"
strings[10] = @"Mythor:
Oh, okay. Wanna just talk then? That's kinda fun I guess. Talkin' about things,
I guess how life is and such.
"
strings[11] = @"Kassadin:
Myth.
"
strings[12] = @"Mythor:
Yea.
"
strings[13] = @"Kassadin:
I'm busy right now.
"
strings[14] = @"Mythor:
Well what exactly are you doing?
"
strings[15] = @"Kassadin:
You can't be serious, you're looking right at what I'm doing. I've been doing this
all day.
"
strings[16] = @"Kassadin:
REALLY!? DID HE JUST-!? HOPEFULLY SOL SAW THAT!
"
strings[17] = @"Mythor:
(Oh, Sol's playing too huh. Figured.)
"
strings[18] = @"Kassadin:
The Tank just instantly died to one of the obvious mechanics! They're playing like trash,
there's no defending them.
"
strings[19] = @"Mythor:
Oh, riiiiiiight...but can you take a bre-
"
strings[20] = @"Kassadin:
No. Hold on, we're almost done with this raid. Go do something else in the meantime.
"
strings[21] = @"Mythor:
Like???
"
strings[22] = @"Kassadin:
...
"
strings[23] = @"Mythor:
Well???
"
strings[24] = @"Kassadin:
You know what? Myth give me maybe 15-20 minutes and we'll train, I promise.
"
strings[25] = @"Mythor:
REALLY!? YOU MEAN IT!?
"
strings[26] = @"Kassadin:
...
"
strings[27] = @"Mythor:
This is a yes! In my book anyway!
"

