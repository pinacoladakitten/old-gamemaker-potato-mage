/// @description Draw All
//Clock Light
draw_set_colour(c_white);
if timer_count >= timer_alarm[1] and timer_count < timer_alarm[2]{
    draw_primitive_begin(pr_trianglestrip);
    draw_vertex(760, 1080); //Vertex 1
    draw_vertex(1200, 1080); //Vertex 2
    draw_vertex(900, 950); //Vertex 3
    draw_vertex(1100, 950); //Vertex 4
    draw_primitive_end();
    //Clock Pendulum Shadow
    draw_sprite_ext(spr_clock_pendulum, 0, 960, 1380, 1.25, 0.9, penr, -1, 1);
}
if timer_count >= timer_alarm[2]{
    draw_primitive_begin(pr_trianglestrip);
    draw_vertex(760, 1080); //Vertex 1
    draw_vertex(1160, 1080); //Vertex 2
    draw_vertex(900, 750); //Vertex 3
    draw_vertex(1000, 750); //Vertex 4
    draw_primitive_end();
    //Clock Pendulum Shadow
    draw_sprite_ext(spr_clock_pendulum, 0, 960, 1280, 0.5, 1, penr, -1, 1);
}
draw_set_colour(-1);

