/// @description Setup
//Event timers
timer_alarm[0] = 25.951*room_speed;
timer_alarm[1] = 29.294*room_speed;
timer_alarm[2] = 32.554*room_speed;
timer_alarm[3] = 35.775*room_speed;
timer_alarm[4] = 39*room_speed;
timer_alarm[5] = 42*room_speed;
timer_count = 0;
//Startup
instance_create(0,0,obj_trans_fade_out);
obj_trans_fade_out.spd = 0.001;
obj_trans_fade_out.alarm[0] = 12*room_speed;
//Pendulum 
penr = 0;
pencycle = 0;

