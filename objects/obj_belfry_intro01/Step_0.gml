/// @description Main Event
++timer_count;
if timer_count >= timer_alarm[0]{
    __background_set( e__BG.Visible, 0, false );
    __background_set( e__BG.Visible, 1, true );
}
if timer_count >= timer_alarm[1]{
    __background_set( e__BG.XScale, 1, 0.5 );
    __background_set( e__BG.YScale, 1, 0.5 );
    __background_set( e__BG.X, 1, 400 );
    __background_set( e__BG.Y, 1, 100 );
}
if timer_count >= timer_alarm[2]{
    __background_set( e__BG.XScale, 1, 0.25 );
    __background_set( e__BG.YScale, 1, 0.25 );
    __background_set( e__BG.X, 1, 600 );
    __background_set( e__BG.Y, 1, 300 );
}
if timer_count >= timer_alarm[3]{
    __background_set( e__BG.XScale, 1, 0.1 );
    __background_set( e__BG.YScale, 1, 0.1 );
    __background_set( e__BG.X, 1, 700 );
    __background_set( e__BG.Y, 1, 500 );
}
if timer_count >= timer_alarm[4]{
    __background_set( e__BG.XScale, 1, 0.1 );
    __background_set( e__BG.YScale, 1, 0.1 );
    __background_set( e__BG.X, 1, 700 );
    __background_set( e__BG.Y, 1, 500 );
    //background_xscale[1] = 0.05;
    //background_yscale[1] = 0.05;
    //background_x[1] = 950;
    //background_y[1] = 600;
}
if timer_count >= timer_alarm[5]{
    room_goto(rm_BELFRY_ow01);
}
//Pendulum
if pencycle == 0{
    ++penr;
    if penr >= 60{
        pencycle = 1;
    }
}
if pencycle == 1{
    --penr;
    if penr <= -60{
        pencycle = 0;
    }
}

