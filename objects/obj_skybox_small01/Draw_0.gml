tex = background_get_texture(sky)
draw_set_alpha(alpha)
d3d_set_lighting(false)
//Skybox Type
d3d_transform_add_rotation_z(90)
d3d_transform_add_translation(obj_camera_ow.x,obj_camera_ow.y,0)
d3d_draw_ellipsoid(0-550,0-550,550,0+550,0+550,-550,tex,8,4,24)
d3d_draw_cylinder(0-550,0-550,1,0+550,0+550,-1,tex,1,1,1,24)
d3d_transform_set_identity();
d3d_set_lighting(true)
draw_set_alpha(1)

