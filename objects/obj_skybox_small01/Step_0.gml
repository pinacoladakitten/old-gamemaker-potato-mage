pause = 1
if fade = 1
{
    //Alpha
    alphaspeed = min(alphaspeed+0.001,0.01)
    alpha = max(alpha-alphaspeed*0.25,0)
    //Disappear
    if alpha = 0
    {
        audio_sound_gain(snd_battle_evt01,0,25*room_speed)
        instance_destroy();
    }
}
if fade = 0
{
    alphaspeed = min(alphaspeed+0.0005,0.01)
    alpha = min(alpha+alphaspeed*0.25,1)
    if alpha = 1
    {
        if instance_exists(obj_enemy_encounter01){obj_enemy_encounter01.active = 1}
    }
}
if !instance_exists(obj_enemy_encounter01)
{
    fade = 1
}
//Music
if audio_is_playing(overw_music)
{
    audio_sound_gain(overw_music,0,10*room_speed);
    if audio_sound_get_gain(overw_music) = 0
    {
        audio_stop_sound(overw_music);
    }
}

