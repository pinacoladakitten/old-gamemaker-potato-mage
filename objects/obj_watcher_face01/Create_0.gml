/// @description Start up
pos = 1
text = 0
active = 0
draw_scene = 0
image_speed = 0.05
angle = 0
angle01 = 0
z = 0
alpha = 0
show = 0
port = spr_port

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
Hehehehe...
"
strings[1] = @"Mythor:
Hm?
"
strings[2] = @"???:
Hahahaha!
"
strings[3] = @"Mythor:
...!
"
strings[4] = @"???:
What? Never seen a face on a wall before? You know it's
not easy being stuck like this.
"
strings[5] = @"???:
I mean come on, everyone I've come across is too afraid
to say hi or ask how my day's been. 
"
strings[6] = @"???:
Meanwhile they're busy complaining, 'Agggh! I'm suffering!
I have problems and I want attention now! I'm so lonely!' 
Blah blah blah...
"
strings[7] = @"???:
*Sigh*...anyway, how's your day been? Good?
"
strings[8] = @"Mythor:
U-um...i-it's been going well I gu-
"
strings[9] = @"???:
You guess? So, you guess that your day's been going 'well'.
"
strings[10] = @"Mythor:
Y-yeah...
"
strings[11] = @"???:
Stop giving me that load of rubbish.
"
strings[12] = @"Mythor:
W-what!? No no...I mean-
"
strings[13] = @"???:
I know EXACTLY how you feel!
"
strings[14] = @"???:
Surely it was going 'well' for you earlier, but now you're
as clueless as any other being in this place.
"
strings[15] = @"???:
Now you feel as if you're treading into the unknown alone,
which makes you scared and sad. Am I right or am I right?
"
strings[16] = @"Mythor:
...Y-yeah.
"
strings[17] = @"???:
Exactly, but don't worry I'm here to listen to whatever
you have to say so you don't feel 'lonely' or 'afraid'.
"
strings[18] = @"Mythor:
Really!? Please! I'm so confused right now!
"
strings[19] = @"???:
Hahaha...No...
"
strings[20] = @"Mythor:
....w-what?
"
strings[21] = @"???:
You'll figure out everything soon enough on your own, hehehehe...
why spoil the fun of what you have yet to discover.
"
strings[22] = @"???:
Before I go, here's a little advice...not all spirits are exactly
'friendly'. Do not, and I repeat, do not trust anyone. They've all 
lost their minds and only worry for themselves. Disheartening 
indeed, but hey, that's a dose of this reality for ya.
"
strings[23] = @"???:
Be on your guard, hehehe...for a kid like you I'd recommend holding
onto that courage and grit you still attain. I'm sort of jealous, but 
all I can ever hope for now is to not see yet another young
soul lose its identity.
"
strings[24] = @"???:
Hehehehe, so long!
"
strings[25] = @"Mythor:
W-Wait!...(and he's gone)
"
strings[26] = @"Mythor:
(Losing my identity? What's he mean by that?)
"
strings[27] = @"Mythor:
If you can still hear me...t-thanks...I guess...
"

