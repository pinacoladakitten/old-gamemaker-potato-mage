/// @description Angles
angle+=2
if angle = 360
{
    angle = 0
}

///Events
if active = 1
{
    pause = 1
}
if place_meeting(x,y,obj_potato_overw)
or show = 1
{
    z = max(z-1.5,-32)
    alpha = min(alpha+0.05,1)
}
if !place_meeting(x,y,obj_potato_overw)
if show = 0
{
    z = min(z+1.5,0)
    alpha = max(alpha-0.05,0)
}
//Text Events
if active = 1
{
    if text = 1
    {
        alarm[0] = 10
        active = 0
    }
    if text = 2
    {
        if keyboard_check_pressed(ord("Z"))
        {
            alarm[3] = 10
        }
    }
    if text = 3
    {
        active = 0
    }
    if text = 23
    {
        if keyboard_check_pressed(ord("Z"))
        {
            with obj_watcherface_col01
            {
                instance_destroy();
            }
        }
    }
    if text = 24
    {
        show = 0
    }
}
//Portraits
if string_char_at(strings[text], 1) = "M"
{
    port = spr_potato_portrait_text
}
else
{
    port = spr_watcherface_portrait
}

///Text Control
if active = 1
{
    if keyboard_check_pressed(ord("Z"))
    if string_length(strings[text]) <= pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        if (text > maxstrings)
        {
            pos = 0
            text = 0
            active = 0
            pause = 0
            instance_destroy();
        }
    }
    maxstrings = 27
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

