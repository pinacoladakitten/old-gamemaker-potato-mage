/// @description Setup
image_speed = 0.05
angle = 0
angle01 = 0
z = 0
pos = 1
text = 0
active = 0
draw_scene = 0
port = 0
image_index = irandom_range(0,7);
if global.sewer01_jace_evt01 = 0
if !instance_exists(obj_sewer01_nrat01_talked)
{text = 9;}

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Mythor:
Uh, hey-um, I was sent here by Kassadin, do you perhaps know where you guys sell
any hot sauce around here? It might be some kind of general store or something right?
"
strings[1] = @"Sewer Dweller:
You were sent by Kassadin!? Good! Good! We need your help!
"
strings[2] = @"Mythor:
Um, what? Wait wait, there must be some kind of misunderstanding, I'm only looking for
a bottle of hot sauce for him, he didn't mention anything about-
"
strings[3] = @"Sewer Dweller:
Please! My friends might be in trouble, I came back from the market only to hear screams.
I'm too scared to check it out myself, they're my friends and I have no idea what to do.
"
strings[4] = @"Sewer Dweller:
But I'm so so glad you showed up! The All Powerful Kassadin sending us his apprentice will 
surely be a great help to us all! I owe both of you my gratitude!
"
strings[5] = @"Mythor:
Woah woah-...*sigh* (Kass told me this was gonna be a walk in the park, he never mentioned
any of this, all I was getting was hot sauce, but I can't turn these people down.)
"
strings[6] = @"Mythor:
Alright, I'll check it out for ya. Um, where do I go?
"
strings[7] = @"Sewer Dweller:
Uh, directly to the right sir.
"
strings[8] = @"Mythor:
Oh, right...thanks.
"
strings[9] = @"Sewer Dweller:
Eh-um, if you don't mind me asking, do you have some sort of weapon, you know, just incase
there are any monsters running about. 
"
strings[10] = @"Mythor:
Now that you mentioned it, no I don't actually...
"
strings[11] = @"Sewer Dweller:
Well here take this then, I'm sure you'll find better with this more than me.
"
strings[12] = @"Mythor:
Thank you...
"
strings[13] = @"Sewer Dweller:
It's no problem, you're doing me a favor since I'm so scared to check it out myself.
I should be saying thanks, haha!
"
strings[14] = @"Sewer Dweller:
Anyway, don't stay for too long, you'll get me even more worried, I don't wanna have to
come looking for ya. 
"
strings[15] = @"Mythor:
Um, yeah...
"
strings[16] = @"Sewer Dweller:
Not that I don't want to look after you just incase you get into trouble or anything!
Haha!...
"
strings[17] = @"Mythor:
Right...
"
strings[18] = @"Sewer Dweller:
Look, the point is I want you to come back safe, keep me informed if there's anything 
troubling. In other words, don't leave me hanging ok? (Man, this is awkward.)
"
strings[19] = @"Mythor:
I won't let you down, I promise. See you later!
"

