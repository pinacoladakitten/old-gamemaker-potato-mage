/// @description Video Settings
r = global.display_width/global.display_height;
//Camera
d3d_set_projection_ext(x+camx+camxmove,y+camy+camymove,camzfrom+camz,camxto,camyto,camzto+camz,xu,yu,zu,40,r,1,32000)

///Fog
if room = rm_training_01{d3d_set_fog(false, c_black, 150, 1000)}
if room = rm_boss_sol{d3d_set_fog(false, c_black, 500, 1000)}
if room = rm_boss_firesp_dream3D{d3d_set_fog(true, __background_get_colour( ), 100, 1250)}
if room = rm_overw_home01{d3d_set_fog(false, c_black, 500, 1000)}
if room = rm_overw_dream3D{d3d_set_fog(false, c_black, 150, 1000)}
if room = rm_battle_dream3D{d3d_set_fog(true, __background_get_colour( ), 50, 800)}
if room = rm_boss_big_n01{d3d_set_fog(true, __background_get_colour( ), 2000, 30000)}

