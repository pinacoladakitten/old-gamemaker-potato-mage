/// @description Main
alarm[0] = 20*room_speed
y += 5
//X Vars
camxzoom = -70;
camx = 0 + camxzoom;
camxmove = 0;
camxto = 560;
camzoomtype = 1;
//Y Vars
camyto = 310
camy = 0
camymove = 0
camyspeed = 0
camydir = 0
//Z Vars
camz = 2
camzfrom = 18
camzto = 10
camf = 11
//Rotation Vars
xu = 0
yu = 0
zu = 1
cam_control = 0
eff_alpha = 0
cam_move = 0;
//Misc Stuff
cam_shake = 0
cam_shakeleng = 1
r = 1920/1080
//Start 3D mode
d3d_start(); 
d3d_set_hidden(true);
draw_set_alpha(1);
draw_set_color(c_white);
d3d_set_shading(true)
//Enable lighting
d3d_set_lighting(true);
//Enable Fog
d3d_set_fog(true, c_black, 50, 800)
//Enable backface culling. This stops the renderer from drawing polygons that
//face away from the camera, speeding the render up.
d3d_set_culling(false); 
//Define and enable a global directional light.
d3d_light_define_direction(1,1,0.5,-0.25,c_white);
d3d_light_enable(1,true);
d3d_light_enable(2,true);

