/// @description Cam Control
if cam_control = 1
{
    if keyboard_check(vk_up)
    {
        camz += 1
    }
    if keyboard_check(vk_down)
    {
        camz -= 1
    }
    if keyboard_check(vk_left)
    {
        camy -= 1
    }
    if keyboard_check(vk_right)
    {
        camy += 1
    }
    if keyboard_check(ord("S"))
    {
        camf += 1
    }
    if keyboard_check(ord("W"))
    {
        camf -= 1
    }
}

///Hit
yu = max(yu-0.005,0)

///Cam Movement Pan
if instance_exists(obj_comm_wheel_potato)
if cam_move = 1
{
    camy += camyspeed
}
if !instance_exists(obj_comm_wheel_potato)
{
    camyspeed = 0
    cam_move = 0
}

if camy >= 2.5
{
    camydir = 1
}
if camy <= -2.5
{
    camydir = 0
}
if camydir = 0
{
    camyspeed = min(camyspeed+0.0006,0.0045)
}
if camydir = 1
{
    camyspeed = max(camyspeed-0.0006,-0.0045)
}

///Cam Control Move and Zoom
if instance_exists(obj_potato_combat)
{
    var player = obj_potato_combat;
    if (player.pause_combat == 0)
    {
        camymove = clamp(camymove+player.hsp,player.yminmove,player.ymaxmove);
        camxmove = clamp(camxmove+player.vsp,player.xminmove,player.xmaxmove);
    }
}
//Zoom
if camx > camxzoom
{camx -= 1}
if camx < camxzoom
{camx += 1}
//Zoom Start Effect
if(camxzoom < 15){
    camxzoom += 1;
}
else{
    //Scroll Mouse
    if mouse_wheel_down()
    {camxzoom = min(camxzoom+5,40);}
    if mouse_wheel_up()
    {camxzoom = max(camxzoom-5,15);}
    //Scroll Controller
    if gamepad_button_check_pressed(0, gp_stickr)
    {camxzoom = min(camxzoom+10,40);}
    if gamepad_button_check_pressed(0, gp_stickl)
    {camxzoom = max(camxzoom-10,15);}
}

///Misc
//Screen Shake
if cam_shake = 1
{
    camz = clamp(irandom_range(-1,+1),-cam_shakeleng,cam_shakeleng);
}

