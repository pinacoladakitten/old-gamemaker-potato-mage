/// @description Room Settings
d3d_light_define_direction(1,1,0.5,-0.25,c_white);
d3d_light_enable(1,true);
if room = rm_boss_firesp_dream3D
{
    d3d_light_define_direction(1,0,0,0.25,c_purple);
    d3d_light_enable(1,true);
    d3d_light_define_direction(7,1,0,0.25,c_navy);
    d3d_light_enable(7,true);
    zoom = 130
}
if room = rm_boss_firesp_prev_dream3D
{
    d3d_light_define_direction(1,0,0,0.25,c_purple);
    d3d_light_enable(1,true);
    d3d_light_define_direction(7,1,0,0.25,c_navy);
    d3d_light_enable(7,true);
    zoom = 130
}
//Lights
if room = rm_battle_sewer3D
{
    d3d_light_define_direction(1,0.1,0,0.1,c_orange);
    d3d_light_enable(1,true);
    d3d_light_define_direction(7,-0.1,0,-0.1,c_orange);
    d3d_light_enable(7,true);
    zoom = 130
}
//Lights
if room = rm_boss_gsquid_prev
{
    d3d_light_define_direction(1,0.1,0,0.1,c_orange);
    d3d_light_enable(1,true);
    d3d_light_define_direction(7,-0.1,0,-0.1,c_orange);
    d3d_light_enable(7,true);
    zoom = 130
}

