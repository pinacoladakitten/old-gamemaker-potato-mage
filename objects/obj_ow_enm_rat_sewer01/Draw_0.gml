/// @description Draw All
if active = 1
{
    d3d_set_lighting(true);
    draw_set_alpha(alpha);
    var ss,cc,tex;
    tex = sprite_get_texture(spr,-1);
    tex01 = sprite_get_texture(spr_ow_shadow,-1);
    ss = sin(obj_camera_ow.direction*pi/180);
    cc = cos(obj_camera_ow.direction*pi/180);
    d3d_draw_wall(x+12*ss,y+12*cc,z+50,x-12*ss,y-12*cc,z+0,tex,1,1)
    d3d_draw_wall(x+20*ss,y+20*cc,z+3,x-20*ss,y-20*cc,z+0,tex01,1,1)
    draw_set_alpha(1);
    d3d_set_lighting(false);
}

