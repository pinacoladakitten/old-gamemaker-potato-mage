/// @description Main
d3d_set_lighting(false);
d3d_set_fog(false, -1, 0, 10000);
draw_set_font(font0);
draw_set_alpha(0.75);
draw_sprite(spr_combat_panel03,-1,20,90);
draw_set_alpha(1);

///Potato Mage
if instance_exists(obj_potato_combat){
    //Set Up
    var hpx, mpx, stmx, hpy, mpy, stmy;
    hpx = 50;
    hpy = 130;
    mpx = 200;
    mpy = 130;
    stmx = 50;
    stmy = 183;
    var hpx_val, mpx_val, stmx_val, hpy_val, mpy_val, stmy_val;
    hpx_val = 100;
    hpy_val = 130;
    mpx_val = 250;
    mpy_val = 130;
    stmx_val = 120;
    stmy_val = 180;
    var hp_barx, hp_bary, mp_barx, mp_bary, stm_barx, stm_bary;
    hp_barx = 50;
    hp_bary = 160;
    mp_barx = 200;
    mp_bary = 160;
    stm_barx = 125;
    stm_bary = 190;
    draw_set_font(font3);
    draw_set_halign(fa_left);
    ///Text Shadows
    draw_text_colour(55,105,string_hash_to_newline("Mythor:"),c_black,c_black,c_black,c_black,1);
    draw_text_colour(hpx+3, hpy+3,string_hash_to_newline("HP:"),c_black,c_black,c_black,c_black,0.75);
    draw_text_colour(hpx_val+3,hpy_val+3,string_hash_to_newline(round(hp_potato)),c_black,c_black,c_black,c_black,0.75);
    draw_text_colour(mpx+3, mpy+3,string_hash_to_newline("WP:"),c_black,c_black,c_black,c_black,0.75);
    draw_text_colour(mpx_val+3, mpy_val+3,string_hash_to_newline(round(mp_potato)),c_black,c_black,c_black,c_black,0.75);
    
    //Text
    draw_text(50,100,string_hash_to_newline("Mythor:"));
    draw_text(hpx,hpy,string_hash_to_newline("HP:"));
    draw_text(hpx_val,hpy_val,string_hash_to_newline(round(hp_potato)));
    draw_text(mpx,mpy,string_hash_to_newline("WP:"));
    draw_text(mpx_val,mpy_val,string_hash_to_newline(round(mp_potato)));
    draw_text(stmx, stmy, string_hash_to_newline("SOL:"));
    //Combo
    if !instance_exists(obj_victory)
    {
        if potato_combo > 1
        {
            draw_set_font(combofont);
            //Shadows
            draw_text_colour(15, 20, string_hash_to_newline("Combo x"), c_black, c_black, c_black, c_black, 1);
            draw_text_colour(155, 20, string_hash_to_newline(potato_combo), c_black, c_black, c_black, c_black,1);
            //Regular
            draw_text_colour(10, 20, string_hash_to_newline("Combo x"), c_yellow, c_yellow, c_yellow, c_yellow, 1);
            draw_text_colour(150, 20, string_hash_to_newline(potato_combo), c_yellow, c_yellow, c_yellow, c_yellow, 1);
            draw_set_font(font0);
        }
    }
    //Bar Colors
    var hp_barcol, mp_barcol, stm_barcol;
    hp_barcol = make_color_rgb(255, 185, 185);
    mp_barcol = make_color_rgb(184, 242, 255);
    stm_barcol = make_color_rgb(233, 215, 255);
    //Value Difference Bars
    draw_healthbar(hp_barx, hp_bary, hp_barx+100, hp_bary+12, (obj_potato_combat.health_diff/hp_potato_max)*100, c_black, c_red, c_red, 0, false, false);
    draw_healthbar(mp_barx, mp_bary, mp_barx+100, mp_bary+12, (obj_potato_combat.willpower_diff/mp_potato_max)*100, c_black, c_white, c_white, 0, false, false);
    //Health, Mana, Stamina Bar Stuff
    draw_healthbar(hp_barx, hp_bary, hp_barx+100, hp_bary+12, (hp_potato/hp_potato_max)*100, c_black, hp_barcol, hp_barcol, 0, false, false); //Health
    draw_healthbar(mp_barx, mp_bary, mp_barx+100, mp_bary+12, (mp_potato/mp_potato_max)*100, c_black, mp_barcol, mp_barcol, 0, false, false); //Willpower
    draw_healthbar(stm_barx, stm_bary, stm_barx+102, stm_bary+12, obj_potato_combat.time, c_black, stm_barcol, stm_barcol, 0, false, false); //Soul
    draw_sprite(spr_hp_bar, 0, hp_barx, hp_bary);
    draw_sprite(spr_mana_bar, 0, mp_barx, mp_bary);
    draw_sprite(spr_time_bar, 0, stm_barx, stm_bary);
    //Buffs
    for (b = 0; b < pbuffs; b += 1)
    {
        if ds_list_find_value(pbufftype, b) = 1{draw_sprite(spr_stamina_regen01, 0, (b * 21)+145, 115);}
        if ds_list_find_value(pbufftype, b) = 2{draw_sprite(spr_skill_purepotato_icon02_small, 0, (b * 21)+145, 115);}
        if ds_list_find_value(pbufftype, b) = 3{draw_sprite(spr_skill_purepotato_icon03_small, 0, (b * 21)+145, 115);}
        if ds_list_find_value(pbufftype, b) = "doom"{draw_sprite(spr_status_doom, 0, (b * 21)+145, 115);}
        if ds_list_find_value(pbufftype, b) = "bignslime"{draw_sprite(spr_status_bign_slime01, 0, (b * 21)+145, 115);}
		if ds_list_find_value(pbufftype, b) = "deathspike"{draw_sprite(spr_status_doom, 0, (b * 21)+145, 115);}
    }
    //Weapon Durability
    var duraWidth = sprite_get_width(spr_wepdura_bar);
    var duraHeight = sprite_get_height(spr_wepdura_bar);
    var duraPercent = global.wep_durability/global.wep_durability_max;
    draw_sprite(spr_wepdura_bar01, 1, 50, 210);
    draw_sprite_part(spr_wepdura_bar, 0, 0, 0, duraWidth*duraPercent, duraHeight, 50, 210);
    //Anim Trigger
    if (wepdura_prev != global.wep_durability){
        duraeff_xsc = 1;
        duraeff_ysc = 1;
        wepdura_dmg = true;
        wepdura_prev = global.wep_durability;
    }
    //Animaton
    if (wepdura_dmg == true){
        duraeff_xsc -= 0.1;
        duraeff_ysc += 0.1;
        draw_sprite_ext(spr_webdura_eff01, 0, 50+(duraWidth*duraPercent), 200+(duraHeight), duraeff_xsc, duraeff_ysc, 0, -1, 1);
        if (duraeff_xsc == 0)
        {
            wepdura_dmg = false;
        }
    }
    else{
        duraeff_xsc = 1;
        duraeff_ysc = 1;
        if (global.wep_durability < global.wep_durability_max){
            draw_sprite_ext(spr_webdura_eff01, 0, 50+(duraWidth*duraPercent), 200+(duraHeight), duraeff_xsc, duraeff_ysc, 0, -1, 1);
        }
    }
}

