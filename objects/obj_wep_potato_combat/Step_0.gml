/// @description Animation Data
//Idle
if obj_potato_combat.spr = spr_potato_combat01
{
    if obj_potato_combat.image_index = 0
    or obj_potato_combat.image_index = 1
    or obj_potato_combat.image_index = 9
    {
    yy = 6
    z = 0
    rot = -120
    }
    if obj_potato_combat.image_index = 2
    or obj_potato_combat.image_index = 8
    {
    yy = 5.75
    z = 0.25
    rot = -120
    }
    if obj_potato_combat.image_index = 3
    or obj_potato_combat.image_index = 7
    {
    yy = 5.6
    z = 0.45
    rot = -120
    }
    if obj_potato_combat.image_index = 4
    or obj_potato_combat.image_index = 5
    or obj_potato_combat.image_index = 6
    {
    yy = 5.7
    z = 0.5
    rot = -120
    }
}
//Attack
if obj_potato_combat.spr = spr_potato_attack01
{
    if obj_potato_combat.image_index = 0
    or obj_potato_combat.image_index = 14
    {
    yy = 6
    z = 0
    rot = -120
    }
    if obj_potato_combat.image_index = 1
    {
    yy = 7.5
    z = 1.4
    rot = -120
    }
    if obj_potato_combat.image_index = 2
    {
    yy = 7
    z = 8
    rot = -80
    }
    if obj_potato_combat.image_index = 3
    {
    yy = -5
    z = 12.5
    rot = -10
    }
    if obj_potato_combat.image_index = 4
    {
    yy = -2.7
    z = 15
    rot = -10
    }
    if obj_potato_combat.image_index = 5
    {
    yy = -13.5
    z = 6.5
    rot = 99
    }
    if obj_potato_combat.image_index = 6
    {
    yy = -13.25
    z = 6.5
    rot = 99
    }
    if obj_potato_combat.image_index = 7
    {
    yy = -13
    z = 6.65
    rot = 99
    }
    if obj_potato_combat.image_index = 8
    {
    yy = -13
    z = 6
    rot = 99
    }
    if obj_potato_combat.image_index = 9
    {
    yy = -13
    z = 5.5
    rot = 99
    }
    if obj_potato_combat.image_index = 10
    {
    yy = 4.5
    z = 6.5
    rot = -80
    }
    if obj_potato_combat.image_index = 11
    {
    yy = 4.75
    z = 6.5
    rot = -80
    }
    if obj_potato_combat.image_index = 12
    or obj_potato_combat.image_index = 13
    {
    yy = 4
    z = 5.5
    rot = -80
    }
}

///Sprites
if potato_equip = 0 //Default
{sprite_index = spr_none}
if potato_equip = 1 //Dagger
{sprite_index = spr_dagger_proj}
if potato_equip = 2 //Dark Stick
{sprite_index = spr_dark_wood_stick_proj}
if potato_equip = 3 //Hand Scythe
{sprite_index = spr_handscythe_item01}
//Check Pos
x = obj_potato_combat.x
y = obj_potato_combat.y

