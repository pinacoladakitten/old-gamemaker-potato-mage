/// @description Draw Everything
draw_set_alpha(alpha);
var tex, ss, cc;
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
tex = sprite_get_texture(spr_parasite, -1);
//Draw Effect
if image_index <= 33{
    d3d_transform_add_translation(x,y,0);
    d3d_draw_wall(0+5, 0+10, 0+20, 0+5, 0-10, 0, tex, 1, 1);
    d3d_transform_set_identity();
    draw_set_alpha(1);
}else{
    image_speed = 0;
}

