/// @description Damage Tick
if instance_exists(target)
if fade != 1
{
    var damage_pot = (potency - (potency*target.nature_defense));
    target.hp -= damage_pot;
    instance_create(x+1,y,obj_dmg_numbers02);
    var targetdmg = instance_find(obj_dmg_numbers02, instance_number(obj_dmg_numbers02)-1);
    targetdmg.dmg = damage_pot;
    potency += potency*0.1;
}
if !instance_exists(target){
    fade = 1;
}
alarm[0] = 1*room_speed;

