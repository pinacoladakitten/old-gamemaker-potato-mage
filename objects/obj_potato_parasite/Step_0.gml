/// @description Main Event
if fade = 0{
    alpha = min(alpha+0.05,1);
}
if fade = 1{
    alpha = max(alpha-0.01,0);
    mask_index = spr_none;
}
if alpha <= 0 {
    instance_destroy();
}
//Target Check
if target = 0{
    fade = 1;
    alpha = 0;
}
if instance_exists(target){
    x = target.x;
    y = target.y;
}

//If Overlapping (OUTDATED)
//if place_meeting(x ,y, obj_potato_parasite){
    //var paraOther = instance_find(obj_potato_parasite, instance_number(obj_potato_parasite-1));
    //if (id == paraOther){
        //alarm[1] = 10*room_speed;
    //}
    //else{
        //fade = 1;
    //}
//}

