/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(ord("Z"))
    if string_length(strings[text]) < pos
        {
        pos = 0
        text = text+1
        }
        
        if (text > maxstrings)
        {
        if instance_exists(obj_intro_soul)
            {
                with obj_intro_soul
                {
                pause = 0
                }
            }
        instance_destroy();
        }
    maxstrings = 4
}

///Events

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
audio_play_sound(snd_text_speech,10,false)
}

