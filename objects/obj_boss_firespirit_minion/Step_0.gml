/// @description Time
if enm_pause = 0
{
    time = min(time+0.19,timemax)
}

if time >= timemax
{
    alarm[0] = 1
}
//Target Pos
if instance_exists(obj_target)
{
    if obj_target.target = id
    {
        obj_target.z = -20
    }
}

///Attack Commence
if spr = spr_firesp_mini01_attk
{
    if image_index = 3
    {
        instance_create(obj_potato_combat.x+1,obj_potato_combat.y,obj_eff_enm)
        //Attack01
        if attk_numb = 1
        {
            audio_play_sound(snd_enm_slash01,10,false)
            with obj_eff_enm
            {
                spr = spr_slash_fire
            }
            attk_numb = 0
        }
    }
    if image_index = 5
    {
        spr = spr_firesp_mini01
        image_index = 0
        enm_pause = 0
    }
    else
    {
        enm_pause = 1
    }
}

///Reaction to Player Attacks
scr_Player_Attacks_ALL();

///Dead
if hp < 1
{
    with obj_eff_enm
    {
        instance_destroy();
    }
    if spr != spr_firesp_mini01_d
    {
        spr = spr_firesp_mini01_d
        image_index = 0
    }
    enm_pause = 1
    with obj_target
    {
        s = 0
    }
    if image_index = 0
    {
        audio_play_sound(snd_enm_dead01,10,false)
    }
    if image_index >= 8
    {
        image_speed = 0
    }
    xs = max(xs-0.015,0.15)
    ys = max(ys-0.015,0.15)
    zs = max(zs-0.015,0.15)
    r += 7
    if xs = 0.15
    {
		enm_pause = 0;
        ds_list_delete(enemies,self)
        enm_numb -= 1
        exp_obtained += 50
        instance_destroy();
    }
}

///Position
if dir = -1
{
    if y > position
    {
        y -= 2
    }
}
if dir = 1
{
    if y < position
    {
        y += 2
    }
}
if y >= position
if pos_found = 0
{
    audio_play_sound(snd_attack_firesp_spawnmin,10,false)
    instance_create(x,y,obj_eff_enm_misc01)
    pos_found = 1
}
if pos_found = 1
{
    alpha = min(alpha+0.02,1)
}

