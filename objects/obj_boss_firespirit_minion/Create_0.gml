/// @description Vars
image_speed = 0.125;
draw_set_alpha_test(true);
draw_set_alpha_test_ref_value(40);
hp = 90;
attack = 0;
time = 0;
timemax = irandom_range(30,45);
explode = 0;
enm_pause = 0;
attk_numb = 0;
spr = spr_firesp_mini01;
dir = 0;
position = 0;
alpha = 0;
pos_found = 0;
xs = 1;
ys = 1;
zs = 1;
r = 0;
//Add to list
identifier = instance_find(obj_boss_firespirit_minion, instance_number(obj_boss_firespirit_minion)-1);
ds_list_add(enemies, identifier);
enm_numb += 1;

///Movement to Pos

if instance_number(obj_boss_firespirit_minion) = 1
{
    position = y+40
    dir = 1
    x += 10
}
if instance_number(obj_boss_firespirit_minion) = 2
{
    position = y+70
    dir = 1
    x -= 20
}
if instance_number(obj_boss_firespirit_minion) = 3
{
    position = y+100
    dir = 1
    x += 10
}

///Defensive Vars
//(0.1 = 10% dmg reduction, so on...)
physical_defense = 0;
fire_defense = 0.5;
nature_defense = 0;
magic_defense = 0;
lightning_defense = 0;
dark_defense = 0;
earth_defense = 0;

