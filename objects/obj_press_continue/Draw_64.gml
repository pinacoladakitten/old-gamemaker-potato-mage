/// @description Press Z prompt
draw_set_font(font3);
draw_set_halign(fa_left);
draw_text(10,10,string_hash_to_newline("Press 'Z' to continue (keyboard)"));
draw_text(10,30,string_hash_to_newline("Press 'A' to continue (controller)"));
draw_set_font(font0);

