camx = 0
camy= 0
camz = 0
camf = 0

//Start 3D mode
d3d_start(); 

//Enable lighting
d3d_set_lighting(false); 

//Enable backface culling. This stops the renderer from drawing polygons that
//face away from the camera, speeding the render up.
d3d_set_culling(true); 

//Define and enable a global directional light.
d3d_light_define_direction(1,1,0.5,0,c_white);
d3d_light_enable(1,false);

