/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(ord("Z"))
    if string_length(strings[text]) <= pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        if text = 1
        {
            obj_potato_overw.sprite_index = spr_potato_ow_idle_look
        }
        if text = 3
        {
            instance_create(0,0,obj_scene_picture);
            instance_create(0,0,obj_trans_fade_in_out);
            obj_trans_fade_in_out.fadespd = 0.01
        }
        if text = 17
        {
            audio_play_sound(snd_spiritsup01,10,true);
            audio_sound_gain(snd_spiritsup01,0.4,0);
            instance_create(0,0,obj_trans_fade_in_out);
            obj_trans_fade_in_out.fadespd = 0.01
        }
        if text = 23
        {
            instance_create(0,0,obj_trans_fade_in_out);
            obj_trans_fade_in_out.fadespd = 0.01
        }
        if text = 40
        {   
            combat_music = snd_battle_evt01
            audio_play_sound(combat_music,10,true);
            audio_sound_gain(combat_music,0.6,0);
            instance_create(0,0,obj_trans_fade_in_out);
            obj_trans_fade_in_out.fadespd = 0.01
        }
        if (text > maxstrings)
        {
            pos = 0
            text = 0
            pause = 0
            //Spawn Type
            with obj_spawn
            {
                spawn1 = irandom_range(1,1)
                spawn2 = irandom_range(1,1)
                spawn3 = irandom_range(1,1)
                spawn4 = irandom_range(2,2)
                spawn5 = irandom_range(2,2)
                
                enemy[0] = obj_enm_floaty01
                if spawn2 = 1 {enemy[1] = obj_enm_floaty01}
                if spawn3 = 1 {enemy[2] = obj_enm_floaty01}
                if spawn4 = 1 {enemy[3] = obj_enm_floaty01}
                if spawn5 = 1 {enemy[4] = obj_enm_floaty01}
            }
            //Delete Object
            maxobject += 1
            global.objectcollected[maxobject-1] = id
            //Transition
            potato_battle_room = rm_battle_dream3D
            audio_play_sound(snd_encounter,10,false);
            instance_create(512,384,obj_trans_battle);
            obj_trans_battle.battle_music = 0
            //Destroy
            instance_destroy();
        }
    }
    maxstrings = 48
}

///Events
if active = 1
{
    pause = 1
}
//Portraits
if string_char_at(strings[text], 1) = "M"
{
    port = spr_potato_portrait_text
}
if string_char_at(strings[text], 1) = "K"
{
    port = spr_kaas_portrait
}
if string_char_at(strings[text], 1) = "F"
{
    port = spr_watcherface_portrait
}
if string_char_at(strings[text], 1) = "?"
{
    port = spr_port
}
//Scenes
if active = 1
if instance_exists(obj_trans_fade_in_out)
if instance_exists(obj_scene_picture)
{
    if obj_trans_fade_in_out.alpha >= 1
    {
        obj_scene_picture.drawscene = 1
        obj_scene_picture.bg = bg_dream01_bg03
        if text = 17
        {
            obj_scene_picture.bg = bg_dream01_bg05
        }
        if text = 23
        {
            obj_scene_picture.bg = bg_dream01_bg06
        }
        if text = 40
        {
            obj_scene_picture.bg = bg_dream01_bg07
        }
    }
}
//Music
if text >= 35
{
    audio_sound_gain(snd_spiritsup01,0,10*room_speed);
    if audio_sound_get_gain(snd_spiritsup01) = 0
    {
        audio_stop_sound(snd_spiritsup01);
    }
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !instance_exists(obj_trans_battle)
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

