/// @description Start up
pos = 1
text = 0
active = 0
active2 = 0
maxstrings = 48
port = spr_port
vol = 1

///Arrays
for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
Is this really the one to have the opportunity to press on?
"
strings[1] = @"???:
Yeah, why him!? He's so weak, what's he gonna do? How is he
going to make a difference?
"
strings[2] = @"???:
Exactly! Why does HE have the chance at getting out of here?
I'd bet if I were chosen I would make it out guaranteed.
"
strings[3] = @"Mythor:
I-I didn't mean to cause any trouble-I'm sorry.
"
strings[4] = @"???:
Guys! This is what's wrong, I haven't realized it before, but
now I see it! People like him, they don't deserve ANY of what
they're given!
"
strings[5] = @"???:
They have all of the benefits! Meanwhile, the rest of us are putting
all of our efforts to simply get a sliver of those benefits! How rotten!
"
strings[6] = @"Mythor:
I'm sorry! I-I-I just don't know what's-
"
strings[7] = @"???:
How can you be so sorry!? Huh!? Sorry to be picked!!? 
"
strings[8] = @"???:
Look at you, a shrimp in the big sea!! Clueless of your size so you
act like you own the place!!
"
strings[9] = @"???:
Yet I'm here seeing you! You of all people get the chance!!
When there are those more capable who deserve it!!!
"
strings[10] = @"Mythor:
Stop! Stop! Please! Here I have the stone!
"
strings[11] = @"???:
Giving us the stone won't save you!!
"
strings[12] = @"Mythor:
Then what will!? I'll do anything! Just leave me alone!!
"
strings[13] = @"????:
Enough! Those voices mean nothing Myth!...Myth?...Mythor!
"
strings[14] = @"Mythor:
J-just...leave me be...I don't want to be apart of this any
longer...
"
strings[15] = @"????:
Myth...it's alright, ok? I've got your back, just look at me.
"
strings[16] = @"Mythor:
W-who...are you....
"
strings[17] = @"Mythor:
Kass?...
"
strings[18] = @"Kassadin:
Myth, you can't let the negativity of others get to your head like that.
They're only words, and let them stay as just that.
"
strings[19] = @"Kassadin:
Don't be too hard on yourself Myth, have some self confidence to stand up
against those voices.
"
strings[20] = @"Mythor:
I-I'm sorry...Kass.
"
strings[21] = @"Kassadin:
Don't be sorry Myth, it's ok, none of this is your fault. 
"
strings[22] = @"Kassadin:
Here...take my hand, you don't have to go about this alone anymore.
"
strings[23] = @"Mythor:
Ok...
"
strings[24] = @"Kassadin:
See? Don't you feel better now? 
"
strings[25] = @"Mythor:
I-I guess...I feel warmer, it was so cold and I was by myself against
everything. 
"
strings[26] = @"Mythor:
Who were those voices Kass? Why did they decided to say those things!?
Were they right about all of this!?-
"
strings[27] = @"Kassadin:
Shhhh...Myth, those voices come from those who spread their hate upon
others. There are many who are bothered by things in life, and they
stick to them, not being able to move on.
"
strings[28] = @"Kassadin:
I don't want you to be consumed by their influence Myth, I need you to
stand stronger than them. 
"
strings[29] = @"Kassadin:
Will you do that for me?
"
strings[30] = @"Mythor:
Alright.
"
strings[31] = @"Kassadin:
Good good, the world is a rough place Myth. There are many who have sacrificed
everything, yet have gotten nothing in return or they may have ended up in an
even worse situation than before.
"
strings[32] = @"Kassadin:
It's no wonder these souls are filled with such anger and hate. They feel as if
they don't deserve anything life has offered them. So they blame others, allowing
jealousy to take its toll.
"
strings[33] = @"Mythor:
I-I don't know what to say...
"
strings[34] = @"Mythor:
Kass, what exactly is this place anyway?
"
strings[35] = @"Kassadin:
This place?-
"
strings[36] = @"???:
hehehe...hehehe...
"
strings[37] = @"???:
...mythor...mythor...mythor...come...come...come...
"
strings[38] = @"???:
...don't you want....salvation....
"
strings[39] = @"???:
...hehehe...hehehe...
"
strings[40] = @"Mythor:
Huh!? Who else is here?-
"
strings[41] = @"Kassadin:
Quick! Myth there's no time! They're already here!
"
strings[42] = @"Kassadin:
They've sensed your emotion, well, the disturbance in it more of! 
Now they've come!
"
strings[43] = @"Mythor:
Who-what!?-
"
strings[44] = @"Kassadin:
Myth listen to me! You need to keep going, I'll always be beside
you, even when it doesn't seem like it ok? 
"
strings[45] = @"Mythor:
You're leaving!? B-But-
"
strings[46] = @"Kassadin:
You must resist them! To prove that it isn't your time just yet! 
Please! Everything will come clear sooner or later!
"
strings[47] = @"???:
...but myth...don't...don't...don't worry...come...closer....
"
strings[48] = @"Mythor:
Oh great, I'm not sure what exactly these things are and I'm kind of
freaking out a bit, but like what Kass said, I need to stand and fight! 
I'll get out of here soon, I swear it!
"

