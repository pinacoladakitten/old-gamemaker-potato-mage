/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if string_length(strings[text]) < pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        if (text > maxstrings)
        {
            pos = 0
            text = 0
            active = 0
            instance_create(0,0,obj_trans_fade_in);
            obj_trans_fade_in.roomgoto = rm_thanks02
            instance_destroy();
        }
    }
    maxstrings = 0
}

///Events
if active = 1
{
    enm_pause = 1
    obj_potato_combat.pause_combat = 1
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

