/// @description When Destroyed
if instance_exists(obj_potato_combat)
{
    if instance_exists(obj_potato_heal_haste01)
    {
        with obj_potato_heal_haste01{instance_destroy();}
    }
    instance_create(obj_potato_combat.x,obj_potato_combat.y,obj_potato_heal_haste01);
}
instance_create(x,y,obj_potato_asterid_eff);
audio_play_sound(snd_mag_potato_heal01,10,false)
//Combo
potato_combo += 0.02
potato_attk *= potato_combo
potato_mag *= potato_combo

