/// @description Hitbox
if place_meeting(x,y,obj_potato_combat)
if hit = 0
{
    hp_potato -= 18 * obj_bign_attkStats.attkScalingHard;;
    obj_potato_combat.hit = 1;
    hit = 1;
    fade = 1;
}

///Movement
r += 10
z = min(z+0.25,10);
xsc = min(xsc+0.025,0.8);
ysc = xsc;
zsc = xsc;
if fade = 0
{alpha = min(alpha+0.05,1);}
if fade = 1
{
    alpha = max(alpha-0.05,0);
    xsc = max(xsc-0.05,0);
    if alpha <= 0
    {instance_destroy();}
}

///Misc
d3d_set_lighting(true);
d3d_light_define_point(3,x,y,0,200,c_red)
d3d_light_enable(3,true);
d3d_set_lighting(false);

