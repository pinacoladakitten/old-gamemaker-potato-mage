/// @description Main Event: Fade In
if fade = 1
{
    alpha += 0.01
    if alpha > 1
    {
    room_goto_next();
    d3d_end();
    d3d_light_enable(0,false);
    d3d_light_enable(1,false);
    d3d_light_enable(2,false);
    d3d_light_enable(3,false);
    d3d_light_enable(4,false);
    d3d_light_enable(5,false);
    d3d_light_enable(6,false);
    d3d_light_enable(7,false);
    d3d_set_lighting(false)
    instance_destroy();
    }
}

