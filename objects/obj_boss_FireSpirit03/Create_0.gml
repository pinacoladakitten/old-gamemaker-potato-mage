/// @description Setup
image_speed = 0.05;
draw_set_alpha_test(true);
draw_set_alpha_test_ref_value(40);
maxhp = 9500;
hp = 9500;
attack = 0;
time = 0;
timemax = 35;
explode = 0;
enm_pause = 0;
attk_numb = 0;
spr = sprite_index;
enm_numb += 1;
ds_list_add(enemies, id);
xs = 1;
ys = 1;
zs = 1;
r = 0;
attack_commence = 0;
attack_times = 0;

///Defensive Vars
//(0.1 = 10% dmg reduction, so on...)
physical_defense = 0.2;
fire_defense = -0.12;
nature_defense = 0.2;
magic_defense = 0.2;
lightning_defense = 0;
dark_defense = 0.5;
earth_defense = 0.2;

