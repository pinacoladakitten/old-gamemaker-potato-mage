/// @description Insert description here
// You can write your code in this editor
if (instance_exists(obj_playerTarget)){
	if (place_meeting(x,y,obj_playerTarget) and !active)
	{
	    tex = sprite_get_texture(spr_interact,-1);
	    angle+=2
	    d3d_set_lighting(false);
	    d3d_transform_add_rotation_z(angle)
	    d3d_transform_add_translation(obj_playerTarget.x,obj_playerTarget.y,obj_playerTarget.z+2);
	    d3d_draw_wall(0,0+3,28,0,0-3,22,tex,1,1)
	    d3d_transform_set_identity();
	}
}