/// @description Text event
// You can write your code in this editor
if (!instance_exists(obj_trans_fade_out))
if (keyboard_check_pressed(global.keyb_confirm) or gamepad_button_check_pressed(0, global.gpi_confirm))
if (pause == 0){
	//Set Active
	active = true;
	// Create Textbox
	instance_create(x, y, obj_BELFRY_textbox);
	obj_BELFRY_textbox.windowDarken = true;
	// Use text according to which text event
	switch(type){
		case 0:
			dia_belfryOw_text000();
			break;
		case 1:
			dia_belfryOw_text001();
			break;
		case 2:
			dia_belfryOw_text002();
			break;
		default:
			break;
	}
}

//If Text End
if (instance_exists(obj_BELFRY_textbox)){
    if (obj_BELFRY_textbox.active == 0){
		pause = 0;
		active = 0;
		with(obj_BELFRY_textbox){instance_destroy(self);}
	}
}