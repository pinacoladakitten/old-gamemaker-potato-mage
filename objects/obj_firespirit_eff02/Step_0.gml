/// @description Movement
r += 10
if z < 65
{
    z += 0.5
}
if xsc <= 20
if shrink = 0
{
    xsc += 0.25
    ysc += 0.25
}
if shrink = 1
{
    xsc -= 8
    ysc -= 8
    z += 20
    zsph += 20
    alpha = max(alpha-0.025,0)
}
if alpha = 0
{
    instance_destroy();
}

if distance_to_object(obj_potato_overw) < 200
{
    d3d_set_lighting(true);
    d3d_light_define_point(3,x,y,0,200,c_yellow)
    d3d_light_enable(3,true);
    d3d_set_lighting(false);
}

