/// @description Setup
draw_set_alpha_test(true)
draw_set_alpha_test_ref_value(40)
hsp = 0
vsp = 0
__view_set( e__VW.XView, 0, x )
__view_set( e__VW.YView, 0, y )
encounter_chance = 0
globalvar pause;
pause = 0;
potato_room = room;
steps = 0
z = 10
if potato_x != 0{x = potato_x}
if potato_y != 0{y = potato_y}
if potato_z != 0{z = potato_z}
movev = 0
moveh = 0
key_up = 0
key_down = 0
key_left = 0
key_right = 0
movemax = 1.5
zspeed = 0
idle = 1
//Events
evt = 0;
instance_create(x, y, obj_playerTarget);
//Reset Potato Pos
potato_x = x;
potato_y = y;
//Misc
dust_time = 0;
potatoColor = true;
//Set Sprite (Outdated)
spr = spr_potato_ow_idle_f;

///p3dc vars
zdir = 0
zspeed = 0
zprevious = 50
accelerate = 0
zref = 0
zshadowdist = 0
zshadow = 0
clipout = 0
on_floor = 0

