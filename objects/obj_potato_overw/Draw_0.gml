/// @description Draw
d3d_set_lighting(true);
var ss,cc,tex;
tex = sprite_get_texture(sprite_index,-1);
tex01 = sprite_get_texture(spr_ow_shadow,-1);
ss = sin(obj_camera_ow.direction*pi/180);
cc = cos(obj_camera_ow.direction*pi/180);
draw_set_alpha(1)
d3d_transform_add_translation(x,y,-5);
d3d_draw_wall(0+10*ss,0+10*cc,z+20,0-10*ss,0-10*cc,z+0,tex,1,1)
d3d_transform_set_identity();
draw_set_alpha(1)
//Shadow
d3d_draw_wall(x+1, y+13*cc, zshadow+6, x+1, y-13*cc, zshadow+0, tex01, 1, 1);
d3d_set_lighting(false);
