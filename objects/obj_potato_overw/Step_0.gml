/// @description Enemy Encounter
if pause = 0
if (hsp > 0) or (vsp > 0)
if enm_encounter = 1
{
encounter_chance = irandom_range(1,100)
    if encounter_chance = 5
    {
    room_goto(potato_battle_room)
    }
}

///Movement
audio_sound_pitch(snd_step, 1);
if pause = 0
{
    //**Inputs Vertical**\\
    key_up = -(keyboard_check(global.keyb_up) or (gamepad_axis_value(0, gp_axislv) < 0)); //Key Up
    key_down = keyboard_check(global.keyb_down) or (gamepad_axis_value(0, gp_axislv) > 0); //Key Down
    
    //If on the ground, can move
    if (on_floor == 1){movev = key_down + key_up}
    
    //Reaction to Inputs Vertical
    if vsp >= 0{vsp = min(vsp+movev,movemax)} //Calculate movement from moving up & down
    if vsp <= 0{vsp = max(-1*movemax,vsp+movev)}
    
    //Hinder movement if already moving in other direction Vert
    if ((sprite_index == spr_potato_ow_l or sprite_index == spr_potato_ow_r) or (sprite_index == spr_potato_ow_l_white or sprite_index == spr_potato_ow_r_white))
    {
        var limitV = movemax*0.55;
        if vsp >= 0{vsp = min(vsp+movev, limitV)} //Speed limit (movemax)
        if vsp <= 0{vsp = max(-limitV, vsp+movev)}
    }
    
    
    //**Inputs Horizontal**\\
    key_left = keyboard_check(global.keyb_left) or (gamepad_axis_value(0, gp_axislh) < 0); //Keys Left & Right
    key_right = -(keyboard_check(global.keyb_right) or (gamepad_axis_value(0, gp_axislh) > 0));
    
    //If on the ground, can move
    if (on_floor == 1){moveh = (key_left + key_right) * 0.5}
    
    //Reaction to Inputs Horizontal
    if hsp >= 0{hsp = min(hsp+moveh,movemax)} //Calculate movement for moving left & right
    if hsp <= 0{hsp = max(-1*movemax,hsp+moveh)}
    
    //Hinder movement if already moving in other direction Horiz
    if ((sprite_index == spr_potato_ow_f or sprite_index == spr_potato_ow_b) or (sprite_index == spr_potato_ow_f_white or sprite_index == spr_potato_ow_b_white))
    {
        var limitH = movemax*0.55;
        if hsp >= 0{hsp = min(hsp+moveh, limitH);} //Speed limit (movemax)
        if hsp <= 0{hsp = max(-limitH, hsp+moveh);}
    }

    
    //**Movement Process WITH SPRITE INDEXES**\\
    //If Stopped
    if (hsp == 0)
    and (vsp == 0){
        image_speed = 0;
    }
    //Stopped moving Vertically
    if (movev == 0)
    {
        if vsp >= 0{vsp = max(vsp-0.5,0)} //Deccelerate in either direction vertically
        if vsp <= 0{vsp = min(vsp+0.5,0)}
    }
    //Stopped moving Horizontally
    if (moveh == 0)
    {
        if hsp >= 0{hsp = max(hsp-0.5,0)} //Deccelerate in either direction horizontally
        if hsp <= 0{hsp = min(hsp+0.5,0)}
    }
    //Vertical Sprite Indexes
    if (vsp != 0) //If MOVING Vertically
    {
        image_speed = 0.1;
        if (vsp < 0)
        {
            if (idle == 1)
            if (hsp == 0)
            {
                if (potatoColor == true){   //Color/Skin Check
                    sprite_index = spr_potato_ow_b;
                }
                else{
                    sprite_index = spr_potato_ow_b_white;
                }
                idle = 0;
            }
            //Default Color
            if (sprite_index == spr_potato_ow_f)
            {
                sprite_index = spr_potato_ow_b;
            }
            //*White
            if (sprite_index == spr_potato_ow_f_white){
                sprite_index = spr_potato_ow_b_white;
            }
        }
        if (vsp > 0)
        {
            if (idle == 1)
            if (hsp == 0)
            {
                if (potatoColor == true){   //Color/Skin Check
                    sprite_index = spr_potato_ow_f;
                }
                else{
                    sprite_index = spr_potato_ow_f_white;
                }
                idle = 0;
            }
            //Default
            if (sprite_index == spr_potato_ow_b)
            {
                sprite_index = spr_potato_ow_f;
            }
            //*White
            if (sprite_index == spr_potato_ow_b_white){
                sprite_index = spr_potato_ow_f_white;
            }
        }
    }
    //Horizontal Sprites
    if (hsp != 0) //If MOVING Horizontally
    {
        image_speed = 0.1;
        if (hsp > 0)
        {
            if (idle == 1)
            if (vsp == 0)
            {
                if (potatoColor == true){   //Color/Skin Check
                    sprite_index = spr_potato_ow_l;
                }
                else{
                    sprite_index = spr_potato_ow_l_white;
                }
                idle = 0;
            }
            //Default
            if (sprite_index == spr_potato_ow_r)
            {
                sprite_index = spr_potato_ow_l;
            }
            //*White
            if (sprite_index == spr_potato_ow_r_white){
                sprite_index = spr_potato_ow_l_white;
            }
        }
        if (hsp < 0)
        {
            if (idle == 1)
            if (vsp == 0)
            {
                if (potatoColor == true){   //Color/Skin Check
                    sprite_index = spr_potato_ow_r;
                }
                else{
                    sprite_index = spr_potato_ow_r_white;
                }
                idle = 0;
            }
            //Default
            if (sprite_index == spr_potato_ow_l)
            {
                sprite_index = spr_potato_ow_r;
            }
            //*White
            if (sprite_index == spr_potato_ow_l_white){
                sprite_index = spr_potato_ow_r_white;
            }
        }
    }
}
if (vsp == 0)
or (hsp == 0){
    idle = 1;
}
//**Completely Idle (NO Movement in a direction!)**\\
if (vsp == 0)
and (hsp == 0)
{
    //Idle Vertical\\
    //Default
    if (potatoColor == true){
        if (sprite_index == spr_potato_ow_b){
            sprite_index = spr_potato_ow_idle_b;
        }
        if (sprite_index == spr_potato_ow_f){
            sprite_index = spr_potato_ow_idle_f;
        }
    }
    else{
        //*White
        if (sprite_index == spr_potato_ow_b_white){
            sprite_index = spr_potato_ow_idle_b_white;
        }
        if (sprite_index == spr_potato_ow_f_white){
            sprite_index = spr_potato_ow_idle_f_white;
        }
    }
    //Idle Horizontal\\
    //Default
    if (potatoColor == true){
        if (sprite_index == spr_potato_ow_l){
            sprite_index = spr_potato_ow_idle_l;
        }
        if (sprite_index == spr_potato_ow_r){
            sprite_index = spr_potato_ow_idle_r;
        }
    }
    else{
        //*White
        if (sprite_index == spr_potato_ow_l_white){
            sprite_index = spr_potato_ow_idle_l_white;
        }
        if (sprite_index == spr_potato_ow_r_white){
            sprite_index = spr_potato_ow_idle_r_white;
        }
    }
    image_index = 0;
    idle = 1;
}
//Dash
if (pause == 0)
{
    if (on_floor == 1){
        movemax = 1.5;
        if (keyboard_check(global.keyb_cancel))
        or (gamepad_button_check(0, global.gpi_cancel))
        {
            movemax = movemax * 1.5;
            image_speed *= 1.5;
            audio_sound_pitch(snd_step, 1.25);
        }
    }
}
//Misc Sprites
if (sprite_index == spr_potato_ow_idle_look)
{
    image_speed = 0;
}
//Pause Check
if (pause == 1)
if (evt == 0)
{
    vsp = 0;
    hsp = 0;
    movev = 0;
    moveh = 0;
}
//Jumping and Gravity
if (keyboard_check_pressed(global.keyb_jump))
or (gamepad_button_check_pressed(0, global.gpi_jump))
if (on_floor == 1)
if (pause == 0)
{
    zspeed = -3.5;
}

/* */
///Collision 2D
//Collision blocks
if (place_meeting(x+vsp,y,obj_col))
{
    while(!place_meeting(x+sign(vsp),y,obj_col)) 
    {
    x += sign(round(vsp));
    }
    vsp = 0;
}
//Vertical Collision
if (place_meeting(x,y+hsp,obj_col))
{
    while(!place_meeting(x,y+sign(hsp),obj_col))
    {
    y += sign(hsp);
    }
    hsp = 0;
}
if p3dc = 0
{
    x += vsp
    y += hsp
}

/* */

///Collision 3D
//P3DC
if p3dc = 1
{
    //Horzontal Collision
    if(p3dc_check(collision_player,x+(2.25*sign(vsp)),y,z+1,collision_map,map_x,map_y,0) == 0)
    {
        x += vsp
    }
    if(p3dc_check(collision_player,x,y+(2.25*sign(hsp)),z+1,collision_map,map_x,map_y,0) == 0)
    {
        y += hsp
    }
    //Avoid Collision
    if(!p3dc_check(collision_player,x+(2.5*sign(vsp)),y,z,collision_map,map_x,map_y,0) == 0)
    {
        x -= vsp*sign(p3dc_ray(collision_map,map_x,map_y,0,x,y,z,0,1*sign(vsp),0))
    }
    if(!p3dc_check(collision_player,x,y+(2.5*sign(hsp)),z,collision_map,map_x,map_y,0) == 0)
    {
        y -= hsp*sign(p3dc_ray(collision_map,map_x,map_y,0,x,y,z,0,1*sign(hsp),0))
    }
    if(!p3dc_check(collision_player,x+(1*sign(vsp)),y+(1*sign(hsp)),z+1,collision_map,map_x,map_y,0) == 0)
    {
        x = xprevious
        y = yprevious
    }

}
//Gravity
z -= zspeed
if p3dc = 1 //p3dc check//
{
    //Collision Slopes
    zref=min(p3dc_ray(collision_map,map_x,map_y,0,x-1,y-1,z,0,0,-1),
    p3dc_ray(collision_map,map_x,map_y,0,x-1,y+1,z,0,0,-1),
    p3dc_ray(collision_map,map_x,map_y,0,x+1,y-1,z,0,0,-1),
    p3dc_ray(collision_map,map_x,map_y,0,x+1,y+1,z,0,0,-1));
}
//Z Collision Checking & Gravity
if(zref<=5)
{
    if(zspeed<=0)
    {
        zref=zprevious-4;
        zspeed=1;
        z=zprevious-1;
    }
    else
    {
        z=5+z-zref;
        zspeed=1;
    }
}
else
{   zspeed = min(zspeed+0.25,6);
}
zprevious=z;
on_floor = 0;
//Solid Collision Checks
if p3dc = 1 //p3dc check//
{
    //Z Collision
    if (p3dc_check(collision_player,x,y,z-6,collision_map,map_x,map_y,0) == 1)
    {
        zspeed = 0
    }
    if(!p3dc_check(collision_player,x,y,z-5,collision_map,map_x,map_y,0) == 0)
    {
        zspeed = 1
    }
    //Roof Check
    if (p3dc_check(collision_player,x,y,z+10,collision_map,map_x,map_y,0) == 1)
    {
        zspeed = 1
    }
    //Floor Check
    if (!p3dc_check(collision_player,x,y,z-6,collision_map,map_x,map_y,0) == 0)
    {
        on_floor = 1
    }
}

/////Misc Stuff//////
//Default floor if no p3dc
if p3dc_floor = 1
{
    z = max(z,5);
    if z = 5
    {
        zspeed = 0
        on_floor = 1
    }
}
//Z Shadow
if p3dc = 1
{
    zshadowdist = p3dc_ray(collision_map,map_x,map_y,0,x,y,z+1,0,0,-1)
    if p3dc_floor = 0 
    {zshadow = max(z-zshadowdist+1,-1000);}
    if p3dc_floor = 1
    {zshadow = max(z-zshadowdist+1,0);}
}

/* */
///Backgrounds
__background_set( e__BG.HSpeed, 0, 0.1 )


///Sounds
if steps = 1
if vsp != 0
or hsp != 0
{
    if sprite_index = spr_potato_ow_f
    or sprite_index = spr_potato_ow_b
    {
        if !audio_is_playing(snd_step)
        {
        audio_play_sound(snd_step,10,false)
        }
    }
    if sprite_index = spr_potato_ow_l
    or sprite_index = spr_potato_ow_r
    {
        if round(image_index) = 1 
        or round(image_index) = 3
        {
        if !audio_is_playing(snd_step){audio_play_sound(snd_step,10,false)}
        }
    }
}


///Misc
//Water Eff
if place_meeting(x,y,obj_water_col01){
    if z <= obj_water_col01.z
    {
        if image_index = 1 or image_index = 1.05 or image_index = 3
        {instance_create(x,y,obj_water_splash01);}
    }
    if (abs(z-obj_water_col01.z) <= 6) and on_floor = 0
    {
        if !audio_is_playing(snd_water_enter)
        {audio_play_sound(snd_water_enter,10,false);
        instance_create(x,y,obj_water_splash01);}
    }
}
//Dust Eff Movement
if (on_floor = 1)
if (vsp != 0) or (hsp != 0){
    dust_time += 1;
    if (dust_time >= 15){
        instance_create(x, y, obj_dust);
        dust_time = 0;
    }
    
}
/* */
/*  */
