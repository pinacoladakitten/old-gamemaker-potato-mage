/// @description Respawn
if dead = 1
{
    if potato_x != 0{x = potato_x}
    if potato_y != 0{y = potato_y}
    //Set Health and Stuff
    hp_potato = hp_potato_max
    mp_potato = mp_potato_max
    alarm[0] = 2
}
//Set Room
potato_room = room;

if (!potatoColor){
	sprite_index = spr_potato_ow_idle_f_white;
}
//Hud
if(potatoColor == true){
	instance_create(0,0,obj_overw_hud_all);
}