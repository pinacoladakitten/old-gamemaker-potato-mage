/// @description G Squid Proj01
spr = spr_eff_gsquid01
image_index = 0
xsc = 1
ysc = 1
zsc = 1
yscmax = 1
r = 0
hit = 0
hit2 = 0
z = 6
alarm[0] = 4*room_speed
alarm[1] = 0.1*room_speed
alpha = 0
image_speed = 0.25
pspeed = irandom_range(0,0);
pspeedup = choose(0.05,0.05);
pspeedmax = 1
audio_play_sound(snd_bign_fire02,10,false);
phase = 0

