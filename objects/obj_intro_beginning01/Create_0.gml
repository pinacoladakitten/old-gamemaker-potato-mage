/// @description Setup
//Vars
alpha = 0;
alpha01 = 0;
sayNoCount = 0;
text = 0;
fade = true;
active = 0;
option = 0;
cursor_spin = 0;
alarm[0] = 4*room_speed;
//Dialogue
strings[0] = "I only ask for one soul to take in what I have to offer..."
strings[1] = @"Pleading heavenward, those from above, would you please grant me
this last wish?"
strings[2] = "This is all I beg of you, before I-I...can be put to rest."
strings[3] = "Will you respond to the voice's call?"
//If not
strings[4] = @"Please! Please! This is my last request! I'm desperate for anyone
to hear me out!"
strings[5] = @"I don't understand...why doesn't anyone want to listen to me? Do they
simply not trust me? Again, it is but a small request..."
strings[6] = @"I'll tell you everything about me! I'm begging you! Talk to me! Ask me
anything you want to know!"
strings[7] = @"How dismaying, I can't believe I could be so gullible. No matter how many
times I want to express myself, all of them close me off..."
strings[8] = "Maybe it's time I do something about it then...sorry to bother."
strings[9] = "..."

