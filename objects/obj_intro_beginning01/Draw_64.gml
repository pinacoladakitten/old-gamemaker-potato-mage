/// @description Main event
//Setup
draw_set_font(introfont);
draw_set_halign(fa_center);
draw_set_color(c_white);
draw_set_alpha(alpha);
//The Text
draw_text_ext_transformed(640,360,string_hash_to_newline(strings[text]),-1,-1,1,1,0);
//Text Options
if text = 3
{
    draw_set_alpha(alpha01);
    draw_text(560,410,string_hash_to_newline("Yes"));
    if global.intro_umbra_gone01 = 0
    {draw_text(720,410,string_hash_to_newline("No"));}
    image_speed = 0.25;
    if option = 0
    {draw_sprite(spr_intro_pointer01,-1,535,422);}
    if option = 1
    {draw_sprite(spr_intro_pointer01,-1,695,422);}
}
draw_set_alpha(1);

