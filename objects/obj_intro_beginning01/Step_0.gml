/// @description Main event
//Text Fade
if fade = false
{alpha = min(alpha+0.05,1);
alpha01 = min(alpha01+0.05,1);}
else
{alpha = max(alpha-0.05,0);
alpha01 = max(alpha01-0.05,0);}
if active = 1
{
    //If Press Confirm
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    {
        //Fade If Confirm
        if alpha = 1
        {fade = true;}
        //Text Question
        if text = 3
        if alpha = 1
        {
            //Pick Option
            if option = 0 //Yes
            {
                active = 0;
                fade = true;
            }
            if option = 1 //No
            {sayNoCount += 1;
            sayNoCount = clamp(sayNoCount,0,6);}
        }
    }
    //Option Pick Controls
    if keyboard_check_pressed(global.keyb_left)
    or gamepad_button_check_pressed(0, global.gpi_left)
    {option = max(option-1,0);}
    if keyboard_check_pressed(global.keyb_right)
    or gamepad_button_check_pressed(0, global.gpi_right)
    {option = min(option+1,1);}
    //If Alpha is 0
    if alpha = 0
    {
        fade = false;
        if text < 3
        {text += 1;
        exit;}
        if text = 3
        {text = 3+sayNoCount;
        exit;}
        if text > 3
        {text = 3;
        exit;}
    }
    //Text is maxed
    if text = 9
    {global.intro_umbra_gone01 = 1;}
    if global.intro_umbra_gone01 = 1
    {option = clamp(option,0,0);}
}

