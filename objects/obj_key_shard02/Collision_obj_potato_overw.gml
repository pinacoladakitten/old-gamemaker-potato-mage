if pause = 0
{
    audio_play_sound(snd_item_pickup01,10,false)
    with obj_inventory_ow
    {
        for (s = 0; s < maxitem; s += 1)
            {
                if ds_list_find_value(global.inventorykeyitems,s) = 1
                {
                    ds_list_replace(global.inventorykeyitems, s, 2);
                }
            }
    }
    scr_DestroyUsedObjs();
    activee = 1
    with obj_dream01_barrier
    {fade = 1;}
    with obj_enemy_encounter01
    {active2 = 1;}
    if instance_exists(obj_dream01_wall01){obj_dream01_wall01.text = 3}
    //Create Prompt
    instance_create(0,0,obj_item_obtained)
    obj_item_obtained.item = spr_key03
    obj_item_obtained.fade = 0
    obj_item_obtained.alarm[0] = 3*room_speed
    //Destroy
    instance_destroy();
}

