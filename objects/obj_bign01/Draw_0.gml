/// @description Draw
if room = rm_boss_big_n01 or room = rm_boss_big_n01_hard
{
    d3d_set_lighting(true); 
    texture_set_repeat(true);
    d3d_transform_add_scaling(1,1,1)
    d3d_transform_add_rotation_z(90)
    d3d_transform_add_translation(x,y,0)
    d3d_model_draw(model0,0,0,0,background_get_texture(bg_castle_brick03));
    d3d_model_draw(model1,0,0,0,background_get_texture(bg_castle_brick04));
    d3d_model_draw(model2,0,0,0,background_get_texture(bg_castle_brick05));
    d3d_model_draw(model3,0,0,0,background_get_texture(bg_castle01_brick3));
    d3d_model_draw(model4,0,0,0,background_get_texture(bg_castle01_brick4));
    d3d_model_draw(model5,0,0,0,background_get_texture(bg_castle01_nolight));
    d3d_model_draw(model6,0,0,0,background_get_texture(bg_water));
    d3d_model_draw(model7,0,0,0,background_get_texture(gray_wood01));
    d3d_model_draw(model8,0,0,0,background_get_texture(mountain01));
    d3d_transform_set_identity();
}

