/// @description Loading
if modelload = 0
{
    d3d_model_load(model0, "maps/battle/big_n01/bg_castle_brick03.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 1
{
    d3d_model_load(model1, "maps/battle/big_n01/bg_castle_brick04.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 2
{
    d3d_model_load(model2, "maps/battle/big_n01/bg_castle_brick05.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 3
{
    d3d_model_load(model3, "maps/battle/big_n01/bg_castle01_brick3.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 4
{
    d3d_model_load(model4, "maps/battle/big_n01/bg_castle01_brick4.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 5
{
    d3d_model_load(model5, "maps/battle/big_n01/bg_castle01_nolight.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 6
{
    d3d_model_load(model6, "maps/battle/big_n01/bg_water.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 7
{
    d3d_model_load(model7, "maps/battle/big_n01/gray_wood01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 8
{
    d3d_model_load(model8, "maps/battle/big_n01/mountain01.d3d")
}

