/// @description Set-Up
if backg = 0
{
    __background_set( e__BG.Alpha, 3, alpha )
}
if backg = 1
{
    __background_set( e__BG.Alpha, 2, alpha )
}
if backg = 2
{
    __background_set( e__BG.Alpha, 1, alpha )
}
if fade = 1
{
    alpha -= 0.01
}

///Events
if alpha = 0
{
    if backg = 0
    {
    __background_set( e__BG.Visible, 3, false )
    __background_set( e__BG.Alpha, 3, 0 )
    }
    if backg = 1
    {
    __background_set( e__BG.Visible, 2, false )
    __background_set( e__BG.Alpha, 2, 0 )
    }
    if backg = 2
    {
    __background_set( e__BG.Visible, 1, false )
    __background_set( e__BG.Alpha, 1, 0 )
    }
    if backg < 3
    {
        alarm[0] = 5*room_speed
        fade = 0
        alpha = 1
    }
backg += 1
}
if backg = 3
{
    instance_create(0,0,obj_trans_fade_in_next)
    obj_trans_fade_in_next.alarm[0] = 5*room_speed
    backg += 1
}

