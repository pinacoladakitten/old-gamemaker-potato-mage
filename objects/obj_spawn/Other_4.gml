/// @description Restart Destroy Enemies
if instance_exists(obj_global_combat_enm)
if dead = 1
{
    dead = 0
}
//Reset Pos
x = 590;
y = 290;

///Spawn List
if instance_exists(obj_global_combat_enm)
{
instance_create(x,y,obj_target);
    if spawn1 = 1
    {
        instance_create(x,y,enemy[0])
        var enemy0;
        enemy0 = instance_find(enemy[0],instance_number(enemy[0])-1)
        ds_list_add(enemies, enemy0);
    }
    if spawn2 = 1 
    {
        instance_create(540,300,enemy[1])
        var enemy1;
        enemy1 = instance_find(enemy[1],instance_number(enemy[1])-1)
        ds_list_add(enemies, enemy1);
    }
    if spawn3 = 1 
    {
        instance_create(520,340,enemy[2])
        var enemy2;
        enemy2 = instance_find(enemy[2],instance_number(enemy[2])-1)
        ds_list_add(enemies, enemy2);
    }
    if spawn4 = 1 
    {
        instance_create(520,390,enemy[3])
        var enemy3;
        enemy3 = instance_find(enemy[3],instance_number(enemy[3])-1)
        ds_list_add(enemies, enemy3);
    }
    if spawn5 = 1 
    {
        instance_create(550,405,enemy[4])
        var enemy4;
        enemy4 = instance_find(enemy[4],instance_number(enemy[4])-1)
        ds_list_add(enemies, enemy4);
    }
}

