/// @description Check Enemy Array
if draw_check = 1
{
    if is_array(enemy)
    {
        draw_set_font(font4);
        draw_text(0,0,string_hash_to_newline("True"))
    }
    else
    {
        draw_text(0,0,string_hash_to_newline("False"))
        draw_set_font(font0);
    }
}

