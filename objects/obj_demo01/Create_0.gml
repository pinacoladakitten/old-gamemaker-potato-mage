/// @description Start up
pos = 1
text = 0
active = 0
port = spr_port
vol = 0
alarm[0] = 1.5*room_speed

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Umbra:
Oh uh, hey, well it looks like we've reached the end for now.
"
strings[1] = @"Umbra:
No I'm not the dude who was just talking in the scene before this, I know I know.
"
strings[2] = @"Umbra:
Anyway, the tale of the Potato Mage isn't full done yet, still thinking about how
it's all gonna play out and stuff.
"
strings[3] = @"Umbra:
Uh yeah, I don't know when it'll be done, stories take a while to make up you know.
"
strings[4] = @"Umbra:
So...um...well I guess you could fight whoever that one dude was again I dunno.
"
strings[5] = @"Umbra:
Well, see you later, I'll have more in the future I swear.
"

