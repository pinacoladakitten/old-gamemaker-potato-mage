pos = 1
text = 0

if instance_exists(obj_potato_overw)
{
    with obj_potato_overw
    {
    pause = 1
    }
}

///Strings
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue

//Event 1
if event = 0
{
strings[0] = "Hm..."
strings[1] = "These guys don't look very friendly."
strings[2] = "Their stare potrays a sort of...hostile feeling, and it's making me uneasy."
strings[3] = "Perhaps I should stay on my guard, just incase."
strings[4] = @"I mean, you never know, they could be friendly and maybe be the ones to tell
me where the heck I am. Oh, and maybe they could supply some food too because
I'm starving!"
strings[5] = "Ok, I'm getting off topic, let's just focus on getting out of here first."
}
//Event 2
if event = 1
{
strings[0] = "This is a save station."
strings[1] = @"You fully restore your hp and mp, assign skill points, and of course, 
save your game here."
}

