/// @description Main Event
if fade = 1
{
    alpha -= 0.1
}
if alpha <= 0
{
    instance_destroy();
}

if spr = spr_fantastic
{
    image_angle = min(image_angle+50,360)
    y -= 1
}
if spr = spr_alright
{
    image_angle = min(image_angle+45,360)
    y -= 0.5
}
if spr = spr_meh
{
    y -= 0.5
}
if spr = spr_dodged or spr = spr_level_up or spr = spr_critical or spr = spr_parried
{
    image_angle = min(image_angle+22.5,360)
    y -= 0.5
}
angle01 += 4
angle02 -= 4

