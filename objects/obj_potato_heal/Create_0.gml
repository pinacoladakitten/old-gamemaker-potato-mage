/// @description Vars and Stuff
hit = 0
z = 0
grav = 0.15
vsp = 3
effect = 0
target = 0
image_speed = 0.5
alarm[0] = 0.7*room_speed

///Sprites
spr = spr_potato_proj

///Stats
potency = 1.2*potato_mag*0.1 + potato_haste + potato_vitality*0.25

if potato_pure_skill01 = 1
{
    potency += potency*0.05
}
if potato_pure_skill02 = 1
{
    potency += potency*0.1
}

