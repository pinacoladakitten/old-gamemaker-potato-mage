/// @description Upon Destroy
//Weapon Effects
instance_create(x,y,obj_potato_heal_eff)
//Sound
audio_play_sound(snd_mag_potato_heal01,10,false)
//Combo
potato_combo += 0.02
potato_attk *= potato_combo
potato_mag *= potato_combo
//Effect
//if ds_list_find_value(allies,obj_target_ally.s) = instance_find(obj_potato_combat,0)
if (hp_potato > 0)
{
    hp_potato += (4+potency);
	global.wep_durability = global.wep_durability_max;
}

//Amount Healed
instance_create(x+1,y,obj_heal_numbers);
obj_heal_numbers.heal = (4+potency);
//Skill Effects
if potato_pure_skill02 = 1
{
    if instance_exists(obj_potato_heal_haste)
    {
        with obj_potato_heal_haste{instance_destroy();}
    }
    instance_create(obj_potato_combat.x,obj_potato_combat.y,obj_potato_heal_haste);
}

