/// @description Text Control, Sounds, Portraits
if active = 1
{
    //Get Dialogue Amt.
    textLength = array_length_1d(strings);
    //Text Control
    if (keyboard_check_pressed(global.keyb_confirm) or gamepad_button_check_pressed(0, global.gpi_confirm))
    if (string_length(strings[text]) >= pos)
    if (!instance_exists(obj_trans_fade_in_out))
    {
        pos = 0;
        text = text+1;
        if (text >= textLength)
        {
            pos = 0;
            text = 0;
            active = 0;
        }
    }
    if (text < textLength)
    {
        //Sounds
        if (string_length(strings[text]) > pos)
        {
			switch(soundType){
			case "Default":
				if(!audio_is_playing(snd_text_speech))
					audio_play_sound(snd_text_speech, 10, false);
				break;
			case "Telephone":
				if(!audio_is_playing(snd_text_speechPhone))
					audio_play_sound(snd_text_speechPhone, 10, false);
				break;
			case "TelephoneHigh":
				if(!audio_is_playing(snd_text_speechPhoneHigh))
					audio_play_sound(snd_text_speechPhoneHigh, 10, false);
				break;
			case "Spirit":
				if(!audio_is_playing(snd_text_speechSpirit))
					audio_play_sound(snd_text_speechSpirit, 10, false);
				break;
			default:
				break;
			}
        }
        //Portraits
        if (portraits = true)
        {
            if string_char_at(strings[text], 1) = "M"
            {port = spr_potato_portrait_text;}
        }
    }
}

///Events

