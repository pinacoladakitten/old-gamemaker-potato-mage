/// @description Main event
if (active = 1 and text < textLength)
{
    if (windowDarken = true)
    {
        draw_set_alpha(0.5);
        draw_rectangle_colour(0,0,global.display_gui_width,global.display_gui_height,c_black,c_black,c_black,c_black,0);
    }
    //Setup
    draw_set_font(font0);
    draw_set_halign(fa_left);
    draw_set_color(c_white);
    draw_set_alpha(1);
    d3d_set_lighting(false);
    //Textbox
    var width = global.display_gui_width;
    var height = global.display_gui_height;
    //Portrait XY
    var x_posPort = (width*0.5)-600;
    var y_posPort = (height*0.8)+10;
    //Textbox XY
    var x_posBox = (width*0.5)-640;
    var y_posBox = height*0.8;
    //**Draw Textbox**
    if (textbox = true){
        draw_sprite(spr_textbox, 0, x_posBox, y_posBox);
    }
    //**Draw Portrait**
    if (portraits = true){
        draw_sprite(port , 0, x_posPort, y_posPort);
    }
    //**The Text**
    //Text XY
    var x_posText = (width*0.5)-430;
    var y_posText = (height*0.8)+10;
    //Colors
    var textShadow = make_colour_hsv(160, 0, 34);
    //Text Position
    cstr = string_copy(strings[text],1,pos);
    //Draw the Text
    draw_text_ext_transformed_colour(x_posText+3, y_posText+3, string_hash_to_newline(cstr), -1, -1, 1, 1, 0, textShadow, textShadow, textShadow, textShadow, 1); //Shadow
    draw_text_ext_transformed(x_posText, y_posText, string_hash_to_newline(cstr), -1, -1, 1, 1, 0); //Normal
    //Position Max
    pos = min(pos+2, string_length(strings[text]));
    //Confirm Check
    if (keyboard_check(global.keyb_cancel) or gamepad_button_check(0, global.gpi_cancel))
    {
        pos = min(pos+5, string_length(strings[text]));
    }
}

/* */
/*  */
