/// @description Time
if enm_pause = 0
{
    time = min(time+0.2,timemax)
}

if time = timemax
if instance_number(obj_eff_enm) = 0
{
    alarm[0] = 1
}

///Dead
if hp < 1
{
    with obj_eff_enm
    {
        instance_destroy();
    }
    if spr != spr_enm_floaty_d
    {
        spr = spr_enm_floaty_d
        image_index = 0
    }
    enm_pause = 1
    with obj_target
    {
        s = 0
    }
    if image_index = 0
    {
        audio_play_sound(snd_enm_dead01,10,false)
    }
    if image_index = 8
    {
        image_speed = 0
    }
    xs = max(xs-0.015,0.15)
    ys = max(ys-0.015,0.15)
    zs = max(zs-0.015,0.15)
    r += 7
    if xs = 0.15
    {
		enm_pause = 0;
        ds_list_delete(enemies,self)
        enm_numb -= 1
        exp_obtained += 50
		if (!instance_exists(obj_victory) && enm_numb = 0){instance_create(x,y,obj_victory);}
        instance_destroy();
    }
}

///Attack Commence
if spr = spr_enm_floaty_attk
{
    if image_index < 5
    {
        enm_pause = 1;
    }
    if image_index = 3
    {
        instance_create(obj_potato_combat.x+1,obj_potato_combat.y,obj_eff_enm)
        //Attack01
        if attk_numb = 1
        {
            audio_play_sound(snd_enm_slash01,10,false)
            with obj_eff_enm
            {
                spr = spr_slash_fire
            }
        attk_numb = 0
        }
        //Attack02
        if attk_numb = 2
        {
            audio_play_sound(snd_enm_slash03,10,false)
            alarm[0] = 1*room_speed
            instance_create(obj_potato_combat.x+1,obj_potato_combat.y,obj_eff_enm)
                with obj_eff_enm
                {
                    spr = spr_dslash01
                }
            attk_numb = 2.5
        }
        //Attack03
        if attk_numb = 3
        {
            audio_play_sound(snd_enm_slash03,10,false)
            instance_create(obj_potato_combat.x+1,obj_potato_combat.y,obj_eff_enm)
            with obj_eff_enm
            {
                spr = spr_dslash02
            }
         attk_numb = 0
        }
    }
    if attk_numb = 0
    if image_index = 5
    {
        spr = spr_enm_floaty_01;
        image_index = 0;
        enm_pause = 0;
    }
    if attk_numb = 2.5
    if image_index = 5
    {
        spr = spr_enm_floaty_01
        image_index = 0
    }
}

///Reaction to Player Attacks
scr_Player_Attacks_ALL();

