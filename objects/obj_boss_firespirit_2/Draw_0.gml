/// @description Draw
draw_set_alpha(alpha)
d3d_set_lighting(false)
var ss,cc,tex,tex02,tex03,tex04;
tex = sprite_get_texture(spr,-1);
tex02 = sprite_get_texture(spr_firesp2_pt2,-1);
tex03 = sprite_get_texture(spr_firesp2_pt3,-1);
tex04 = sprite_get_texture(spr_firesp2_pt4,-1);
tex01 = sprite_get_texture(spr_shadow,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
//pt1&2
d3d_transform_add_translation(x,y,0)
d3d_draw_wall(0+45*ss,0+45*cc,82,0-45*ss,0-45*cc,-8,tex,1,1)
d3d_draw_wall(xarm1+45*ss,xarm1+45*cc,82,0-45*ss,0-45*cc,-8,tex02,1,1)
d3d_transform_set_identity();
//pt3
d3d_transform_add_rotation_x(r)
d3d_transform_add_translation(x,y,30)
d3d_draw_wall(0-1,0+45*cc,45,0-1,0-45*cc,-45,tex03,1,1)
d3d_transform_set_identity();
//pt4
d3d_transform_add_rotation_x(-r)
d3d_transform_add_translation(x,y,30)
d3d_draw_wall(0-1,0+45*cc,45,0-1,0-45*cc,-45,tex04,1,1)
d3d_transform_set_identity();
//pt5
d3d_transform_add_scaling(2,2,2)
d3d_transform_add_rotation_x(r)
d3d_transform_add_translation(x,y,30)
d3d_draw_wall(0-1,0+45*cc,45,0-1,0-45*cc,-45,tex03,1,1)
d3d_transform_set_identity();
//pt6
d3d_transform_add_scaling(2,2,2)
d3d_transform_add_rotation_x(-r)
d3d_transform_add_translation(x,y,30)
d3d_draw_wall(0-1,0+45*cc,45,0-1,0-45*cc,-45,tex04,1,1)
d3d_transform_set_identity();

d3d_draw_wall(x+20*ss,y+20*cc,1,x-20*ss,y-20*cc,0,tex01,1,1)
draw_set_alpha(1)

