/// @description Attack Initial
if (obj_potato_combat.time < 90)
if !instance_exists(obj_eff_enm)
if enm_pause = 0
if attack_times = -1
{
    time = 0
    enm_pause = 1
    if summon = 0
    {
        attk_numb = irandom_range(1,attack_max)
    }
    if summon = 1{attk_numb = irandom_range(10,10)}
    instance_create(x+1,y,obj_eff_enm)
    if attk_numb = 1 or attk_numb = 2 or attk_numb = 4 or attk_numb = 5
    {
        obj_eff_enm.spr = spr_cast_eff01
    }
    if attk_numb = 3
    {
        obj_eff_enm.spr = spr_cast_eff
    }
    if attk_numb = 10
    {
        obj_eff_enm.spr = spr_cast_eff02
    }
    image_index = 0
    alarm[1] = 0.5*room_speed
}

