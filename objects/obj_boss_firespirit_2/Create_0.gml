/// @description Setup
image_speed = 0.05
draw_set_alpha_test(true)
draw_set_alpha_test_ref_value(40)
hpmax = 1850
hp = 1850
attack = 0
time = 0
timemax = 45
explode = 0
enm_pause = 1
attk_numb = 0
spr = spr_firesp2_pt1
enm_numb += 1
ds_list_add(enemies, id);
attack_times = -1
r = 0
z = 0
xarm1 = 0
xarm1spd = 0
xcycle1 = 0
alpha = 0
summon = 0
summon_active = 0
attack_max = 4

///Defensive Vars
//(0.1 = 10% dmg reduction, so on...)
physical_defense = 0.1;
fire_defense = 0.8;
nature_defense = -0.25;
magic_defense = 0;
lightning_defense = -0.1;
dark_defense = 0;
earth_defense = 0;

