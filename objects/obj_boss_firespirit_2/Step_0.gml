/// @description Main Step
if enm_pause = 0
if !instance_exists(obj_boss_firespirit_minion)
{
    time = min(time+0.17,timemax)


    if time = timemax
    if instance_number(obj_eff_enm) = 0
    {
        alarm[0] = 1
    }
}
///Target Pos
if instance_exists(obj_target)
{
    if obj_target.target = id
    {
        obj_target.z = 0
    }
}

///Attack Commence
if spr = spr_firesp2_pt1_attk
{
    //Attack 1
    if attk_numb = 1
    {
        image_speed = 0.1
        if image_index = 0
        if attack_times = -1
        {
            time = 0
            enm_pause = 1
            attack_times = irandom_range(4,6)
        }
        if image_index = 3
        {
            if attack_times = 5 or attack_times = 3 or attack_times = 1
            {
                instance_create(obj_potato_combat.x,obj_potato_combat.y-40,obj_eff_enm_firesp01)
            }
            if attack_times = 6 or attack_times = 4 or attack_times = 2
            {
                instance_create(obj_potato_combat.x-30,obj_potato_combat.y+30,obj_eff_enm_firesp01)
            }
            if instance_exists(obj_eff_enm_firesp01){obj_eff_enm_firesp01.zmax = 15}
            attack_times -= 1
        }
        if image_index >= 5
        if attack_times = 0
        {
            spr = spr_firesp2_pt1
            image_speed = 0.05
            enm_pause = 0
            attk_numb = 0
            attack_times = -1
            image_index = 0
        }
        else
        {
            image_index = 0
        }
    }
    //Attack 2
    if attk_numb = 2
    {
        image_speed = 0.1
        if image_index = 0
        if attack_times = -1
        {
            time = 0
            enm_pause = 1
            attack_times = irandom_range(3,4)
        }
        if image_index = 3
        {
            instance_create(x,y,obj_eff_enm_firesp02)
            obj_eff_enm_firesp02.yscmax = 10
            obj_eff_enm_firesp02.z = 20
            obj_eff_enm_firesp02.xsc = 10
            attack_times -= 1
        }
        if image_index >= 5
        if attack_times = 0
        {
            spr = spr_firesp2_pt1
            image_speed = 0.05
            enm_pause = 0
            attk_numb = 0
            attack_times = -1
            image_index = 0
        }
        else
        {
            image_index = 0
        }
    }
    //Attack 3
    if attk_numb = 3
    {
        image_speed = 0.1
        if image_index = 0
        if attack_times = -1
        {
            time = 0
            enm_pause = 1
            attack_times = irandom_range(1,1)
        }
        if image_index = 3
        {
            instance_create(obj_potato_combat.x,obj_potato_combat.y,obj_eff_enm_firesp03)
            attack_times -= 1
        }
        if image_index >= 5
        if attack_times = 0
        {
            spr = spr_firesp2_pt1
            image_speed = 0.05
            attack_times = -1
            enm_pause = 0
            attk_numb = 0
        }
        else
        {
            image_index = 0
        }
    }
    //Attack 4
    if attk_numb = 4
    {
        image_speed = 0.1
        if image_index = 0
        if attack_times = -1
        {
            time = 0
            enm_pause = 1
            attack_times = irandom_range(1,1)
        }
        if image_index = 3
        {
            instance_create(obj_potato_combat.x,obj_potato_combat.y-40,obj_eff_enm_firesp04)
            if instance_exists(obj_eff_enm_firesp04){obj_eff_enm_firesp04.zmax = 10}
            attack_times -= 1
        }
        if image_index >= 5
        if attack_times = 0
        {
            spr = spr_firesp2_pt1
            image_speed = 0.05
            enm_pause = 0
            attk_numb = 0
            attack_times = -1
            image_index = 0
        }
        else
        {
            image_index = 0
        }
    }
    //Attack 5
    if attk_numb = 5
    {
        image_speed = 0.05
        if image_index = 0
        if attack_times = -1
        {
            time = 0
            enm_pause = 1
            attack_times = irandom_range(1,3)
        }
        if image_index = 3
        {
            if attack_times = 1
            {
                instance_create(576,320,obj_eff_enm_firesp_fireball)
            }
            if attack_times = 2
            {
                instance_create(600,270,obj_eff_enm_firesp_fireball)
            }
            if attack_times = 3
            {
                instance_create(540,320,obj_eff_enm_firesp_fireball)
            }
            attack_times -= 1
        }
        if image_index >= 5
        if attack_times = 0
        {
            spr = spr_firesp2_pt1
            image_speed = 0.05
            enm_pause = 0
            attk_numb = 0
            attack_times = -1
            image_index = 0
        }
        else
        {
            image_index = 0
        }
    }
    //Attack 10 (Summon)
    if attk_numb = 10
    {
        image_speed = 0.1
        if image_index = 0
        if attack_times = -1
        {
            time = 0
            enm_pause = 0
            attack_times = irandom_range(1,1)
        }
        if image_index = 3
        {
            instance_create(x-2,y,obj_boss_firespirit_minion)
            instance_create(x-2,y,obj_boss_firespirit_minion)
            instance_create(x-2,y,obj_boss_firespirit_minion)
            instance_create(x,y,obj_eff_enm_firesp_shield)
            attack_times -= 1
        }
        if image_index >= 5
        if attack_times = 0
        {
            spr = spr_firesp2_pt1
            image_speed = 0.05
            attack_times = -1
            enm_pause = 0
            attk_numb = 0
            summon = 0
        }
        else
        {
            image_index = 0
        }
    }
}

///Reaction to Player Attacks
scr_Player_Attacks_ALL();
if instance_exists(obj_boss_firespirit_minion)
{
    physical_defense = 1;
    fire_defense = 1;
    nature_defense = 1;
    magic_defense = 1;
    lightning_defense = 1;
    dark_defense = 1;
    earth_defense = 1;
}
else
{
    physical_defense = 0.1;
    fire_defense = -0.15;
    nature_defense = -0.25;
    magic_defense = 0;
    lightning_defense = -0.1;
    dark_defense = 0;
    earth_defense = 0;
}

///Dead
if hp < 1
{
    with obj_eff_enm
    {
        instance_destroy();
    }
    if spr != spr_firesp2_pt1_d
    {
        spr = spr_firesp2_pt1_d
        image_index = 0
    }
    enm_pause = 1
    with obj_target
    {
        s = 0
    }
    if image_index = 0
    {
        audio_play_sound(snd_enm_dead01,10,false)
        image_speed = 0.1
    }
    if image_index = 5
    {
        ds_list_delete(enemies,self)
        enm_numb -= 1
        potato_exp += 2000
        if !instance_exists(obj_boss_firesp_dia03)
        {
            instance_create(0,0,obj_boss_firesp_dia03);
        }
    }
    if image_index = 6
    {
        image_speed = 0
        obj_potato_combat.pause_combat = 1
        enm_pause = 1
    }
}

///Misc
//Movement Anim
if hp > 1
{
    r += 1
    if r = 360
    {
        r = 0
    }
    xarm1 += xarm1spd
    if xcycle1 = 0
    {
        xarm1spd = min(xarm1spd+0.001,0.05)
    }
    if xcycle1 = 1
    {
        xarm1spd = max(xarm1spd-0.001,-0.05)
    }
    if xarm1 >= 5
    {
        xcycle1 = 1
    }
    if xarm1 <= -5
    {
        xcycle1 = 0
    }
}
//Shield
if !instance_exists(obj_boss_firespirit_minion)
{
    if instance_exists(obj_eff_enm_firesp_shield)
    {
        obj_eff_enm_firesp_shield.fade = 1
    }
}
//Summon Health
if hp <= hpmax*0.5
if summon_active = 0
    {
        summon = 1
        summon_active = 1
    }
if hp <= hpmax*0.25
if summon_active = 1
    {
        summon = 1
        summon_active = 2
    }
//Extra Attacks
if hp <= hpmax*0.45
if attack_max = 4
{
    attack_max = 5
    randomize();
}
//Speed Up
if hp < hpmax*0.5
{
    timemax = 35
}
//Target lock
if !instance_exists(obj_boss_firespirit_minion)
if instance_exists(obj_target)
{
    obj_target.x = x
    obj_target.y = y
}

