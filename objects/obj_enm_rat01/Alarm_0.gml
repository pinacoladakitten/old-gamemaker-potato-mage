/// @description Pre-Attack
//Attack
if (obj_potato_combat.time < 90)
if !instance_exists(obj_eff_enm)
if enm_pause = 0
if attk_numb = 0
{
    //Time Pause/Reset
    time = 0
    enm_pause = 1
    //Reset to first frame
    image_index = 0
    //Attack Type
    attk_numb = irandom_range(1,2);
    //Cast Effect
    instance_create(x+1,y,obj_eff_enm)
    //Effect Attack Type
    if attk_numb = 1{
        with obj_eff_enm
        {spr = spr_cast_eff}}
    if attk_numb = 2{
        with obj_eff_enm
        {spr = spr_cast_eff03}}
    //Attack Commence
    alarm[1] = 0.5*room_speed
}
else
{time -= irandom_range(10,20)}


