/// @description Time in Battle
if (enm_pause == 0 and attack_commence == 0)
{time = min(time+0.2,timemax);}

if (time == timemax and instance_number(obj_eff_enm) == 0)
{alarm[0] = 1;}

///Dead
if (hp < 1)
{
    //Stop Attacks
    with (obj_eff_enm)
    {instance_destroy();}
	
    //Set Sprite
    if (spr != spr_enm_rat01_d){
        spr = spr_enm_rat01_d;
        image_index = 0;
    }
	
    //Pause
    enm_pause = 1;
	
    //Reset Target
    with (obj_target){s = 0;}
	
    //Play sound on first frame
    if (image_index == 0){
        if (!audio_is_playing(snd_enm_dead01)){
            audio_play_sound(snd_enm_dead01,10,false);
        }
    }
	
    //Stop dead animation
    if (image_index == 8){image_speed = 0;}
    //Shrink and Rotate Enemy when dead
    xs = max(xs-0.015,0.15);
    ys = max(ys-0.015,0.15);
    zs = max(zs-0.015,0.15);
    r += 7;
	
    //Enemy Explode/Actually Dead
    if (xs == 0.15){
		enm_pause = 0;
        ds_list_delete(enemies,self);
        enm_numb -= 1;
        exp_obtained += 40;
	    if (!instance_exists(obj_victory) and enm_numb = 0){instance_create(x,y,obj_victory);}
		instance_destroy();
    }
}

///Attack Commence
//Attack Animation Type
if (attack_commence == 1){
    //Attack01
    if (attk_numb == 1){
        //Attack Sprite
        spr = spr_enm_rat01_attack01;
		
        //Pause
        if (image_index < 6){enm_pause = 1;}
		
        //Do Attack
        if (image_index == 2){
            //Create Attack
            instance_create(obj_potato_combat.x+1,obj_potato_combat.y,obj_eff_enm);
            with (obj_eff_enm)
            {spr = spr_slash_fire;}
            //Attack Sound
            audio_play_sound(snd_enm_slash01,10,false);
        }
		
        //Attack End
        if (image_index >= 6){
            spr = spr_enm_rat01;
            image_index = 0;
            enm_pause = 0;
            attack_commence = 0;
            attk_numb = 0;
        }
    }
	
    //Attack02
    if (attk_numb == 2){
        instance_create(x,y,obj_enm_rat_aoemark01);
        attk_numb = 0;
        alarm[2] = 4*room_speed;
    }
}

//Cancel Regardless
if (attack_commence == 0 and attk_numb == 0){
    if (spr == spr_enm_rat01_attack01 and image_index >= 6)
    {
        spr = spr_enm_rat01;
        image_index = 0;
        enm_pause = 0;
        attack_commence = 0;
        attk_numb = 0;
    }
}

///Reaction to Player Attacks
scr_Player_Attacks_ALL();

