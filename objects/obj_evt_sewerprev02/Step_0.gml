/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if string_length(strings[text]) <= pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        if (text = 26)
        {
            pos = 0;
            text = 0;
            active = 1.5;
            instance_create(0,0,obj_trans_fade_in_out);
        }
        if (text > maxstrings)
        {
            pos = 0;
            text = 0;
            active = 1.5;
            pause = 0
            alarm[2] = 1*room_speed;
        }
    }
    maxstrings = 27
}

///Events
if active = 1
{pause = 1}
//Portraits
if active = 1
{
    if string_char_at(strings[text], 1) = "M"
    {port = spr_potato_portrait_text}
    if string_char_at(strings[text], 1) = "K"
    {port = spr_kaas_portrait}
    port_alpha = min(port_alpha+0.05,1);
    if text = 4
    {kass_port = spr_kaas_port_s;}
    if text = 6
    {potato_port = spr_potato_port_b;
    potato_xsc = -1;}
    if text = 7
    {kass_port = spr_kaas_port_default;}
    if text = 9
    {potato_port = spr_potato_port_q;}
    if text = 12
    {kass_port = spr_kaas_port_h;}
    if text = 15
    {kass_port = spr_kaas_port_default;}
    if text = 16
    {potato_port = spr_potato_port_b;}
    if text = 19
    {kass_port = spr_kaas_port_h;}
    if text = 20
    {kass_port = spr_kaas_port_default;}
    if text = 22
    {kass_port = spr_kaas_port_h;}
}
if instance_exists(obj_trans_fade_in_out)
{
    obj_trans_fade_in_out.fadespd = 0.005;
    if obj_trans_fade_in_out.alpha >= 1
    {
        obj_potato_overw.x = 1488;
        obj_potato_overw.y = 496;
        obj_potato_overw.z = 0;
        obj_potato_overw.sprite_index = spr_potato_ow_idle_b;
        pause = 0;
        obj_camera_ow.camz = obj_potato_overw.z;
        alarm[1] = 4*room_speed;
    }
    else
    {pause = 1;}
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

