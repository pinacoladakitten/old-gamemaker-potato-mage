/// @description Start up
pos = 1
text = 0
active = 0
port = spr_port
vol = 0
potato_port = spr_potato_port_f;
kass_port = spr_kaas_port_default;
potato_xsc = 1;
kass_xsc = -1;
port_alpha = 0;
pause = 1;
if place_meeting(x,y,obj_potato_overw)
{alarm[0] = 15*room_speed;}

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Mythor:
Woah, what is this place?
"
strings[1] = @"Kassadin:
These are the sewers to the city Myth, don't get too excited.
"
strings[2] = @"Mythor:
Yeah, but I never would've expected the sewers of Barbossa to be on a scale like this!
It looks like there's quite a lot down here don't you think?
"
strings[3] = @"Kassadin:
Uh-huh, riight...anyway, for your next test-...Myth, are you listening?
"
strings[4] = @"Mythor:
Um what? Sorry, I'm just lost from how they were able to fit all of this under Barbossa.
Like how does any of this even work? I know it's a big city and all, and-
"
strings[5] = @"Kassadin:
Myth...
"
strings[6] = @"Mythor:
Sorry Kass.
"
strings[7] = @"Kassadin:
Anyway, as I was saying, for your next test you are to traverse through these sewers
to get something I need you to bring back to me. It's very precious and one of a kind.
"
strings[8] = @"Kassadin:
You can't get it anywhere else in the world, this I'm sure about. For some reason my
guy selling it is only located within these sewers. Say, about 100 Traps should do it.
"
strings[9] = @"Mythor:
Well what is it exactly? Or can you at least tell me what it looks like so I know what to
buy?
"
strings[10] = @"Kassadin:
Hot sauce.
"
strings[11] = @"Mythor:
Eh...what-really? Are you be serious?
"
strings[12] = @"Kassadin:
I'm DEAD serious Myth! This hot sauce isn't anything ordinary I assure you. The taste,
the scent, it just blends sooo well with so many great foods!
"
strings[13] = @"Kassadin:
Plus I seem to have MUCH better luck when Tanking raids, Myth you have to understand,
I can't live without this stuff! So you've got a great task ahead of you.
"
strings[14] = @"Mythor:
*Yeah because playing that MMO game is so important*
"
strings[15] = @"Kassadin:
What did you say?!
"
strings[16] = @"Mythor:
Uh-um...
"
strings[17] = @"Kassadin:
Myyyth! Ugh, nevermind, I know you didn't mean anything.
"
strings[18] = @"Mythor:
Sorry Kass, I-...yeah.
"
strings[19] = @"Kassadin:
Hmmph, one day you'll understand, if only I had a second computer for you too. Then we
can both play together, oh how fun that'll be! 
"
strings[20] = @"Kassadin:
...Sorry, almost got lost in the future there...*sigh*...anyway back to what's going on
now. So are you ready to proceed with this test? Of course you are! Why wouldn't you be!
"
strings[21] = @"Mythor:
Uh-um, okay Kass.
"
strings[22] = @"Kassadin:
Great! See you when you get back! Be extra extra careful Myth! I'm only doing this
because I trust that you can take care of yourself by now. This should be a walk in
the park for ya.
"
strings[23] = @"Kassadin:
Also, if you happen to come across any of the uh...dwellers of this place, tell'em
I sent you. Trust me, I know'em personally, we all go waayy back together and I've
sort of grown a strong affinity with them. So, yeah, kay bye Myth!
"
strings[24] = @"Mythor:
S-See ya, I-I'll be back soon Kass.
"
strings[25] = @"Kassadin:
...Make sure to come home before sunrise. That's the time limit alright? Good, go 
along now, you'll be fine.
"
strings[26] = @"Mythor:
Welp, guess it's time to find that hot sauce. (Really? Hot Sauce?)
"
strings[27] = @"Mythor:
(I don't understand, now where's that one merchant at?)
"

