/// @description Vars and Stuff (IDK WHAT THIS IS)
/// @param IDK WHAT THIS IS
hit = 0
z = 0
grav = 0.25
vsp = 0
effect = 0
on_hit = 0
move_towards_point(obj_target.x,obj_target.y,5)
image_speed = 0.5

///Stats
proj_attk = potato_attk*0.15

//Gloom Skill 1
if potato_gloom_skill01 = 1
{
    proj_attk += potato_attk*0.05
    if hp_potato = hp_potato_max
    {
        proj_attk += potato_attk*0.025
    }
}
//Gloom Skill 3
if potato_gloom_skill03 = 1
{
    if hp_potato = hp_potato_max
    {
        proj_attk += potato_attk*0.05
    }
    //Critical
    var critical = irandom_range(1,20)
    if critical = 1
    {
        proj_attk *= 1.25
        instance_create(x,y,obj_encourage_words);
        obj_encourage_words.spr = spr_critical;
    }
}

///Sprites
spr = spr_potato_proj

