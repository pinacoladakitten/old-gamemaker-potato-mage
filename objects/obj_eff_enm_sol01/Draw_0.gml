/// @description Draw
var ss,cc,tex,tex01;
tex = sprite_get_texture(spr,-1);
tex01 = sprite_get_texture(spr_firesp_eff01,-1);
tex03 = sprite_get_texture(spr_sol_fireint01,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);

d3d_draw_wall(x+10*ss,y+5*cc,10,x+10*ss,y-5*cc,0,tex,1,1)

d3d_set_lighting(false);
draw_set_alpha(alpha);
//1
d3d_transform_add_rotation_z(r)
d3d_transform_add_translation(x,y,zed)
d3d_draw_cylinder(0+(xsc),0+(ysc),z,0-(xsc),0-(ysc),0,tex03,1,1,0,10)
d3d_transform_set_identity();
//2
d3d_transform_add_rotation_z(r+25)
d3d_transform_add_translation(x,y,zed)
d3d_draw_cylinder(0+(xsc)*1.4,0+(ysc)*1.4,z*0.6,0-(xsc)*1.4,0-(ysc)*1.4,0,tex03,1,1,0,24)
d3d_transform_set_identity();
//3
d3d_transform_add_rotation_z(r+50)
d3d_transform_add_translation(x,y,zed)
d3d_draw_cylinder(0+(xsc)*1.6,0+(ysc)*1.6,z*0.3,0-(xsc)*1.6,0-(ysc)*1.6,0,tex03,1,1,0,24)
d3d_transform_set_identity();
//4
d3d_transform_add_rotation_z(r+75)
d3d_transform_add_translation(x,y,zed)
d3d_draw_cylinder(0+(xsc)*1.8,0+(ysc)*1.8,z*0.15,0-(xsc)*1.8,0-(ysc)*1.8,0,tex03,1,1,0,24)
d3d_transform_set_identity();
//5
d3d_transform_add_rotation_z(r+75)
d3d_transform_add_translation(x,y,zed)
d3d_draw_cylinder(0+(xsc)*1.85,0+(ysc)*1.85,5,0-(xsc)*1.85,0-(ysc)*1.85,0,tex03,2,1,0,24)
d3d_transform_set_identity();
//6
d3d_transform_add_rotation_z(r+75)
d3d_transform_add_translation(x,y,zed)
d3d_draw_cylinder(0+(xsc)*1.9,0+(ysc)*1.9,z,0-(xsc)*1.9,0-(ysc)*1.9,0,tex01,4,1,0,24)
d3d_transform_set_identity();
draw_set_alpha(1)

