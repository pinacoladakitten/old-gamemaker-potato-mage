/// @description Hitbox
if place_meeting(x,y,obj_potato_combat)
if hit = 0
{
    hp_potato -= 10
    obj_potato_combat.hit = 1
    hit = 1
}

///Movement
r += 10
z = min(z+1,zmax);
xsc = min(xsc+0.25,7.5);
ysc = min(ysc+0.25,7.5);
enm_pause = 1
if fade = 1
{
    alpha -= 0.01
    if alpha <= 0
    {
        enm_pause = 0
        if hit = 0
        {
            if instance_exists(obj_evt_tutorial_sol01)
            {obj_evt_tutorial_sol01.alarm[1] = 1*room_speed}
        }
        instance_destroy();
    }
}

d3d_set_lighting(true);
d3d_light_define_point(3,x,y,0,200,c_yellow)
d3d_light_enable(3,true);
d3d_set_lighting(false);

