/// @description Debug
if instance_exists(obj_global_overw)
if help_toggle = 1
{
    if obj_global_overw.draw_coords = 1
    {
        draw_text(0,0,string_hash_to_newline("xx: " + string(xx) + "yy: " + string(yy)));
        yy -= keyboard_check(vk_up)*0.5;
        yy += keyboard_check(vk_down)*0.5;
        xx += keyboard_check(vk_right)*0.5;
        xx -= keyboard_check(vk_left)*0.5;
    }
}

///Draw Help Controls
d3d_set_lighting(false);
if room = rm_start{ //Start Screen
    if controller = 0{
        draw_set_font(font0);
        draw_sprite(spr_ord_z,0,1120+xx,640+yy);
        draw_text(1140+xx,625+yy,string_hash_to_newline(" :Continue"));
    }
    if controller = 1{
        draw_set_font(font0);
        draw_sprite(spr_a,0,1120+xx,640+yy);
        draw_text(1140+xx,625+yy,string_hash_to_newline(" :Continue"));
    }
}
if (room = rm_title or room = rm_BELFRY_title) //Title Screen
if instance_exists(obj_title)
{
    if obj_title.main_menu = 1
    if help_toggle = 1
    {
        alpha = min(alpha+0.1,1);
        draw_set_alpha(alpha);
        //Draw Back
        draw_background_ext(bg_fade_black,0,980,20,1,0,-1,alpha);
        if controller = 0
        {
            draw_set_halign(fa_left);
            draw_set_font(font5);
            //Cursor Move
            draw_sprite(spr_arrows01,0,350+xx,700+yy);
            draw_sprite(spr_arrows01,1,374+xx,700+yy);
            draw_sprite(spr_arrows01,2,398+xx,700+yy);
            draw_sprite(spr_arrows01,3,420+xx,700+yy);
            draw_text(435+xx,690+yy,string_hash_to_newline(":Move Cursor"));
            //Confirm
            draw_sprite(spr_ord_z01,0,800+xx,700+yy);
            draw_text(820+xx,690+yy,string_hash_to_newline(":Okay"));
            //Toggle Help
            draw_sprite(spr_bkspace01,0,100+xx,700+yy);
            draw_text(120+xx,690+yy,string_hash_to_newline(" :Toggle Help"));
            //Cancel
            draw_sprite(spr_ord_x01,0,980+xx,700+yy);
            draw_text(1000+xx,690+yy,string_hash_to_newline(":Back Out"));
            draw_set_font(font0);
        }
        if controller = 1
        {
            draw_set_halign(fa_left);
            draw_set_font(font5);
            //Cursor Move
            draw_sprite(spr_dpad,0,350+xx,700+yy);
            draw_text(380+xx,690+yy,string_hash_to_newline(":Move Cursor"));
            //Confirm
            draw_sprite(spr_a01,0,800+xx,700+yy);
            draw_text(820+xx,690+yy,string_hash_to_newline(":Okay"));
            //Toggle Help
            draw_sprite(spr_select,0,100+xx,700+yy);
            draw_text(120+xx,690+yy,string_hash_to_newline(":Toggle Help"));
            //Cancel
            draw_sprite(spr_b01,0,980+xx,700+yy);
            draw_text(1000+xx,690+yy,string_hash_to_newline(":Back Out"));
            draw_set_font(font0);
        }
        draw_set_alpha(1);
    }
    else
    {alpha = max(alpha-0.1,0);}
}
//Overworld
if instance_exists(obj_inventory_ow)
if instance_exists(obj_potato_overw)
if pause = 0
{
    if help_toggle = 1
    {alpha = min(alpha+0.1,1);}
    else
    {alpha = max(alpha-0.1,0);}
    draw_set_alpha(alpha);
    if obj_inventory_ow.active = 0 //Outside Inventory
    {
        //Draw Back
        draw_background_ext(bg_fade_black,0,980,20,1,0,-1,alpha);
        if controller = 0
        {
            draw_set_halign(fa_left);
            draw_set_font(font5);
            //Movement
            draw_sprite(spr_arrows01,0,350+xx,700+yy);
            draw_sprite(spr_arrows01,1,374+xx,700+yy);
            draw_sprite(spr_arrows01,2,398+xx,700+yy);
            draw_sprite(spr_arrows01,3,420+xx,700+yy);
            draw_text(435+xx,690+yy,string_hash_to_newline(":Move"));
            //Confirm
            draw_sprite(spr_ord_z01,0,800+xx,700+yy);
            draw_text(820+xx,690+yy,string_hash_to_newline(":Interact/Okay"));
            //Jump
            draw_sprite(spr_space01,0,600+xx,700+yy);
            draw_text(640+xx,690+yy,string_hash_to_newline(":Jump"));
            //Toggle Help
            draw_sprite(spr_bkspace01,0,100+xx,700+yy);
            draw_text(120+xx,690+yy,string_hash_to_newline(" :Toggle Help"));
            //Cancel/Sprint
            draw_sprite(spr_ord_x01,0,980+xx,700+yy);
            draw_text(1000+xx,690+yy,string_hash_to_newline(":Sprint"));
            draw_set_font(font0);
        }
        if controller = 1
        {
            draw_set_halign(fa_left);
            draw_set_font(font5);
            //Movement
            draw_sprite(spr_lstick,0,350+xx,700+yy);
            draw_text(380+xx,690+yy,string_hash_to_newline(":Move"));
            //Confirm
            draw_sprite(spr_a01,0,800+xx,700+yy);
            draw_text(820+xx,690+yy,string_hash_to_newline(":Interact/Okay"));
            //Jump
            draw_sprite(spr_y01,0,600+xx,700+yy);
            draw_text(620+xx,690+yy,string_hash_to_newline(":Jump"));
            //Toggle Help
            draw_sprite(spr_select,0,100+xx,700+yy);
            draw_text(120+xx,690+yy,string_hash_to_newline(":Toggle Help"));
            //Cancel/Sprint
            draw_sprite(spr_b01,0,980+xx,700+yy);
            draw_text(1000+xx,690+yy,string_hash_to_newline(":Sprint"));
            draw_set_font(font0);
        }
    }
    draw_set_alpha(1);
}
//Combat
if instance_exists(obj_potato_combat)
{
    if help_toggle = 1
    {alpha = min(alpha+0.1,1);}
    else
    {alpha = max(alpha-0.1,0);}
    draw_set_alpha(alpha);
    //Draw Back
    draw_background_ext(bg_fade_black,0,980,20,1,0,-1,alpha);
    if controller = 0
    {
        draw_set_halign(fa_left);
        draw_set_font(font5);
        //Movement
        draw_sprite(spr_ord_wasd01,0,350+xx,700+yy);
        draw_sprite(spr_ord_wasd01,1,374+xx,700+yy);
        draw_sprite(spr_ord_wasd01,2,398+xx,700+yy);
        draw_sprite(spr_ord_wasd01,3,420+xx,700+yy);
        draw_text(435+xx,690+yy,string_hash_to_newline(":Move"));
        //Menu Select
        draw_sprite(spr_arrows01,0,550+xx,700+yy);
        draw_sprite(spr_arrows01,1,574+xx,700+yy);
        draw_sprite(spr_arrows01,2,598+xx,700+yy);
        draw_sprite(spr_arrows01,3,620+xx,700+yy);
        draw_text(635+xx,690+yy,string_hash_to_newline(":Menu Selection"));
        //Confirm
        draw_sprite(spr_ord_z01,0,800+xx,700+yy);
        draw_text(820+xx,690+yy,string_hash_to_newline(":Okay"));
        //Skill Action
        draw_sprite(spr_ord_c01,0,900+xx,700+yy);
        draw_text(920+xx,690+yy,string_hash_to_newline(":Char. Action"));
        //Toggle Help
        draw_sprite(spr_bkspace01,0,100+xx,700+yy);
        draw_text(120+xx,690+yy,string_hash_to_newline(" :Toggle Help"));
        draw_set_font(font0);
    }
    if controller = 1
    {
        draw_set_halign(fa_left);
        draw_set_font(font5);
        //Movement
        draw_sprite(spr_lstick,0,300+xx,700+yy);
        draw_text(330+xx,690+yy,string_hash_to_newline(":Move"));
        //Menu Select
        draw_sprite(spr_dpad,0,450+xx,700+yy);
        draw_text(480+xx,690+yy,string_hash_to_newline(":Menu Selection"));
        //Confirm
        draw_sprite(spr_a01,0,700+xx,700+yy);
        draw_text(720+xx,690+yy,string_hash_to_newline(":Okay"));
        //Skill Action
        draw_sprite(spr_rb01,0,850+xx,700+yy);
        draw_text(870+xx,690+yy,string_hash_to_newline(":Char. Action"));
        //Toggle Help
        draw_sprite(spr_select,0,100+xx,700+yy);
        draw_text(120+xx,690+yy,string_hash_to_newline(":Toggle Help"));
        draw_set_font(font0);
    }
    draw_set_alpha(1);
}

