/// @description Black Fire Column 1
spr = spr_bign_eff_fire01
image_speed = 0.25
image_index = 0
audio_play_sound(snd_bign_fire01,10,false);
xsc = 6
ysc = 6
z = 0
zmax = 5
r = 0
hit = 0
hit2 = 0
aoe = 0
fade = 0
choice = 0
dir = 0
attack_times = irandom_range(2,3);
//Create Effect
alarm[1] = 0.1*room_speed
alarm[0] = 1*room_speed
if attack_times = 2
{alarm[3] = 8*room_speed;}
if attack_times = 3
{alarm[3] = 10*room_speed;}

