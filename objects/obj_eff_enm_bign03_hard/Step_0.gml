/// @description Movement
r += 10
if fade = 0
{
    z = min(z+0.5,100);
    xsc = min(xsc+0.5,20);
    ysc = min(ysc+0.5,20);
}
if fade = 1
{
    xsc = max(xsc-0.5,0);
    ysc = max(ysc-0.5,0);
    if xsc = 0
    {
        instance_destroy();
    }
}

///Lighting Effect
d3d_set_lighting(true);
d3d_light_define_point(3,x,y,0,2000,c_red)
d3d_light_enable(3,true);
d3d_set_lighting(false);

///Misc

