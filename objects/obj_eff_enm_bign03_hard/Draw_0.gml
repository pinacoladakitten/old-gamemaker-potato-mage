/// @description Draw
d3d_set_lighting(false);
var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
var tex03;
tex03 = sprite_get_texture(spr,-1);
//Model
draw_set_alpha(0.5);
d3d_transform_add_rotation_z(r);
d3d_transform_add_translation(x,y,0);
d3d_draw_cylinder(0+(xsc),0+(ysc),z,0-(xsc),0-(ysc),0,tex03,1,1,0,10)
d3d_draw_cylinder(0+(xsc)*1.4,0+(ysc)*1.4,z*0.75,0-(xsc)*1.4,0-(ysc)*1.4,0,tex03,1,1,0,10)
d3d_draw_cylinder(0+(xsc)*1.6,0+(ysc)*1.6,z*0.5,0-(xsc)*1.6,0-(ysc)*1.6,0,tex03,1,1,0,10)
d3d_draw_cylinder(0+(xsc)*1.8,0+(ysc)*1.8,z*0.05,0-(xsc)*1.8,0-(ysc)*1.8,0,tex03,1,1,0,10)
d3d_transform_set_identity();
//Model 2
d3d_draw_wall(x+10*ss,y+5*cc,10,x+10*ss,y-5*cc,0,tex,1,1)
draw_set_alpha(1);

