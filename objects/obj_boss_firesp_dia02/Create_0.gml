/// @description Start up
pos = 1
text = 0
active = 0
draw_scene = 0
port = spr_port
obj_potato_combat.alpha = 0
alarm[0] = 3*room_speed

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
You've severely pushed me passed my limit...
"
strings[1] = @"???:
...now all that remains left in this miserable reality...
"
strings[2] = @"???:
...is you...
"
strings[3] = @"???:
...and...
"
strings[4] = @"???:
...ME!...
"
strings[5] = @"???:
...now please...drown in misery...just for me...hehe...haha-
"
strings[6] = @"???:
HHHAAAAAHHAHAHHAAAAHHAAHHAAAAA!!!
"
strings[7] = @"Mythor:
WHAT'S GOING ON!!!!???
"
strings[8] = @"????:
That spirit's too far gone, it's unstable, causing an unnatural shift 
in that area!-
"
strings[9] = @"????:
This world...it's falling apart from this spirit's destructive energy,
sheesh this guy really wants to kill you, what did you do to em'?
"
strings[10] = @"Mythor:
I didn't do anything!! He just holds some kind of grudge against me for
some reason!! I literally tried being nice to the guy before all of this 
started and-
"
strings[11] = @"????:
-Ok ok I get it fine! Now you seem to have fallen into quite the predicament, 
especially since you seem like fresh meat around here! So if you want to
live you need to get out of there as soon as possible!
"
strings[12] = @"????:
That spirit's energy is ripping the fabrics that hold this world from its
raw power alone, at this point it's desperate to keep living in denial of its 
fate.
"
strings[13] = @"Mythor:
Wait wait I'm completely baffled right now...just who are you exactly? Why are 
you helping me? Most of all, what the heck is even going on!?
"
strings[14] = @"????:
Are you seriously asking me that right now!? You're stuck in limbo with a spirit
that could potentially kill you and erase any being around there from existence!
Also, I literally just told you what IS going on!
"
strings[15] = @"????:
Look Mythor-...I know you're curious about me and all of this, but I need you to suck
it up and trust me! Now all you need to do is weaken him to a state which will paralyze
him for a short time.
"
strings[16] = @"????:
As soon as that happens, book it! There's a door straight ahead that will allow you 
to escape safely.
"
strings[17] = @"????:
Mythor...I believe in you, you can do this!
"
strings[18] = @"Mythor:
...A-alright, here goes...everything.
"

