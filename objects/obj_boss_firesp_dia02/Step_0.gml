/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if string_length(strings[text]) < pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        if (text > maxstrings)
        {
            pos = 0
            text = 0
            active = 0
            enm_pause = 0
            obj_potato_combat.pause_combat = 0
            obj_skybox.fade = 1
            audio_play_sound(snd_boss_firesp2_pt1,10,false)
            audio_sound_gain(snd_boss_firesp2_pt1,0.65,0)
            obj_boss_firespirit2_music.music_active = 1
            instance_destroy();
        }
    }
    maxstrings = 18
}

///Events
if active = 1
{
    enm_pause = 1
    obj_potato_combat.pause_combat = 1
}
if keyboard_check_pressed(global.keyb_confirm)
or gamepad_button_check_pressed(0, global.gpi_confirm)
{
    if text = 2
    {
        obj_spotlight.alph = 0.6
        audio_play_sound(snd_lamp01,10,false)
        obj_potato_combat.alpha = 1
    }
    if text = 4
    {
        obj_spotlight.alph2 = 0.6
        audio_play_sound(snd_lamp01,10,false)
        obj_boss_firespirit_2.alpha = 1
    }
}
if text = 0 or text = 1 or text = 2 or text = 3
{
    port = spr_firespirit_portrait2_blk
}
if text = 4 or text = 5 or text = 6
{
    port = spr_firespirit_portrait2
}
if text = 7 or text = 10 or text = 13 or text = 18
{
    port = spr_potato_portrait_text
}
if text = 8 or text = 9 or text = 11 or text = 12 or text = 14 or text = 15
or text = 16 or text = 17
{
    port = spr_port
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

