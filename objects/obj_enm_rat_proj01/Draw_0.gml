/// @description Draw
d3d_set_lighting(false);
var ss,cc,tex;
tex = sprite_get_texture(spr,-1);
ss = sin(obj_camera.direction*pi/180);
cc = cos(obj_camera.direction*pi/180);
var tex02;
tex02 = sprite_get_texture(spr01,-1);
var tex03;
tex03 = sprite_get_texture(spr02,-1);
//Model
draw_set_alpha(alpha);
d3d_transform_add_rotation_z(r);
d3d_transform_add_translation(x,y,0);
d3d_draw_cylinder(0+(xsc),0+(ysc),z,0-(xsc),0-(ysc),0,tex,2,1,0,10)
d3d_draw_cylinder(0+(xsc)*1.4,0+(ysc)*1.4,z*0.75,0-(xsc)*1.4,0-(ysc)*1.4,0,tex02,1,4,0,10)
d3d_draw_cylinder(0+(xsc)*1.6,0+(ysc)*1.6,z*0.5,0-(xsc)*1.6,0-(ysc)*1.6,0,tex03,1,6,0,10)
d3d_draw_cylinder(0+(xsc)*1.8,0+(ysc)*1.8,z*0.1,0-(xsc)*1.8,0-(ysc)*1.8,0,tex02,1,2,0,10)
d3d_transform_set_identity();
draw_set_alpha(1);

