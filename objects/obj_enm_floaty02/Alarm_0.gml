if (obj_potato_combat.time < 90)
if !instance_exists(obj_eff_enm)
if enm_pause = 0
{
    time = 0
    enm_pause = 1
    if attk_numb = 2.5
    {
        instance_create(x+1,y,obj_eff_enm)
        with obj_eff_enm
        {
            spr = spr_cast_eff
        }
        image_index = 0
        alarm[1] = 1*room_speed
        attk_numb = 3
    }
    else
    {
        instance_create(x+1,y,obj_eff_enm)
        with obj_eff_enm
        {
            spr = spr_cast_eff
        }
        image_index = 0
        attk_numb = irandom_range(1,1)
        alarm[1] = 0.5*room_speed
    }
}
else
{
time -= irandom_range(20,40)
}

