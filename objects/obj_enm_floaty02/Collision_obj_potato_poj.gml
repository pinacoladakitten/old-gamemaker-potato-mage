/// @description Potato Attk
if obj_potato_poj.hit = 0
{
    hp -= obj_potato_poj.proj_attk-5
    instance_create(x+1,y,obj_dmg_numbers)
    obj_dmg_numbers.dmg = obj_potato_poj.proj_attk-5
    
    if potato_attk < 15
    {
        instance_create(x+1,y,obj_eff)
    }
    if potato_attk > 15
    {
        instance_create(x+1,y,obj_eff)
        instance_create(x+1,y,obj_eff)
        instance_create(x+1,y,obj_eff)
    }
    with obj_eff
    {
        spr = spr_eff_hit01
    }
    with other
    {
        hit = 1
        vsp += 1
    }
    with obj_dmg_numbers
    {
        potato = 1
    }
    if hp < 1
    {
        image_index = 0
    }
}

