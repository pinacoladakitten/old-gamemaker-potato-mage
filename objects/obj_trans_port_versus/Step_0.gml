if room = rm_battle_trans
{
    active = 1
}
else
{
    active = 0
}
if active = 1
{
    if port = 1
    {
        alpha = min(alpha+0.01,1)
        x += hsp
        text01 += 3
        text02 -= 5
        text03 += 5
        if alarm[0] = -1
        if alarm[1] = -1
        {
            alarm[0] = 0.5*room_speed
        }
    }
    if port = 2
    {
        alpha2 = min(alpha2+0.01,1)
        x -= hsp
        text01 += 3
        text02 -= 5
        text03 += 5
    }
    if port = 3
    {
        hhsp += hsp
        alpha3 = max(alpha3-0.01,0)
        size = max(size-0.2,1)
        rot = max(rot-15,0)
    }
}

///Destroy
if instance_exists(obj_trans_fade_in)
{
    if obj_trans_fade_in.alpha = 1
    if destroy = 1
    {
        instance_destroy();
    }
}

