if active = 1
{
    if port = 1
    {
        draw_sprite_ext(spr_potato_port_versus,0,x,y+75,1,1,0,-1,alpha)
        draw_set_halign(fa_center)
        draw_set_font(font2)
        draw_text(text01,100,string_hash_to_newline("The Potato Mage?"))
        draw_text(text02,500,string_hash_to_newline("Seriously terrified right now."))
        draw_text(text03,150,string_hash_to_newline("\""+"What did I do to get involved!?"+"\""))
        draw_set_font(font0)
        draw_set_halign(fa_left)
    }
    if port = 2
    {
        draw_sprite_ext(boss_enm,0,x,y+50,1,1,0,-1,alpha2)
        draw_set_halign(fa_center)
        draw_set_font(font2)
        draw_text(text01,100,string_hash_to_newline("Sol, the fire mage."))
        draw_text(text02,500,string_hash_to_newline("Helping a friend..."))
        draw_text(text03,150,string_hash_to_newline("\""+"Sorry about this dude."+"\""))
        draw_set_font(font0)
        draw_set_halign(fa_left)
    }
    if port < 3
    {
        draw_set_alpha(0.8)
        draw_rectangle_colour(0,500,1280,720,c_black,c_black,c_black,c_black,0)
        draw_rectangle_colour(0,200,1280,-100,c_black,c_black,c_black,c_black,0)
        draw_set_alpha(1)
    }
    if port = 3
    {
        draw_sprite_ext(spr_potato_port_versus,0,350-hhsp,y,1,1,0,-1,1)
        draw_sprite_ext(boss_enm,0,930+hhsp,y,1,1,0,-1,1)
        draw_sprite_ext(spr_versus,-1,640,500,size,size,rot,-1,1)
        draw_set_alpha(alpha3)
        draw_rectangle_colour(0,0,1280,720,c_white,c_white,c_white,c_white,0)
        draw_set_alpha(1)
    }
}

