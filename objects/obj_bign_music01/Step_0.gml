/// @description Activate Part 1
if !instance_exists(obj_bign_battle_end)
if !instance_exists(obj_gameover)
{
    if active = 1
    {
        combat_music = snd_bign_battle01
        if !audio_is_playing(combat_music)
        {
            audio_play_sound(combat_music,10,false);
            active = 2
        }
    }
    ///Activate Part 2
    if active = 2
    if !audio_is_playing(combat_music)
    {
        combat_music = snd_bign_battle02
        audio_play_sound(combat_music,10,true);
        active = 3
    }
}
if instance_exists(obj_bign_battle_end)
{
    instance_destroy();
}

