/// @description Movement
r += 0.5
if fade = 0
{
    alpha = min(alpha+0.1,1);
}
if fade = 1
{
    alpha = max(alpha-0.1,0);
    if alpha = 0
    {
        instance_destroy();
    }
}

///Misc
d3d_set_lighting(true);
d3d_light_define_point(3,x,y,0,200,c_red)
d3d_light_enable(3,true);
d3d_set_lighting(false);

