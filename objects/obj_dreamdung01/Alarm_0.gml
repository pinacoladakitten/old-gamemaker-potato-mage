if modelload <=20{obj_loading.image_index += 0.5}
if modelload >=20{obj_loading.image_speed = 0.05}
if modelload = 0
{
    d3d_model_load(model0, "maps/mp_dreamdung01/dream01_bg_bamboo01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 1
{
    d3d_model_load(model1, "maps/mp_dreamdung01/dream01_bg_bamboo02.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 2
{
    d3d_model_load(model2, "maps/mp_dreamdung01/dream01_bg_castle_brick02.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 3
{
    d3d_model_load(model3, "maps/mp_dreamdung01/dream01_bg_castle_brick03.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 4
{
    d3d_model_load(model4, "maps/mp_dreamdung01/dream01_bg_castle_brick04.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 5
{
    d3d_model_load(model5, "maps/mp_dreamdung01/dream01_bg_castle_brick05.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 6
{
    d3d_model_load(model6, "maps/mp_dreamdung01/dream01_bg_castle01_brick.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 7
{
    d3d_model_load(model7, "maps/mp_dreamdung01/dream01_bg_castle01_brick2.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 8
{
    d3d_model_load(model8, "maps/mp_dreamdung01/dream01_bg_castle01_brick3.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 9
{
    d3d_model_load(model9, "maps/mp_dreamdung01/dream01_bg_castle01_brick4.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 10
{
    d3d_model_load(model10, "maps/mp_dreamdung01/dream01_bg_castle01_brick42.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 11
{
    d3d_model_load(model11, "maps/mp_dreamdung01/dream01_bg_castle01_light.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 12
{
    d3d_model_load(model12, "maps/mp_dreamdung01/dream01_bg_castle01_light02.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 13
{
    d3d_model_load(model13, "maps/mp_dreamdung01/dream01_bg_castle01_nolight.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 14
{
    d3d_model_load(model14, "maps/mp_dreamdung01/dream01_bg_castle01_nolight2.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 15
{
    d3d_model_load(model15, "maps/mp_dreamdung01/dream01_bg_castle01_outline.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 16
{
    d3d_model_load(model16, "maps/mp_dreamdung01/dream01_bg_castle01_roof.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 17
{
    d3d_model_load(model17, "maps/mp_dreamdung01/dream01_bg_leaves01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 18
{
    d3d_model_load(model18, "maps/mp_dreamdung01/dream01_brown_wood01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 19
{
    d3d_model_load(model19, "maps/mp_dreamdung01/dream01_floor_grass01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 20
{
    d3d_model_load(model20, "maps/mp_dreamdung01/dream01_gray_wood01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 21
{
    d3d_model_load(model21, "maps/mp_dreamdung01/dream01_house_wood.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 22
{
    d3d_model_load(model22, "maps/mp_dreamdung01/dream01_mountain01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 23
{
    d3d_model_load(model23, "maps/mp_dreamdung01/dream01_red_wood01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 24
{
    d3d_model_load(model24, "maps/mp_dreamdung01/dream01_rock01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 25
{
    d3d_model_load(model25, "maps/mp_dreamdung01/dream01_rock02.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 26
{
    d3d_model_load(model26, "maps/mp_dreamdung01/dream01_roof_tiles01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 27
{
    d3d_model_load(model27, "maps/mp_dreamdung01/dream01_stone_tile_east01.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 28
{
    d3d_model_load(model28, "maps/mp_dreamdung01/dream01_white_cloth.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 29
{
    d3d_model_load(model29, "maps/mp_dreamdung01/dream01_white_cloth02.d3d")
    modelload += 1
    alarm[0] = 1
    exit;
}
if modelload = 30
{
    d3d_model_load(model30, "maps/mp_dreamdung01/dream01_window01.d3d")
    audio_play_sound(overw_music,10,true);
    obj_loading.draw = 0
}

