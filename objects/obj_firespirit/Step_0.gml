/// @description Angles
angle+=2
if angle = 360
{
    angle = 0
}
if (obj_potato_overw.y < y)
if distance_to_object(obj_potato_overw) < 60
{
    angle01 = min(angle01+10,180)
}
if (obj_potato_overw.y > y)
if distance_to_object(obj_potato_overw) < 60
{
    angle01 = max(angle01-10,0)
}
if distance_to_object(obj_potato_overw) > 60
{
    angle01 = max(angle01-10,0)
}

///Text Control
if active = 1
{
    if keyboard_check_pressed(ord("Z"))
    if string_length(strings[text]) < pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        //Zoom Out
        if text = 2
        {
            obj_dream01_firesp_evt01.camevt01 = 0
            with obj_camera_ow
            {
                zoom = 100
                camzto = 20
                camzfrom = 34
                camz = 0
                camxto = obj_potato_overw.x
                camyto = obj_potato_overw.y
                x = obj_potato_overw.x+zoom
                y = obj_potato_overw.y
            }
        }
        if text = 4
        {
            obj_dream01_firesp_evt01.camevt01 = 2
        }
        if text = 7
        {
            fade = 1
            active = 0
        }
        //Potato Notice
        if text = 8
        {
            obj_potato_overw.sprite_index = spr_potato_ow_idle_look
            obj_potato_overw.image_index = 1
            instance_create(obj_potato_overw.x,obj_potato_overw.y,obj_emotes);
        }
        if text = 10
        {
            audio_sound_gain(overw_music,0,10*room_speed);
        }
        //Start Tremmors
        if text = 11
        {
            obj_camera_ow.cam_shake = 1
            audio_play_sound(snd_tremmor01,10,true);
            audio_sound_gain(snd_tremmor01,1,0);
        }
        //Battle Trigger
        if text = 14
        {
            obj_dream01_firesp_evt01.camevt01 = 4
            instance_create(x,y,obj_firespirit_eff02);
            audio_stop_sound(overw_music);
            audio_play_sound(snd_boss_firesp_prev,10,true);
            audio_sound_gain(snd_boss_firesp_prev,0.5,0);
            combat_music = snd_boss_firesp_prev
        }
        if text = 26
        {
            audio_sound_gain(snd_tremmor01,0,10*room_speed);
        }
        //Battle Start
        if text = 27
        {
            pos = 0
            text = 0
            active = 0
            pause = 0
            //Spawn Type
            with(obj_spawn)
            {
                spawn1 = irandom_range(2,2)
                spawn2 = irandom_range(2,2)
                spawn3 = irandom_range(2,2)
                spawn4 = irandom_range(2,2)
                spawn5 = irandom_range(2,2)
                
                enemy[0] = obj_enm_floaty01
                if spawn2 = 1 {enemy[1] = obj_enm_floaty01}
                if spawn3 = 1 {enemy[2] = obj_enm_floaty01}
                if spawn4 = 1 {enemy[3] = obj_enm_floaty01}
                if spawn5 = 1 {enemy[4] = obj_enm_floaty01}
            }
            alarm[0] = 10
        }
        //Aftermath of battle
        if text = 28
        {
            audio_sound_gain(snd_boss_firesp_prev,0,25*room_speed);
            audio_play_sound(snd_fireexp02,10,false);
            obj_firespirit_eff02.shrink = 1
            alarm[2] = 2*room_speed
        }
        if text = 30
        {
            obj_dream01_firesp_evt01.camevt01 = 5
        }
        //Pickup Potato
        if text = 32
        {
            pos = 0
            text = 0
            active = 0
            instance_create(obj_potato_overw.x,obj_potato_overw.y,obj_firespirit_eff03)
            with obj_camera_ow
            {
                zoom = min(zoom+(200-zoom)*0.05,130)
            }
        }
        if text = 37
        {
            pos = 0
            active = 0
            alarm[3] = 2*room_speed
        }
        //After dropping Potato
        if (text > 39)
        {
            pos = 0
            text = 0
            active = 0
            pause = 0
            obj_potato_overw.sprite_index = spr_potato_ow_idle_f
            mp_potato = mp_potato_max
            hp_potato = hp_potato_max
            with obj_dream01_firesp_evt01
            {
                instance_destroy();
            }
        }
    }
    maxstrings = 39
}

///Events
if active = 1
{
    pause = 1
}
if text = 7
{
    if fade = 1
    {
        alpha = max(alpha-0.025,0)
        if alpha <= 0
        {
            fade = 0
            obj_dream01_firesp_evt01.camevt01 = 3
        }
    }
    if fade = 0
    {
        x = obj_potato_overw.x
        y = obj_potato_overw.y+55
        z = 0
        alpha = min(alpha+0.01,1)
    }
    if alpha = 1
    {
        active = 1
    }
}
//Portraits
if string_char_at(strings[text], 1) = "M"
{
    port = spr_potato_portrait_text
}
else
{
    port = spr_firespirit_portrait
}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false)
}

