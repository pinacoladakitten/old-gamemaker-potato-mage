d3d_set_lighting(false);
var ss,cc,tex;
tex = sprite_get_texture(spr_firesp_idle_ow,-1);
tex03 = sprite_get_texture(spr_talk,-1);
tex01 = sprite_get_texture(spr_ow_shadow,-1);
ss = sin(obj_camera_ow.direction*pi/180);
cc = cos(obj_camera_ow.direction*pi/180);

draw_set_alpha(alpha)
d3d_transform_add_rotation_z(angle01)
d3d_transform_add_scaling(2,2,2)
d3d_transform_add_translation(x,y,0)
d3d_draw_wall(0+15*ss,0+15*cc,32+z,0-15*ss,0-15*cc,2+z,tex,1,1)
d3d_draw_wall(0+20*ss,0+20*cc,3,0-20*ss,0-20*cc,0,tex01,1,1)
d3d_transform_set_identity();
draw_set_alpha(1)

if place_meeting(x,y,obj_potato_overw)
{
    d3d_set_lighting(false);
    d3d_transform_add_rotation_z(angle)
    d3d_transform_add_translation(x,y,0)
    d3d_draw_wall(0,0+3,34,0,0-3,28,tex03,1,1)
    d3d_transform_set_identity();
}


