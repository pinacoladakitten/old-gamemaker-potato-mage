image_speed = 0.1
angle = 0
angle01 = 0
z = 45
alpha = 0
fade = 0

///Start up
pos = 1
text = 0
active = 0
draw_scene = 0

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"???:
*YAAAAAAAAAAAAAAAAAAWWWWWWWWWWWWWWWN*
"
strings[1] = @"???:
Who was dumb enough to step onto MY bridge at this time!
"
strings[2] = @"Mythor:
Uh-uh-um... I'm only looking to cross this bridge, see all I look for 
is a way out of this place. 
"
strings[3] = @"Mythor:
Sorry if I've disturbed you or anything, but this bridge is the only
way for me to go.
"
strings[4] = @"???:
What!? This is who I get!? Such a puny spirit-...
"
strings[5] = @"???:
Say, you have the 'Key Plate' right?
"
strings[6] = @"Mythor:
Yeah I have it, I've made a promise to someone, to use this key in order
to find my way out of here.
"
strings[7] = @"???:
You what!? How could some-...*sigh*
"
strings[8] = @"???:
Ok ok, see THAT key is what many strive to attain and...I just wouldn't expect
someone like you to...bear such an artifact.
"
strings[9] = @"Mythor:
I know! I've been through much trouble ever since I picked this thing up.
I can't get rid of it though, I've made my promise as I said before.
"
strings[10] = @"???:
Well...uh-ehum...alright...um...
"
strings[11] = @"???:
-NO MATTER, IF YOU WISH TO BE THE BEARER OF THE KEY PLATE THEN 
SO BE IT!
"
strings[12] = @"???:
BUT THIS IS STILL MY BRIDGE, AND BY MY RULES YOU ARE TRESPASSING,
YOUNG SPIRIT I'LL HAVE YOU KNOW THAT IS ILLEGAL!
"
strings[13] = @"Mythor:
Are you sure there's no other way for me to cross? In a peaceful manner-
"
strings[14] = @"???:
DO NOT SPEAK WHILE I'M SPEAKING!!!!
"
strings[15] = @"???:
THIS IS MY BRIDGE, THEREFORE YOU MAY ONLY SPEAK WHEN I SAY YOU CAN!
"
strings[16] = @"Mythor:
S-sorry, but can I just-
"
strings[17] = @"???:
ALRIGHT SINCE YOU'RE SO 'EAGER' TO GET INTO TROUBLE, WE WILL SETTLE 
THIS RIGHT NOW!
"
strings[18] = @"???:
TRIAL BY COMBAT!
"
strings[19] = @"Mythor:
Trial by WHAT!-
"
strings[20] = @"???:
YES, YOU SEEM TO HAVE CAUGHT YOURSELF IN SOME BIIIG TROUBLE! 
HOWEVER! BY MY UN-OUTDATED RULES YOU MAY STILL PLEAD INNOCENT 
IF YOU WERE TO DEFEAT ME IN COMBAT!
"
strings[21] = @"???:
DOES THAT SUIT YOUR FANCY? WHO AM I KIDDING OF COURSE IT DOES!
"
strings[22] = @"Mythor:
I-I don't want to fight you! All I need to do is cross this bridge!-
"
strings[23] = @"???:
SHUUUUUSHHH!!! WE ALL NEED TO DO THINGS, TOO BAD YOURS INVOLVES 
A SERIOUS CRIME!
"
strings[24] = @"Mythor:
But it's not a crime!-
"
strings[25] = @"???:
OH MY GOODNESS! YOU KNOW WHAT? YOU'RE FIGHTING NO MATTER WHAT 
NOW! FIRST A TRESPASSING VIOLATION, NOW A SPEECH VIOLATION!
"
strings[26] = @"???:
NOW COME HITHER AND GIVE IT YOUR ALL!!!
"
strings[27] = @"???:
OK THAT'S ENOUGH! THIS BATTLE HAS BEEN TEMPORARILY CALLED OFF!
"
strings[28] = @"???:
Now get out!...
"
strings[29] = @"???:
I need some time to myself...
"
strings[30] = @"Mythor:
I'm sorry about this really, but you're letting me cross right?
"
strings[31] = @"???:
OH MY GOODNESS GET OUT!!!
"
strings[32] = @"???:
DON'T COMEBACK UNTIL I SAY SO, YOU HEAR ME!?
"
strings[33] = @"???:
I NEED TO MEDITATE TO CLEAR MY MIND OF THE DISAPPOINTMENT THAT
LINGERS AROUND ME!
"
strings[34] = @"Mythor:
...welp...
"
strings[35] = @"Mythor:
I crossed the bridge, finally. All that just to cross a bridge.
"
strings[36] = @"Mythor:
Hmm...I wonder what else I'll run into here. Perhaps who's also
going to cause even MORE trouble.
"
strings[37] = @"Mythor:
.....
"
strings[38] = @"Mythor:
Oh great, it's suddenly raining! Didn't expect this.
"
strings[39] = @"Mythor:
I guess the only thing to do is press onward...*sigh*
"


