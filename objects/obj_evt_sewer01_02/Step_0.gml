/// @description Text Control
if active = 1
{
    if keyboard_check_pressed(global.keyb_confirm)
    or gamepad_button_check_pressed(0, global.gpi_confirm)
    if string_length(strings[text]) <= pos
    if !instance_exists(obj_trans_fade_in_out)
    {
        pos = 0
        text = text+1
        if (text = 1)
        {active = 0;}
        if (text = 8)
        {active = 0;}
        if (text = 10)
        {
            //Audio Change & SFX
            audio_stop_sound(overw_music);
            audio_play_sound(snd_darkness01,10,false);
            combat_music = snd_battle_evt01;
            combat_music_pt2 = snd_battle_evt01;
            audio_play_sound(combat_music,10,true);
            audio_sound_gain(combat_music,0.65*global.music_volume,0);
            //With Rat
            with obj_ow_enm_rat_evt_sewer01
            {
                instance_create(x-20,y,obj_darkness_swirl01);
                dupe = 1;
            }
        alarm[2] = 4*room_speed;
        active = 0;}
        if (text = 16)
        {
            pos = 0;
            active = 0;
            pause = 1;
            evt = 1;
            alarm[1] = 1;
            alarm[2] = 7*room_speed;
            obj_darkness_swirl01.fade = 1;
        }
        if (text = 20)
        {
            overw_music = snd_sorrow01;
            audio_play_sound(overw_music,10,true);
            audio_sound_gain(overw_music,0.55*global.music_volume,1*room_speed);
        }
        if (text = 21)
        {
            with obj_camera_ow
            {camyfrom = camyto+30;}
        }
        if (text = 39)
        {audio_sound_gain(overw_music,0,30*room_speed);}
        if (text > maxstrings)
        {
            audio_stop_sound(overw_music);
            instance_create(0,0,obj_trans_fade_in);
            obj_trans_fade_in.roomgoto = rm_overw_sewdung01;
            instance_create(0,0,obj_evt_sewer01_03);
            room_persistent = false;
            scr_DestroyUsedObjs();
            instance_destroy();
        }
    }
    maxstrings = 45;
}

///Events
if active = 1
or evt = 1
{pause = 1}
//Portraits
if active = 1
if (text < maxstrings)
{
    if string_char_at(strings[text], 1) = "M"
    {port = spr_potato_portrait_text;}
    if string_char_at(strings[text], 1) = "S"
    {port = spr_port;}
    if string_char_at(strings[text], 1) = "P"
    {port = spr_port;}
    if string_char_at(strings[text], 1) = "M"
    {port = spr_port;}
    if string_char_at(strings[text], 1) = "F"
    {port = spr_port;}
}
//Events for reals
//Event 1
if text = 1
{
    //Turn Down Music
    audio_sound_gain(overw_music,0,30*room_speed);
    //Move Camera to Position
    accel01 = min(accel01+0.02,2);
    with obj_camera_ow
    {
        if x > 250
        {x -= obj_evt_sewer01_02.accel01;}
        camxto = 430;
        if x <= camxto
        {obj_potato_overw.sprite_index = spr_potato_ow_idle_look;
        obj_potato_overw.y = 100;}
    }
    //Look to Position
    if obj_camera_ow.x <= 250
    {
        text += 1;
        active = 1;
    }
}
//Event 2
if text = 8
{
    //Move Camera to Position
    accel02 = min(accel02+0.05,2);
    with obj_camera_ow
    {
        if x < 500
        {x += obj_evt_sewer01_02.accel02;}
        camxto = 430;
    }
    //Move Potato then Look
    with obj_potato_overw
    {
        evt = 1;
        if x > 382
        {
            x += vsp;
            vsp = -1.5;
            if sprite_index != spr_potato_ow_f
            {sprite_index = spr_potato_ow_f;
            image_speed = 0.1;}
        }
        if x < 382
        {
            vsp = 0;
            evt = 0;
            sprite_index = spr_potato_ow_idle_look;
            image_index = 1;
        }
    }
    if obj_potato_overw.x < 382
    {active = 1;}
}
//Event 2
if text = 16
{
    pause = 1;
    with obj_trans_fade_out
    {music = 3;}
}
//Event 3
if text >= 21
{
    //Move Camera to Position
    with obj_camera_ow
    {
        x = 608;
        camz = 0;
        camzto = 0;
        camzfrom = 65;
        camxto = 368;
        camyto = 112;
        y = max(y-(y-32)*0.0025,32);
    }
}
//Event 4
if text = 42
{obj_potato_overw.sprite_index = spr_potato_ow_idle_r;}

///Sounds
if active = 1
if (text <= maxstrings)
if pos < string_length(strings[text])
if !audio_is_playing(snd_text_speech)
{
    audio_play_sound(snd_text_speech,10,false);
}

