/// @description Start up
pos = 1
text = 0
active = 0
port = spr_port
vol = 0
potato_port = spr_potato_port_f;
kass_port = spr_kaas_port_default;
potato_xsc = 1;
kass_xsc = -1;
port_alpha = 0;
pause = 1;
//Camera
accel01 = 0;
accel02 = 0;
//Event Pause
evt = 0;

///Arrays
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue
strings[0] = @"Sewer Dweller:
Please Jace! Don't you remember me!?
"
strings[1] = @"Sewer Dweller:
Your best friend Marine! Please! I need you! You're not yourself! Th-The others...I-I don't
know what's going on. You were going to protect me from them, b-but now-...
"
strings[2] = @"Possessed Dweller, Jace:
UGH!...I have...guh!...no...friends-ugh!...my soul...my vessel belongs...to Hades...
"
strings[3] = @"Sewer Dweller:
What are you even saying!? Jace!? What's wrong with you!!?
"
strings[4] = @"Mythor:
What the!? (Should I act now? No, now isn't the right time, or is it? Dang it I don't
know!)...(Why did Kass send me on this mission again, oh yeah, for his dumb hotsauce.)
...(Crud I can't just stand here, I'm doing this, dang it Kass!). Hey! Get away from 
him! Something's obviously not right at all with'em!
"
strings[5] = @"Sewer Dweller, Marine:
But he's my best friend! I could never abandon him! He was supposed to protect me from the
rest of them! I can't leave him behind-never! Please Jace! Come back to me!
"
strings[6] = @"Possessed Dweller, Jace:
...s-stare into the abyss...become a servant of Him...you will no longer...feel so useless
...and alone...he will always be with you...
"
strings[7] = @"Mythor:
(Now that's a huge red flag, I've got no choice but to actually step in!) 
Alright that's it! I can't watch anymore of this! Hey you! Get away from her!!
"
strings[8] = @"Possessed Dweller, Jace:
UGGGHHHH! THERE SHALL BE NO INTURRUPTIONS!
"
strings[9] = @"Sewer Dweller, Marine:
Jace! W-What are you!?
"
strings[10] = @"Possessed Dweller, Jace:
THIS WORLD IS FILLED WITH NARCISSISTIC FOOLS WHO DON'T KNOW THEIR 
PLACE!  THE LACK OF EMPATHY TODAY HAS DRAINED THE HOPE I'D ONCE HAD!!
"
strings[11] = @"Possessed Dweller, Jace:
DON'T YOU SEE MARINE!? HADES WILL PROVIDE A WORLD WHO CARES FOR US! 
WE WON'T HAVE TO LIVE IN THE SEWERS ANY LONGER! WITH THIS POWER WE 
CAN PROSPER OVER THOSE SICK RICH BASTARDS ON THE SURFACE! WE CAN 
FINALLY LIVE!!
"
strings[12] = @"Sewer Dweller, Marine:
*Gasp*...you're...you're lost Jace. This isn't the way to go.
"
strings[13] = @"Possessed Dweller, Jace:
Ugh!...Then-..Then die-...Ugh!...Those who betray Him aren't worthy to walk this world
any longer.
"
strings[14] = @"Sewer Dweller, Marine:
Betray? W-what!?
"
strings[15] = @"Mythor:
Here he comes! I'll take care of this!
"
strings[16] = @"Sewer Dweller, Marine:
Jace!? JACE!? Where are you!? No! No! No!-
"
strings[17] = @"Freed Dweller, Jace:
It's ok Marine, I'm still here, well-...
"
strings[18] = @"Sewer Dweller, Marine:
What do you mean still here!? I can't see you!! Please tell me you're alright! I don't
know-
"
strings[19] = @"Freed Dweller, Jace:
Marine, I-...I'm fine ok?
"
strings[20] = @"Sewer Dweller, Marine:
No you're not! You're not here with us! Please! Please tell me you're coming back! We
were in this together!
"
strings[21] = @"Freed Dweller, Jace:
*Sigh*, Marine, I just told you how I feel. Here, I'll tell you this, if you can do me
a solid favor, one favor is all I ask of you, I'll be back before you know it.
"
strings[22] = @"Sewer Dweller, Marine:
*Sob* *Sniff*...yes Jace?
"
strings[23] = @"Freed Dweller, Jace:
I want you to grow and overcome what troubles you, to grow and become the figure YOU
yourself looked up to. I know this is all sudden, and I know that you've relied on me
in the past.
"
strings[24] = @"Freed Dweller, Jace:
But...please! The one thing I want to see more than anything, is to see you succeed in
what my anger, frustration, and worry kept me from doing. To simply move on from hate
and become a figure those will look up upon.
"
strings[22] = @"Sewer Dweller, Marine:
I...I don't know what to do Jace...I-I can't do this alone.
"
strings[23] = @"Freed Dweller, Jace:
I know you feel this way and I get it, but if you simply believe in yourself, 
if you commit to your self-improvement, things will come your way. Sooner or 
later you'll come across more people who will walk into your life.
"
strings[24] = @"Freed Dweller, Jace:
Then the world becomes a little less lonely right? Still, I'll always be there whenever
you need me, if anything, remember this favor I ask of you anytime when you're in doubt.
"
strings[25] = @"Freed Dweller, Jace:
Don't take this as a pressuring matter, haha! I just believe in you, and I'm so confident
about it! 
"
strings[26] = @"Sewer Dweller, Marine:
.....ok *sniff*.
"
strings[27] = @"Freed Dweller, Jace:
Kid? Sorry for what I caused back there, I know things got a bit rough, but anyway
you have my thanks for freeing me from the coils of His curse.
"
strings[28] = @"Mythor:
Jace I-I'm confused, what exactly is going on here?
"
strings[29] = @"Freed Dweller, Jace:
Oh right, you've must've walked straight into all of this unknowingly and I understand.
In the end, I'm glad you've come, that's all that matters to me at this point.
"
strings[30] = @"Freed Dweller, Jace:
I have one final request for you, please hear my word. My friends, family, everyone,
they have all been clutched by the claws of Hades's curse.
"
strings[31] = @"Mythor:
Hades? 
"
strings[32] = @"Freed Dweller, Jace:
I don't know who He is exactly, but all I know is that His power stems from negative
auras; doubt, pain, hatred, he uses those to manipulate his subjects. I don't know why
he targeted us, he just came out of nowhere.
"
strings[33] = @"Freed Dweller, Jace:
Kid? Erm...what's your name?
"
strings[31] = @"Mythor:
Mythor...
"
strings[32] = @"Freed Dweller, Jace:
Mythor, please, please save us all, I want you to save anyone you can from His curse. 
I don't know what happens to us or where we go after being freed, but I beg of you, it's 
better than being in His clutches for all eternity.
"
strings[33] = @"Mythor:
I-I'll t-try...
"
strings[34] = @"Freed Dweller, Jace:
Hey, don't doubt yourself Mythor, come on, you were able to free me, have you forgotten
already? I know this is such a major task given to you so suddenly, but YOU can do this,
I don't need to bring up any lectures about it since it's obvious by this point.
"
strings[35] = @"Mythor:
Thank you Jace, I-I won't let you down, I promise!
"
strings[36] = @"Freed Dweller, Jace:
I trust you Mythor, deeply. Again thank you so much for everything! I'd definitely be
one who looks up to someone like you and take advice. Thanks again.
"
strings[37] = @"Mythor:
Thank you...*sigh*
"
strings[38] = @"Freed Dweller, Jace:
Well, it looks like I haven't got much time left, I'll watch over you both I promise,
Marine, Mythor, take care.
"
strings[39] = @"Sewer Dweller, Marine:
JACE!!
"
strings[40] = @"...:
......
"
strings[41] = @"Sewer Dweller, Marine:
He's...he's gone, I didn't even say goodbye to him. *sniff*
"
strings[42] = @"Mythor:
Marine, remember isn't he always with you? There's no need for that right?
"
strings[43] = @"Sewer Dweller, Marine:
Y-yeah...*sniff*
"
strings[44] = @"Mythor:
Let's say we head back, there's someone else who's survived waiting for me to come back,
not sure if you know him or not.
"
strings[45] = @"Sewer Dweller, Marine:
Okay...let's go back. *sniff*
"

