/// @description Main event
if active = 1
{
    draw_set_alpha(0.5);
    draw_rectangle_colour(0,0,window_get_width(),window_get_height(),c_black,c_black,c_black,c_black,0);
    //Setup
    draw_set_font(font0);
    draw_set_halign(fa_left);
    draw_set_color(c_white);
    draw_set_alpha(1);
    //Textbox
    draw_sprite(spr_textbox,0,0,550);
    draw_sprite(port,0,40,560);
    //The Text
    cstr = string_copy(strings[text],1,pos);
    draw_text_ext_transformed(250,560,string_hash_to_newline(cstr),-1,-1,1,1,0);
    pos = min(pos+2,string_length(strings[text])+1);
    if keyboard_check(global.keyb_cancel)
    or gamepad_button_check(0, global.gpi_cancel)
    {pos = min(pos+5,string_length(strings[text])+1);}
}

