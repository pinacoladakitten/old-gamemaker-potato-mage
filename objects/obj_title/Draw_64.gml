/// @description Draw
draw_set_alpha(0.25);
draw_rectangle_colour(0, 0, global.display_width, global.display_height, c_black, c_black, c_black, c_black, 0);
draw_set_alpha(1);
draw_set_alpha(alpha);
//Vars
var text_x = global.display_gui_width;
var text_y = global.display_gui_height;
//Title
draw_sprite(spr_BELFRY_title, 0, x, y-350);
if (main_menu = 0 or main_menu = 0.5) //Press Enter
{draw_sprite(spr_enter, -1, x, y+100)}
if (main_menu = 1) //Main Menu
{
    //Options
    draw_set_alpha(alpha01);
    draw_sprite(spr_continue,-1, text_x*0.50+xx, text_y*0.8+yy);
    draw_sprite(spr_newgame,-1, text_x*0.50+xx, text_y*0.90+yy);
    //Selected Pointer
    if i = 0
    {draw_sprite(spr_inventory_select,-1, text_x*0.55+xx, text_y*0.8+yy);}
    if i = 1
    {draw_sprite(spr_inventory_select,-1, text_x*0.55+xx, text_y*0.90+yy);}
}
draw_set_alpha(1);

//Press Start Anim
if(main_menu > 0){
    if (rectX < global.display_gui_width)
    {
        rectX = min(rectX+75, global.display_gui_width);
        rectY = min(rectY+1, 25);
        draw_rectangle(0, y, y+rectX, y+rectY, false);
    }
    else
    {   
        rectX = global.display_gui_width;
        var rectYspeedAccel = 3;
        rectYspeed += rectYspeedAccel;
        rectY += rectYspeed;
        //Clamp vars to limit
        rectYspeed = clamp(rectYspeed, -2000, 2000);
        rectY = clamp(rectY, -2000, 2000);
        //draw rectangles
        draw_rectangle(0, y+rectY, rectX, y+25+rectY, false);
        draw_rectangle(0, y-rectY, rectX, y+25-rectY, false);
        if (rectY <= text_y*0.8){
            //Set Menu
            main_menu = 1;
            //Fade Eff for Menu
            alpha01 = min(alpha01+0.1, 1);
        }
    }
}

///Debug
if instance_exists(obj_global_overw)
{
    if obj_global_overw.draw_coords = 1
    {
        draw_text(0,0,string_hash_to_newline("xx: " + string(xx) + "yy: " + string(yy)));
        yy -= keyboard_check(vk_up)*0.5;
        yy += keyboard_check(vk_down)*0.5;
        xx += keyboard_check(vk_right)*0.5;
        xx -= keyboard_check(vk_left)*0.5;
    }
}

