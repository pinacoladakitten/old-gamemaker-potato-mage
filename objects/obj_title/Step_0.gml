/// @description Keyboard Select and Controller
if (alpha = 1)
{
    if main_menu = 1
    and !instance_exists(obj_trans_fade_in_next)
    {
        //Move Selection
        if keyboard_check_pressed(global.keyb_up)
        or gamepad_button_check_pressed(0, global.gpi_up)
        {
            i = max(i-1,0)
        }
        if keyboard_check_pressed(global.keyb_down)
        or gamepad_button_check_pressed(0, global.gpi_down)
        {
            i = min(i+1,1)
        }
        //Select Stuff
        if keyboard_check_pressed(global.keyb_confirm)
        or gamepad_button_check_pressed(0, global.gpi_confirm)
        or keyboard_check_pressed(vk_enter)
        {
            //New Game
            if i = 1
            {
                if !instance_exists(obj_trans_fade_in_next)
                {
                    instance_create(0,0,obj_trans_fade_in_next)
                    with obj_inventory_ow
                    {
                        ds_list_add(inventorymagic, 2);
                        maxmagic += 1
                        ds_list_add(inventorymagic, 1);
                        maxmagic += 1
                        ds_list_add(inventorymagic, "tremmor");
                        maxmagic += 1
                        ds_list_add(inventorymagic, "parasite");
                        maxmagic += 1
                        ds_list_add(inventorymagic, "deathspike");
                        maxmagic += 1
                    }
                    audio_sound_gain(snd_BELFRY_title, 0, 10*room_speed);
                }
            }
            //Load Game
            if i = 0
            {
                if !instance_exists(obj_save_menu){instance_create(0,0,obj_save_menu)}
                instance_destroy();
            }
        }
    }
    //Press Start
    if keyboard_check_pressed(vk_anykey) and main_menu = 0{
        audio_play_sound(snd_title_start,10,false);
        main_menu = 0.5;
    }
}

///Controller Press Start
//Controller Any Button
var anyPressed = false;
for (var c=gp_face1; c<gp_axisrv; c++){
    //Any Button
    if (gamepad_button_check_pressed(0, c))
    and !instance_exists(obj_trans_fade_in_next)
    and (alpha = 1)
    {
        //Press Start
        if !instance_exists(obj_trans_fade_in_next)
        and (main_menu = 0)
        {
            audio_play_sound(snd_title_start,10,false);;
            main_menu = 1;
        }
    }
}

///Misc
//Alpha fade
if (fade >= fadeMax){
    alpha += 0.01;
    alpha = clamp(alpha, 0, 1);
}
else{
    fade += 1;
}
//Press Start Anim


