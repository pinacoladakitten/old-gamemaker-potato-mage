tex = background_get_texture(sky);
draw_set_alpha(alpha);
d3d_set_lighting(false)
//Fog
if room = rm_overw_home01
if room = rm_overw_town01
{
    d3d_set_fog(false, __background_get_colour( ), 50, 800)
}
//Skybox Type
if room = rm_battle_dream3D
{
    d3d_draw_ellipsoid(0,0,250,-850,850,-200,tex,4,2,24)
}
if room = rm_overw_home01
{
    d3d_draw_ellipsoid(-3000,-3000,100,3000,3000,-250,tex,12,1,24)
}
if room = rm_overw_town01
{
    d3d_draw_ellipsoid(-30000,-30000,9000,30000,30000,-9000,tex,12,3,24)
}
if room = rm_overw_dream3D or room = rm_test_stage or room = rm_boss_big_n01
or sky_autobox = 1
{
    d3d_draw_ellipsoid(-30000,-30000,9000,30000,30000,-9000,tex,12,3,24)
}
if room = rm_boss_firesp_dream3D
{
    d3d_transform_add_scaling(obj_floor.xs,obj_floor.ys,obj_floor.zs)
    d3d_transform_add_translation(obj_camera.x,obj_camera.y,0)
    d3d_draw_ellipsoid(0-1000,0-950,500,0+1000,0+1050,-450,tex,8,4,24)
    d3d_transform_set_identity();
}
if room = rm_boss_firesp2_dream3D
{
    d3d_transform_add_rotation_z(90)
    d3d_transform_add_translation(obj_camera.x,obj_camera.y,0)
    d3d_draw_ellipsoid(0-1000,0-950,500,0+1000,0+1050,-450,tex,8,4,24)
    d3d_transform_set_identity();
}
if room = rm_boss_sol
{
    d3d_draw_ellipsoid(obj_camera.x-1000,obj_camera.y-1000,500,obj_camera.x+1000,obj_camera.y+1000,-450,tex,8,4,24)
}
d3d_set_lighting(true)
draw_set_alpha(1);

