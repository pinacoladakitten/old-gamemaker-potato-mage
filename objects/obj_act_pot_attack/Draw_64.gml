/// @description Draw Sprite
//Attack Type Sprites
d3d_set_fog(false, -1, 0, 10000);
switch attack_type{
    case 0:
        sprite_index = spr_wep_slot;
        break;
    case 1:
        sprite_index = spr_inv_dagger;
        break;
    case 2:
        sprite_index = spr_mag_psolanum;
        break;
    case 3:
        sprite_index = spr_mag_pfire;
        break;
    case 4:
        sprite_index = spr_mag_pasterid;
        break;
    case 5:
        sprite_index = spr_mag_tremmor;
        break;
    case 6:
        sprite_index = spr_mag_parasite;
        break;
    case 7:
        sprite_index = spr_mag_deathspike;
        break;
}
draw_sprite(sprite_index, 0, x, y);

