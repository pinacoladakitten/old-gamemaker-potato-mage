/// @description Main Event
if mouse_check_button(mb_left){
    x = window_mouse_get_x();
    y = window_mouse_get_y();
}
else{
    //Drag and Drop
    var xx = global.display_gui_width; //X Based on screen size
    var yy = global.display_gui_height; //Y Based on screen size
    //Drop Action
    for (b = 0; b < ds_list_size(global.actions_pot_equip); ++b)
    {
        if attack_type = 1{
            if point_in_rectangle(x, y, (xx*0.3+88*b)-42, (yy*0.8)-42, (xx*0.3+88*b)+42, (yy*0.8)+42){
                ds_list_replace(global.actions_pot_equip, b, "attack");
            }
        }
        if attack_type = 2{
            if point_in_rectangle(x, y, (xx*0.3+88*b)-42, (yy*0.8)-42, (xx*0.3+88*b)+42, (yy*0.8)+42){
                ds_list_replace(global.actions_pot_equip, b, "solanum");
            }
        }
        if attack_type = 3{
            if point_in_rectangle(x, y, (xx*0.3+88*b)-42, (yy*0.8)-42, (xx*0.3+88*b)+42, (yy*0.8)+42){
                ds_list_replace(global.actions_pot_equip, b, "ember");
            }
        }
        if attack_type = 4{
            if point_in_rectangle(x, y, (xx*0.3+88*b)-42, (yy*0.8)-42, (xx*0.3+88*b)+42, (yy*0.8)+42){
                ds_list_replace(global.actions_pot_equip, b, "asterid");
            }
        }
        if attack_type = 5{
            if point_in_rectangle(x, y, (xx*0.3+88*b)-42, (yy*0.8)-42, (xx*0.3+88*b)+42, (yy*0.8)+42){
                ds_list_replace(global.actions_pot_equip, b, "tremmor");
            }
        }
        if attack_type = 6{
            if point_in_rectangle(x, y, (xx*0.3+88*b)-42, (yy*0.8)-42, (xx*0.3+88*b)+42, (yy*0.8)+42){
                ds_list_replace(global.actions_pot_equip, b, "parasite");
            }
        }
        if attack_type = 7{
            if point_in_rectangle(x, y, (xx*0.3+88*b)-42, (yy*0.8)-42, (xx*0.3+88*b)+42, (yy*0.8)+42){
                ds_list_replace(global.actions_pot_equip, b, "deathspike");
            }
        }
    }
    instance_destroy();
}

