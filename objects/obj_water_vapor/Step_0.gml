/// @description Destroy
x += xdir
y += ydir
z += zdir
r += 1
if phase = 0
{
    xsc += 0.02
    ysc += 0.02
    zsc += 0.02
    alpha += 0.02
    if xsc >= xscmax
    {phase = 1;}
}
if phase = 1
{
    xsc -= 0.02
    ysc -= 0.02
    zsc -= 0.02
    alpha -= 0.02
}
if alpha <= 0 
{
    instance_destroy();
}

