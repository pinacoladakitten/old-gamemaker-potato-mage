/// @description Hitbox
if place_meeting(x,y,obj_potato_combat)
if hit = 0
if fade = 0
{
    hp_potato -= 8 * obj_bign_attkStats.attkScaling;
    obj_potato_combat.hit = 1
    hit = 1
}
if hit = 1
{
    obj_eff_enm_trail_bign03.hit = 1
}

///Main
if fade = 0
{
    xsc = min(xsc+0.1,xmax);
    ysc = min(ysc+0.1,ymax);
    zsc = min(zsc+0.1,zmax);
}
if fade = 1
{
    xsc = max(xsc-0.05,xmax);
    ysc = max(ysc-0.05,ymax);
    zsc = max(zsc-0.05,zmax);
    alpha -= 0.01
    if alpha <= 0 
    {
        instance_destroy();
    }
}

