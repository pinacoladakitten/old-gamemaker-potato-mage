/// @description Setup
spr = spr_bign_eff_fire03
image_speed = 0.25
image_index = irandom_range(0,9);
xsc = 0
ysc = 0
zsc = 0
xmax = 2
ymax = 2
zmax = 2
z = 0
r = 0
ymove = irandom_range(-3,+3)
alpha = 1
fade = 0
hit = 0
alarm[0] = 1.5*room_speed

