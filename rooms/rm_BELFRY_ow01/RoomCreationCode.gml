audio_sound_gain(snd_overw_ambience, 0, 0);
if !audio_is_playing(overw_music){
    overw_music = snd_none;
    audio_sound_gain(overw_music,0.65,0);
}
audio_play_sound(snd_overw_ambience, 10, true);
audio_sound_gain(snd_overw_ambience, 0.65, 10*room_speed);
potato_battle_room = rm_battle_dream3D;
potato_room_name = "BELFRY: Exterior";
p3dc = 1;
p3dc_floor = 1;