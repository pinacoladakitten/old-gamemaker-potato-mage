//Fade Out
audio_stop_all();
audio_play_sound(snd_thunderstorm_amb, 10, true);
instance_create(0,0,obj_trans_fade_out);
//Cam Settings
if (instance_exists(obj_camera_ow))
{
    obj_camera_ow.camz = 0;
    obj_camera_ow.camxto = x-1000;
    obj_camera_ow.camyto = y;
    obj_camera_ow.camzto = 0;
}
//MISC
potato_room_name = "BELFRY: Title";
p3dc = 0;
p3dc_floor = 0;