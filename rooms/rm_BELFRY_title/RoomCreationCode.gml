//Fade Out
instance_create(0,0,obj_trans_fade_out);
//Help Toggle
if instance_exists(obj_button_help01)
{obj_button_help01.help_toggle = 1}
//Cam Settings
if (instance_exists(obj_camera_ow))
{
    obj_camera_ow.camz = 180;
    obj_camera_ow.camxto = -1595;
    obj_camera_ow.camyto = 510;
    obj_camera_ow.camzto = 600;
}
//MISC
potato_room_name = "BELFRY: Title";
p3dc = 0;
p3dc_floor = 0;
music[0] = snd_BELFRY_title;
var m = irandom_range(0, 0);
audio_play_sound(music[m], 10, true);