instance_create(__view_get( e__VW.XView, 0 ),__view_get( e__VW.YView, 0 ),obj_trans_fade_out)
obj_trans_fade_out.color = c_purple
display_set_gui_size(1280, 720);
combat_music = snd_boss_firesp
if !audio_is_playing(combat_music)
{
    audio_stop_all();
    audio_play_sound(combat_music,10,true)
    audio_sound_gain(combat_music,0.65,0)
}
obj_floor.floor_bg = 1
obj_dream02_barrier.zs = 0.75
obj_dream02_barrier.z01 = -50
//Spawn Type
with(obj_spawn)
{
    spawn1 = irandom_range(2,2)
    spawn2 = irandom_range(2,2)
    spawn3 = irandom_range(2,2)
    spawn4 = irandom_range(2,2)
    spawn5 = irandom_range(2,2)
    
    enemy[0] = obj_enm_floaty01
    if spawn2 = 1 {enemy[1] = obj_enm_floaty01}
    if spawn3 = 1 {enemy[2] = obj_enm_floaty01}
    if spawn4 = 1 {enemy[3] = obj_enm_floaty01}
    if spawn5 = 1 {enemy[4] = obj_enm_floaty01}
}