instance_create(__view_get( e__VW.XView, 0 ),__view_get( e__VW.YView, 0 ),obj_trans_fade_out)
obj_trans_fade_out.color = c_purple
if instance_exists(obj_skybox)
{obj_skybox.sky = bg_sky
obj_skybox.sky_autobox = 1}
//Spawn Type
with(obj_spawn)
{
    spawn1 = irandom_range(2,2)
    spawn2 = irandom_range(2,2)
    spawn3 = irandom_range(2,2)
    spawn4 = irandom_range(2,2)
    spawn5 = irandom_range(2,2)
    
    enemy[0] = obj_enm_dummy01
    if spawn2 = 1 {enemy[1] = obj_enm_dummy01}
    if spawn3 = 1 {enemy[2] = obj_enm_dummy01}
    if spawn4 = 1 {enemy[3] = obj_enm_dummy01}
    if spawn5 = 1 {enemy[4] = obj_enm_dummy01}
}
if !audio_is_playing(combat_music) or !audio_is_playing(combat_music_pt2)
{
    combat_music = snd_boss_firesp_prev;
    combat_music_pt2 = snd_boss_firesp_prev;
}