function dia_belfryOw_text002() {
	if(instance_exists(obj_BELFRY_textbox)){
		obj_BELFRY_textbox.soundType = "Default";
		obj_BELFRY_textbox.strings[0] = "A broken worn out bell, it looks very similar to the Belfry's bell.\n" +
		"Perhaps it was replaced by that newer bell.";
	}


}
