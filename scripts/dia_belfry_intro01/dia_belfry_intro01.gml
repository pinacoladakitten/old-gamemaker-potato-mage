function dia_belfry_intro01() {
	if(instance_exists(obj_BELFRY_textbox)){
		obj_BELFRY_textbox.soundType = "Spirit";
		obj_BELFRY_textbox.strings[0] = "I-...";
		obj_BELFRY_textbox.strings[1] = "I'm sorry kid, this is all I can do.";
		obj_BELFRY_textbox.strings[2] = "It's going to be dangerous, but I believe in you!\n" +
		"You're already going strong, so please, just do me a favor and not give up!";
		obj_BELFRY_textbox.strings[3] = "I want to see you and I swear it's going to happen!\n";
		obj_BELFRY_textbox.strings[4] = "But no matter...you're in His care I suppose.";
		obj_BELFRY_textbox.strings[5] = "Rest easy now Myth.";
	}


}
