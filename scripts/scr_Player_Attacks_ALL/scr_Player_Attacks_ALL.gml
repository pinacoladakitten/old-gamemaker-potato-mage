/// @description Player Attack Reactions (ALL)
/// @param ALL
function scr_Player_Attacks_ALL() {
	//Mythor Attacks-------------------------------------------------
	//Potato Attack~~~
	if place_meeting(x,y,obj_potato_poj)
	{
	    if obj_potato_poj.hit = 0
	    {
	        //Hp Reduction
	        var potato_proj_dmg = (obj_potato_poj.proj_attk-(obj_potato_poj.proj_attk*physical_defense));
	        hp -= potato_proj_dmg;
	        //Dmg Numbers
	        instance_create(x+1,y,obj_dmg_numbers);
	        obj_dmg_numbers.dmg = potato_proj_dmg;
	        //Create Effect
	        if potato_attk < 15
	        {instance_create(x+1,y,obj_eff);}
	        if potato_attk > 15
	        {
	            instance_create(x+1,y,obj_eff);
	            instance_create(x+1,y,obj_eff);
	            instance_create(x+1,y,obj_eff);
	        }
	        with obj_eff
	        {spr = spr_eff_hit01;}
	        //Upon Hit with Potato Proj
	        with obj_potato_poj
	        {hit = 1;
	        vsp += 1;}
	        //Dmg Numbers for Char (Idk why this is here)
	        with obj_dmg_numbers
	        {potato = 1;}
	        //If Dead From Attack
	        if hp < 1
	        {image_index = 0;}
	    }
	}
	//Potato Attack(BONUS ACTION01)~~~
	if place_meeting(x,y,obj_potato_poj01)
	{
	    if obj_potato_poj01.hit = 0
	    {
	        //Hp Reduction
	        var potato_proj_dmg = (obj_potato_poj01.proj_attk-(obj_potato_poj01.proj_attk*physical_defense));
	        hp -= potato_proj_dmg;
	        //Dmg Numbers
	        instance_create(x+1,y,obj_dmg_numbers);
	        obj_dmg_numbers.dmg = potato_proj_dmg;
	        //Create Effect
	        if potato_attk < 15
	        {instance_create(x+1,y,obj_eff);}
	        if potato_attk > 15
	        {
	            instance_create(x+1,y,obj_eff);
	            instance_create(x+1,y,obj_eff);
	            instance_create(x+1,y,obj_eff);
	        }
	        with obj_eff
	        {spr = spr_eff_hit01;}
	        //Upon Hit with Potato Proj
	        with obj_potato_poj01
	        {hit = 1;
	        vsp += 1;}
	        //Dmg Numbers for Char (Idk why this is here)
	        with obj_dmg_numbers
	        {potato = 1;}
	        //If Dead From Attack
	        if hp < 1
	        {image_index = 0;}
	    }
	}
	//Potato Ember~~~
	if place_meeting(x,y,obj_potatofire_proj)
	{
	    //Hp Reduction
	    var potato_fire_proj_dmg = (obj_potatofire_proj.potency-(obj_potatofire_proj.potency*fire_defense));
	    hp -= potato_fire_proj_dmg;
	    //Dmg Numbers
	    instance_create(x+1,y,obj_dmg_numbers);
	    obj_dmg_numbers.dmg = potato_fire_proj_dmg;
	    //Create Effect
	    instance_create(x+1,y,obj_eff);
	    with obj_eff
	    {spr = spr_fire;}
	    //Destroy Projectile (prevent multiple hits)
	    with obj_potatofire_proj
	    {instance_destroy();}
	    //Dmg Numbers for Char (Idk why this is here)
	    with obj_dmg_numbers
	    {potato = 1;}
	    //If Dead from attack
	    if hp < 1
	    {image_index = 0;}
	}
	//Potato Skill Fire Dot~
	if place_meeting(x,y,obj_potato_fire_dot01)  and instance_exists(obj_target)
	{
	    if id == obj_target.target{
	        var targetfiredot = instance_find(obj_potato_fire_dot01,instance_number(obj_potato_fire_dot01)-1);
	        targetfiredot.target = self;
	    }
	}
	//Potato Asterid~~~
	if place_meeting(x,y,obj_potato_asterid)
	{
	    //Hp Reduction
	    var potato_asterid_dmg = (obj_potato_asterid.potency-(obj_potato_asterid.potency*nature_defense));
	    hp -= potato_asterid_dmg;
	    //Damage Numbers
	    instance_create(x+1,y,obj_dmg_numbers);
	    obj_dmg_numbers.dmg = potato_asterid_dmg;
	    //Create Effect
	    instance_create(x+1,y,obj_eff);
	    with obj_eff
	    {spr = spr_eff_hit01;}
	    //Destroy Projectile
	    with obj_potato_asterid
	    {instance_destroy();}
	    //Useless Damage Number thing that's still here...
	    with obj_dmg_numbers
	    {potato = 1;}
	    //If Dead from attack
	    if hp < 1
	    {image_index = 0;}
	}
	//Potato Tremmor~~~
	if instance_exists(obj_potato_tremmor){
	    if ds_list_find_index(global.tremmor_targets_hit, id) == -1{
	        if place_meeting(x,y,obj_potato_tremmor){
	            if (obj_potato_tremmor.shockwave == true){
	                //Hp Reduction
	                var potato_tremmor_dmg = (obj_potato_tremmor.potency-(obj_potato_tremmor.potency*nature_defense));
	                hp -= potato_tremmor_dmg;
	                //Damage Numbers
	                instance_create(x+1,y,obj_dmg_numbers);
	                var targetnumb = instance_find(obj_dmg_numbers, instance_number(obj_dmg_numbers)-1);
	                targetnumb.dmg = potato_tremmor_dmg;
	                //Useless Damage Number thing that's still here...
	                with obj_dmg_numbers
	                {potato = 1;}
	                //Create Effect
	                instance_create(x+1,y,obj_eff);
	                var targeteff = instance_find(obj_eff, instance_number(obj_eff)-1);
	                targeteff.spr = spr_eff_hit01;
	                //If Dead from attack
	                if hp < 1
	                {image_index = 0;}
	                //Add to hit enemies
	                ds_list_add(global.tremmor_targets_hit, id);
	            }
	        }
	    }
	}
	//Potato Parasite~~~
	if place_meeting(x, y, obj_potato_parasite) and instance_exists(obj_target)
	{
	    if id == obj_target.target{
	        var targetparadot = instance_find(obj_potato_parasite, instance_number(obj_potato_parasite)-1);
	        targetparadot.target = id;
	    }
	}
	//Potato DeathSpike~~~
	if place_meeting(x,y,obj_potato_deathspike)
	{
	    //Hp Reduction
	    var potato_deathspike_dmg = (obj_potato_deathspike.potency-(obj_potato_deathspike.potency*dark_defense));
	    hp -= potato_deathspike_dmg;
	    //Damage Numbers
	    instance_create(x+1,y,obj_dmg_numbers);
	    obj_dmg_numbers.dmg = potato_deathspike_dmg;
	    //Create Effect
	    instance_create(x+1,y,obj_eff);
	    var targeteff = instance_find(obj_eff, instance_number(obj_eff)-1);
	    targeteff.spr = spr_eff_hit01;
	    //Destroy Hitbox
	    with (obj_potato_deathspike){
	        mask_index = spr_none;
	        hit = 1;
	    }
	    //Useless Damage Number thing that's still here...
	    with obj_dmg_numbers
	    {potato = 1;}
	    //If Dead from attack
	    if hp < 1
	    {image_index = 0;}
	}



}
