function dia_belfryOw_text001() {
	if(instance_exists(obj_BELFRY_textbox)){
		obj_BELFRY_textbox.soundType = "Default";
		obj_BELFRY_textbox.strings[0] = "This tree seems to endlessly shed leaves off of its branches.";
		obj_BELFRY_textbox.strings[1] = "The leaves continue to remain full for some reason.";
	}


}
