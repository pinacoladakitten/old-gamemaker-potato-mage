function dia_belfry_intro00() {
	if(instance_exists(obj_BELFRY_textbox)){
		if(obj_BELFRY_textbox.text == 0){obj_BELFRY_textbox.soundType = "TelephoneHigh"};
		obj_BELFRY_textbox.strings[0] = "They seem to be late.";
	
		if(obj_BELFRY_textbox.text == 1){obj_BELFRY_textbox.soundType = "Telephone";}
		obj_BELFRY_textbox.strings[1] = "Late? No, right on time actually, I just finished everything,\n" +
		"had to tidy up the place a little. Should be all nice and clean.";
	
		if(obj_BELFRY_textbox.text == 2){obj_BELFRY_textbox.soundType = "TelephoneHigh";}
		obj_BELFRY_textbox.strings[2] = "Clean? Why do you care about this arrival so much? They're just\n" +
		"like any other from the looks of it.";
	
		if(obj_BELFRY_textbox.text == 3){obj_BELFRY_textbox.soundType = "Telephone"};
		obj_BELFRY_textbox.strings[3] = "Oh no no my dear! I wouldn't say it's like that. This is much more\n" +
		"important, you see, I have a favor to return.";
	
		if(obj_BELFRY_textbox.text == 4){obj_BELFRY_textbox.soundType = "TelephoneHigh"};
		obj_BELFRY_textbox.strings[4] = "A favor? Since when have you started taking favors? You're a-";
	
		if(obj_BELFRY_textbox.text == 5){obj_BELFRY_textbox.soundType = "Telephone"};
		obj_BELFRY_textbox.strings[5] = "Quite I am! Now shush! Do not mention that word...please.";
	
		if(obj_BELFRY_textbox.text == 6){obj_BELFRY_textbox.soundType = "TelephoneHigh"};
		obj_BELFRY_textbox.strings[6] = "I'm sorry, I-I should get ready then.";
	
		if(obj_BELFRY_textbox.text == 7){obj_BELFRY_textbox.soundType = "Telephone"};
		obj_BELFRY_textbox.strings[7] = "It's alright, I'll see you when the time comes.";
	}


}
