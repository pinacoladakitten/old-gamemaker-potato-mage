function scr_loadgame() {
	if file_exists("savedata.sav")
	{
	    ini_open("savedata.sav");
	    //Base Game Save
	    potato_room = ini_read_real("Save"+string(file),"Room",rm_title)
	    potato_x = ini_read_real("Save"+string(file),"Xcor",0)
	    potato_y = ini_read_real("Save"+string(file),"Ycor",0)
	    potato_z = ini_read_real("Save"+string(file),"Zcor",0)
	    //Character Stats
	    //Potato Variables--------------------------
	    //Stats
	    potato_vitality = ini_read_real("Save"+string(file),"Potato_Vit",0)
	    potato_intellect = ini_read_real("Save"+string(file),"Potato_Int",0)
	    potato_dexterity = ini_read_real("Save"+string(file),"Potato_Dex",0)
	    potato_haste = ini_read_real("Save"+string(file),"Potato_Haste",0)
	    //Experience
	    potato_max_exp = ini_read_real("Save"+string(file),"Potato_MaxExp",0)
	    potato_exp = ini_read_real("Save"+string(file),"Potato_Exp",0)
	    potato_skillp = ini_read_real("Save"+string(file),"Potato_SkillP",0)
	    potato_equip = ini_read_real("Save"+string(file),"Potato_Equip",0)
	    //Objects
	    maxobject = ini_read_real("Save"+string(file),"MaxObjectsCollected",0)
	    //Inventory
	    with obj_inventory_ow
	    {
	    maxwep = ini_read_real("Save"+string(file),"MaxWep_pot",1)
	    maxmagic = ini_read_real("Save"+string(file),"MaxMag_pot",1)
	    maxitem = ini_read_real("Save"+string(file),"MaxItem",1)
	    scrolld = ini_read_real("Save"+string(file),"Wepscroll_pot",0)
	        //Weapons
	        for (w = 0; w < maxwep; w += 1)
	        {inventorywep[w] = ini_read_real("Save"+string(file),"Inv_Wep_Potato" + string(w),0);}
	        //Abilities_Potato
	        for (m = 0; m < maxmagic; m += 1)
	        {ds_list_insert(inventorymagic, m, ini_read_real("Save"+string(file),"Inv_Mag_Potato" + string(m),0));}
	        //Items
	        for (i = 0; i < maxitem; i += 1)
	        {inventoryitem[i] = ini_read_real("Save"+string(file),"Inv_Items" + string(i),0);}
	    }
	    //Deleted Objects
	    for (global.o = 0; global.o < global.maxobject; global.o += 1)
	    {global.objectcollected[global.o] = ini_read_real("Save"+string(file),"Collected_Objs" + string(global.o),0);}
	    //Events----------------
	    //Sewer01
	    global.sewer01_jace_evt01 = ini_read_real("Save"+string(file),"Sewer01_Jace_Event01",1);
	    ini_close();
	}
	else
	{
	    //Do Nothing
	}



}
