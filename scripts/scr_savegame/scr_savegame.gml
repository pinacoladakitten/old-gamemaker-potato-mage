function scr_savegame() {
	ini_open("savedata.sav");
	ini_section_delete("Save"+string(file));
	//Base Game Save
	ini_write_real("Save"+string(file),"Room",potato_room);
	ini_write_string("Save"+string(file),"RoomName",potato_room_name);
	ini_write_real("Save"+string(file),"Party",party);
	ini_write_real("Save"+string(file),"Xcor",potato_x);
	ini_write_real("Save"+string(file),"Ycor",potato_y);
	ini_write_real("Save"+string(file),"Zcor",potato_z);
	//Character Stats
	//Potato Variables--------------------------
	//Stats
	ini_write_real("Save"+string(file),"Potato_Vit",potato_vitality);
	ini_write_real("Save"+string(file),"Potato_Int",potato_intellect);
	ini_write_real("Save"+string(file),"Potato_Dex",potato_dexterity);
	ini_write_real("Save"+string(file),"Potato_Haste",potato_haste);
	//Experience
	ini_write_real("Save"+string(file),"Potato_MaxExp",potato_max_exp);
	ini_write_real("Save"+string(file),"Potato_Exp",potato_exp);
	ini_write_real("Save"+string(file),"Potato_SkillP",potato_skillp);
	ini_write_real("Save"+string(file),"Potato_Equip",potato_equip);
	//Objects
	ini_write_real("Save"+string(file),"MaxObjectsCollected",global.maxobject);
	//Arrays---------------------------
	with obj_inventory_ow
	{
	    ini_write_real("Save"+string(file),"MaxWep_pot",maxwep);
	    ini_write_real("Save"+string(file),"MaxMag_pot",maxmagic);
	    ini_write_real("Save"+string(file),"MaxItem",maxitem);
	    ini_write_real("Save"+string(file),"Wepscroll_pot",scrolld);
	    //Weapons
	    for (w = 0; w < maxwep; w += 1)
	    {ini_write_real("Save"+string(file),"Inv_Wep_Potato" + string(w),inventorywep[w]);}
	    //Abilities_Potato
	    for (m = 0; m < maxmagic; m += 1)
	    {ini_write_real("Save"+string(file),"Inv_Mag_Potato" + string(m),ds_list_find_value(inventorymagic, get_integer(m,0)));}
	    //Items
	    for (i = 0; i < maxitem; i += 1)
	    {ini_write_real("Save"+string(file),"Inv_Items" + string(i),inventoryitem[i]);}
	}
	//Deleted Objects
	for (global.o = 0; global.o < global.maxobject; global.o += 1)
	{ini_write_real("Save"+string(file),"Collected_Objs" + string(global.o),global.objectcollected[global.o]);}
	//Events----------------
	//Sewer01
	ini_write_real("Save"+string(file),"Sewer01_Jace_Event01",global.sewer01_jace_evt01);
	ini_close();



}
